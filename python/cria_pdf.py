from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration
import os

DIR_LOCAL = "/home/lantri_rafael/codigo"


def main():
    font_config = FontConfiguration()
    caminho_html = f"{DIR_LOCAL}/labri/python/html"
    caminho_pdf = f"{DIR_LOCAL}/labri/python/pdf"
    arquivos = os.listdir(caminho_html)
    arquivos.sort()

    for arquivo in arquivos:
        print(arquivo, "Gerando PDF...")
        arquivo_sem_extensao = arquivo.split(".")[0]
        css = CSS(f"{DIR_LOCAL}/labri/python/style.css")
        HTML(f"{caminho_html}/{arquivo}").write_pdf(
            f"{caminho_pdf}/{arquivo_sem_extensao}.pdf",
            stylesheets=[css],
            font_config=font_config,
        )


if __name__ == "__main__":
    main()
