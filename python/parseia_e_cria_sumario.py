from bs4 import BeautifulSoup
import os
import tira_toc

DIR_LOCAL = "/home/lantri_rafael/codigo"


def main():
    caminho = f"{DIR_LOCAL}/labri/python/html"
    arquivos = os.listdir(caminho)
    arquivos.sort()
    caminho_arquivos = caminho.replace("/", "//")
    for arquivo in arquivos:

        print(arquivo)
        caminho_arquivo = caminho_arquivos + "//" + arquivo
        with open(caminho_arquivo, "r") as file:
            soup = BeautifulSoup(file, "lxml")
            toc = soup.find("div", class_="toc")
            print(f"toc: {toc}")
            lista_sumario = toc.find_all("li")
            summary = soup.find("article", id="sumario")
            summary.insert(-1, toc)
            if lista_sumario == [] or len(lista_sumario) == 1:
                summary.decompose()

        with open(caminho_arquivo, "w") as file:
            file.write(str(soup))
    tira_toc.tira_toc_todos_os_arquivos()


if __name__ == "__main__":
    main()