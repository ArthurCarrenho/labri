# Guia de escrita de artigos em markdown do LabRI

## Boas!


Este é o **Guia de escrita de artigos em markdown do LabRI**

A partir deste guia, o objetivo é que qualquer pessoa possa escrever um artigo nos modelos do labRI sem se preocupar muito com a formatação, que é realizada de maneira automática. Mas para que isso funcione, devemos seguir alguns padrões para que tudo funcione de maneira correta.

a ideia é transformar um texto simples em markdown em um arquivo em PDF, Epub e outras distriuições que facilitem a leitura na maior quantidade de dispositivos, como mostra a figura abaixo.

![image](/python/style/guia/md_to_pdf.png)

Para isso, usaremos o Markdown, que é uma forma simples de escrever textos na web.

## O que é Markdown?


O Markdown é uma linguagem de marcação simples que converte textos em elementos válidos HTML, que é a linguagem usada em toda a web. Com ele é possível fazer estilizações simples sem a necessidade de preocupar-se com a formatação. 

Por exemplo, ao colocar-se ** entre as palavras, elas ficam em **negrito**, caso seja apenas um *  elas ficam em *itálico*. Esse tipo de formatação pode ser até usada nas suas mensagens de WhatsApp!!

Mas não se preocupe com todos esses símbolos, diversos sites oferecem uma referência ou até mesmo uma colinha pra ajudar a lembrar de forma mais rápida como fazer as formatações.

Além disso, para os artigos, o LabRI criou algumas estilizações de acordo com o que se é necessário, como citações diretas longas que possuem uma formatação bastante específica. 

## Como usar o Markdown para escrever um artigo?

É, na verade, muito simples! 

Você deverá escrever o texto normalmente usando a referência do Markdown e Modelo do LabRI e nós nos encarregamos de gerar o arquivo formatado com o seu texto!

Para Tal, é importante seguir os modelos, e caso tenha quaisquer dúvidas, pode entrar em contato com a equipe do LabRI por [e-mail](unesplabri@gmail.com), mas não se preocupe, pois vamos explicar tudo nesse guia.


## Modelo Labri

O modelo a se basear está pronto e disponível por esse [link](/home/labri_pedro/labri/python/guia_markdown.md).

Agora explicaremos as funções e como fazer ou transformar seu texto de acordo com o modelo.

Comecemos pela primeira parte, que é uma tabela com os metadados do artigo

![](/home/labri_pedro/labri/python/style/guia/metadados.JPG)

Essa área é bastante importante pois é a partir dela que são definidos por exemplo, o título e os autores, além de outras informação que possibilitam que o arquivo seja achado de forma mais fácil na web.

Slug: é o nome que o arquivo terá no site (caso ele seja parte de uma série do labRI), caso não saiba, pode deixar em branco
Ex: labri_pandemia 001

Tags: São como as palavras-chave. São usados tanto para classificar s artigos no site do labri como para identificar as séries, por exemplo. Devem estar dentro de [], como no exemplo, e separados por vírgula
Ex: [COVID, Artigos LabRI]

Title: Título do Artigo
Ex: História das Relações Internacionais
Subtitulo: Subtítulo da Publicação
Ex: Breve histórico e contextualização das Reções Internacionais como área científica

Author: Autores - devem estar separados por vírgulas
Ex: Alexandre de Gusmão, Antônio Patriota

Author_title: Titulação dos Autores - deve estar separada por vírgulas e na mesma ordem dos autores.
Ex: Doutor em Relações Internacionais, Mestre em Direito

Tipo_publicação: Tipo de publicação no site do LabRI. Pode conter a série e número da edição. Caso não souber, deixar em branco.
Ex: Série História das Relações Internacionais - Núm. 1

Image_url: Link da imagem que acompanhará o artigo no site do labRI
Ex:https://labriunesp.org/img/labriunesp-12.svg

Descrição: Breve Descrição do Texto.
Ex: Primeiro artigo da série de publicações sobre História das Relações Internacionais. É realizada uma retomada histórica das Relações Internacionais de forma geral e como disciplina, além da apresentação de uma linha do tempo com as principais linhas de pensamento das teorias de relações internacionais.


## TOC / Sumário

O Sumário está marcado no modelo como `[TOC]`, mas não se preocupe, pois o mesmo é gerado de forma automática, desde se sigam a organização de títulos. **Não exclua o TOC**.

## Organização de Títulos

O sumário é gerado com base na organização de títulos, e trabalhamos com o markdown para tornar esse proceso o mais simples possível:

Os títulos de nível 1 são definidos por ###,

Títulos de nível 2 por ####,

Títulos de nível 3 por #####,

Dessa forma, temos:

### Título 1
#### Título 2
##### Título 3


## Citações

As citações diretas e indiretas curtas devem ser inseridas ao longo do texto, nas normas ABNT. Já as citações diretas longas são identificadas pelo símbolo >, seguido pela citação, conforme as normas ABNT

Ex:

> O LabRI é um espaço do Departamento de Relações Internacionais (DERI) da UNESP de Franca para o desenvolvimento e experimentação de atividades de pesquisa e extensão intensivas no uso de novas tecnologias de informação e comunicação, assim como de novas metodologias de trabalho(LabRI, 2020. p1)

## Notas de Fim

Dentro dos artigos do LabRI, além das referências bibliográficas, podem ser inseridas notas de fim, e não se preocupe, por que no próprio documento ficam disponíveis links para que o leitor possa fácilmente acessá-las e voltar para o texto, após sua leitura.
A informação de titulariadade dos autores, por exemplo, ficam disponíveis no texto como notas de fim.

Para inserí-la deve-se controlar seu número no texto através só símbolo [^n], onde n é o número da nota, que deve ser controlado pelo autor.

Ex: Quando quando pensamos no período do surgimento da bossa nova [^1], devemos considerar as condições econômicas e as transformações culturais do período.

E então, deve-se retomar as notas no final do texto, antes da marcação article e das referências bibliográficas
![endnotes](/python/style/guia/end_notas.JPG)

Com o mesmo símbolo usado no texto, deve-se trazer o texto na área reservada para as notas de fim indicada no modelo.

Ex:
[^1]: Considera-se o como período de surgimento da bossa nova e tempo entre as ida dos 8 batutas ao exterior à surgimento de chega de saudade, música de Vinícius de Morais e João Gilberto.

## Referências Bibliograficas

As referências bibliográficas devem ser escritas como as normas ABNT, sempre com uma linha em branco entre cada referência.

Ex:

NAVES, Santuza Cambraia. Da bossa nova à tropicália. Zahar, 2001.

CASTRO, Ruy. Bossa nova: the story of the Brazilian music that seduced the world. Chicago Review Press, 2000.

## Imagens

As imagens no texto devem ser inseridas a partir de seus links, a partir da seguinte sintaxe`![nome da imagem](link_da_imagem)`.

Exemplo:

![logo do labRI](https://labriunesp.org/img/labriunesp-12.svg)
`![logo do labRI](https://labriunesp.org/img/labriunesp-12.svg)`

### Fonte das Imagens

Para a publicação em textos do LabRI, **todas** as imagens devem ser acompanhadas de sua fonte, de acordo com as normas ABNT. Isso é realizadao no modelo a partir da tag `<figcaption>` após a inserçao da imagem. Veja a sintaxe abaixo:

`<figcaption> Fonte: os Autores </figcaption>`

Dessa forma, temos como resultado final de nossas imagens o link seguido pela fonte. Segue Exemplo:

![logo do labRI](https://labriunesp.org/img/labriunesp-12.svg)
<figcaption> Fonte: Autores</figcaption>

Também pode-se usar tags html dentro das tags figcapiton caso deseje realizar a fonte de acordo com as normas ABNT.
Para ta, usa-se <br> para quebrar as linhas e o texto em negrito deve estar entre <b> texto </b>. Segue Exemplo:

![logo do labRI](https://labriunesp.org/img/labriunesp-12.svg)
<figcaption><b>Quadro 1:</b> Perfil geral dos moradores de rua <br>
<b>Fonte</b>: Ministério do Desenvolvimento Social e Combate à Fome, 2008</figcaption>

## Links

Para adicionar Links no documento, deve-se usar a sintaxe:

`[texto no qual o link estará vinculado](link)`

Ex:

[site do labri](https://labriunesp.org/img/labriunesp-12.svg)

Ao ouvir [Chega de Saudade](https://youtu.be/yUuJrpP0Mak) de João Gilberto, ou mesmo uma das promeiras versões de [Erizeth Cardoso](https://youtu.be/rZ13bQvvHEY) podemos perceber uma quebra bastante grande com a tradição chorista até então.

## Destaques

Pode se adicionar em qualquer lugar no texto um destaque. Este costuma ser uma frase importante.
Para fazê-lo, deve-se inserir a frase desejada entre as tags <span> FRASE IMPORTANTE AQUI </span>

## Comparação

Neste [link](python/style/guia/modelo.md) pode e ver o arquivo modelo como markdown, e como ele ficou após a transformação em [PDF](python/pdf/2021-04-08-labri-000.pdf)