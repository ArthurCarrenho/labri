import os

DIR_LOCAL = "/home/lantri_rafael/codigo"


def tira_toc(local_arquivo):
    with open(local_arquivo, "r") as f:
        linhas = f.readlines()
        linhas.remove("[TOC]\n")
    with open(local_arquivo, "w") as f:
        f.writelines(linhas)


# tira_toc("/workspace/labri/website/cadernos copy/2020-04-24-labri-001.md")


def coloca_toc(local_arquivo):
    with open(local_arquivo, "r") as f:
        linhas = f.readlines()
        for i, linha in enumerate(linhas):
            if linha == "---\n" and i != 0:
                linhas.insert(i + 1, "[TOC]\n")
    with open(local_arquivo, "w") as f:
        f.writelines(linhas)


# coloca_toc("/workspace/labri/website/cadernos copy/2020-04-24-labri-001.md")


def tira_toc_todos_os_arquivos():
    local_arquivos = f"{DIR_LOCAL}/labri/website/cadernos"
    arquivos = os.listdir(local_arquivos)
    for arquivo in arquivos:
        tira_toc(f"{local_arquivos}/{arquivo}")


# tira_toc_todos_os_arquivos()

if __name__ == "__main__":
    tira_toc_todos_os_arquivos()