const isBootstrapPreset = process.env.DOCUSAURUS_PRESET === "bootstrap"; //add
module.exports = {
  title: "LabRI-UNESP",
  tagline: "Laboratório de Relações Internacionais - UNESP",
  url: "https://labriunesp.org",
  baseUrl: "/",
  onBrokenLinks: "throw",
  favicon: "./img/icone-favicon.ico",
  organizationName: "labriunesp", // Usually your GitHub org/user name.
  projectName: "site", // Usually your repo name.

  themes: [
    // ... Your other themes.
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        // For Docs using Chinese, The `language` is recommended to set to:
        language: ["en", "pt", "es"],
        highlightSearchTermsOnTargetPage: true,
      },
    ],
  ],

  presets: [
    [
      isBootstrapPreset // add
        ? "@docusaurus/preset-bootstrap" // add
        : "@docusaurus/preset-classic", // add
      {
        debug: true, // force debug plugin usage // add
        docs: {
          path: "docs",
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          editUrl:
            "https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website/",
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
        },
        blog: {
          showReadingTime: true,
          //postsPerPage: 3,
          blogSidebarCount: 2,
          blogSidebarTitle: "Posts recentes",
          blogTagsListComponent: "@theme/BlogTagsListPage",
          blogTagsPostsComponent: "@theme/BlogTagsPostsPage",
          // Please change this to your repo.
          editUrl:
            "https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website/blog",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.scss"),
        },
      },
    ],
  ],
  plugins: [
    [
      "@docusaurus/plugin-google-gtag",
      {
        trackingID: "G-0P65JF2TWY",
        anonymizeIP: true,
      },
    ],

    "docusaurus-plugin-sass",
    "docusaurus-plugin-hotjar",
    [
      "@docusaurus/plugin-content-blog",
      {
        /**
         * Required for any multi-instance plugin
         */
        id: "cadernos",
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: "cadernos",
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: "cadernos",
        blogTitle: "Cadernos LabRI/UNESP",
        showReadingTime: true,
        //postsPerPage: 3,
        blogSidebarCount: 0,
        //blogSidebarTitle: 'Posts recentes',
        // Please change this to your repo.
        editUrl:
          "https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website",
      },
    ],
    [
      "@docusaurus/plugin-content-blog",
      {
        /**
         * Required for any multi-instance plugin
         */
        id: "noticias",
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: "noticias",
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: "noticias",
        showReadingTime: true,
        //postsPerPage: 3,
        // blogSidebarCount: 0,
        //blogSidebarTitle: 'Posts recentes',
        // Please change this to your repo.
        editUrl:
          "https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website",
      },
    ],
    [
      "@docusaurus/plugin-content-blog",
      {
        /**
         * Required for any multi-instance plugin
         */
        id: "fourth-blog",
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: "/lantri/noticias",
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: "noticias-lantri",
        blogTitle: "Notícias LANTRI",
        showReadingTime: true,
        //postsPerPage: 3,
        blogSidebarCount: 0,
        //blogSidebarTitle: 'Posts recentes',
        // Please change this to your repo.
        editUrl:
          "https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website",
      },
    ],
    [
      "@docusaurus/plugin-content-blog",
      {
        /**
         * Required for any multi-instance plugin
         */
        id: "noticias-dipp",
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: "/dipp/noticias",
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: "noticias-dipp",
        blogTitle: "Notícias e Eventos",
        showReadingTime: true,
        //postsPerPage: 3,
        blogSidebarCount: 0,
        //blogSidebarTitle: 'Posts recentes',
        // Please change this to your repo.
        editUrl:
          "https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website",
      },
    ],
    [
      "@docusaurus/plugin-ideal-image",
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
      },
    ],
  ],
  //themes: ['@docusaurus/theme-bootstrap'],

  themeConfig: {
    hotjar: {
      applicationId: "3427792",
    },
    head: [
      {
        tag: "script",
        src: "https://www.googletagmanager.com/gtag/js?id=G-0P65JF2TWY",
      },
      {
        tag: "script",
        src: "/js/analytics.js",
      },
    ],

    docs: {
      sidebar: {
        hideable: true,
        autoCollapseCategories: true,
      },
    },
    colorMode: {
      // add
      defaultMode: "light",
      disableSwitch: false,
      respectPrefersColorScheme: true,
    },
    navbar: {
      hideOnScroll: true, // add
      title: "Home",
      logo: {
        alt: " Logo LabRI UNESP",
        src: "img/navbar-logo.svg",
        width: 32,
        height: 32,
        //srcDark: 'img/labriunesp-12.svg',
        srcDark: "img/navbar-logo.svg",
      },
      items: [
        {
          to: "sobre/",
          label: "Sobre",
          position: "right",
          id: "sobre",
        },
        {
          to: "docs/equipe",
          label: "Equipe",
          position: "right",
          id: "equipe",
        },
        {
          to: "projetos/",
          activeBasePath: "projetos",
          label: "Projetos",
          position: "right",
        },
        //{to: 'blog', label: 'Blog', position: 'right'}, //nadd
        {
          to: "cadernos/",
          label: "Publicações",
          position: "right",
          id: "cadernos",
        },
        //{to: 'noticias-lantri/', label: 'Noticias LANTRI', position: 'right', id: 'noticias-lantri'},
        {
          to: "docs/atendimento/",
          activeBasePath: "atendimento",
          label: "Atendimento",
          position: "right",
        },
        //{
        //to: 'docs/',
        //activeBasePath: 'docs',
        //label: 'Docs',
        //position: 'right',
        //},
        {
          href: "https://gitlab.com/unesp-labri",
          label: "Gitlab",
          position: "right",
          className: "header-gitlab-link",
          "aria-label": "GitLab repository",
        },
      ],
    },
    footer: {
      style: "dark",
      links: [
        //{
        //  title: 'Docs',
        //items: [
        //{
        //  label: 'Style Guide',
        //  to: 'docs/',
        //  },
        //  {
        //  label: 'Second Doc',
        //  to: 'docs/doc2/',
        //},
        //],
        //},
        {
          title: "Redes Sociais",
          items: [
            {
              label: "Youtube",
              href: "https://www.youtube.com/channel/UCHx_m-4Cv7_ZLEDUe_wYATA/featured",
            },
            {
              label: "Facebook",
              href: "https://www.facebook.com/labriunesp",
            },
            {
              label: "Twitter",
              href: "https://twitter.com/labriunesp",
            },
            {
              label: "Instagram",
              href: "https://www.instagram.com/unesplabri/",
            },
            {
              label: "Linkedin",
              href: "https://www.linkedin.com/company/labri-unesp-franca/",
            },
          ],
        },
        {
          title: "More",
          items: [
            {
              label: "Notícias",
              to: "noticias",
            },
            {
              label: "GitLab",
              href: "https://gitlab.com/unesp-labri",
            },
          ],
        },
      ],
      //copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
      copyright: `${new Date().getFullYear()} Site feito e mantido pelos membros do LabRI/UNESP. Construído com Docusaurus.`,
    },
  },
 
};
