---
id: series-labri-unesp
title: Séries LabRI
sidebar_label: Séries LabRI/UNESP
slug: /cadernos/series-labri-unesp
---

### Séries LabRI/UNESP

Os Cadernos LabRI/UNESP estão organizados em uma numeração contínua e categorizadas por séries que podem ser temáticas ou vinculada a algum grupo específico

### Séries LabRI/UNESP disponíveis

- [Covid-19](https://labriunesp.org/cadernos/tags/covid-19)
