---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /cadernos/intro
---




Os Cadernos LabRI/UNESP são uma publicação circunscrita a divulgar os trabalhos de grupos e indivíduos vinculados ao LabRI/UNESP. Os principais objetivos desta publicação são:

1. Fornecer um espaço aos grupos para divulgação de seus trabalhos.
2. Auxiliar uma melhor comunicação dos projetos e dos produtos derivados
3. Estimular a utilização de tecnologias digitais no cotidiano acadêmico

Devido aos aspectos apontados acima, os Cadernos LabRI/UNESP não estão abertos a submissão de trabalhos externos para a publicação. Apesar disso, eventualmente, convidados também poderão ter seus trabalhos divulgados nos Cadernos LabRI/UNESP.



