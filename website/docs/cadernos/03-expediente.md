---
id: expediente
title: Expediente LabRI/UNESP
sidebar_label: Expediente
slug: /cadernos/expediente
---

Os cadernos LabRI/UNESP são uma publicação virtual do departamento de Relações Internacionais, da Faculdade de Ciências Humanas e Sociais da UNESP - Campus Franca. Seu logradouro atual é:

> Av. Eufrásia Monteiro Petráglia, 900 - Jd. Dr. Antonio Petráglia - Franca/SP - CEP 14409-160

- __Periodicidade de publicação dos Cadernos LabRI/UNESP: Irregular__

### Expediente Geral

O editor principal responsável pelos Cadernos LabRI/UNESP é o docente do departamento de Relações Internacionais da UNESP/Franca que coordena o LabRI/UNESP. Porém, as séries temáticas e de grupos de pesquisa terão como editor responsável o coordenador da respectiva série temática ou grupo de pesquisa. Atualmente, o expediente geral dos Cadernos LabRI/UNESP é composto por:

- Editor principal atual: Marcelo Passini Mariano
- Assistentes editoriais atuais: Júlia dos Santos Silveira, Cintia Iório, Rafael de Almeida

### Expediente Série Pandemia

O projeto [As Relações Internacionais e o Novo Coronavírus](/docs/projetos/extensao/covid19/intro) teve como assistentes editoriais os colaboradores indicados abaixo:

- Marcelo Passini Mariano (editor)
- Léa Briese Staschower (assistente editorial)
- Cristiano Manhães (assistente editorial)
- Sofia Navarro Aguiar (assistente editorial)
- Bruna de Souza Luiz (assistente editorial)
- Pedro Campagna (assistente técnico)
- Júlia dos Santos Silveira (assistente técnico)
- Rafael de Almeida (assistente técnico)

As edições do Cadernos LabRI/UNESP vinculados a esse projeto podem ser acessadas a partir da página principal de publicações no site do LabRI/UNESP ou a partir da tag [COVID-19](/cadernos/tags/covid-19). Este projeto foi divulgado em 49 números dos Cadernos LabRI/UNESP.
