---
id: equipe
title: Apresentação da equipe LabRI/UNESP
sidebar_label: Atual
slug: /equipe
---

## Marcelo Passini Mariano

<img className="img-equipe-foto" src="/img/equipe/marcelo.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/3505849964932313"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Coordenador**

*2012 - atual*

Possui graduação em Ciências Sociais pela Universidade de São Paulo (1992), mestrado em Ciência Política pela Universidade de São Paulo (1998) e doutorado em Sociologia pela Universidade Estadual Paulista (2007). Professor Assistente Doutor do curso de Relações Internacionais da Faculdade de Ciências Humanas e Sociais da Universidade Estadual Paulista - UNESP/Campus de Franca. Professor do Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP). Coordenador do Laboratório de Novas Tecnologias de Pesquisa em Relações Internacionais (LANTRI /FCHS /UNESP), que integra a Rede de Pesquisa em Política Externa e Regionalismo (REPRI). É pesquisador do Centro de Estudos de Cultura Contemporânea (CEDEC). Tem experiência na área de Relações Internacionais com pesquisas nos seguintes temas: processos de integração regional (Mercosul, Alca, Relação Mercosul União Européia e integração na América Latina), política externa brasileira, controle democrático de política externa, processos de tomada de decisão e gestão de relações internacionais de governos não-centrais (paradiplomacia), técnicas de pesquisa em ciências humanas e o uso das tecnologias de informação e comunicação.

</div>

## Rafael de Almeida

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Colaborador**

*2012 - atual*

Mestre em Relações Internacionais pela Universidade Federal de Uberlândia (UFU). Possui graduação em Relações Internacionais pela Universidade Estadual Paulista "Julio de Mesquita Filho", Campus Franca (2012). Membro do Laboratório de Novas Tecnologias de Pesquisa em Relações Internacionais (LANTRI/UNESP), que integra a Rede de Pesquisa em Política Externa e Regionalismo (REPRI). Entre 2008 e 2011 foi redator-colaborador do Observatório de Política Externa Brasileira (OPEB) vinculado ao Grupo de Estudos de Defesa e Segurança Internacional (GEDES/UNESP). Foi assistente de pesquisa no Centro de Estudos de Cultura Contemporânea (CEDEC). Na graduação foi bolsista PIBIC/CNPq e, posteriormente, bolsista FAPESP. No mestrado foi bolsista FAPEMIG.

</div>

## Natan Willian de Souza Noronha

<img className="img-equipe-foto" src="/img/equipe/natan-noronha.jpeg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/natan-willian-noronha"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/worldofnatan/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*2023*

Graduando em Relações Internacionais pela Universidade Estadual Paulista (UNESP). Técnico em Administração pela ETEC Presidente Vargas (Mogi das Cruzes). Membro do grupo de extensão em Marketing Internacional (MKI)

</div>

## Brunna Fonseca Sousa 

<img className="img-equipe-foto" src="/img/equipe/brunna-fonseca.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/brunna-fonseca-sousa-776913206/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://www.instagram.com/bruhh.fs/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiária Voluntária**

*2022 - atual*

Graduanda em Relações Internacionais pela Universidade Estadual Paulista (UNESP). Técnica em Nutrição e dietética pela Escola Técnica Estadual (Etec) Getulio Vargas em São Paulo. Especializações em marketing digital, comunicação e vendas. Estagiária na AIESEC Franca na área de B2B. Membro da ENACTUS (programa que visa expandir o empreendorismo). Em 2019 atuei no programa voluntário 1MIO da UNICEF, sendo gerente de marketing digital.

</div>

## Gabriela Vano

<img className="img-equipe-foto" src="/img/equipe/gabriela-vano.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/gabrielavano/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://gitlab.com/gabsv"> <img className="img-icon-redes" src="/img/social/gitlab.png" /> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiária Voluntária**

*2022 - atual*

Graduanda em Relações Internacionais pela Faculdade Estadual Paulista Julio Mesquita Filho (UNESP) Campus de Franca. 

</div>

## Melissa de Lucena e Nakajo

<img className="img-equipe-foto" src="/img/equipe/melissa-lucena.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://instagram.com/mel_nakajo?igshid=YmMyMTA2M2Y="> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiária Voluntária**

*2022 - atual*

Graduanda em Relações Internacionais na UNESP - FRANCA. Técnica em Eventos pelo Instituto Federal de São Paulo - Campus Cubatão.

</div>

## Ezequiel Barbosa dos Santos 

<img className="img-equipe-foto" src="/img/equipe/ezequiel-barbosa.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/ezequiel-barbosa/"> <img className="img-icon-redes" src="/img/social/linkedin.png" /> </a>

<a href="https://github.com/ezxpro"> <img className="img-icon-redes" src="/img/social/github.png" /> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário voluntário**

*2022 - atual*

Graduando do 4º ano do curso de Relações Internacionais pela Universidade Estadual Julio Mesquita Filho, no campus de Franca. Graduando em Análise de Sistemas pela Anhanguera. Conhecimento e habilidades em programação Python e C#.

</div>

## Maria Júlia Barbosa Sena Nunes Scandiuzzi

<img className="img-equipe-foto" src="/img/equipe/mariaj-barbosa.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/maria-j%C3%BAlia-barbosa-sena-nunes-scandiuzzi-627bba233/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://instagram.com/mariajulia_s03"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiária voluntária**

*2022 - atual*

Graduanda em Relações Internacionais pela Universidade Estadual Paulista (UNESP). Técnica em Química pelo Instituto Federal de Goiás (2018-2020). Coordenadora do setor de Recursos Humanos da Empresa Júnior Orbe Consultoria Internacional.

</div>
