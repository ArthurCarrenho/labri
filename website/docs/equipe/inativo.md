---
id: equipe-inativa
title: Já passaram por aqui
sidebar_label: Ex-membros
slug: /equipe_inativo
---

## Artur Damiano Dantas

<img className="img-equipe-foto" src="/img/equipe/artur-damiano.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/artur-dantas"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/arddantas/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*2023*

Graduando em Relações Internacionais pela Universidade Estadual Paulista (UNESP). Membro do grupo de extensão em Marketing Internacional (MKI) e do Núcleo de Estudos em Políticas Públicas (NEPPs). Foi professor no Cursinho S.E.U., da Unesp Franca e no Cursinho Sônia Guimarães, da Unesp São José do Rio Preto.

</div>

## Cintia Paulena Di Iório

<img className="img-equipe-foto" src="/img/equipe/cintia-paulena.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/cíntia-di-iório-999138150/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/cintiadiiorio/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>


</div>

<div style={{textAlign: 'center'}}>

**Estagiária Remunerada**

*2021 - 2022*

Graduanda em Relações Internacionais pela Faculdade Estadual Paulista Julio Mesquita Filho (UNESP) Campus de Franca. Técnica em Informática para Internet pela Escola Técnica Estadual (ETEC) de Poá. Gerente de Desenvolvimento de Negócios da AIESEC em Franca.

</div>

## Treyce Annunciado

<img className="img-equipe-foto" src="/img/equipe/treyce-annunciado.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/treyce-h-r-annunciado-459680125/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="http://lattes.cnpq.br/7001164021728775"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Voluntário**

*10/01/2022 - 28/06/2022*


</div>

## Fábio de Oliveira Paron

<img className="img-equipe-foto" src="/img/equipe/fabio-oliveira.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/f%C3%A1bio-paron-3386a61ba/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://www.instagram.com/paronfabio/"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*14/06/2021 - 13/09/2021*


</div>

## Júlia Silveira

<img className="img-equipe-foto" src="/img/equipe/julia-silveira.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/julia-silveira23/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/rikamishiro"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiária Voluntário**

*10/12/2019 - 30/09/2022*

</div>

## Pedro Henrique Campagna

<img className="img-equipe-foto" src="/img/equipe/pedro-campagna.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/pedrohcmds/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/pedrohcmds"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*05/12/2019 - 28/05/2021*

</div>

## Ivan Beretta

<img className="img-equipe-foto" src="/img/equipe/ivan.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/7866238160042906"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>
<a href="https://www.linkedin.com/in/berettaivan/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*09/04/2018 - 15/12/2019*

</div>

## Guilherme Henrique

<img className="img-equipe-foto" src="/img/equipe/guilherme-henrique.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/2148212212846762"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>
<a href="https://www.linkedin.com/in/ghpinto/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Voluntário**

*09/04/2018 - 15/12/2019*

</div>

## Cassiano Baldin

<img className="img-equipe-foto" src="/img/equipe/cassiano.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/cassiano-baldin/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://www.instagram.com/cassianobaldin/"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Remunerado / Voluntário**

*02/04/2018 a 15/12/2019 - 28/09/2016 a 16/12/2016*

</div>

## Jaqueline Trevisan Pigatto

<img className="img-equipe-foto" src="/img/equipe/jaqueline-trevisan.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/8846306750388567"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>
<a href="https://www.linkedin.com/in/jaqueline-trevisan-pigatto/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Voluntário**

*01/08/2017 a 07/12/2017*

</div>

## Norberto Vanderlei Simões Filho

<img className="img-equipe-foto" src="/img/equipe/norberto-vanderlei.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/4818573113426474"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>
<a href="https://www.instagram.com/norberto.filho/"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Voluntário**

*01/08/2017 a 07/12/2017*

</div>

## Virgínia Maria Corrêa

<img className="img-equipe-foto" src="/img/equipe/virginia-maria.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/virgíniacorrêa/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://www.instagram.com/virginiamariacorrea/"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*01/09/2015 a 31/12/2016*

</div>

## Andrey Ide

<img className="img-equipe-foto" src="/img/equipe/andrey-ide.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/andreyide/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://www.instagram.com/andreyide/"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*01/04/2014 a 31/12/2014*

</div>