---
id: intro
title: Pesquisa em Buscadores
sidebar_label: Pesquisa em Buscadores
slug: /projetos/ensino/tec-digitais/buscadores/intro
---
:::tip
O nosso intuito com essa página é auxiliar nos mecanismos de pesquisa em buscadores, mais especificamente o Recoll e a pesquisa do próprio Google. Nos vídeos abaixo, você irá assistir exemplos de como utilizar estratégias e palavras-chave para otimizar a sua pesquisa.
:::


### Operadores Booleanos 

Vídeo sobre o que são operadores booleanos e como usar nos sites de bases de dados acadêmicos e nos buscadores como Google, Bing, etc.

[![Video1](https://i.imgur.com/sVMIZSP.png)](https://youtu.be/X6qkLPLz4IE "Video1")

#### Ex: Procura no Google Acadêmico com Operadores Booleanos 

Exemplo de procura utilizando os operadores AND e OR.

[![Video2](https://i.imgur.com/0WOvf40.png)](https://youtu.be/ASxJZK_m4Mk "Video2")

### Pesquisa programável Google

Vídeo sobre o serviço do Google que permite configurar o buscador para procurar em sites escolhidos por você. Pode ser muito útil para o estudo e a pesquisa acadêmica.

[![Video3](https://i.imgur.com/Bkn1KqD.png)](https://youtu.be/3SwwHiwCDlo "Video3")

### Pesquisa em Base de Dados Acadêmicas 

Vídeo sobre algumas das principais bases de dados de artigos científicos disponíveis na internet.

[![Video4](https://i.imgur.com/a0NTD5H.png)](https://youtu.be/wjcYz3cctvI "Video4")