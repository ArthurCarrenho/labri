---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/filezilla/intro
---

:::tip
O FileZilla é um aplicativo de código aberto disponível para MAC OS, Windows e Linux. Sua função é realizar a transferência de arquivos entre dois computadores via acesso remoto. Nesta página será demonstrado o passo a passo para seu uso.
:::

### Configuração geral

- Aqui são fornecidas informações gerais da configuração do FileZilla. O passo a passo mais detalhado pode ser visto na página "Configuração do FileZilla" ao lado.

**1 - Host:** digite o endereço da máquina que será acessada.

**2 - Porta:** digite o número de acesso à máquina (normalmente é a porta 22).

**3 - Protocolo:** Selecione a opção SFTP - SSH File Transfer Protocol.

**4 - Tipo de logon:** selecione a opção:

- Normal: para acesso de modo automático, armazena o login de usuário e a senha;
- Pedir a senha: grava o login de usuário, porém precisa digitar a senha a todo acesso;
- Interativo: grava o login de usuário, porém precisa digitar a senha a todo acesso (recomendado);

**5 - Usuário:** digite o seu usuário de login da máquina a ser acessada.

**6 - Senha:** digite a senha do seu usuário de login da máquina a ser acessada.

**7 -** Clique em Conectar para acessar imediatamente a máquina remotamente ou clique em OK para acessar posteriormente o Novo Site.

- Caso você tenha dúvidas ou dificuldades de configurar seu computador ou notebook envie um email para: unesplabri@gmail.com. 😉
- Caso não consigamos resolver as dificuldades por email podemos marcar uma videoconferência para realizada a configuração em conjunto com você.

### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla?author=rafaelrdealmeida)
- [Artur Dantas](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla?author=Artur%20Dantas)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla)