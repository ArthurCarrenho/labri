---
id: configuracao
title: Configuração do Filezilla
sidebar_label: Configuração do Filezilla
slug: /projetos/ensino/filezilla/configuracao
---
:::tip
Essa página visa instruir o usuário sobre a utilização e configuração passo a passo do FileZilla. Uma apresentação geral desse programa já foi feita na página "Apresentação". Caso ainda não tenha lido, sugerimos que comece por ela.
:::


### Passo 1

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo1.png" alt="centered image"/>
</div>

- Abra o Menu de aplicações do computador e vá em Internet e procure por pelo aplicativo FileZilla.

### Passo 2

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo2.png" alt="centered image"/>
</div>

- Clique no ícone no canto superior esquerda da tela, logo abaixo de Arquivo.

### Passo 3

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo3.png" alt="centered image"/>
</div>

- Após a janela abrir clique em Novo Site

### Passo 4

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo4.png" alt="centered image"/>
</div>

- Para nomear o atalho Novo Site, clique em Renomear e coloque um nome de sua preferência para este acesso sFTP.

### Passo 5

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo6.png" alt="centered image"/>
</div>

- Quando aparecer uma mensagem de Chave Host Desconhecida,  selecione a caixa de confiar nesse host e depois clique em OK.

### Passo 6

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo7.png" alt="centered image"/>
</div>

- A disposição do Filezilla é a seguinte: 1) Lado esquerdo: é o computador o qual você realmente está. 2) Lado direito: é o computador o qual você entrou remotamente.

### Passo 7

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo8.png" alt="centered image"/>
</div>

- A navegação pelas pastas se dá de duas maneiras, isto para ambos os lados: 1) Selecionar um Endereço de uma pasta e colar no Endereço local; 2) Navegar pelas pastas como mostra o próprio Filezilla.

### Passo 8

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo9.png" alt="centered image"/>
</div>

- Para transferir um arquivo ou uma pasta entre primeiramente no local onde se encontra o arquivo ou pasta, em uma das máquinas. Depois abra o local para onde o arquivo será transferido na outra máquina. Volte ao local onde se encontra o arquivo a ser transferido, selecione, segure-o e arraste para a outra máquina.

### Passo 9

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo10.png" alt="centered image"/>
</div>

- A transferência bem sucedida deve se parecer com a imagem acima.

### Passo 10

<div style={{textAlign: "center"}}>
    <img src="/img/projetos/ensino/filezilla/passo11.png" alt="centered image"/>
</div>

- Para desconectar da máquina, clique no ícone como mostrado na imagem acima.

### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla?author=rafaelrdealmeida)
- [Artur Dantas](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla?author=Artur%20Dantas)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla)