---
id: lista
title: Lista Simplificada Projetos Ensino
sidebar_label: Lista Projetos Ensino
slug: /projetos/ensino/lista
---

<center>
    <img src="/img/projetos/ensino/logo-projetos-ensino.png" alt="centered image" />
</center>

Os projetos de ensino são iniciativas voltadas à promoção de oficinas, material de apoio e cursos que incentivem e auxiliem uma melhor incorporação de ferramentas tecnológicas nas atividades de pesquisa.

Para voltar para página principal dos projetos de ensino, [clique aqui](https://labriunesp.org/projetos/ensino/).



|Nome do Projeto de dados|Informações|Local da documentação|
|---|---|---|
|Trilha de Dados||[Local](https://labriunesp.org/docs/projetos/ensino/trilha-dados/intro)|
|Tecnologias Digitais||[Local](https://labriunesp.org/docs/projetos/ensino/tec-digitais/intro)|
|Processar imagens que contém texto||[Local](https://labriunesp.org/projetos/ensino/pos-processamento)|
|Acesso Remoto||[Local](https://labriunesp.org/projetos/ensino/acesso-remoto)|
|Recoll||[Local](https://labriunesp.org/projetos/ensino/recoll)|
|Filezilla||[Local](https://labriunesp.org/projetos/ensino/filezilla)|
|Material Bibliográfico||[Local](https://labriunesp.org/docs/projetos/ensino/material-bibliografico/intro)|
|Curso: Governança da Internet||[Local](https://labriunesp.org/docs/projetos/ensino/governanca/intro)|