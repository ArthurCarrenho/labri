---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/intro
---

<center>
    <img src="/img/projetos/ensino/logo-projetos-ensino.png" alt="centered image" />
</center>

O principal objetivo dos projetos de ensino são desenvolver iniciativas voltadas à promoção de oficinas, material de apoio, tutoriais, treinamentos e cursos que incentivem e auxiliem uma melhor incorporação de ferramentas tecnológicas nas atividades de pesquisa na área das Ciências Humanas e Sociais.

A principal iniciativa neste momento é o que denominamos de **Trilhas de Fundamentos de Tecnologias Digitais**.

As trilhas são, fundamentalmente, voltadas a auxiliar nos treinamentos dos colaboradores envolvidos nos [projetos](/projetos) desenvolvidos pelo LabRI/UNESP e pelo IPPRI/UNESP. Mas, eventualmente, também podem ser úteis aos demais interessados no conteúdo tratado em cada trilha.

## Como as Trilhas estão organizadas?

Cada **TRILHA** aborda um tema amplo, por exemplo, a **Trilha de Dados - Python** busca desenvolver um treinamento sobre fundamentos de ciência de dados a partir da linguagem de programação Python.

Para isso, apresenta módulos chamados de **PARADAS**. E em cada parada é realizado um teste sobre o conhecimento adquirido para a avaliar a consolidação do aprendizado.

## Quais trilhas estão disponíveis?

- [Trilha de Dados - Python](/docs/projetos/ensino/trilha-dados/intro)

