---
id: assinatura
title: Assinatura digital GOV.br
sidebar_label: Assinatura digital GOV.br
slug: /projetos/ensino/assinatura
---

### Por que usar uma assinatura eletrônica?

A assinatura eletrônica é um recurso utilizado para substituir a necessidade do encontro físico para validação de documentos. O uso de assinatura eletrônica foi popularizado após a pandemia do Covid-19, quando ter encontros físicos passou a ser uma dificuldade. Existem sites e aplicativos que permitem a assinatura de documentos, mas nem sempre são aceitos em qualquer ocasião. Por isso, o Governo do Brasil possui um recurso de assinatura eletrônica gratuito válido em território nacional. O documento com a assinatura digital Gov.BR tem a mesma validade de um documento com assinatura física e é regulamentado pelo Decreto Nº 10.543, de 13/11/2020.

### Tutorial

Abaixo está um passo a passo de como obter a assinatura eletrônica no site do Governo Federal. Com este tutorial, você pode validar qualquer documento ou imagem que desejar. Lembrando que a assinatura é pessoal, portanto, não é uma afirmação governamental de que o documento é válido, é somente uma assinatura pessoal física sendo substituída pela eletrônica. Uma validação de um documento é feita em um cartório, e o Gov.BR não substitui esse serviço.

**1.** Para utilizar a assinatura digital, você precisa ter uma conta no [gov.br](https://www.gov.br/pt-br). Caso ainda não tenha, clique no botão "Acesse sua conta" localizado no canto superior direito da página inicial. Em seguida, clique em "Crie sua conta" e preencha as informações solicitadas, como nome completo, CPF, data de nascimento, e-mail e senha. Siga as instruções e finalize o processo de criação da conta.

**2.** Para acessar o portal de Assinatura Eletrônica, clique no Menu principal no site, vá em "Por dentro do Gov.BR > Governo Digital". Já na página do Governo Digital, clique novamente no Menu e clique em "Assinatura Eletrônica", ou [clique aqui](https://www.gov.br/governodigital/pt-br/assinatura-eletronica).

**3.** Antes, para fazer a assinatura digital, você precisa ter baixado o aplciativo Gov.BR no seu celular. Para isso, acesse o Google Play Store ou a App Store e procure por "Gov.BR". 

**4.** Nesta página, abaixo, há o passo a passo para assinatura eletrônica. Clique no link disponível na Etapa 1 e, na nova página, escolha o arquivo que deseja assinar, nos formatos disponibilizados pelo site.

![assinatura1](/img/projetos/ensino/assinatura1.png)

**5.** Escolha o local da assinatura no documento, clique em “Assinar digitalmente” para validar a assinatura e avance para a próxima etapa.

![assinatura2](/img/projetos/ensino/assinatura2.png)

**6.** Na próxima etapa, na janela dos Provedores de Assinatura, clique em "usar gov.br". Em seguida, insira o código enviado para o seu celular.

**7.** Baixe o documento e ele está pronto para ser usado. É importante ressaltar que o documento não possui validade se impresso. A assinatura é válida somente no documento digital.

