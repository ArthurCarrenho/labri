---
id: atividades
title: Atividades
sidebar_label: Atividades
slug: /projetos/ensino/atividades/trilha-dados-python/govlatinamerica01/atividades
---

## Repositórios

- [Repositório Gitlab - Rafael](https://gitlab.com/rdealmeida/fundamentos-python)

## Tarefas

### Para 02/06/2022

- Definir três fontes de pesquisa e indicar tema de pesquisa

|Nome |Tema de pesquisa|Fontes de pesquisa|
|----------|---------------|------------|
|Bruna| PEB e Regionalismo Latino Americano|CEPAL, MEC, BRICS Policy Center|
|Treyce|Relações Tecnológicas entre EUA e China|BBC, El Pais, XINHUA|
|Gustavo|Relações entre Transnacionais de tecnologia e os Estados|CGI.BR, DataPrivacy, EuroParl|
|João|PEB e a tecnologia 5G|Direito da comunicação, El Pais, Jornal da USP|


- Estrutura inicio da coleta de dados das fontes selecionadas

### Para 09/06/2022

- Mapeamento da fonte de pesquisa
  - fazer levantamento da localização de informações relevantes no site da fonte de pesquisa
  - indicar periodicidade de atualização da fonte de pesquisa

- Identificar tag que armazenam as informações para coleta

### Para 23/06/2022

- Construir um HTML com informações de você e de sua pesquisa
- Buscar coletar todas as informações relevantes da pagina

