---
id: conceitos
title: Conceitos Iniciais
sidebar_label: Conceitos Iniciais
slug: /projetos/ensino/recoll/conceitos
---

### Indexação 

:::tip
O Recoll Desktop Search é um programa de indexação automática e integral de documentos (full-text search tool). Você sabe o que isso significa?
:::

A indexação pode ser caracterizada como o processo de inserção de um documento em um índice. Sendo que o índice pode ser entendido como um banco de dados, o qual será acessado quando as buscas forem realizadas no Recoll.

Por padrão, o Recoll gera um índice único que armazena todos os documentos que estarão disponíveis para pesquisa. Ele denomina este processo de criação de múltiplos índices (multiple indexes). Várias bases de dados do LabRI podem ser pesquisadas a partir da seleção dos índices listados em “External Index Dialog” que está localizado no item “__Query__” do menu. As bases de dados disponíveis do Recoll podem ser encontradas [aqui](https://docs.google.com/document/d/1VAnENddRJThWD8rxhP6V97I6x4KzL-KfJVwWo45l-OE/edit).  😉

### Tipos de dados

* **Metadados** 

👉 São dados que descrevem um determinado dado, para assim tornar mais fácil a sua identificação. 

* **Dados**

👉 Podem ser entendidos como o conjunto de metadados disponíveis, ao inserirmos determinadas informações (metadados), em um arquivo, estamos produzindo um dado. Por exemplo, uma notícia é um dado que contém determinados metadados, como é visto na imagem abaixo.  

![Exemplo de Dado](https://i.imgur.com/KfmlbgZ.png)

### Formatos de arquivos

* A identificação do formato dos arquivos é importante para uma adequada seleção do programa que auxiliará na análise dos dados;

* Arquivos no formato **.pdf, .docx, .odt, .html, .txt, .epub ou similares**, armazenam informações não-estruturadas e programas como o **Recoll** são ferramentas boas para auxiliar análise dos dados contidos nesses formatos de arquivos.

* O Recoll **não** é um programa adequado para análise de dados armazenados  nos formatos **.xlsx, .csv, .json, .sql, .ods**. Programas como **Microsoft Excel ou Libre Office Calc** podem ser utilizados para .xlsx e .csv ou .ods. Já o **.json**, por se tratar do formato JavaScript, é necessário um visualizador do mesmo, como exemplo o **WordPad**. Por fim **.sql** é uma linguagem de banco de dados, sendo necessário um programa como **MySQL**

:::info

Leia mais sobre o Recoll, Indexação, Tipos de dados e Formatos de arquivos **[aqui](https://docs.google.com/document/d/1msD8d3czzZaCW5Iv1Q17CN--OnNc96MZPwO_QxSNB98/edit?usp=sharing)**. 

:::

### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=rafaelrdealmeida)
- [Júlia Silveira](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=Júlia%20Silveira)
- [Cintia Paulena Di Iorio](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=cintia.iorio)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll)