---
id: colaboradores
title: Colaboradores Recoll 
sidebar_label: Colaboradores
slug: /projetos/ensino/recoll/colaboradores
---

## Colaboradores do Projeto

### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=rafaelrdealmeida)
- [Júlia Silveira](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=Júlia%20Silveira)
- [Cintia Paulena Di Iorio](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=cintia.iorio)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll)