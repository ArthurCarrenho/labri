---
id: utilizando
title: Utilizando o Recoll
sidebar_label: Utilizando o Recoll
slug: /projetos/ensino/recoll/utilizando
---

### Selecionar o índice da base de dados desejada 

#### Passo 1:  

Abrir o Recoll → clique no item "_Query_" do menu → clique em "_external index dialog_";

:::tip

Leia mais sobre _external index dialog_ **[aqui](https://docs.google.com/document/d/1msD8d3czzZaCW5Iv1Q17CN--OnNc96MZPwO_QxSNB98/edit?usp=sharing#heading=h.kl72495mtjzv)**

:::

#### Passo 2 

Selecione o índice da base que deseja pesquisar → clique no botão "**ok**";

#### Passo 3 

Na caixa de pesquisa, realize a busca desejada. 

![Gif: Selecionar o índice da base de dados desejada](https://i.imgur.com/f5rjVpE.gif)

### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=rafaelrdealmeida)
- [Júlia Silveira](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=Júlia%20Silveira)
- [Cintia Paulena Di Iorio](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll?author=cintia.iorio)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/recoll)