---
id: dicas-base-de-dados
title: Dicas de Base de Dados
sidebar_label: Dicas de Base de Dados
slug: /projetos/ensino/material-bibliografico/dicas-base-de-dados
---

As bases de dados são importantíssimas para as pesquisas. Abaixo, listam-se algumas: 


|Fonte|Link|
|---|---|
|International Statistics: Top 20 Resourses| [clique aqui](https://campusguides.lib.utah.edu/statsinternational) 
|Repositórios Digitais| [clique aqui](https://labriunesp.org/docs/projetos/ensino/material-bibliografico/intro)
|Clássicos do IPRI| [clique aqui](https://3000-unesplabrisites-labri-grtub41po6b.ws-us38.gitpod.io/docs/projetos/ensino/material-bibliografico/classicos-ipri)
 
 
 




