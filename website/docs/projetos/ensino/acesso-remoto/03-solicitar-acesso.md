---
id: solicitar-acesso
title: Solicitar Acesso
sidebar_label: Solicitar Acesso
slug: /projetos/ensino/acesso-remoto/solicitar-acesso
---
:::tip
Esta pagina reúne perguntas relativas a solicitação de acesso a Estação Remota de Trabalho
:::

## Quem pode ter acesso?

👉 O acesso a estação remota de trabalho esta circunscrita aos docentes e discentes vinculados:

- Ao curso de Relações Internacionais na UNESP, campus Franca.
- Ao Instituto de Políticas Pública e Relações Internacionais (IPPRI/UNESP)
  - PPGRI San Tiago Dantas (UNESP, UNICAMP, PUC-SP)
  - PPG TerritoriAL
- Aos colaboradores dos [projetos](/projetos/) desenvolvidos pelo LabRI/UNESP e pelo IPPRI/UNESP


## Como solicitar acesso?

O acesso é liberado através do preenchimento de um formulário de cadastro. Para ter acesso ao formulário envie um pedido para o email unesplabri@gmail.com 😉

# Autoria

- [Artur Dantas](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto?author=Artur%20Dantas)
- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto?author=rafaelrdealmeida)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto)


