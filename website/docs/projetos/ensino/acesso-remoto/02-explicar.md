---
id: explicar
title: Explicação Geral
sidebar_label: Explicação Geral
slug: /projetos/ensino/acesso-remoto/explicar
---
:::tip
Esta página reúne explicações gerais sobre o acesso remoto disponibilizado LabRI/UNESP e do IPPRI/UNESP.
:::

## O que é uma estação remota de trabalho?

A partir de uma Infraestrutura de Desktop Virtual ([VDI](https://pt.wikipedia.org/wiki/Virtualiza%C3%A7%C3%A3o_de_desktops)) é possivel disponibilizar uma estação remota de trabalho que linha gerais é um desktop completo hospedado em nuvem e acessível via internet independentemente da localidade.

Adotamos um VDI persistente, ou seja, o usuário tem as mesmas funcionalidades que estão presentes um desktop físico pessoal com a vantagem deste desktop estar acessível em qualquer aparelho com conexão a internet. Além disso, o usuário pode customizar seu desktop de acordo com suas necessidades, pois tais ajustes ficam salvos para os próximos acessos realizados.

Atualmente o desktop completo que disponibilizamos é a última versão de suporte de longo prazo [LTS](https://pt.wikipedia.org/wiki/Suporte_de_longo_prazo) do Ubuntu com interface XFCE ou LXDE.


## Como é realizado o acesso?

É necessário uma breve configuração feita via [SSH](https://pt.wikipedia.org/wiki/Secure_Shell) para definição de senha pessoal e para habilitar um duplo fator de autenticação.

Após isso, é ativado o acesso ao desktop completo via [Chrome Remote Desktop](https://remotedesktop.google.com/). Deste modo, simplificamos o acesso, pois o usuário poderá acessar seu desktop através de sua conta Google de qualquer local com acesso a internet.

Também é possivel acessar o desktop através do programa [X2GO](https://wiki.x2go.org/).

## Qual é a utilidade?

- Facilitação do trabalho remoto e colaboração em projetos, permitindo que os usuários acessem seu desktop virtual completo e arquivos a partir de qualquer lugar com uma conexão à internet.
- Acesso a Bases de dados construídas nos [projetos](/projetos) do LabRI/UNESP e do IPPRI/UNESP.
- Redução de custos com hardware e software, pois o desktop virtualizado é mantido e gerenciado centralmente, eliminando a necessidade de atualizações e manutenção em cada computador individualmente.


## Histórico

Desde 2016 o LabRI/UNESP disponibiliza acesso a uma estação remota de Trabalho. 😉

## Material de apoio

- [What is virtual desktop infrastructure (VDI)?](https://azure.microsoft.com/en-us/resources/cloud-computing-dictionary/what-is-virtual-desktop-infrastructure-vdi/#what-is-virtualization)
- [O que é infraestrutura de desktop virtual (VDI) e como ela funciona?](https://www.vmware.com/br/topics/glossary/content/virtual-desktop-infrastructure-vdi.html)
- [Criar uma estação de trabalho virtual](https://cloud.google.com/architecture/creating-a-virtual-workstation?hl=pt-br)


# Autoria

- [Artur Dantas](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto?author=Artur%20Dantas)
- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto?author=rafaelrdealmeida)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto)

