---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/acesso-remoto/intro
---
:::tip
Esta página que reúne informações sobre o acesso a estação remota de trabalho do LabRI/UNESP e do IPPRI/UNESP
:::

Aqui você encontra:

- [Explicações Gerais sobre uma estação remota de trabalho](/docs/projetos/ensino/acesso-remoto/explicar)
- [Aspectos relacionados a solicitação de acesso](/docs/projetos/ensino/acesso-remoto/solicitar-acesso)
- [Instruções para a realização do acesso](/docs/projetos/ensino/acesso-remoto/instrucao-inicial)

# Autoria

- [Artur Dantas](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto?author=Artur%20Dantas)
- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto?author=rafaelrdealmeida)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto)