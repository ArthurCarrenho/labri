---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/processamento-imagens/intro
---

:::tip
Você tem um arquivo, tirou foto ou digitalizou um texto, mas não sabe como buscar palavras chaves nele? Este arquivo é uma imagem (png, jpeg ou tiff) ou um pdf? Neste tutorial indicamos como processar imagens textuais para gerar um pdf pesquisável e, deste modo, conseguir buscar as palavras chaves contidas dentro deste arquivo e facilitar a sua vida dentro da pesquisa acadêmica tanto para graduação quanto pós-graduação.
:::

### O uso de textos digitalizados

Textos digitalizados em PDF substituem os textos em papel no meio acadêmico devido à sua praticidade, facilidade de busca, portabilidade, sustentabilidade e recursos de anotação. Os PDFs são acessíveis em dispositivos eletrônicos, permitem buscas rápidas, eliminam a necessidade de transportar materiais físicos, são mais sustentáveis e permitem anotações virtuais. Essas vantagens tornam os textos digitalizados uma opção eficiente para estudantes universitários e pesquisadores. 😄

### Reconhecimento óptico de caracteres (OCR)

OCR é um acrónimo para o inglês Optical Character Recognition, é uma tecnologia para reconhecer caracteres a partir de um arquivo de imagem ou mapa de bits sejam eles escaneados, escritos a mão, datilografados ou impressos. Dessa forma, através do OCR é possível obter um arquivo de texto editável por um computador.

Combinado com outras tecnologias, como a inteligência artificial, empresas de diversos segmentos têm aplicado o OCR para automatizar processos de cadastro, onboarding e formalização, extraindo informações de documentos de identificação pessoal, contratos e comprovantes de residência.*

-----

- Caso você tenha dúvidas ou dificuldades de configurar seu computador ou notebook envie um email para: unesplabri@gmail.com.
- Caso não consigamos resolver as dificuldades por email podemos marcar uma videoconferência para realizada a configuração em conjunto com você. 😉

### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla?author=rafaelrdealmeida)
- [Artur Dantas](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla?author=Artur%20Dantas)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/processamento-imagens)

*Texto retirado de: [Wikipedia - Reconhecimento ótico de caracteres](https://pt.wikipedia.org/wiki/Reconhecimento_%C3%B3tico_de_caracteres)