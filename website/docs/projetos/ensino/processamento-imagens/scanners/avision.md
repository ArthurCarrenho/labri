---
id: avision
title: AVISION
sidebar_label: AVISION
slug: /projetos/ensino/processamento-imagens/scanners/avision
---

![avision1](/img/projetos/sistemas/avision1.jpeg)

![avision2](/img/projetos/sistemas/avision2.jpeg)

### Configurações da digitalização

**- Modo de Imagem:** Aqui você escolherá se o scaneamento será colorido, preto e branco, ou cinza. Para imagens mais detalhadas que fica impossível de ser compreendida em preto e branco, ou mapas com esquemas de cores sofisticados para a explicação de algum tema, recomenda-se utilizar o esquema de cor colorido. Entretanto na maior parte das vezes as digitalizações realizadas aqui no LabRI são feitas em preto e banco. Preto e Branco é mais rápido, economizando muito tempo do processo. Não recomenda-se digitalizar o livro todo colorido, esse processo seria demorado e desnecessário. Apenas páginas específicas já seria o suficiente. 

**- Parâmetros de digitalização:** Aqui se decide o DPI( qualidade da resolução) e o tamnho do papel a ser utilizado. Quanto maior o DPI melhor vai ser a qualidade da digitalização, entretanto mais o processo fica demorado. Utilizamos por padrão no LabRI um DPI de 300 para a maioria das digitalizações, mas é claro que isso pode ser alterado conforme a necessidade do usuário. A segunda opção importante que se encontra aqui é o tamanho do papel, isso é, o tamnho do livro que vai ser digitalizado.

**- Local do arquivo:** Aqui você deve selecionar a pasta para onde as digitalizações irão. Nomear o arquivo com a data de publicação, sobrenome  do autor maiúsculo, nome minúsculo, e nome da obra em questão. Selecione o formato do arquivo para TIFF. Destino Iscan.