---
id: intro
title: Comece por aqui
sidebar_label: Comece por aqui
slug: /projetos/ensino/trilha-dados/intro
---

**Seja bem-vindo(a)!**

- Nesta página você irá iniciar as suas atividades na trilha de dados, partindo de um início básico e mantendo uma construção de raciocínio até o intermediário.

- Além disso, cada tarefa cumprida irá gerar um certificado válido para composição de horas complementares.

### Estrutura

- A equipe do LabRI/UNESP separou as tarefas da trilha de dados em "Paradas". Cada parada condiz com um tema importante da análise de dados e possui uma ordem configurada para a construção do estudo. E cada uma irá gerar um certificado quando terminada.

### Objetivo

- Ao final do seu estudo, queremos que você esteja inserido no tema, estando apto(a) para o uso da análise de dados tanto para um melhor aproveitamento na vida acadêmica quanto para vagas de estágio na área voltada às Relações Internacionais.
Esta página reúne as trilhas de dados que o LabRI/UNESP tem desenvolvido treinamentos para as pessoas envolvidas em seus projetos.

### Trilha de dados com Python

Abaixo indicamos as paradas da trilha de dados que utiliza primordialmente a linguagem Python:

- __*[Ambiente de Trabalho](/docs/projetos/ensino/trilha-dados/ambiente/intro)*__
- __*[Fundamentos de algoritmos e programação](/docs/projetos/ensino/trilha-dados/linguagens/python/intro)*__
- __*[Coleta de dados](/docs/projetos/ensino/trilha-dados/coleta-dados/intro)*__
- __*[Análise de dados](/docs/projetos/ensino/trilha-dados/analise-dados/intro)*__
- __*[Banco de dados](/docs/projetos/ensino/trilha-dados/banco-dados/intro)*__
- __*[Métodos Quantitativos](/docs/projetos/ensino/trilha-dados/metodos-quanti/intro)*__


### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados?author=rafaelrdealmeida)
- [Artur Dantas](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino?author=Artur%20Dantas)
- [Leonardo de Almeida Petrilli](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino?author=Leonardo%20Petrilli)
- [Ver todos os autores](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/ambiente)
