---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/trilha-dados/ambiente/intro
---


Nesta parada vamos tratar da Preparação do Ambiente de Trabalho. Mas, provavelmente você deve esta se perguntando: o que isso quer dizer 🤔?

Nós denominamos de **Ambiente de Trabalho** os serviços e programas que utilizamos no dia a dia dos trabalhos e treinamentos em torno dos [projetos de dados](/projetos/dados). 

Será comum você ouvir de algum colega de [projetos de dados](/projetos/dados) e ao longo desta parada: 

 - Já fez o *commit*? 
 - Pode atualizar as informações da *documentação* do projeto?
 - Atualizou seu *ambiente virtual*? Tem uma versão nova de uma das *libs* que usamos no projeto.
 - Em qual pasta está o script? 
 - Precisa de ajuda para editar o código que você esta construindo?


Estas perguntas se relacionam a utilização de serviços, como o gitlab ou github, e programas, como o git, o conda e o vscode, que estarão no seu cotidiano ao longo desta *Trilha* bem como nos projetos que desenvolvemos.

As perguntas e os serviços e programas indicados acima podem parecer uma sopa de letrilhas para você agora, mas não se preocupe eles serão explicados nas proximas sessões!


:::tip

**O importante neste momento é compreender que o objetivo desta parada** é construir um ambiente de trabalho que facilite seu trabalho cotidiano.

:::




## Qual é a importância disso?

✅ Versionamento (git e gitlab/github) e ambiente virtual (conda)
  - Garantem uma maior reprodutibilidade e compartilhamento
  - Facilitam a identificação e recuperação de possíveis erros

✅ Linguagem de marcação (markdown)
  - Auxilia na documentação das atividades realizadas
  - Esta página, por exemplo, foi escrita utilizando markdown


✅ Editor de código (vscode)
  - Facilita a produção dos códigos desenvolvidos

✅ Comandos linux básicos
  - Auxiliam em uma melhor interação com o código gerado 



##  Quanto tempo para concluir essa parada?

Nós Estimamos que o tempo de concluir esta parada (🗺️📌) seja de aproximadamente **120 minutos**



## Quantos mirantes existem nessa parada?

São Cinco mirantes 🧐 nessa parada. Eles estão indicados no menu lateral esquerdo (👈):

- Versionamento
- Markdown
- Comandos linux
- Editor de código
- Ambiente Virtual


## Como obtenho o certificado?

O certificado 🎯 é emitido após a realização de um teste. Se você alcançar o minimo de 80% de acerto🚦 o certificado é emitido.




## Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados?author=rafaelrdealmeida)
- [Leonardo de Almeida Petrilli](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino?author=Leonardo%20Petrilli)
- [Ver todos os autores](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/ambiente)