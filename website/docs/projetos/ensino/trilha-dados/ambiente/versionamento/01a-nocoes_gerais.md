---
id: nocoes-gerais
title: Noções Gerais
sidebar_label: Noções Gerais
slug: /projetos/ensino/trilha-dados/ambiente/nocoes-gerais
---


Antes de falarmos do programa (`git`) e dos serviços (`gitlab` e `github`) que são o foco principal desse tópico. Cabe discutirmos algumas noções gerais para uma melhor contextualização e entendimento sobre a utilização dessas ferramentas.

:::tip

**O objetivo deste tópico** é compreender o que siginifica *VERSIONAMENTO*.

:::






## O que é Git?

O Git é um sistema de controle de versão de arquivos de código-fonte e gerenciamento de projetos. 📚
Ele é amplamente utilizado em desenvolvimento de software, mas também pode ser utilizado em outros contextos, como na ciência de dados. 📊

### Qual a diferença entre Git, Github e Gitlab?

O Git é o sistema de controle de versão de código-fonte em si. Github e Gitlab são plataformas que fornecem hospedagem para repositórios Git remotos. Eles são usados para compartilhar, colaborar e gerenciar o código-fonte com outras pessoas. 🤩

### Versionamento e reprodutibilidade cientifica

O controle de versão é uma técnica utilizada em desenvolvimento de software que permite aos desenvolvedores acompanhar as alterações em um conjunto de arquivos ao longo do tempo. Com o controle de versão, é possível recuperar uma versão anterior do código-fonte e comparar as diferenças entre as versões. Isso é importante para garantir a reprodutibilidade científica, onde é necessário rastrear e documentar todas as etapas do trabalho realizado.




## Sugestão de Estrutura

✅ Noções Gerais
  - Contexto - lidar com qual problema?
    - O que é versionamento? Qual a sua importância?
  - Encaminhamento - como lidar com o problema?
  - Ferramentas - que ferramentas dão apoio ao encaminhamento?

    - diferença entre GIT e GITLAB/GITHUB
    - Zenodo (CERN)

✅ Dinamica de versionamento
  - Implementação - como o encaminhamento/ferramenta funciona?
    - Fluxo do versionamento
    - Versionamento semántico


✅ Repositório
  - Tipos de licenças
  - Criação de repositório
    - Instalação do GIT
  - Tipos de acessos (https, ssh, token)

- GIT - Básico
- GIT- Indo Além

✅ Sugestões:
  - Criar repositório chamado `ambiente_trabalho`


  ## Material de Apoio

- [Academic Benefits of Using git and GitHub](https://archive.is/tqpaC)
- [Version Control with Git](https://swcarpentry.github.io/git-novice/)
- [GitHub for Academics: the open-source way to host, create and curate knowledge](https://blogs.lse.ac.uk/impactofsocialsciences/2013/06/04/github-for-academics/)
- http://www.cs.toronto.edu/~kenpu/articles/cs/git-intro.html
- https://www.nature.com/articles/sc201717
- https://hackmd.io/@vivek-blog/github_article
- https://slate.com/technology/2017/04/we-need-a-github-for-academic-research.html
- https://dl.acm.org/doi/abs/10.1145/3366423.3380145
- https://opendreamkit.org/index.html