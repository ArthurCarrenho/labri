---
id: comandos-linux
title: Comandos Linux
sidebar_label: Comandos Linux
slug: /projetos/ensino/trilha-dados/ambiente/comandos-linux
---
*Tempo estimado: 2 minutos*

:::tip
Neste tutorial, vamos abordar alguns dos comandos básicos do Linux que você pode usar no seu dia a dia e nas tarefas do LabRI.
:::

O Linux é um sistema operacional de código aberto que é usado por muitas empresas, organizações e desenvolvedores para executar servidores, desktops, dispositivos móveis, além de ser o sistema utilizado amplamente no LabRI. 😊

## O que é o Linux? 

O Linux é um sistema operacional de código aberto baseado no kernel do Unix. Ele foi desenvolvido por Linus Torvalds em 1991 e é um dos sistemas operacionais mais populares do mundo. O Linux é usado em servidores, desktops e dispositivos móveis.
    
## Onde usar o Linux? 

O Linux é usado em muitos lugares, incluindo servidores, desktops, dispositivos móveis e sistemas embarcados. Ele é especialmente popular em servidores web, onde é usado para executar sites e aplicativos da web.
    
## Vantagens do Linux 

O Linux tem muitas vantagens sobre outros sistemas operacionais. Algumas das vantagens incluem:
-   É de código aberto, o que significa que você pode ver o código-fonte e modificá-lo para atender às suas necessidades.
-   É gratuito e pode ser usado em qualquer lugar sem custo.
-   É altamente personalizável, permitindo que você personalize o sistema operacional de acordo com suas necessidades.
-   É mais seguro do que outros sistemas operacionais devido à sua arquitetura de segurança e ao fato de ser menos visado por hackers.

## Como usar o Linux? 

Para usar o Linux, você precisa ter acesso a um terminal ou shell, que é uma interface de linha de comando. Você pode usar o terminal para executar comandos, navegar em diretórios e executar aplicativos.

Para abrir o terminal no Linux, basta pressionar as teclas "Ctrl + Alt + T". Isso abrirá uma janela do terminal onde você pode inserir comandos.


## Principais comandos do Linux 

| Comando | Funcionalidade | Descrição/Forma de usar |
| :-----: | :------------: | :---------------------: |
| ls | Listar pastas|ls -la: lista todas as pastas, inclusive as ocultas|
| cd| Comando para se locomover |cd `nome da pasta`: entrar em uma pasta; cd `nome da pasta`: sair de uma pasta|
| rm | Comando de remoção |rm -r: remover arquivos/diretórios; rmdir: remover um diretório vazio|
| mkdir| Comando de criação de pastas|Ex: mkdir `nome da pasta`|
| touch | Comando de criação de arquivo vazio |-|
| history | Comando para listar o que já foi editado|-|
| clear | Comando para limpar o terminal |-|
| mv | Comando para mover ou renomear arquivos|-|
| ps | Comando que exibe informações sobre os processos atuais|ps - e: exibe todos os processos|
| wget | Comando para fazer download de arquivos |--no-clobber: evita o download de arquivos repetidos; `--directory-prefix=nome_da_pasta`: faz o download do arquivo em uma pasta específica|
| bash script | executa um comando shell |-|

Esses são apenas alguns dos muitos comandos disponíveis no Linux. Para saber mais sobre outros comandos e como usá-los, consulte a documentação do Linux ou procure tutoriais online.


## Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados?author=rafaelrdealmeida)
- [Leonardo de Almeida Petrilli](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino?author=Leonardo%20Petrilli)
- [Ver todos os autores](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/ambiente)


