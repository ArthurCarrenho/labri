---
id: nocoes-gerais
title: Noções Gerais
sidebar_label: Noções Gerais
slug: /projetos/ensino/trilha-dados/ambiente/markdown/nocoes-gerais
---
*Tempo estimado: 5 - 7 minutos*

✅ Noções Gerais
- O que é Markdown? qual é a importancia?

✅ Sintaxe Markdown
- Generico
- Docusaurus

✅ Criação Github profile

## 1. O que é Markdown?

Markdown é uma linguagem de marcação leve e simples, que permite criar documentos em formato de texto simples, com marcações para formatação de texto, listas, links e imagens, entre outras funcionalidades. A linguagem foi criada por John Gruber em 2004, com o objetivo de ser fácil de ler e escrever, e é amplamente utilizada por desenvolvedores, escritores, blogueiros e estudantes.

## 2. Onde usar Markdown?

Markdown é uma linguagem de marcação universal, que pode ser usada em praticamente qualquer lugar onde você pode escrever texto. Alguns exemplos de lugares onde você pode usar Markdown são:

-   Editores de texto e IDEs para programação;
-   Editores de texto online, como o Google Docs;
-   Plataformas de blog, como o WordPress e o Medium;
-   Redes sociais, como o GitHub, o Reddit e o Twitter.

## 3. Vantagens do Markdown

As vantagens do Markdown incluem:

-   É fácil de aprender e usar;
-   Não requer um software específico, já que pode ser editado em qualquer editor de texto;
-   O texto formatado em Markdown é legível mesmo sem renderização, o que torna fácil de editar e compartilhar;
-   Inserir fórmulas matemáticas complexas.
-   É compatível com HTML, permitindo a adição de funcionalidades mais avançadas;
-   É suportado por muitas plataformas e ferramentas online.

## 4. Como usar o Markdown

Para usar o Markdown, você precisa apenas de um editor de texto. A ideia é escrever o texto normalmente, adicionando as marcações de formatação conforme necessário. Para ver como o texto formatado ficará, é preciso renderizá-lo usando um conversor de Markdown para HTML ou outro formato.

## 5. Principais comandos do Markdown

Aqui estão alguns dos principais comandos do Markdown:

-   **Títulos**: use o caractere "#" para criar títulos em diferentes níveis, por exemplo,  **"# Título 1"**  cria um título de primeiro nível, enquanto  **"## Título 2"**  cria um título de segundo nível.
-   **Negrito e itálico**: use os caracteres "*" ou "_" antes e depois do texto para criar negrito ou itálico, por exemplo, "Este texto está em ** negrito ** " e "Este texto está em  _itálico_ ".
-   **Links**: use colchetes para criar um link, e parenteses para especificar o endereço do link, por exemplo, "[Texto do link](https://www.exemplo.com/)".
-   **Imagens**: use colchetes para criar um texto alternativo para a imagem, e parenteses para especificar o endereço da imagem, por exemplo, "![Texto alternativo da imagem]  ([https://www.exemplo.com/imagem.jpg)"](https://www.exemplo.com/imagem.jpg)%22).
-   **Listas**: use o caractere "-" ou "*" para criar uma lista com marcadores, e use números para criar uma lista numerada, por exemplo, "- Item 1" cria um item de lista com marcador, enquanto "1. Item 1" cria um item de lista numerada.

## Material de apoio

- [ASCIIDOCTOR](https://asciidoctor.org)
- [Embedbase](https://archive.is/sO0vy)
- [Welcome to markdown-guide’s documentation!](https://markdown-guide.readthedocs.io/en/latest/index.html)
- [Introdução ao Markdown](https://programminghistorian.org/pt/licoes/introducao-ao-markdown)

## Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados?author=rafaelrdealmeida)
- [Leonardo de Almeida Petrilli](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino?author=Leonardo%20Petrilli)
- [Ver todos os autores](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/ambiente)