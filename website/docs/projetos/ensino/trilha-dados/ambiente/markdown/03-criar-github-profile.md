---
id: github-profile
title: Github Profile
sidebar_label: Github Profile
slug: /projetos/ensino/trilha-dados/ambiente/markdown/github-profile
---

:::tip
Aqui você irá aprender a como criar um perfil no GitHub. Isso é importante pois o GitHub é a página mais usada para contato com outros desenvolvedores.
:::

### Preparação

1. Crie uma conta no Github
2. Crie um repositório com nome **idêntico** ao seu nome de usuário e torne ele público. Selecione a opção "Add a README file" e, em "Choose a license", selecione "MIT License". Como abaixo:

![github1](/img/projetos/ensino/trilha-dados/github1.jpg)

Observação: Na imagem o nome do repositório está diferente do nome de usuário. Não cometa esse erro. 😉

3. Agora, vamos customizar o seu perfil! Pense na arte que combine com a sua personalidade: capa, tons, sobretons, imagens, ícones, pensando sempre no objetivo do seu perfil.
4. Texto de boas-vindas, apresentação contando as linguagens e tecnologias que utiliza, suas habilidades, suas redes sociais etc.

5. Dica: O site [https://coolors.co/](https://coolors.co/) pode ser um grande aliado na hora de escolher a paleta de cores do seu perfil. 

   a. Clique em **Start the generador**, uma paleta de cores aleatórias surgirá na página. Passe o mouse em cima das cores e repare que cada uma delas possui um menu próprio, você pode alterar o tom de uma cor clicando em: 

![github2](/img/projetos/ensino/trilha-dados/github2.png)

   b. Esse menu permite que você deslize as cores na paleta, copie o código de uma cor, favorite, etc. O ícone de cadeado fixa a cor em questão na sua paleta. Aperte a barra de espaço do seu teclado para alterar as cores da paleta que não estiverem travadas (cadeado)

### Mão na massa

1. Utilize um editor de imagens como o Figma ou Canvas no seu navegador.
2. Escolha imagens png com fundo transparente. Aqui estão alguns sites com imagens disponíveis:

    a. [https://www.freepik.com](https://www.freepik.com/)

    b. [https://lukaszadam.com](https://lukaszadam.com/)

    c. [https://blush.design/pt](https://blush.design/pt)

3. Recomenda-se fortemente [essa publicação](https://dev.to/dii_lua/github-profile-como-fazer-54o0) para ter uma noção melhor de como montar o fundo de uma imagem, adicionar gradiente e painéis interativos, além de várias dicas de como montar seu perfil no Github. 
4. Crie um texto de boas vindas, utilize os recursos do Markdown para isso. É possível adicionar imagens, gifs, ajustar o alinhamento e várias outras funções apenas alterando o arquivo README.md do seu repositório. Para saber mais sobre markdown [clique aqui](https://labriunesp.org/docs/projetos/ensino/trilha-dados/ambiente/markdown/nocoes-gerais).
5. Adicione as informações usando sua criatividade e não se esqueça de **salvar seu progresso**❗
6. Entre no seu perfil do github e olhe para o canto superior direito. Uma mensagem deve aparecer para anexar o seu repositório como capa do seu perfil.
7. Quando você acessar o repositório essa mensagem deve aparecer:

![github3](/img/projetos/ensino/trilha-dados/github3.png)

### Exemplos de perfis

https://github.com/anabastos

https://github.com/rafaballerini

https://github.com/wnqueiroz

https://github.com/katiehuangx

https://github.com/GMBermeo/

https://github.com/leticiadasilva

https://github.com/villares

https://github.com/gabsv

https://github.com/anamacao

https://github.com/JL-Motta01

### Material de apoio

[Github Profile: Como fazer?](https://dev.to/dii_lua/github-profile-como-fazer-54o0)

[Awesome GitHub Profile READMEs](https://zzetao.github.io/awesome-github-profile/)

[Como personalizar o seu perfil no Github](https://sujeitoprogramador.com/como-personalizar-o-seu-perfil-no-github/)