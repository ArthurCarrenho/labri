---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/trilha-dados/banco-dados/intro
---




### Material de apoio

- [Trilha SQL](https://www.youtube.com/playlist?list=PLvlkVRRKOYFQrPsRLU-53-No8c4e-RvHk)
- [Téo Me Why](https://www.youtube.com/channel/UC-Xa9J9-B4jBOoBNIHkMMKA)
- [TeoCalvo/teoSQL-V2](https://github.com/TeoCalvo/teoSQL-V2)
