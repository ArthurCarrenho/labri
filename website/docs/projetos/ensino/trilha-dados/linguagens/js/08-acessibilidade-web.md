---
id: acessibilidade-web
title: Acessibilidade no Desenvolvimento Web
sidebar_label: Acessibilidade no Desenvolvimento Web
slug: /projetos/ensino/trilha-dados/linguagens/js/acessibilidade-web
---

## Acessibilidade Web: Introdução

### O que é "Acessibilidade Web"?

:::tip
O propósito da **Acessibilidade Web** é utilizar práticas e **ferramentas de programação, design e tecnologia** para tornar os conteúdos da web acessíveis para todos. 
:::


Ou seja, pensar em acessibilidade é garantir que usuários da internet que possuam algum tipo de deficiência (_cognitiva, neurológica, física, discursiva, visual, auditiva_) e/ou que enfrente alguma limitação (_problemas de conexão com a internet, lesão temporária, etc_) consigam usufruir da internet sem encontrar barreiras. 


:::note

 Muitas vezes Acessibilidade Web também é conhecida como **a11y**, sendo possível encontrar dados estatísticos e dicas no site oficial do [Projeto A11y](https://www.a11yproject.com)

:::

### Princípio POUR 

Para a construção de um site acessível é necessário seguir o princípio POUR:

* **Perceivable** (Perceptível): o conteúdo da web deve se adaptar aos sentidos (_visão, toque e audição_) através de diversos meios (navegador, leitor de tela, etc);

* **Operable** (Funcional): o conteúdo da web deve permiter que o usuário interaja com todos os controles e elementos interativos. 

* **Understandable** (Compreensível): o conteúdo da web deve ser _claro_, _conciso_ e de fácil entendimento. 

* **Robust** (Robusto): garantir consistência na web. 

## Aplicando práticas de acessibilidade no Desenvolvimento Web

### Onde aplicar

De modo geral, tornar um site acessível não demanda conhecimentos avançados de desenvolvimento web. Por exemplo, é  possível executar as práticas de acessibilidade através de conhecimentos básicos de HTML. 

Além disso, na maioria das vezes as mudanças acontecem em tags específicas que ajudam ferramentas externas (leitores de tela, etc) a compreender e interpretar o conteúdo disponível. 

* **HTML e acessibilidade**:Conheça algumas dicas de como utilizar o HTML para tornar seu site mais inclusivo. [Acesse aqui](https://developer.mozilla.org/pt-BR/docs/Learn/Accessibility/HTML)

### Pautas de acessibilidade web 

A _Web Accessibility Initiative_ (WAI) é responsável por emitir diretrizes padronizadas e reconhecidas a nível internacional que auxiliam o processo de tornar um site acessível.  

Essas diretrizes estão divididas em três blocos: 

1. **Diretrizes de acessibilidade para conteúdo da web** (WCAG) 

> "Diretrizes para criar conteúdos acessíveis e compatíveis com qualquer tipo de tecnologias de apoio, dispositivos, navegadores e linguagens de programação. Incluem o uso de letras grandes, designs adaptados, textos preditivos, assistentes de navegação, etc."

2. **Diretrizes de acessibilidade para ferramentas de autoria** (ATAG) 

> "Normas que dizem respeito ao desenvolvimento dos programas e aplicativos usados para criar, gerenciar e publicar conteúdos digitais. Estas ferramentas abarcam processadores de textos, gestores de bases de dados, programas de edição de vídeo, etc."

3. **Diretrizes de acessibilidade para agentes de usuário** (UAAG)

> "Pautas destinadas ao desenvolvimento dos programas necessários para interagir com conteúdos web tais como navegadores, reprodutores multimídia, leitores de tela, etc."

_Descrições por: [Iberdrola](https://www.iberdrola.com/inovacao/o-que-e-acessibilidade-na-web)_

## Sugestões de ferramentas

* [a11y.css](https://ffoodd.github.io/a11y.css/)
* [Accessibility insights](https://accessibilityinsights.io)
* [Axe](https://www.deque.com/axe/) 
    * _Teste de Acessibilidade_
* [Chromebook Help](https://support.google.com/chromebook/answer/7031755)
* [Contrast Checker](https://contrastchecker.com) 
    * _Utilizado para testar o contraste entre cores_
* [Lighthouse](https://contrastchecker.com)
* [Pa11y](https://pa11y.org)
* [tota11y](https://khan.github.io/tota11y/)
* [Tenon](https://tenon.io)
* [Voice Over](https://www.apple.com/accessibility/vision/)
    * _exclusivos para dispositivos iOS/Mac OS_
* [Wave](https://wave.webaim.org)

Descubra mais ferramentas através do site [Web Accessibility Evaluation Tools List](https://www.w3.org/WAI/ER/tools/). 

## Referências 

https://desenvolvimentoparaweb.com/miscelanea/acessibilidade-web-para-iniciantes/

https://www.a11yproject.com

https://developer.mozilla.org/pt-BR/docs/Learn/Accessibility/HTML

https://www.iberdrola.com/inovacao/o-que-e-acessibilidade-na-web
