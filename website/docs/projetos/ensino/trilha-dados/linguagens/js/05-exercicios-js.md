---
id: exercicios-js
title: Exercícios de JavaScript (JS)
sidebar_label: Exercícios de JavaScript (JS)
slug: /projetos/ensino/trilha-dados/linguagens/js/exercicios-js
---

## Área de Exercícios de JavaScript 

Aprender linguagens de programação pode ser mais complicado do que aprender linguagens de marcação (HTML, CSS). Por este motivo, é recomendado que ao decorrer do treinamento seja desenvolvido diversos exercícios relacionados a linguagem JavaScript para facilitar a compreensão dos ensinamentos teóricos. 

Além dos exercícios que serão propostos nesta página, é possível acessar websites que disponibilizam diversos exercícios com visualização IDE: 

* [JS Hero](https://www.jshero.net/en/success.html):
    * _Website gratuito que disponibiliza exercícios para quem quer praticar conhecimentos introdutórios de JavaScript_
    * **Nível de dificuldade**: Iniciante
    * **Língua**: Inglês

* [Exercism](https://exercism.org/tracks/javascript/exercises): 
    * _Website gratuito que disponibiliza exercícios de JavaScript, sendo possível visualizar a sua própria área de progresso de conhecimento_
    * **Nível de dificuldade**: Iniciante ao Avançado
    * **Língua**: Inglês 

* [freeCodeCamp](https://www.freecodecamp.org/learn/):
    * _Website gratuito que disponibiliza exercícios e possui formações gratuitas com certificado_
    * **Nível de dificuldade**: Iniciante ao Avançado
    * **Língua**: Inglês

* [Learn JavaScript](https://learnjavascript.online):
    * _Website interativo que disponibiliza diversos exercícios e conteúdos gratuitos, mas o curso completo **não** é gratuito_
    * **Nível de dificuldade**: Iniciante
    * **Língua**: Inglês

* [School of Net](https://www.schoolofnet.com/curso/frontend/javascript/iniciando-com-javascript-rev3/):
    * _Website **brasileiro** que oferece um curso gratuito para aprender JavaScript_
    * **Nível de dificuldade**: Iniciante
    * **Língua**: Português BR

## EXERCÍCIOS: Operadores

* Acesse o conteúdo teórico disponibilizado no Módulo 2 da [página JS](https://labriunesp.org/docs/projetos/ensino/trilha-dados/linguagens/js/js) antes de resolver os exercícios. 

* Para a resolução dos exercícios utilize um editor de código (ex: VSCode).

### Exercício 1 

Crie um algoritmo que 1) solicite um valor numérico ao usuário; 2) acrescente 15% de desconto no valor aplicado e; 3) retorne o valor final em reais para o usuário.

### Exercício 2

Crie um algoritmo que 1) solicite dois valores numéricos ao usuário; 2) divida os dois valores e; 3) retorne o valor final para o usuário. 

### Exercício 3

Crie um algoritmo que 1) solicite a idade do usuário; 2) calcule quando **dias** o usuário viveu e; 3) retorne o valor final para o usuário. 

### Exercício 4

De acordo com o resultado do exercício 3, crie um algoritmo que 1) solicite o resultado do exercício 3; 2) transforme o resultado em um valor de anos, meses e dias e; 3) retorne o valor final para o usuário. 

### Exercício 5

Um usuário deseja calcular a média do gasto de litros por quilômetro de sua moto. Sendo assim, crie um algoritmo que 1) solicite a distância (em quilômetros) percorrida; 2) solicite a quantidade de combustível (em litros) gastos; 3) calcule a média e; 4) retorne o valor final para o usuário.

### Exercício 6

O cliente de uma loja deseja saber a porcentagem de desconto aplicada dos produtos em promoção. Sendo assim, crie um algoritmo que 1) solicite o valor total do produto; 2) solicite o valor com desconto; 3) calcule o valor solicitado e; 4) retorne o valor final para o cliente.

### Resolução dos exercícios

[Acesse aqui](https://replit.com/join/spangfsend-rikamishiro)


## EXERCÍCIOS: "if" e "switch"

### Exercício 1

Faça um algoritmo que receba uma letra e determine se ela é vogal ou consoante. Desenvolva duas respostas (uma com "if" e a outra com "switch").

### Exercício 2

Escreva um código que receba um número (1-7) e devolva o dia da semana correspondente.

### Exercício 3

Escreva um código que receba um número (1-12), e diga qual mês e quantos dias ele tem. 

**Sugestão**: Desenvolva duas respostas, uma com apenas switch e a outra com switch e if else. 

### Exercício 4

Escreva um código que recehba dois números e determine qual deles é o maior.

### Exercício 5

Escreva um código que receba um número e determine se ele é par ou ímpar, e se é negativo ou positivo. 

### Exercício 6

Escreva um código que receba três ângulos e determine se eles formam um triângulo válido. Ou seja, a soma dos três ângulos é igual a 180º. 

### Exercício 7

Elabore um algoritmo que lê 2 valores a e b e informa se são múltiplos ou não. 

### Exercício 8

Escreva um código que retorne a quantidade de pontos que seu time fez de acordo com o resultado do jodo. Isto é, se o seu time venceu ele recebe 3 pontos, se ele empatou ele recebe 1 ponto e se ele perdeu não recebe nada. 

### Exercício 9

Elabore um programa que dada a idade de um nadador classifica-o em uma das seguintes categorias 

|Idade|Categoria|
|-----|---------|
|5-7 anos|Infantil A|
|8-10 anos|Infantil B|
|11-13 anos|Juvenil A|
|14-17 anos|Juvenil B|
|+18|Adulto|

### Exercício 10

Faça um algoritmo que dado um número, retorne a classificação do ângulo. 

|Tipo de ângulo|Significado|
|--------------|-----------|
|Ângulo agudo|ângulo com medida maior que 0º e menor que 90º|
|Ângulo reto|ângulo com medida igual a 90º|
|Ângulo obtuso|ângulo com medida maior que 90º|
|Ângulo rado|ângulo com a medida igual a 0º ou 180º|
|Ângulo côncavo|ângulo com medida entre 180º e 360º|
|Ângulo completo|ângulo com medida igual a 360º|

### Resolução dos exercícios 

[Acesse aqui](https://replit.com/join/spangfsend-rikamishiro)

## DESAFIO: Exercícios complementares de "Estruturas Condicionais" 

### Exercício 1 

Baseando na tabela abaixo, retorne a classificação de um produto. 

|Código|Classificação|
|------|-------------|
|1|Alimento não perecível|
|2 - 4|Alimento perecível|
|5 - 6|Vestuário|
|7|Higiene Pessoal|
|8 - 15|Limpeza e utensílios domésticos|
|Qualquer outro código|Código inválido|

### Exercício 2

Baseado na tabela abaixo, escreva um algoritmo que leia o código do item adquirido pelo consumidor e a quantidade, calculando e mostrando o valor a pagar. A mensagem de retorno deve conter o produto, a quantidade e o valor total a pagar. 

|Código|Produto|Preço Unitário (R$)|
|------|-------|-------------------|
|200|Camiseta|R$ 25,00|
|201|Calça|R$ 35,00|
|202|Jaqueta|R$ 45,00|
|203|Saia|R$ 55,00|

## DESAFIO: Criação de página com JS (Bootstrap e JQuery)

O intuito deste desafio é proporcionar a aplicação dos conhecimentos básicos de HTML, CSS, JavaScript (+ Bootstrap e JQuery) em um projeto prático. A ideia é construir um portifólio próprio seguindo um modelo específico. 

O modelo final será baseado na imagem abaixo:

Siga os próximos exercícios para resolver o desafio: 

### Exercício 1 

## EXERCÍCIOS: Array 

### Exercício 1

Acrescente a letra "a" para todas as palavras a seguir:

> desenvolvedor; programador; autor; vereador; professor 

### Exercício 2

Retorne o nome de 6 frutas com somente a primeira letra maiúscula

### Exercício 3

Retorne uma nova array com o nome dos estudantes e com a nota final

|Estudantes|Matemática|Peso de M.|Português|Peso de P.|
|----------|----------|----------|---------|----------|
|Nathalia|7|1.5|5|2|
|Zelia|5|1.5|8|2|
|Raquel|8|1.5|9|2|



