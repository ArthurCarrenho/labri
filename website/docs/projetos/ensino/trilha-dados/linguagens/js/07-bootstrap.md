---
id: bootstrap
title: Bootstrap
sidebar_label: Bootstrap
slug: /projetos/ensino/trilha-dados/linguagens/js/bootstrap
---

Assim como já foi visto na página de [treinamento CSS](https://labriunesp.org/docs/projetos/ensino/trilha-dados/linguagens/js/css), o Bootstrap pode ser definido como um framework com uma série de arquivos CSS e JS que determinam características específicas e pré-determinadas aos elementos da página.

Nesta página iremos nos aprofundar nos conhecimentos de Bootstrap.

* [Documentação Bootstrap](https://getbootstrap.com.br/docs/4.1/getting-started/introduction/)

## Módulo 1: Conhecimentos básicos  

### Layout responsivo

Uma das principais vantagens de utilizar o Bootstrap é a garantia de poder adaptar a página de acordo com o dispositivo utilizado. 

Por exemplo, ele atribui ao elemento ```<div>``` a característica da classe container: 

|Tipo de Container|Significado|
|-----------------|-----------|
|_container-fluid_|Considera a largura do dispositivo, ou seja, é considerado width:100% para todo o container|
|```container--{breakpoint}```|Considera width:100% até determinado tamanho de dispositivo|

### Bibliota de componentes 

O Bootstrap possibilita a estilização de alguns componentes do JavaScript e CSS. 

Veja a seguir:

|Componente|Significado|Exemplo|
|----------|-----------|-------|
|Alerts|Estiliza o componente alert com cores específicas|```<div class= "alert alert-danger">```|
|Carousel|Um slideshow responsivo com a possibilidade de utilizar efeitos especiais|
|Navbar|Torna responsivo o sistema de navegação de uma página/website|


## Módulo 2: Projeto Bootstrap 

Projeto utilizando **JavaScript**, **Bootstrap** e **JQuery**. 
