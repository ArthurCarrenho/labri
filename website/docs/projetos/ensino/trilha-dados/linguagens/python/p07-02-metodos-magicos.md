---
id: p07-02-metodos-magicos
title: Métodos Mágicos
sidebar_label: Métodos Mágicos
slug: /projetos/ensino/trilha-dados/linguagens/python/p07-02-metodos-magicos
---

- [Replit](https://replit.com/@fparon/objetos#metodos-magicos.py)



- Composição: ligar objetos sem herança
- `super()`: tem a função de chamar objeto definido na superclasse



### Sobrecarga de operadores 



- https://youtu.be/XSVvXso_Ukc 


### Interfaces e ABCs

- "Interfaces são métodos ou atributos públicos que objetos utilizam para se __comunicar__ com outros objetos"
- "Subconjunto de métodos públicos de um objeto que permitem desempenhar um papel específico em um sistema"


- https://www.youtube.com/watch?v=yLHV1__nZZw&list=PLOQgLBuj2-3L_L6ahsBVA_SzuGtKre3OK&index=5

### Material de Apoio

- https://youtu.be/MYaXUrmvrho

