---
id: geral
title: Material de Apoio
sidebar_label: Materail de apoio
slug: /projetos/ensino/trilha-dados/linguagens/python/material-apoio/geral
---




### Material de apoio

- [How to Use Generators and yield in Python](https://realpython.com/introduction-to-python-generators/)
- [Code Style](https://docs.python-guide.org/writing/style/)
- [Python zip()](https://www.programiz.com/python-programming/methods/built-in/zip)
- [How To Pretty Print in Python](https://betterprogramming.pub/how-to-pretty-print-in-python-9b1d8764d151)
- [Working With Files in Python](https://realpython.com/working-with-files-in-python/)
- [Working With Files in Python](https://realpython.com/python-csv/)
- [Why (and how) to put notebooks in production](https://towardsdatascience.com/why-and-how-to-put-notebooks-in-production-667fc3979dca)
- [Python Generators](https://www.programiz.com/python-programming/generator)
- [Welcome to Lazy Predict’s documentation!](https://lazypredict.readthedocs.io/en/latest/)
- [Algoritmo Prophet - Amazon Forecast](https://docs.aws.amazon.com/pt_br/forecast/latest/dg/aws-forecast-recipe-prophet.html)

- https://rogeriopradoj.com/2019/07/14/como-tirar-acentos-de-string-no-python-transliterate-unicodedata-e-unidecode//
- https://www.delftstack.com/pt/howto/python/python-leading-zeros/
- https://www.delftstack.com/pt/howto/python/remove-numbers-from-string-python/
- https://www.delftstack.com/pt/howto/python/how-to-remove-whitespace-in-a-string/



### Sites com conteúdo de Python

- [Algoritmos em Python](https://algoritmosempython.com.br/)
- [LearnPython](https://www.learnpython.dev/)
