---
id: lista
title: Lista Simplificada Projetos Extensão
sidebar_label: Lista Projetos Extensão
slug: /projetos/extensao/lista
---

<center>
    <img src="/img/projetos/extensao/logo-projetos-extensao.svg" alt="centered image" />
</center>


Os projetos de extensão são iniciativas voltadas para melhorar e incentivar a interação entre o meio acadêmico e a sociedade. Assim, neste espaço é possível encontrar projetos que buscam envolver docentes e discentes em atividades voltadas à divulgação científica. Para voltar para a página principal dos projetos de extensão, [clique aqui](https://labriunesp.org/projetos/extensao/).

|Nome do Projeto de dados|Informações|Local da documentação|
|---|---|---|
|Pod-RI|Projeto divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/pod-ri)|
|Conhecer Para Acolher|Projeto divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/conhecer-para-acolher)|
|Cidades Saudáveis e Sustentáveis|Projeto divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/cidades-sustentaveis)|
|Pandemia e as Relações Internacionais|Projeto divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/extensao/covid19/intro)|