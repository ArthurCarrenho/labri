---
id: iniciativas
title: Iniciativas
sidebar_label: Iniciativas
slug: /projetos/extensao/iniciativas
---

<center>
    <img src="/img/projetos/extensao/logo/logo-projetos-extensao.png" alt="centered image" />
</center>

- [Pandemia e as Relações Internacionais](/docs/projetos/extensao/covid19/intro)
- [Pod-RI](/docs/projetos/extensao/pod-ri/intro)