---
id: episodios
title: Episódios Disponíveis 
sidebar_label: Episódios
slug: /projetos/extensao/pod-ri/episodios
---

O Pod-RI está disponível em diversas plataformas de streaming, sendo possível encontrar todos os links através da página oficial no __*[Linktree](https://linktr.ee/podri)*__. 

Acesse as redes sociais do Pod-RI para acompanhar quando os novos episódios serão lançados.

- __*[Instagram](https://www.instagram.com/podr_i/)*__

- __*[Facebook](https://www.facebook.com/podcastRI/)*__