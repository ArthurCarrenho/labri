---
id: intro
title: Apresentação Pod-RI
sidebar_label: Apresentação
slug: /projetos/extensao/pod-ri/intro
---

<center>
    <img src="/img/projetos/extensao/pod-ri/pod-ri.svg" alt="centered image" width="300" height="300" />
</center>


O Pod-RI é um projeto feito por estudantes, no formato de podcast, iniciado como uma ideia entre amigos. Gravamos episódios com diversas temáticas das Relações Internacionais, assim como conversas com especialistas e profissionais. Além disso, produzimos conteúdos para redes sociais, onde entramos em contato mais direto com a comunidade de Relações Internacionais do Brasil.