---
id: intro
title: Apresentação Pandemia e as Relações Internacionais
sidebar_label: Apresentação
slug: /projetos/extensao/covid19/intro
---

<center>
    <img src="/img/projetos/extensao/labri-pandemia/labri-pandemia.svg" alt="centered image" width="500" height="500" />
</center>

O projeto "As Relações Internacionais e o Novo Coronavírus" surgiu a partir da iniciativa do Laboratório de Relações Internacionais (LabRI) da UNESP Franca. O projeto contou com a participação de vários docentes e discentes da universidade e tinha como principal objetivo produzir conteúdos sobre a pandemia de COVID-19 sob a perspectiva das Relações Internacionais.

