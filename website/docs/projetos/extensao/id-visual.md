---
id: id-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/extensao/id-visual
---

### Sobre o Logo

* Fontes: Fire Sans Thin; Voga
* Cores: #962932; #CE8F7D 
* [Link (Canva)](https://www.canva.com/design/DAEdhYi4uJ0/s3vDkL8vudvpqh_vMKw7yQ/view?utm_content=DAEdhYi4uJ0&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)