---
id: doc
title: Documentação Acervo Redalint
sidebar_label: Documentação
slug: /projetos/dados/acervo-redalint/doc
---

## Repositório

- Para acessar o repositório do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/acervo-redalint)

## Informações Adicionais

- Para acessar a plataforma do acervo, [clique aqui](https://acervo.redalint.org/)

- Entre em contato pelo nosso *e-mail* acervoredalint@gmail.com

- Para acessar a pasta do *google drive* do Acervo Redalint, [clique aqui](https://drive.google.com/drive/u/0/folders/1npycu_yv0hd-nuASxNJuH1oVXsMbo49F).

- [Docker Recoll WebUI](https://github.com/dsheyp/docker-recoll-webui/)

- [Docker Recoll WebUI - Docker HUB](https://hub.docker.com/r/dsheyp/docker-recoll-webui/)
