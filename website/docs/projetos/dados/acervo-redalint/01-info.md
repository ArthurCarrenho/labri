---
id: info
title: Apresentação Acervo Redalint
sidebar_label: Apresentação
slug: /projetos/dados/acervo-redalint/info
---

<center>
    <img src="/img/projetos/dados/acervo-redalint/redalint.svg" alt="centered image" />
</center>

A produção científica de acesso aberto sobre a internacionalização da educação superior na América Latina se encontra dispersa em variados portais, há certa inconsistência nos metadados destas publicações e a indexação integral deste material é rara. A partir deste diagnóstico, o **Acervo Redalint** surgiu com o objetivo de reunir em uma plataforma a produção científica sobre esta temática fornecendo metadados consistentes e a indexação integral do conteúdo disponível.

✅ Para acessar a *plataforma* do acervo, [clique aqui](https://acervo.redalint.org/).

✅ Para acessar o *repositório* do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/acervo-redalint).

✅ Para acessar a pasta do *google drive*, [clique aqui](https://drive.google.com/drive/u/0/folders/1npycu_yv0hd-nuASxNJuH1oVXsMbo49F).