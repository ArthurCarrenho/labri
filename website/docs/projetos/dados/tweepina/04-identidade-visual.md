---
id: identidade-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/dados/tweepina/identidade-visual
---

### Sobre o Logo: 

* Fonte: Bebas Neue Cyrillic
* Cores: #444446; #50ACE4
* [Link (Canva)](https://www.canva.com/design/DAEOn9JDEko/UkG36icGx3N2m-czVdYiRA/view?utm_content=DAEOn9JDEko&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)

