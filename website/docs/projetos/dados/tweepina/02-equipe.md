---
id: equipe
title: Equipe TweePInA
sidebar_label: Equipe
slug: /projetos/dados/tweepina/equipe
---

Abaixo são indicadas as pessoas que contribuem com o projeto

## Coordenadores do projeto

### [Marcelo Mariano](/docs/equipe#marcelo-passini-mariano)

<img className="img-equipe-foto" src="/img/equipe/marcelo.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/3505849964932313"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>


</div>

## Colaboradores do projeto

### [Rafael de Almeida](/docs/equipe#rafael-de-almeida) 

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2020 - atual*
</div>

### [Cíntia Di Iório](/docs/equipe#cintia-paulena-di-iório) 

<img className="img-equipe-foto" src="/img/equipe/cintia-paulena.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/cíntia-di-iório-999138150/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/cintiadiiorio/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>


</div>

<div style={{textAlign: 'center'}}>

*2021 - atual*
</div>


### [Ezequiel Santos](/docs/equipe#ezequiel-barbosa-dos-santos)

<img className="img-equipe-foto" src="/img/equipe/ezequiel-barbosa.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/ezequiel-barbosa/"> <img className="img-icon-redes" src="/img/social/linkedin.png" /> </a>

<a href="https://gitlab.com/ezxpro"> <img className="img-icon-redes" src="/img/social/gitlab.png" /> </a>

</div>

<div style={{textAlign: 'center'}}>

*2022 - atual*
</div>

### Júlia Borba 

<img className="img-equipe-foto" src="/img/equipe/julia-borba.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/julia-borba-342554174/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://scholar.google.com.br/citations?user=Z0jnKt0AAAAJ&hl=pt-BR"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2022 - atual*
</div>

## Já passaram por aqui

### Pedro Rocha 

<img className="img-equipe-foto" src="/img/equipe/pedro-rocha.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/8343815486813604"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>

<a href="https://www.linkedin.com/in/pedrodrocha"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2020 - 2021*
</div>

### [Pedro Henrique Campagna](/docs/equipe_inativo#pedro-henrique-campagna)

<img className="img-equipe-foto" src="/img/equipe/pedro-campagna.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/pedrohcmds/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/pedrohcmds"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2020 - 2021*
</div>

### [Júlia Silveira](/docs/equipe_inativo#júlia-silveira)

<img className="img-equipe-foto" src="/img/equipe/julia-silveira.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/julia-silveira23/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/rikamishiro"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2020 - 2021*
</div>