---
id: intro
title: Documentação TweePInA
sidebar_label: Introdução
slug: /projetos/dados/tweepina/infos/intro
---

### Repositório

✅ Para acessar o repositório do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/tweepina).

### Informações Adicionais

✅ Para acessar as pastas do *google drive* do projeto, [clique aqui](https://drive.google.com/drive/u/0/folders/1DCPUHymPWk07yYP6MvyImByttbQpzLJY).

✅ Para acessar a página do *Notion*, [clique aqui](https://www.notion.so/org-geral/Projeto-TweePInA-c9c5edcea2c84a2c8aabcb3e1763e8e9).

✅ Para acessar os fluxogramas do projeto [clique aqui](https://app.diagrams.net/#G1DtLtvntJ48YGEgyAI6SqpsMQduhmMafS)


