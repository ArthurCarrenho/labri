---
id: fluxograma
title: Documentação TweePInA
sidebar_label: Fluxograma
slug: /projetos/dados/tweepina/infos/fluxograma
---

### Fluxograma

Para acessar o *fluxograma* do projeto TweePInA, [clique aqui](https://app.diagrams.net/#G1DtLtvntJ48YGEgyAI6SqpsMQduhmMafS).

## Utilização do repositório

```
cd api-v2/api-twitter/api_academic/

```

- collected_perfis.py
  - get_info.py
  - get_tweets.py
  - twitter_api.py (indicação do perfil a ser coletado)
  - collected_profiles.json (controle de perfis coletados)