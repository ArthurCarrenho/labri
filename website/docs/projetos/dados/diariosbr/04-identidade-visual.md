---
id: identidade-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/dados/diariosbr/identidade-visual
---

### Sobre o Logo

* Fontes: Montserrat; Josefin Sans Bold
* Cores: #BCBBBC; #E7E7E5; #D54050
* [Link (Canva)](https://www.canva.com/design/DAEOn9JDEko/UkG36icGx3N2m-czVdYiRA/view?utm_content=DAEOn9JDEko&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)
