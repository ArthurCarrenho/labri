---
id: equipe
title: Equipe DiáriosBR
sidebar_label: Equipe
slug: /projetos/dados/diariosbr/equipe
---

Abaixo são indicadas as pessoas que contribuem com o projeto

## Coordernadores do projeto

### [Marcelo Mariano](/docs/equipe#marcelo-passini-mariano)

<img className="img-equipe-foto" src="/img/equipe/marcelo.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/3505849964932313"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>


</div>

### Regina Laisner

<img className="img-equipe-foto" src="/img/equipe/regina-laisner.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/2849922787767639"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://www.linkedin.com/in/regina-laisner-9b751735/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>


</div>

## Colaboradores do projeto

### [Rafael de Almeida](/docs/equipe#rafael-de-almeida)

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - atual*

</div>

### [Júlia Silveira](docs/equipe_inativo#júlia-silveira)

<img className="img-equipe-foto" src="/img/equipe/julia-silveira.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/julia-silveira23/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/rikamishiro"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - atual*

</div>

### Vitório Aflalo

<img className="img-equipe-foto" src="/img/equipe/vitorio-aflalo.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/vitorioaflalo/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://www.instagram.com/vitorioaflalo_/"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - atual*

</div>

### João Mateus Dora

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2021 - atual*

</div>

### Bruno Tebet

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2021 - atual*

</div>

## Já passaram por aqui

### [Pedro Henrique Campagna](/docs/equipe_inativo#pedro-henrique-campagna)

<img className="img-equipe-foto" src="/img/equipe/pedro-campagna.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/pedrohcmds"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - 2021*

</div>

### João Tonetto

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - 2021*

</div>

### Luiz Eduardo Moreira

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - 2021*

</div>
