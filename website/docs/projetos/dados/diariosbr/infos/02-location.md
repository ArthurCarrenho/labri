---
id: location
title: Documentação DiáriosBR
sidebar_label: Localização do conteúdo
slug: /projetos/dados/diariosbr/infos/location
---

### Localização do conteúdo dos DiariosBr

Na tabela abaixo é indicado a localização dos dados, dos scripts, do banco de termos e da indexação dos dados no Recoll

| Conteúdo  | Localização |
| --- | --- | 
| Dados | /media/hdvm04/bd/001/002 |
| Dados DOESP Executivo | /media/hdvm04/bd/001/002/001/doesp/ |
| Dados DOESP legislativo| /media/hdvm04/bd/001/002/001-b/doesp-legis/ |
| Dados DOM Franca| /media/hdvm04/bd/001/002/002/domfranca/ |
| Scripts DOESP Executivo | /media/hdvm05/scripts/diariosbr/doesp/ |
| Scripts DOESP Legislativo | /media/hdvm05/scripts/diariosbr/doespleg/ |
| Scripts DOM Franca |/media/hdvm05/scripts/diariosbr/domfranca/ |
| Banco de termos | /media/hdvm10/bd/300/004/nepps |
| Indexação DOESP Executivo| /media/hdvm05/index/001/002/001/doesp/ |
| Indexação DOESP Legislativo | /media/hdvm05/index/001/002/001-b/doesp-legis/ |
| Indexação DOM Franca| /media/hdvm05/index/001/002/002/domfranca/ |

### Diretório dos dados

```
/media/hdvm04/bd/001/002/002/domfranca/

/media/hdvm04/bd/001/002/001-b/doesp-legislativo/

/media/hdvm04/bd/001/002/001/doesp/
```

### Diretório dos scripts de coleta 

```
/media/hdvm05/scripts/diariosbr/domfranca/

/media/hdvm05/scripts/diariosbr/doespleg/

/media/hdvm05/scripts/diariosbr/doesp/
```

### Diretório geral

```
/media/hdvm10/bd/300/004/nepps
```