---
id: intro
title: Documentação DiáriosBR
sidebar_label: Introdução
slug: /projetos/dados/diariosbr/infos/intro
---

### Repositório

- Para acessar o *repositório* do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/diariosbr).

### Informações Adicionais

- Mais informações sobre os *dados* coletados dos Diários Oficiais, [clique aqui](https://docs.google.com/spreadsheets/d/1T2Dz-IoB_3_0NKIEYcUnKY7P1BlG026iTwV8XtAByNE/edit#gid=0).

- Para acessar a pasta do *google drive*, [clique aqui](https://drive.google.com/drive/u/0/folders/1ffm8RaW8LoC1Xq12TdMqIEeikepB_pCJ).

- Acesse a página do *Notion* clicando [aqui](https://www.notion.so/org-geral/Projeto-DiariosBR-64f8f6cac95b4ebb98c98b9955f1fbfa).