---
id: info
title: Apresentação IRJournalsBR
sidebar_label: Apresentação
slug: /projetos/dados/irjournalsbr/info
---

<center>
    <img src="/img/projetos/dados/irjournalsbr/irjournalsbr.svg" alt="centered image" width="200" height="300" />
</center>


As revistas acadêmicas brasileiras de Relações Internacionais seguem as políticas de acesso aberto. Com isso, todo o conhecimento divulgado por estas revistas podem ser acessados gratuitamente por qualquer pessoa interessada e podem ser reutilizados sem prévia autorização dos editores e autores, desde que seja respeitada a licença de uso do Creative Commons adotado pelos respectivos periódicos. Apesar da adoção do acesso aberto ser um aspecto importante para garantir acesso universal ao conhecimento científico, parte dos metadados destas revistas apresentam inconsistências. Ademais, a indexação integral do conteúdo dessas revistas não é disponibilizada publicamente, dificultando a busca por palavras chaves na integralidade dos arquivos. A partir do exposto acima, o projeto **IRjornalsBR** objetiva formar uma base de dados que aglutine tanto metadados mais consistentes como também forneça uma indexação integral destas revistas.

Para acessar o *repositório* do projeto IRJournalsBR, [clique aqui](https://github.com/pedrodrocha/irjournalsbr).

### Informações de Coleta

|Nome da Revista|Sigla Revista|XMP|origem|Periodicidade|
|----|----------|------|---|
|Austral|`austral`| :heavy_check_mark: |OJSv2| não-continua |
|Brazilian Journal of International Relations|`bjir`|  :heavy_check_mark: |OJSv3| não-continua |
|Brazilian Journal of Latin American Studies|`bjlas-prolam`| :heavy_check_mark: | OJSv3| não-continua |
|Carta Internacional|`carta-inter`|  :heavy_check_mark: |OJSv3|continua |
|Conjuntura Austral|`conj_austral`| :heavy_check_mark: |OJSv2| não-continua |
|Conjuntura Global|`conj_glob`|  :heavy_check_mark: |OJSv2| continua |
|Conjuntura Internacional|`conj_inter`|  :heavy_check_mark: |OJSv3| não-continua |
|Contexto Internacional|`contexto_inter`|  :heavy_check_mark: |scielo| não-continua |
|Estudos Internacionais|`estudos-inter`|  :heavy_check_mark: |OJSv3| não-continua |
|Meridiano 47|`meridiano`| :heavy_check_mark: |OJSv3| continua |
|Monções|`moncoes`|  :heavy_check_mark: |OJSv3| não-continua |
|Mural Internacional|`mural_inter`| :heavy_check_mark:|OJSv2| continua |
|Oikos|`oikos`|:heavy_check_mark: | OJSv2| não-continua |
|Revista Brasileira de Estudos de Defesa|`rbed`| :heavy_check_mark: |OJSv2| não-continua |
|Revista Brasileira de Política Internacional|`rbpi`|  :heavy_check_mark: |scielo| continua |