---
id: equipe
title: Equipe IRjournalsBR
sidebar_label: Equipe
slug: /projetos/dados/irjournalsbr/equipe
---

Abaixo são indicadas as pessoas que contribuem com o projeto

## Coordenadores do projeto

### Pedro Rocha

<img className="img-equipe-foto" src="/img/equipe/pedro-rocha.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/8343815486813604"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/pedrodrocha/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
 
<a href="https://orcid.org/0000-0002-1365-3292"> <img className="img-icon-redes" src="/img/social/orcid.png"/> </a>

</div>

## Colaboradores do projeto

### [Rafael de Almeida](/docs/equipe#rafael-de-almeida-2020---atual) 

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2021 - atual*
</div>
