---
id: metadados
title: Documentação IRJournalsBR
sidebar_label: Metadados
slug: /projetos/dados/irjournalsbr/infos/metadados
---


### Metadados de cada artigo das revistas

A definição dos nomes das variáveis dos metadados segue os padrões estabelecidos pelo "Rótulos de campo da Principal Coleção do Web of Science" [clique aqui](https://archive.is/5stfq) para mais detalhes

|variável| significado|
|--------|------------|
|AU|autor|
|OG|instituição|
|TI|titulo|
|AB|resumo|
|DE|palavras chaves|
|CR|referencias citadas|
|BP_EP|pagina inicial e pagina final|
|PY|ano|
|IS|issue|
|LA|lingua|
|DI|Identificador de Objeto Digital (DOI)|
|SO|nome revista|
|SN|ISSN da revista|
|URL|endereço web da pagina html|
|PDFURL|endereço web do pdf|

###  Metadados das referencias citadas em cada artigo das revistas


|variável               | significado|
|-----------------------|------------|
|`cited_in_journal`     |Revista em que a referencia foi citada|
|`cited_in_journal_year`|ano em que a referencia foi citada na revista|
|`references_raw`       |referência completa(crua)|
|`references_date`      |data da referência|
|`references_author`    |autores da referencias|
|`title_monogr`         |titulo|
|`title_analytic`       |titulo|
|`path_pdf`             |local do pdf|
|`path_xml`             |local do xml|

## Pesquisas dos metadados

1. Quantidade de artigos por ano
2. Frequência de artigos de um mesmo autor
3. Quais instituições aparecem, NANs, etc ("varredura nas instituições")
4. Línguas
5. Quantidade de páginas dos artigos
6. Quantidade de artigos com as referências apontadas
7. Frequência de palavras-chave
8. Busca por palavras-chave no título
9. Busca por palavras-chave por ano
