---
id: codigo
title: Documentação IRJournalsBR
sidebar_label: Código das Revistas
slug: /projetos/dados/irjournalsbr/infos/codigo
---


### Código de cada revista


|Código           | Nome da Revista               |
|-----------------|-------------------------------|
| 007/002/002/001 | Boletim EPI Unicamp
| 007/002/002/002 | Revista Cena Internacional
| 007/002/002/003 |	Revista Contexto Internacional
| 007/002/002/004 |	Revista Meridiano 47
| 007/002/002/005 |	RBCE
| 007/002/002/006 |	RBCS
| 007/002/002/007 |	RBPI
| 007/002/002/008 | Revista de la Cepal
| 007/002/002/009 |	Revista DEP
| 007/002/002/010 |	Revista Política Externa
| 007/002/002/011 |	Revista Via Mundi
| 007/002/002/012	| Revista Estudos Internacionais
| 007/002/002/013	| Revista Carta Internacional
| 007/002/002/014	| Revista Monções - Revista de Relações Internacionais da UFGD
| 007/002/002/015	| Revista Mural Internacional
| 007/002/002/016	| Revista Austral
| 007/002/002/017	| Revista Brazilian Journal of International Relations
| 007/002/002/018	| Revista Conjuntura Global
| 007/002/002/019	| Revista Conjuntura Internacional
| 007/002/002/020	| Revista Conjuntura Austral
| 007/002/002/021	| Revista OIkos
| 007/002/002/022	| Revista Brasileira de Estudos de Defesa
| 007/002/002/023	| Brazilian Journal of Latin American Studies