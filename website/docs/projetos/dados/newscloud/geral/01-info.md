---
id: info
title: Apresentação News Cloud
sidebar_label: Apresentação
slug: /projetos/dados/newscloud/geral/info
---

## Apresentação

<center>
    <img src="/img/projetos/dados/news-cloud/newscloud-logo.svg" alt="centered image" width="500" height="500" />
</center>

Uma importante fonte para pesquisa acadêmica são as notícias veiculadas pelos jornais impressos. Nesses veículos de comunicação, além de informações nacionais e internacionais relevantes, são encontrados opiniões de importantes atores políticos. Porém, o conjunto de informações de cada jornal se encontram em bases de dados distintas sem um mecanismo que viabilize uma busca agregada; a indexação integral dos dados vinculados apresenta limitações que dificultam a pesquisa avançada (utilização de operadores booleanos) através da busca por palavras-chaves, especialmente, quando selecionamos um período temporal longo e abarcamos o grande volume de informação; as informações veiculadas em formato textual não estão estruturadas, isso dificulta o cruzamento de metadados importantes (título, autor, caderno, entre outros). Devido a isso, o objetivo geral desse projeto é coletar, indexar, tratar e estruturar as informações veiculadas por jornais impressos. Mais especificamente, o projeto **NewsCloud** visa 

- (1) coletar integralmente os jornais impressos selecionados, realizando o devido tratamento das informações veiculadas para uma melhor utilização dos dados para pesquisas acadêmicas; 
- (2) subsidiar pesquisas acadêmicas que utilizam jornais impressos como fontes de informação ou objeto de estudo; 
- (3) fornecer um instrumento básico para análise das informações veiculadas; 
- (4) indicar possibilidades e instrumentos que auxiliem análises mais detalhadas das informações veiculadas.

Para acessar o *repositório* do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/newscloud).