---
id: equipe
title: Equipe News Cloud
sidebar_label: Equipe
slug: /projetos/dados/newscloud/geral/equipe
---

Abaixo são indicadas as pessoas que contribuem com o projeto

## Coordenadores do projeto

### [Marcelo Mariano](/docs/equipe#marcelo-passini-mariano)

<img className="img-equipe-foto" src="/img/equipe/marcelo.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/3505849964932313"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

### Regina Laisner

<img className="img-equipe-foto" src="/img/equipe/regina-laisner.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/2849922787767639"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://www.linkedin.com/in/regina-laisner-9b751735/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
</div>

### Sérgio Luiz Cruz Aguilar

<img className="img-equipe-foto" src="/img/equipe/sergio-luiz.png"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/7971139957298760"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br/citations?user=7CTN3Z0AAAAJ&hl=pt-BR"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

<a href="https://orcid.org/0000-0003-4757-4426"> <img className="img-icon-redes" src="/img/social/orcid.png" /> </a>

</div>

## Colaboradores do projeto

### [Rafael de Almeida](/docs/equipe#rafael-de-almeida-2020---atual) 

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/brunna-fonseca-sousa-776913206/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<img className="img-icon-redes" src="/img/social/instagram.png" />
<a href="https://www.linkedin.com/in/brunna-fonseca-sousa-776913206/"> </a>

</div>

<div style={{textAlign: 'center'}}>

*2020 - atual*
</div>

### [Cíntia Di Iório](/docs/equipe#cintia-paulena-di-iório) 

<img className="img-equipe-foto" src="/img/equipe/cintia-paulena.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/cíntia-di-iório-999138150/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/cintiadiiorio/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

</div>

<div style={{textAlign: 'center'}}>

*2021 - atual*
</div>

### Vitório Aflalo 

<img className="img-equipe-foto" src="/img/equipe/vitorio-aflalo.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/vitorioaflalo/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://www.instagram.com/vitorioaflalo_/"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2021 - atual*
</div>

### Maria Clara Zani Tomazini 

<img className="img-equipe-foto" src="/img/equipe/maria-clara-tomazini.png"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/maria-clara-zani-tomazini-09353a254/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://www.instagram.com/clara_tomazini/"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2022 - atual*
</div>

## Já passaram por aqui

### [Pedro Henrique Campagna](/docs/equipe_inativo#pedro-henrique-campagna)

<img className="img-equipe-foto" src="/img/equipe/pedro-campagna.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/pedrohcmds/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/pedrohcmds"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - 2021*
</div>

### [Júlia Silveira](/docs/equipe_inativo#júlia-silveira)

<img className="img-equipe-foto" src="/img/equipe/julia-silveira.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/julia-silveira23/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/rikamishiro"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2020 - 2022*
</div>