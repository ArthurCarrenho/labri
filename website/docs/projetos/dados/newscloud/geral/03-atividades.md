---
id: atividades
title: Atividades Realizadas News Cloud
sidebar_label: Atividades Realizadas
slug: /projetos/dados/newscloud/geral/atividades
---

## Atividades Realizadas

|Data|Atividades Realizadas|Presentes| 
|---|---|---|
|09/02/2024|EQUADOR-ECO, PERU-ECO, COLOMBIA-EES - Retomada da segunda coleta | Rafael, Maria Clara Tomazini|
|09/02/2024|EQUADOR-ECO, PERU-ECO, COLOMBIA-EES - Ajustes nas coletas | Rafael, Maria Clara Tomazini|
|02/02/2024|EQUADOR-ECO, PERU-ECO, COLOMBIA-EES - Revisão e planjamento das coletas restantes | Rafael, Maria Clara Tomazini|
|15/12/2023|EQUADOR-ECO, PERU-ECO, COLOMBIA-EES - Início da indexação no recoll | Rafael, Maria Clara Tomazini|
|24/11/2023|EQUADOR-ECO - Ajustes e organização da base de dados | Rafael, Maria Clara Tomazini|
|24/11/2023|PERU-ECO - Ajustes e organização da base de dados | Rafael, Maria Clara Tomazini|
|24/11/2023|COLOMBIA-EES - Ajustes e organização da base de dados | Rafael, Maria Clara Tomazini|
|09/10/2023|COLOMBIA-ELT - Início da estrturação da coleta | Rafael, Maria Clara Tomazini|
|09/10/2023|PERU-LRP - Início da estrturação da coleta | Rafael, Maria Clara Tomazini|
|09/10/2023|PERU-ECO - Ajustes dos scripts e retomada das coleta 01 e 02 | Rafael, Maria Clara Tomazini|
|09/10/2023|EQUADOR-ECO - Ajustes dos scripts e retomada das coleta 01 e 02 | Rafael, Maria Clara Tomazini|
|29/09/2023|COLOMBIA-EES - Ajustes dos scripts e finalização da coleta 02 | Rafael, Maria Clara Tomazini|
|25/09/2023|COLOMBIA-EES - Ajustes dos scripts do tratemento dos links e coleta 02 | Rafael, Maria Clara Tomazini|
|15/09/2023|COLOMBIA-EES - Ajustes finais script coleta 02 | Rafael, Maria Clara Tomazini|
|15/09/2023|COLOMBIA-EES - Tratamento dos links para a coleta 02 | Rafael, Maria Clara Tomazini|
|15/09/2023|COLOMBIA-EES - Ajustes finais no script da coleta 01 | Rafael, Maria Clara Tomazini|
|04/09/2023|COLOMBIA-EES - Ajustes no script e continuação da coleta 01 | Rafael, Maria Clara Tomazini, Malak Chefrid|
|25/08/2023|COLOMBIA-EES - Configuração de contas novo sistema do LabRI | Rafael, Maria Clara Tomazini, Malak Chefrid|
|07/06/2023|EQUADOR-ECO - Início da coleta das notícias | Rafael, Maria Clara Tomazini, Leonardo Petrilli|
|07/06/2023|EQUADOR-ECO - Finalização da estrutura da coleta 01 e 02 | Rafael, Maria Clara Tomazini, Leonardo Petrilli|
|02/06/2023|EQUADOR-ECO - Estruturação da coleta do jornal | Rafael, Maria Clara Tomazini|
|31/05/2023|PERU-ECO - Ajustes na segunda coleta | Rafael, Maria Clara Tomazini|
|24/05/2023|COLOMBIA-EES - Continuação da estruturação da coleta do jornal | Rafael, Maria Clara Tomazini|
|23/05/2023|PERU-DCO, COLOMBIA-HDM - Geração de htmls e Indexação no recoll | Rafael, Maria Clara Tomazini|
|16/05/2023|COLOMBIA-EES - Estruturação da coleta (continuação) | Rafael, Maria Clara Tomazini, Leonardo Petrilli|
|10/05/2023|PERU-ECO, PERU-DCO, COLOMBIA-HDM - Correção de problemas no script | Rafael, Maria Clara Tomazini|
|05/05/2023|COLOMBIA-EES - Estruturação da coleta do jornal | Rafael, Maria Clara Tomazini|
|05/05/2023|EQUADOR- ELU - Estruturação da coleta do jornal | Rafael, Maria Clara Tomazini|
|03/05/2023|PERU-DCO - Ajustes no script | Rafael, Maria Clara Tomazini|
|03/05/2023|PERU-ECO - Ajustes no script | Rafael, Maria Clara Tomazini|
|03/05/2023|COLOMBIA-HDM - Ajustes no script| Rafael, Maria Clara Tomazini|
|28/04/2023|COLOMBIA-HDM - Início da coleta| Rafael, Maria Clara Tomazini|
|28/04/2023|PERU-ECO - Ajustes chrome selenium| Rafael, Maria Clara Tomazini|
|26/04/2023|PERU-ECO - Início da Coleta 02| Rafael, Maria Clara Tomazini|
|25/04/2023|PERU-DCO - Início da Coleta 02| Rafael, Maria Clara Tomazini|
|25/04/2023|PERU-DCO - Ajustes para geração de htmls a partir das informações inseridas no banco json | Rafael, Maria Clara Tomazini|
|14/04/2023|PERU-DCO - Verificação do problema em relação ao intervalo de datas | Rafael, Maria Clara Tomazini|
|14/04/2023|PERU-DCO - Indexação dos arquivos no Recoll | Rafael, Maria Clara Tomazini|
|14/04/2023|Ajuste na estrutura das pastas | Rafael, Maria Clara Tomazini|
|31/03/2023|Fluxograma de interação entre os scripts | Rafael, Maria Clara Tomazini, Artur Dantas, Leonardo Petrilli|
|31/03/2023|Fluxograma das pastas | Rafael, Maria Clara Tomazini, Artur Dantas, Leonardo Petrilli|
|31/03/2023|BRASIL-FBV - Indexação da FBV no Recoll | Rafael, Maria Clara Tomazini, Artur Dantas, Leonardo Petrilli|
|31/03/2023|PERU-DCO - Implementação de controle temporal de coleta por dia | Rafael, Maria Clara Tomazini, Artur Dantas, Leonardo Petrilli|
|24/03/2023|PERU-DCO - Aperfeiçoamento da filtragem de datas no banco json | Rafael, Maria Clara Tomazini|
|10/03/2023|PERU-DCO - Tratamento da autoria finalizado | Rafael, Maria Clara Tomazini|
|10/03/2023|PERU-DCO - Inserção do nome do paíse sede do jornal | Rafael, Maria Clara Tomazini|
|10/03/2023|PERU-DCO - Finalização dos ajustes nos htmls | Rafael, Maria Clara Tomazini|
|09/03/2023|Ajustes nos htmls | Rafael, Maria Clara Tomazini|
|09/03/2023|Reestruturação das pastas | Rafael, Maria Clara Tomazini|
|09/03/2023|BRASIL-FBV - Início da indexação da FBV no Recoll | Rafael, Maria Clara Tomazini|
|03/03/2023|Atualização do script de geração de html | Rafael, Maria Clara Tomazini|
|03/03/2023|Atualização do script do banco json | Rafael, Maria Clara Tomazini|
|02/03/2023|PERU-DCO - Coleta de parágrafos | Rafael, Maria Clara Tomazini|
|17/02/2023|ECU-UNI - Primeira versão do script e coleta finalizada | Rafael, Maria Clara Tomazini|
|10/02/2023|ECU-UNI - Estrturação da coleta | Rafael, Maria Clara Tomazini|
|10/02/2023|BRA-FBV - Realização da coleta dos pdfs | Rafael, Maria Clara Tomazini|
|09/02/2023|BRA-FBV - Início da extração dos pdfs do jornal brasileiro Folha de Boa Vista | Rafael, Maria Clara Tomazini|
|09/02/2023|PERU-ECO - Coleta do jornal peruano El Comercio | Rafael, Maria Clara Tomazini|
|26/01/2023|PERU-DCO - Inicio da coleta do jornal peruano Diario Correo | Rafael, Maria Clara Tomazini|
|08/12/2022|BRA-CBZ - Ajuste na coleta dos pdfs | Rafael, Maria Clara Tomazini|
|01/12/2022|BRA-CBZ - Coleta dos pdfs de 01/06/1999 a 31/08/2020| Rafael, Maria Clara Tomazini|
|21/10/2022|BRA-CBZ e BRA-FBV - Extração de link e data e inserção no banco json| Rafael, Vitório Aflalo, Maria Clara Tomazini|
|21/10/2022|GZP extração dos links das notícias e da data| Rafael, Vitório Aflalo, Maria Clara Tomazini| 
|08/09/2022|Ajuste para tornar mais rápida a coleta das informações que não foram inseridas no banco| Rafael, Vitório Aflalo|
|11/08/2022|ESP - ajustes dos erros finais anotados no README| Rafael, Cíntia|
|10/08/2022|ESP - ajustes no tratamento de listas de autoria nos anos de 2015 e 2016| Rafael, Cíntia|
|08/08/2022|ESP - ajustes de erros anotados no README| Rafael, Cíntia|
|04/08/2022|ESP - adição de novas formas de extração das tags data, horario, data atualizado, horario atualizado, autoria e paragrafo em 2022, ajustes no tratamento da tag atualizacao em 2022 e tentativas de ajuste no erro da tag autoria 3.1 em 2015| Rafael, Cíntia|
|03/08/2022|ESP - adição de novas extrações das tags subtitulo, data, atualizacao, autoria, paragrafo no ano de 2022| Rafael, Cíntia|
|02/08/2022|ESP - ajustes no tratamento da tag titulo em 2021 e no tratamento da tag autoria em 2015| Rafael, Cíntia|
|01/08/2022|ESP - ajustes na extração de dados de 2019, criação da extração de link e categoria na função de exceção, ajustes no tratamento das tags titulo, link e autoria em 2020 e 2021| Rafael, Cíntia|
|29/07/2022|ESP - ajustes no tratamento da tag autoria em 2018, 2019 e 2020; tentativa de ajustar erro em notícia de 2019; início da revisão do ano de 2020| Rafael, Cíntia|
|28/07/2022|ESP - adição de nova forma de extração de parágrafos em 2016; limpeza e ajustes no tratamento das tags autoria e parágrafo em 2016 e 2017; início do tratamento de 2018| Rafael, Cíntia|
|27/07/2022|ESP - ajustes na extração da tag local em 2015, adição de nova forma de extração da tag autoria e horário atualizado em 2015, ajustes no tratamento das tags autoria em 2015 e 2016| Rafael, Cíntia|
|26/07/2022|ESP - ajustes nas extrações e tratamentos das tags local, autoria, palavras-chave entre 2014 e 2015, ajustes na remoção de itens vazios das listas paragrafos| Rafael, Cíntia|
|25/07/2022|ESP - ajustes nas extrações e tratamentos das tags autoria nos anos de 2012 à 2014, ajustes na remoção de itens duplicados das listas| Rafael, Cíntia|
|08/07/2022|ESP - ajustes nas extrações e tratamentos das tags autoria nos anos 2012 e 2013| Rafael, Cíntia|
|07/07/2022|ESP - ajustes na extração e tratamento das tags palavras-chaves e autoria em 2010, e autoria e paragrafo em 2012| Rafael, Cíntia|
|06/07/2022|ESP - ajustes nas extrações da lista paragrafos, e no tratamento das autorias e palavras-chaves em 2010| Rafael, Cíntia|
|05/07/2022|ESP - ajustes na extração e tratamento das tags autoria em 2001, 2008 e 2009, paragrafos, data e horario em 2009 e paragrafos em 2010| Rafael, Cíntia|
|04/07/2022|ESP - ajustes na tag autoria no ano de 2008| Rafael, Cíntia|
|01/07/2022|ESP - ajustes no tratamento da tag data em 2001, titulo em 2004, e adição de extração das tags autoria e horário em 2007 e autoria em 2008| Rafael, Cíntia|
|30/06/2022|ESP - conferindo extração dos anos de 2002 à 2004, e tratamento da tag título no ano de 2004| Rafael, Cíntia|
|28/06/2022|ESP - conclusão da criação da nova função; check nas extrações de 2001, 2002 e 2003| Rafael, Cíntia|
|27/06/2022|ESP - avanços no desenvolvimento de nova função para notícias específicas de 2001: tags autoria, local, data, subtitulo e titulo| Rafael, Cíntia|
|24/06/2022|ESP - ajustes na extração da tag categoria e início da criação de nova função para notícias específicas de 2001| Rafael, Cíntia|
|23/06/2022|ESP - ajustes no tratamento da tag titulo no ano de 2001 (função padrão)| Rafael, Cíntia|
|21/06/2022|ESP - ajustes na extração e no tratamento das tags paragrafos e titulo e readequação das funções padrão e exceção, nos anos 2000 e 2001| Rafael, Cíntia|
|20/06/2022|ESP - ajustes na extração e no tratamento da tag autoria e da tag data, em ambas funções padrão e de exceção, no ano 2000| Rafael, Cíntia|
|14/06/2022|ESP - ajustes na extração e tratamento de subtítulo e autoria em 1998 e 2000 (respectivamente); conferindo notícias disponíveis entre 1997 a 2000| Rafael, Cíntia|
|10/06/2022|ESP - ajustes na extração e tratamento das tags autoria, data e local em 1998| Rafael, Cíntia|
|06/06/2022|ESP - ajustes na extração e tratamento das tags autoria, data e parágrafo em 1998| Rafael, Cíntia|
|03/06/2022|ESP -  início da conferência por ano: 1998; início da extração de autoria; reajustes na extração e tratamentos das tags título, subtítulo e data| Rafael, Cíntia|
|31/05/2022|ESP -  ajustes gerais nas funções do ESP, sem ano específico| Rafael, Cíntia|
|31/05/2022|FSP -  ajustes finais na coleta da FSP de 1994 à 2022| Rafael, Cíntia|
|30/05/2022|FSP -  reajustes no tratamento e extração das tags autoria, local, título e tags de 2019 à 2022| Rafael, Cíntia|
|27/05/2022|FSP -  reajustes no tratamento e extração das tags autoria e local em 2019| Rafael, Cíntia|
|26/05/2022|FSP -  ajuste de erro na extração de dados de 2018 à 2022; tratamento das tags autoria, local, link e categoria em 2018 e 2019| Rafael, Cíntia|
|12/05/2022|FSP -  readequação da extração e do tratamento das tags autoria, subtitulo, categoria e link em 2017, 2018 e 2019| Rafael, Cíntia|
|10/05/2022|FSP -  readequação do tratamentos das tags data atualizado, horario atualizado, local e autoria em 2017| Rafael, Cíntia|
|03/05/2022|GPOV - Revisão do scrpit e arrumação de problemas na coleta|Rafael, Vitório|
|29/03/2022|GPOV - Revisão e documentação das funções de coleta e primeira coleta integral|Rafael, Vitório|
|22/03/2022|GPOV - Início da coleta completa|Rafael, Vitório|
|22/03/2022|GPOV - resolução de problema de página não encontrada|Rafael, Vitório|
|22/03/2022|FSP - início da revisão de 2000| Rafael, Treyce|
|11/03/2022|FSP -  continuação da readequação do período de 1998 à 1999| Rafael, Cíntia|
|10/03/2022|FSP - readequação do período de 1998 à 1999|Rafael, Cintia|
|09/03/2022|FSP - revisão e readequação do período de 1997, 1998 e 1999|Rafael, Cintia|
|08/03/2022|FSP - revisão do período de 1994 à fevereiro de 1997|Rafael, Cintia|
|07/03/2022|FSP - revisão do período de 2015 à 2022 concluída; início da revisão do período de 1994 à 2014|Rafael, Cintia|
|04/03/2022|FSP - readequação na extração de autoria e local e tratamento das tags nos anos de 2013 e 2014; otimização da função 1 para os anos de 2015 a 2019|Rafael, Cintia|
|03/03/2022|FSP - acréscimos na extração de autoria e local entre 2011 e 2012; ajuste nas coletas de  2013 e 2014|Rafael, Cintia|
|25/02/2022|VLR - verificação da coleta realizada (problema na inserção no banco)|Rafael, Júlia|
|25/02/2022|FSP - ajustes na extração da data, local e autoria de 2008 a 2012|Rafael, Cintia|
|24/02/2022|VLR - ajustes na visualização das imagens e no template HTML|Rafael, Júlia|
|24/02/2022|FSP - readequação da extração de autoria no ano de 2004, 2006 e 2007; tratamento na extração de data e condições de título em 2007; avançando na extração de 2008|Rafael, Cintia|
|23/02/2022|FSP - ajustes no tratamento de autoria e local no ano de 2004; criação de nova função para extração de dados em 2005|Rafael, Cintia|
|22/02/2022|VLR - resolução do problema de extração das imagens|Rafael, Júlia|
|22/02/2022|FSP - tratamento na extração das tags data, autoria e local nos anos 2002-2004; readequação na função origem_arquivos|Rafael, Cintia|
|21/02/2022|ajuste na estrutura do template HTML|Rafael, Júlia|
|21/02/2022|ajustes na estrutura da coleta da Carta Capital, El País, BBC e Gazeta do Povo|Rafael, Júlia|
|21/02/2022|VLR - finalização da estrutura da coleta|Rafael, Júlia|
|21/02/2022|FSP - readequação na extração de título, data, autoria e local nos anos 2002-2004|Rafael, Cintia|
|18/02/2022|VLR - coleta das imagens, ajuste no banco json e tratamento de algumas tags (autoria e parágrafos)|Rafael, Júlia|
|18/02/2022|FSP - ajustes nas extrações de autoria; criação de nova função para extração de notícias de 1998-1999; extração de tags até 2003|Rafael, Cintia|
|17/02/2022|VLR - adicionando noticias no bd json e começando a coleta imagens|Rafael, Júlia|
|17/02/2022|FSP - ajuste de erros na limpeza de parágrafos e extração de autoria nas notícias de 1994|Rafael, Cintia|
|16/02/2022|VLR - extração de todas as informações e início da extração das imagens|Rafael, Júlia|
|16/02/2022|FSP - ajustes na extração de notícias de 1994; extração de título, data, parágrafos e autoria na segunda função|Rafael, Cíntia|
|15/02/2022|VLR - início da extração das informações|Rafael, Júlia|
|15/02/2022|FSP - inserção da extração de data e parágrafos em uma segunda função criada; testando variações do código a fim de encontrar o erro ao extrair notícias de 1994 a 2014|Rafael, Cíntia|
|14/02/2022|GP - finalização da extração dos dados|Rafael, Júlia|
|14/02/2022|FSP - testes na extração de notícias nos períodos de 1994 e 2008 até 2014; anotações de erros no README|Rafael, Cíntia|
|11/02/2022|FSP - readequação na extração de tags de origem, data, categorias e paragrafos; extração da tag de autoria; início da inserção no banco de dados de 2015 até 2022|Rafael, Cíntia|
|10/02/2022|GP - continuação da estruturação da coleta|Rafael, Júlia|
|10/02/2022|BBC e El País - ajustes nos templates json e HTML e testes de coleta; inserção no banco e geração dos HTMLs|Rafael, Júlia|
|10/02/2022|FSP - extração de datas, tags e parágrafos das notícias de 2019 a 2022; anotações no README; mapeamento dos problemas na extração de 2018|Rafael, Cíntia|
|09/02/2022|BBC - início da inserção no banco; geração dos HTMLs; problema na inserção dos parágrafos|Rafael, Júlia|
|09/02/2022|El País - início da inserção no banco; geração dos HTMLs|Rafael, Júlia|
|09/02/2022|Carta Capital - continuação da inserção no banco; geração dos HTMLs|Rafael, Júlia|
|09/02/2022|FSP - início da extração de dados do ano de 2020|Rafael, Cíntia|
|08/02/2022|El País - continuidade da extração de dados; início da inserção dos dados no banco|Rafael, Júlia|
|08/02/2022|ESP - readequação da função que acessa as páginas das notícias; inserção de outras formas de extração de parágrafos|Rafael, Cíntia|
|07/02/2022|El País - início da coleta|Rafael, Júlia|
|07/02/2022|ESP - extração de título, datas e parágrafos dos anos de 1997 à 2001; readequação da função que acessa a página|Rafael, Cíntia|
|04/02/2022|BBC - início da inserção no banco e geração de HTML|Rafael, Júlia|
|04/02/2022|Carta Capital - início da inserção no banco e geração de HTML|Rafael, Júlia|
|04/02/2022|ESP - criação de um README_ESP para informações da coleta do ESP; criação de condição para atribuir subtítulos grandes à  parágrafos; readequação na extração da DATA/META; extração até o ano de 2001|Rafael, Cíntia|
|03/02/2022|BBC - finalização da identificação das tags; coleta em andamento|Rafael, Júlia|
|03/02/2022|ESP - readequação na extração da categoria e do subtítulo; documentação de exceções no README; criação de condição para links que não são acessados; criação de condição para ver parágrafos não coletados; extração até o ano de 2004|Rafael, Cíntia|
|02/02/2022|BBC - início da coleta e identificação das tags|Rafael, Júlia|
|02/02/2022|Carta Capital - extraindo informações dos parágrafos e das tags|Rafael, Júlia|
|02/02/2022|ESP - ajustando erros na extração dos parágrafos e categoria; extração completa dos HTMLs de 2018 até 2022; apontamento de exceções no README|Rafael, Cíntia|
|01/02/2022|Carta Capital - readequação da coleta e extração pela tag meta|Rafael, Júlia|
|01/02/2022|ESP - ajustando erros na extração da data, tags e parágrafos dos HTMLs de 2022; e extração completa dos HTMLs do ano de 2021|Rafael, Cíntia|
|31/01/2022|Carta Capital - início da readequação do script de coleta ao novo site da Carta Capital|Rafael, Júlia|
|31/01/2022|ESP - ajustando erros na extração da categoria e extração das datas e horários de atualização dos HTMLs do ano de 2022|Rafael, Cíntia|
|28/01/2022|ESP - avanço na extração das informações dos HTMLs do ano de 2022|Rafael, Cíntia|
|27/01/2022|ESP - iniciando coleta das notícias das páginas antigas|Rafael, Cíntia|
|05/01/2022|Carta Capital - ajustando coleta das páginas antigas|Rafael, Júlia|
|16/12/2021|Carta Capital - ajustando template html|Rafael, Júlia|
|15/12/2021|Carta Capital - começando coleta das notícias das páginas antigas|Rafael, Júlia|
|14/12/2021|Carta Capital - começando coleta das páginas antigas|Rafael, Júlia|
|13/12/2021|Carta Capital - inserindo dir_local e ajustando HTML|Rafael, Júlia|
|09/12/2021|Carta Capital - termino do tratamento dos paragrafos|Rafael, Júlia|
|07/12/2021|Carta Capital - tratando paragrafos|Rafael, Júlia|
|06/12/2021|Carta Capital - inserção dados no db.json e tratando datas|Rafael, Júlia|
|02/12/2021|Carta Capital - extração de tags, paragrafos e subtitulo|Rafael, Júlia|
|23/11/2021|Gazeta do povo - extração do conteúdo das notícias|Rafael, Júlia, Vitório|
|09/11/2021|Resolvendo problema de encode|Rafael, Júlia, Vitório|
|06/10/2021|Gazeta do Povo - coleta dos links das notícias e início da extração do conteúdo|-|
|06/10/2021|Conversa inicial com João Ferreira (NEPPS) sobre criação de material de utilização do Recoll|-|
|29/09/2021|Gazeta do Povo - Estruturação inicial do script de coleta|
|29/09/2021|Carta Capital - coleta pag dinamica com selenium, parseamento da pag com bs e inicio da montagem do banco de dados|
|19/08/2021|Criação do ambiente virtual conda (finalizado)|
|09/08/2021|Término da normalização das variáveis e funções do projeto; Scripts para tratar `html` da fsp estão em funcionamento|
|02/08/2021|Finalização da normalização e tratamento dos erros|
|27/07/2021|Lidando com erros no tratamento das notícias |
|21/07/2021|Continuação da normalização de nomes de variáveis e funções|
|20/07/2021|Normalização de nomes de variáveis e funções|
|19/07/2021|Normalização de nomes de variáveis e funções|
|13/07/2021|Início da estruturação do fluxograma e revisão do código de tratamento atual|
|12/07/2021|Revisão do projeto - problema no momento de limpeza dos arquivos|
