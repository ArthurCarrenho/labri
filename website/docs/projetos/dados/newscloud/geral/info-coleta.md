---
id: info-coleta
title: Informações de Coleta
sidebar_label: Informações de Coleta
slug: /projetos/dados/newscloud/geral/info-coleta
---

Na seguinte tabela são informados os dados referentes a cada jornal utilizado na coleta:

|Jornal                 |sigla| Período            |Conteúdo  |Assinatura|Local   |Diretório        |
|-----------------------|-----|--------------------|----------|----------|--------|-----------------|
|Antigo Estadão         |     | ? - atual          | Integral | Não      | Brasil |`bd/00002-002`   |
|Antigo Folha de SP     |     | ? - atual          | Integral | Não      | Brasil |`bd/00002-003`   |
|Antigo Valor Econômico |     | ? - atual          | Integral | Não      | Brasil |`bd/00002-001`   |
|[BBC](https://www.bbc.com/portuguese)                    | BBC | 01/01/1994 - atual | Integral | Não      | Brasil |`bd/002/001/006/`|
|[Carta Capital](https://www.cartacapital.com.br/)          | CCP | 01/01/1994 - atual | Integral |Verificar | Brasil |`bd/002/001/005/`|
|[Correio Braziliense](https://www.correiobraziliense.com.br/)    | Bradil-CBZ | 01/01/1994 - atual | Integral | Sim      | Brasil |`bd/002/001/009/`|
|[El País](https://elpais.com/america/)                | ELP | 01/01/1994 - atual | Integral | Não      | Brasil |`bd/002/001/007/`|
|[Estado de São Paulo](https://www.estadao.com.br/)   | ESP | 01/01/1994 - atual | Integral | Sim      | Brasil |`bd/002/001/002/`|
|[Folha de Boa Vista](https://folhabv.com.br/)     | Brasil-FBV | 09/01/2013 - 30/04/2021 | Integral | Não      | Brasil |`bd/002/001/010/`|
|[Folha de S. Paulo](https://www.folha.uol.com.br/)      | FSP | 01/01/1994 - atual | Integral | Sim      | Brasil |`bd/002/001/003/`|
|[Gazeta do Povo](https://www.gazetadopovo.com.br/)         | GZP | 05/11/2004 - atual | Integral |Verificar | Brasil |`bd/002/001/004/`| 
|[O Globo](https://oglobo.globo.com/)              | GLP | ? - atual          | Integral |Verificar | Brasil |`bd/002/001/004/`|          
|[Valor Econômico](https://valor.globo.com/)        | VRL | 01/01/1994 - atual | Integral | Sim      | Brasil |`bd/002/001/001/`|    
|[Hoy Diario Del Magdalena](https://www.hoydiariodelmagdalena.com.co/) |COL-HDM| 22/09/2017- 05/05/2023 |          |          |Colombia|                 |
|[El Tiempo](https://www.eltiempo.com/)              |     |                    |          |          |Colombia|                 |
|[El Espectador](https://www.elespectador.com/)|COLOMBIA-EES|20/08/2007 - atual |Integral|Sim|Colômbia|
|[El Comercio](https://www.elcomercio.com/ )|EQUADOR - ECO|  02/01/2011 - atual |Integral|          |Equador |                 |
|[Diario Correo](https://diariocorreo.pe/edicion/tumbes/?ref=menu_edi/)         |Peru-DCO | 01/01/2015 - 17/05/2023 | Integral   |          |Peru    |`bd/002/001/012` |
|[El Comercio](https://www.eluniverso.com/)            | Peru-ECO | 25/08/2011 - atual |          |          |Peru    |`bd/002/001/013` |
|[La Republica]( https://larepublica.pe/)           |     |                    |          |          |Peru    |                 |