---
id: elementos-brasil
title: Elementos Brasil
sidebar_label: Elementos Brasil
slug: /projetos/dados/newscloud/infos/elementos-brasil
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>

  <TabItem value="BBC" label="BBC" default>

|Elemento            |Tipo  |Coleta 01         |Coleta 02          |
|--------------------|------|------------------|-------------------|
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
|                    |      |                  |                   |
 </TabItem>

  <TabItem value="Carta Capital" label="Carta Capital" default>
 </TabItem>

  <TabItem value="Correio Brasiliense" label="Correio Brasiliense" default>
 </TabItem>

  <TabItem value="El País" label="El País" default>
 </TabItem>

  <TabItem value="Estado de São Paulo" label="Estado de São Paulo" default>
 </TabItem>

  <TabItem value="Folha de Boa Vista" label="Folha de Boa Vista" default>
 </TabItem>

  <TabItem value="Folha de São Paulo" label="Folha de São Paulo" default>
 </TabItem>

  <TabItem value="Gazeta do Povo" label="Gazeta do Povo" default>
 </TabItem>

  <TabItem value="O Globo" label="O Globo" default>
 </TabItem>

  <TabItem value="Valor Econômico" label="Valor Econômico" default>
 </TabItem>

</Tabs>
