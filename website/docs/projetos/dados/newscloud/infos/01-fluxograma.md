---
id: fluxograma
title: Documentação NewsCloud
sidebar_label: Fluxograma
slug: /projetos/dados/newscloud/infos/fluxograma
---

### Fluxograma

Para acessar os fluxogramas do projeto NewsCloud, [clique aqui](https://drive.google.com/drive/folders/1-5YoeHOAOUU-WxSejk8IDDmcqJc4PEfb).