---
id: intro
title: Documentação NewsCloud
sidebar_label: Introdução
slug: /projetos/dados/newscloud/infos/intro
---

## Repositório

✅ Para acessar o *repositório* do NewsCloud, [clique aqui](https://gitlab.com/unesp-labri/projeto/newscloud).

✅ Para acessar os fluxogramas do Newscloud [clique aqui](https://app.diagrams.net/#G1ocP0GbS4f3Iw3I_CjBJssart2VJY70SE)

✅ Para acessar a pasta do google drive [clique aqui](https://drive.google.com/drive/u/0/folders/1LZ5QHvSojjB5jXb0Ycp6JTcYbZ5aS2Zj)


