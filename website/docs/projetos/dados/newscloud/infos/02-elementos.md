---
id: elementos
title: Documentação NewsCloud
sidebar_label: Elementos do Banco de Dados
slug: /projetos/dados/newscloud/infos/elementos
---

Questões da coleta - autoria nem sempre aparece na página web, mas pode estar presente no código. Contudo, o formato do nome se apresenta como no exemplo: "/autor/cmgutierrez/".

Questões da coleta - Se atentar com os horários dos jornais peruanos pois apresentam fuso -5.

O nome do autor não é necessariamente de quem escreveu a notícia, pode ser quem publicou ela no site.

## Elementos do Banco de Dados

|Elemento|Significado|Tipo|Exemplo|
|--------|-----------|-------|----|
|`origem`|Fonte do dado coletado|`str`|Casa Civíl|
|`sigla` |País + iniciais do jornal|`str`|Casa Civíl|
|`classificado`|Tipo de dado|`list`|Notícia, discurso|
|`categoria`|Subdivisões feitas pela fonte do dado|`list`|Caderno Mundo, Caderno Brasil|
|`País`|País sede do jornal |`str`|Brasil, Peru|
|`autoria`|Nome dos responsáveis da publicação|`list`|Carta Capital, Jamil Chade|
|`titulo`|Título conferido ao dado coletado|`str`|-|
|`subtitulo`|Subtitulo conferido ao dado coletado|`str`|-|
|`data`|Data inicial|`str`|-|
|`horario`|Horario inicial|`str`|-|
|`data_atualizado`|Data atualizada|`list`|-|
|`horario_atualizado`|Horario atualizado|`list`|-|
|`link_archive`|URL arquivada no Internet Archive|`str`|-|
|`data_archive`|Data do arquivamento no Internet Archive|`str`|-|
|`horario_archive`|Horario do arquivamento no Internet Archive|`str`|-|
|`local`|Localização geografica|`list`|Brasilia|
|`tags`|Etiquetas feitas pela fonte do dado|`list`|-|
|`paragrafos`|Paragrafos da informação|`list`|-|
|`nome_arquivo`|-|`str`|
|`imagens`|-|`list`|
|`dir_bd`|Diretório em que o dado está localizada|`str`|-|
|`dir_arquivo`|-|`str`|
|`codigo_bd`|-|`str`|
|`extra_01`|Informação adicional|-|-|
|`extra_02`|Informação adicional|-|-|
|`extra_03`|Informação adicional|-|-|

## Variáveis

|Variável      |Significado                                                         |Exemplo                                                                  |
|--------------|--------------------------------------------------------------------|-------------------------------------------------------------------------|
|`env_dir_bd`  |Aponta para uma variável de ambiente que leva para o diretório raiz |A variável de ambiente leva para o diretório local/raiz `BD_JORNAL_UNESP`|
|`env_dir_json`|Diretório geral dos arquivos json                                   |`env_dir_bd` + `/json`                                                   |
|`nome_bd_json`|Nome do arquivo json                                                |`env_dir_bd` + `data`                                                    |
|`dir_json`    |Caminho completo do arquivo json                                    |`env_dir_json` + `env_dir_bd` + `data.json`                              |

## Tabela Modelo

|Elemento|Significado|Tipo|Exemplo|
|--------|-----------|----|-------|
|`origem`|-|`str`|-|
|`sigla`|-|`str`|-|
|`classificado`|-|`list`|-|
|`categoria`|-|`list`|-|
|`País`|-|`str`|-|
|`autoria`|-|`list`|-|
|`titulo`|-|`str`|-|
|`subtitulo`|-|`str`|-|
|`data`|-|`str`|-|
|`horario`|-|`str`|-|
|`data_atualizado`|-|`list`|-|
|`horario_atualizado`|-|`list`|-|
|`link_archive`|-|`str`|-|
|`data_archive`|-|`str`|-|
|`horario_archive`|-|`str`|-|
|`local`|-|`list`|-|
|`tags`|-|`list`|-|
|`paragrafos`|-|`list`|-|
|`nome_arquivo`|-|`str`|-|
|`imagens`|-|`list`|-|
|`dir_bd`|-|`str`|-|
|`dir_arquivo`|-|`str`|-|
|`codigo_bd`|-|`str`|-|
|`extra_01`|-|-|-|
|`extra_02`|-|-|-|
|`extra_03`|-|-|-|






## Indexação de Dados

Os htmls gerados podem ser pesquisados através do recoll.

### Índices externos

Para gerar os índices externos é necessário indicarmos a pasta em que se encontram os dados (htmls ou pdfs) e a pasta na qual está a indexação