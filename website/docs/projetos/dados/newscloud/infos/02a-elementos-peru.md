---
id: elementos-peru
title: Elementos Peru
sidebar_label: Elementos Peru
slug: /projetos/dados/newscloud/infos/elementos-peru
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
  <TabItem value="Diario Correo" label="Diario Correo" default>

|Elemento            |Tipo  |Coleta 01         |Coleta 02         |
|--------------------|------|------------------|------------------|
|`origem`            |`str` |:heavy_check_mark:|
|`sigla`             |`str` |:heavy_check_mark:|
|`país`              |`str` |:heavy_check_mark:|
|`classificado`      |`list`|:heavy_check_mark:|
|`categoria`         |`list`|:heavy_check_mark:|
|`País`              |`str` |                  |:heavy_check_mark:|
|`autoria`           |`list`|                  |:heavy_check_mark:|
|`titulo`            |`str` |:heavy_check_mark:|
|`subtitulo`         |`str` |:heavy_check_mark:|       
|`data`              |`str` |:heavy_check_mark:|         
|`horario`           |`str` |                  |:heavy_check_mark:|         
|`data_atualizada`   |`list`|                  |:heavy_check_mark:|                 
|`horario_atualizado`|`list`|                  |:heavy_check_mark:|
|`link`              |`str` |:heavy_check_mark:||
|`link_archive`      |`str` |||
|`data_archive`      |`str` |||
|`horario_archive`   |`str` |||
|`local`             |`list`|||
|`tags`              |`list`|                  |:heavy_check_mark:|
|`paragrafos`        |`list`|                  |:heavy_check_mark:|
|`dir_bd`            |`str` |:heavy_check_mark:||
|`dir_arquivo`       |`str` |||
|`codigo_bd`         |`str` |                  |:heavy_check_mark:|
|`imagens`           |`list`|||
|`nome_arquivo`      |`str` |||
|`extra_01`          |-     |-|-|
|`extra_02`          |-     |-|-|
|`extra_03`          |-     |-|-|

  </TabItem>
  <TabItem value="El Comercio" label="El Comercio">

|Elemento            |Tipo  |Coleta 01         |Coleta 02          |
|--------------------|------|------------------|-------------------|
|`origem`            |`str` |:heavy_check_mark:||
|`classificado`      |`list`|:heavy_check_mark:||
|`sigla`             |`str` |:heavy_check_mark:||
|`país`              |`str` |:heavy_check_mark:|
|`categoria`         |`list`|                  |:heavy_check_mark:|
|`autoria`           |`list`|                  |:heavy_check_mark:|
|`titulo`            |`str` |:heavy_check_mark:||
|`subtitulo`         |`str` |                  |:heavy_check_mark:|       
|`data`              |`str` |:heavy_check_mark:||         
|`horario`           |`str` |:heavy_check_mark:||         
|`data_atualizado`   |`list`|                  |:heavy_check_mark:|                 
|`horario_atualizado`|`list`|                  |:heavy_check_mark:|
|`link`              |`str` |:heavy_check_mark:||
|`link_archive`      |`str` |                  ||
|`data_archive`      |`str` |||
|`horario_archive`   |`str` |||
|`local`             |`list`|||
|`tags`              |`list`|                  |:heavy_check_mark:|
|`paragrafos`        |`list`|                  |:heavy_check_mark:|
|`dir_bd`            |`str` |:heavy_check_mark:||
|`dir_arquivo`       |`str` |||
|`codigo_bd`         |`str` |                  |:heavy_check_mark:|
|`imagens`           |`list`|||
|`nome_arquivo`      |`str` |                  |:heavy_check_mark:|
|`extra_01`          |-     |-|-|
|`extra_02`          |-     |-|-|
|`extra_03`          |-     |-|-|

 </TabItem>
</Tabs>