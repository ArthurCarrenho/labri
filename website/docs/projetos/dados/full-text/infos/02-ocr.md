---
id: ocr
title: Documentação Full Text
sidebar_label: Serviços de OCR
slug: /projetos/dados/diariosbr/infos/ocr
---

## Serviço de OCR - Workflow

Para criar o serviço de *OCR* mais eficiente e atualizado, estão sendo utilizado por base os seguintes projetos do gitHub:

- [OCR My PDF](https://github.com/jbarlow83/OCRmyPDF)
- Clique para ver o [tutorial](http://tarefas.lantri.org/projects/wiki-01/wiki/101_TutorialOcrMyPdf_)

- Serviço Instalado: [Serviço OCR](http://tarefas.lantri.org/projects/wiki-01/wiki/Servi%C3%A7o_OCR)
