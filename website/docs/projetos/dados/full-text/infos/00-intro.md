---
id: intro
title: Documentação Full Text
sidebar_label: Introdução
slug: /projetos/dados/full-text/infos/intro
---

## Repositório

- Para acessar o *repositório* do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/full-text).

### Audio

- Acesse o repositório Full Text *Audio* [clicando aqui](https://gitlab.com/unesp-labri/projeto/full-text/fulltextri-audio).

### Texto

- Acesse o repositório Full Text *Texto* [clicando aqui](https://gitlab.com/unesp-labri/projeto/full-text/fulltextri-text).

### Vídeo

- Acesse o repositório Full Text *Vídeo* [clicando aqui](https://gitlab.com/unesp-labri/projeto/full-text/fulltext-ri-videos).