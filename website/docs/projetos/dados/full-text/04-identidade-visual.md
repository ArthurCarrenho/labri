---
id: identidade-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/dados/full-text/identidade-visual
---

### Sobre o Logo

* Fonte: Alegreya SC
* Cores: #7A7A7A; #BCBBBC; #FF1616; #2E2A13
* [Link (Canva)](https://www.canva.com/design/DAEOn9JDEko/UkG36icGx3N2m-czVdYiRA/view?utm_content=DAEOn9JDEko&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)
