---
id: info
title: Apresentação Full Text
sidebar_label: Apresentação
slug: /projetos/dados/full-text/info
---

<center>
    <img src="/img/projetos/dados/full-text/full-text.svg" alt="centered image" width="200" height="300" />
</center>

 
Em geral, grande parte dos dados disponibilizados publicamente na internet estão em formatos que dificultam uma rápida e adequada utilização. As boas práticas indicadas nas discussões em torno de dados abertos acabam não sendo seguidas. Com isso, é comum vermos dados sendo disponibilizados no formato de imagem, de pdf ou audiovisual sem transcrição textual. O projeto **Full Text** visa auxiliar o tratamento desses dados, convertendo e/ou extraindo tais dados para formato textual passível de indexação e/ou melhor estruturação. A realização de pós-processamento e/ou OCR em textos disponibilizados no formato de imagem, a conversão de áudios e vídeos para o formato textual e a extração de conteúdos disponibilizados em pdfs estão entre as atividades realizadas nesse projeto.

Para acessar o *repositório* do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/full-text).