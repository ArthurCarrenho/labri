---
id: atividades
title: Atividades Realizadas GovLatinAmerica
sidebar_label: Atividades
slug: /projetos/dados/gov-latin-america/atividades
---

Esta página reune tanto o planejamento das atividades como também indica as atividade realizadas 

## Atividades Planejadas

- Do dia 09/08/2023 a dia 06/09/2023
  - Consolidação do treinamento realizado no primeiro semestre 
    - Fundamentos de programação com Python 
    - Coleta de dados (webscrapping)
    - Formação de banco de dados 
    - Análise e visualização de dados 
  - Estruturar a divulgação dos trabalhos realizados 
- A partir do dia 07/09/2023 
  - Métodos e técnicas de pesquisa para análise de dados 

## Atividades Realizadas

|Data|Atividades Realizadas|Participantes|
|---|---|---| 
|14/12/2022| Ajustes e padronização da coleta do Ministério da Infraestrutura| Cíntia, Rafael
|13/12/2022| Finalização da extração do Ministério da Economia e ajustes e padronização da coleta do Ministério da Infraestrutura| Cíntia, Rafael
|10/11/2022| Ajustes e padronização da coleta do Ministério da Economia| Cíntia, Rafael
|08/11/2022| Ajustes e padronização da coleta do Ministério da Economia| Cíntia, Rafael
|07/11/2022| Ajustes e padronização da coleta do Ministério da Economia| Cíntia, Rafael
|19/10/2022| Atualização da função boletins no Ministério da Economia| Cíntia, Rafael
|17/10/2022| Atualização e padronização das extrações dos Ministério da Defesa, Ministério do Desenvolvimento Regional e Ministério da Economia| Cíntia, Rafael
|14/10/2022| Atualização completa e padronização das extrações do Ministério das Comunicações e finalização parcial do Ministério da Defesa| Cíntia, Rafael
|11/10/2022| Finalização parcial das extrações do Ministério da Ciência| Cíntia, Rafael
|10/10/2022| Continuação da atualização e padronização das extrações do Ministério da Ciência (função boletins)| Cíntia, Rafael
|07/10/2022| Continuação da atualização e padronização das extrações do Ministério da Ciência| Cíntia, Rafael
|06/10/2022| Continuação da atualização e padronização das extrações do Ministério da Ciência| Cíntia, Rafael
|05/10/2022| Atualização e padronização das extrações do Ministério da Ciência| Cíntia, Rafael
|04/10/2022| Atualização e padronização completa das extrações do Ministério do Meio Ambiente e início da atualização e padronização no Ministério da Ciência| Cíntia, Rafael
|03/10/2022| Final da atualização e padronização das extrações do Ministério da Agricultura| Cíntia, Rafael
|30/09/2022| Início da atualização e padronização das extrações do Ministério da Agricultura| Cíntia, Rafael
|29/09/2022| Atualização e padronização (10/10 funções) das extrações da Casa Civil| Cíntia, Rafael
|28/09/2022| Atualização e padronização (9/10 funções) das extrações da Casa Civil| Cíntia, Rafael
|27/09/2022| Atualização e padronização (7/10 funções) das extrações da Casa Civil| Cíntia, Rafael
|22/09/2022| Início da conferência (5/10 funções) das extrações da Casa Civil| Cíntia, Rafael
|21/09/2022| Ajustes finais na extração de notícias de todos os ministérios| Cíntia, Rafael
|20/09/2022| Ajustes na extração da tag autoria no Ministério da Infraestrutura| Cíntia, Rafael
|16/09/2022| Ajustes e adição de extrações da tag autoria no Ministério da Infraestrutura| Cíntia, Rafael
|14/09/2022| Ajustes e adição de extrações da tag autoria na Casa Civil e no Ministério da Infraestrutura| Cíntia, Rafael
|12/09/2022| Ajustes e adição de extrações da tag autoria no MME, conferência do ministério da Economia e Casa Civil| Cíntia, Rafael
|09/09/2022| Conferência dos ministérios da mulher e de minas e energia - erros anotados no documento noticias.md| Cíntia, Rafael
|08/09/2022| Conferência e padronização dos ministérios do arquivo coleta_noticias| Cíntia, Rafael
|31/08/2022| Padronização e adição de novas formas de extração de autoria nos ministérios do arquivo coleta_noticias| Cíntia, Rafael
|30/08/2022| Padronização da extração e tratamento das notícias em diferentes ministérios| Cíntia, Rafael
|29/08/2022| Padronização da extração e tratamento das notícias em diferentes ministérios| Cíntia, Rafael
|25/08/2022| Padronização da extração e tratamento das notícias em diferentes ministérios| Cíntia, Rafael
|17/08/2022| Padronização da extração de notícias dos ministérios da defesa, da economia, da infraestrutura e das minas e energia| Rafael, Cíntia|
|16/08/2022| Padronização das notícias no Ministério do Meio Ambiente e no Ministério da Defesa| Rafael, Cíntia|
|15/08/2022| Atualização das tags e organização da função noticias da Casa Civil e inicio da padronização da função noticia do Ministério do Meio Ambiente| Rafael, Cíntia|
|31/05/2022| Restruturação das informações e documentação do projeto| Treyce, Rafael
|19/05/2022| MRE - Coleta completa das notas de imprensa| Treyce, Rafael
|11/05/2022| MRE - Início da coleta total das notas de imprensa| Rafael, Treyce
|06/04/2022| Início da extração de todas as notas de imprensa dinsponíveis atualmente no site do MRE| Rafael, Treyce 
|05/04/2022| Início da extração dos parágrafos às notas de imprensa atuais| Treyce, Rafael|
|25/03/2022| Inserir título, data, hora, n° da edição das notas no banco json| Treyce, Rafael|
|24/03/2022| Continuação da estruturação da coleta das notas de imprensa atuais| Treyce, Rafael|
|23/03/2022| Estruturando a coleta das notas de imprensa atuais| Treyce, Rafael|
|03/03/2022| Agendas ministérios - Seperação dos ministérios em blocos para efetuar a coleta mais rapidamente|João Motta, Rafael|
|23/02/2022| MRE - Início da coleta das notas de imprensa atuais| Treyce, Rafael
|23/02/2022| MRE - Ajustes nas entradas do template html e início da inserção dos anos de 1997 a 2021 no banco json| Treyce, Rafael
|22/02/2022| MRE - Início da inserção dos dados no banco json| Treyce, Rafael
|17/02/2022| Planalto - Substituição das bibliotecas para coleta de PDF| João Motta, Rafael
|15/02/2022| MRE - Início da inserção das notas de imprensa no banco de dados| Treyce, Rafael
|14/02/2022| MRE- Tratamento das notas de imprensa de 2002 a 1997| Treyce, Rafael
|11/02/2022| MRE - Tratamento das notas de imprensa de 2012 a 2003| Treyce, Rafael
|10/02/2022| Ministérios - Finalização da estrutura da coleta das agendas| João Motta, Rafael
|10/02/2022| MRE - Tratamento dos erros das notas de impresa 2009 e 2008; resolução do problema para desconsiderar erros vazios| Treyce, Rafael
|07/02/2022| MRE - Tratamento de erros das notas de imprensa de 2009| Treyce, Rafael|
|04/02/2022| MRE - Tratamento notas de imprensa 2010 e 2009| Treyce, Rafael|
|28/01/2022| Ministérios - Avanços e reformulaçao da coleta das agendas| João Motta, Rafael|
|27/01/2022| MRE - tratamento das notas de imprensa de 2011 e 2012| Treyce, Rafael
|09/12/2021|Integração dos arquivos JSON ao template HTML| Cintia, João, Rafael|
|28/10/2021|Casa Civil/Planalto/MRE - Estruturação da coleta e do template html| Cintia, João, Rafael|
|07/10/2021|Casa Civil - Mapeamento e início da coleta|
|07/10/2021|Site Planalto - Início da inserção dos dados no Banco|
|30/09/2021|Site Planalto - Início da coleta dos discursos presentes no site|
|23/09/2021|Site Planalto - Continuidade da integração dos sitemaps (lidando com os updates das notícias)|
|15/09/2021|Site Planalto - Integração dos sitemaps `gov.br/sitemap.xml`, `gov.br/planalto.xml`|
|09/09/2021|Site Planalto - Preperação para update de novas notícias|
|02/09/2021|Site Planalto - Extração do parágrafo das notícias|
|26/08/2021|Site Planalto - Implementação do `tinydb` para montar a base de dados|
|19/08/2021|Site Planalto - Coleta do sitemap e extração local dos dados disponíveis no `sitemap.xml`|
|12/08/2021|Site Planalto - Término do mapeamento e início da coleta no site do Planalto|
|05/08/2021|Site Planalto - Estruturação do mapeamento e início da estruturação do fluxograma da coleta e tratamento dos dados.|
|29/07/2021|Site Planalto - Ajustes BeautifulSoup e extração das informações do xml do sidemap|
|22/07/2021|Site Planalto - Configuração do ambiente virtual Conda e do VSCode|

## Próximas atividades

- [ ] Rever Readme do repositório
- [ ] Rever estrutura da página aberta do projeto
- [ ] Inserir informações coletadas em banco json 
