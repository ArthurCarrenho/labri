---
id: dados_coletados
title: Dados Coletados 
sidebar_label: Dados Coletados
slug: /projetos/dados/gov-latin-america/dados_coletados
---


## Ministérios do governo federal brasileiro 

|Órgão Público|Notícias|Agenda|Status|
|----------|--------|------|-------|
|[Planalto](https://www.gov.br/planalto/pt-br) | Específico |  Comum | Em coleta |
|[Casa Civil](https://www.gov.br/casacivil/pt-br)| Comum | Comum | Coletado |
|[MRE](https://www.gov.br/mre/pt-br/)| Específico | Comum | Coletado |
|[MMA](https://www.gov.br/mma/pt-br)| Específico | Comum | Coletado |
|[Infraestrutura](https://www.gov.br)|Específico | Comum | Coletado |
|[MME](https://www.gov.br/mme/pt-br)| Comum | Comum | Coletado |
|[Economia](https://www.gov.br/economia/pt-br)| Comum | Sem agenda (fora do ar temporariamente) |  Coletado |
|[Defesa](https://www.gov.br/defesa/pt-br)| Comum | Comum | Não possui |
|[Saúde](https://www.gov.br/saude/pt-br)| Comum | Comum | Coletado |
|[Ciência](https://www.gov.br/mcti/pt-br)| Comum | Comum | Coletado |
|[Mulher](https://www.gov.br/mdh/pt-br/)| Específico | Comum | Coletado |
|[Comunicações](https://www.gov.br/mcom/pt-br/)| Comum | Comum | Coletado |
|[Turismo](https://www.gov.br/turismo/pt-br)| Comum | Apenas agenda antiga / Agenda desconfiguada| Coletado |
|[Desenvolvimento Regional](https://www.gov.br/mdr/pt-br)| Comum | Apenas agenda antiga| Coletado |
|[Biblioteca presidencia](https://www.gov.br/planalto/pt-br/conheca-a-presidencia/acervo/biblioteca-da-presidencia)| Sem notícias | Sem agenda | Coletado |
|[Secretaria-Geral](https://www.gov.br/secretariageral/pt-br)| Específico | Comum | Coletado |
|[Controladoria-Geral da União](https://www.gov.br/cgu/pt-br)| Específico | Comum |
|[Advocacia-Geral da União](https://www.gov.br/agu/pt-br)| Específico | Comum |
|[Cidadania (institucional)](https://www.gov.br/cidadania/pt-br)| Comum (>> dá problema a partir da 9ª notícia) | Específico | Coletado |
|[Cidadania (desenvolvimento social)](https://www.gov.br/cidadania/pt-br)| Específico | Específico | Coletado |
|[Cidadania (esporte)](https://www.gov.br/cidadania/pt-br)| Específico | Específico |
|[Secretaria de Governo](https://www.gov.br/secretariadegoverno/pt-br)| Comum | Comum|
|[Educação](https://www.gov.br/mec/pt-br)| Comum | Agenda antiga apenas | Coletado |
|[Ministério da Justiça e Segurança Pública](https://www.gov.br/mj/pt-br)| Comum | link específico | Coletado |
|[Gabinete de Segurança Institucional](https://www.gov.br/gsi/pt-br)| Específico | link específico|
|[MAPA](https://www.gov.br/agricultura/pt-br/)| Comum | link específico| Coletado |

## Coletas em andamento 

- [Senado Federal (Ana Beatriz)](https://gitlab.com/anamacao/fundamentos-de-ciencia-de-dados/-/tree/main/python?ref_type=heads)
- [Notícias Mercosul (Rebeca)](https://gitlab.com/r.s.tosta/fundamentos-de-ciencia-de-dados/-/tree/main/python?ref_type=heads)
- [CGI e Dataprivacy (Gustavo)](https://gitlab.com/gustavo.castejon/fundamentos-de-ciencia-de-dados/-/tree/main/fundamentos-python?ref_type=heads)
- [Câmara dos Deputados (João)](https://gitlab.com/JLMOTTA/fundamentos-de-ciencias-de-dados/-/tree/main/fundamentos-python?ref_type=heads)
- [Cepal e Brics Policy Center (Bruna)](https://gitlab.com/bruna.domingues01/fundamentos-de-ciencias-de-dados/-/tree/main/fundamentos-python?ref_type=heads)

## Análise de dados 

- [Ana Beatriz](https://github.com/anamacao/analise_dados)
- [Bruna](https://github.com/brudomingues/analise_dados)
- [Gustavo](https://github.com/Castejones/analise_dados)
- [João](https://github.com/JL-Motta01/analise_dados)
- [Rebeca](https://github.com/rtosta/python_analise_dados)