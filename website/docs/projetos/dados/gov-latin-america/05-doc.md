---
id: doc
title: Documentação GovLatinAmerica
sidebar_label: Documentação
slug: /projetos/dados/gov-latin-america/doc
---

### Repositório

✅ Para acessar o *repositório*, [clique aqui](https://gitlab.com/unesp-labri/projeto/govlatinamerica).

✅ Acesse a página do *Google Drive* do projeto GovLatinAmerica clicando [aqui](https://drive.google.com/drive/u/1/folders/1_g01RcccLl2PpTupxQyCoXEJka30VXeG).

✅ Mais informações sobre o projeto na página do [notion](https://www.notion.so/org-geral/Projeto-GovLatinAmerica-9219a9b60ae24cb98a197f7bdab42209).

### Material de Apoio

- [Portuguese encoding Ã£, Ãª, Ã§, Ã¡](https://stackoverflow.com/questions/52749834/portuguese-encoding-%C3%83%C2%A3-%C3%83%C2%AA-%C3%83-%C3%83).

