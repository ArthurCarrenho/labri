---
id: equipe
title: Equipe GovLatinAmerica
sidebar_label: Equipe
slug: /projetos/dados/gov-latin-america/equipe
---

Abaixo são indicadas as pessoas que contribuem com o projeto

## Coordenadores do projeto

### [Marcelo Mariano](/docs/equipe#marcelo-passini-mariano)
<img className="img-equipe-foto" src="/img/equipe/marcelo.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/3505849964932313"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>


</div>


## Colaboradores do Projeto

### [Rafael de Almeida](/docs/equipe#rafael-de-almeida-2020---atual) 

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - atual*

</div>

### João Lucas Motta

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/jlmotta"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2021 - atual*

</div>

### Artur Damiano Dantas

<img className="img-equipe-foto" src="/img/equipe/artur-damiano.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/artur-dantas"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/arddantas/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>


</div>


<div style={{textAlign: 'center'}}>

*2022 - atual*

</div>

### Bruna Domingues

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2022 - atual*

</div>

### Henédio Bernardino Pedrosa Neto

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/henedio-neto-616706268/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/net0_pedrosa/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="https://github.com/HenedioNeto"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2023 - atual*

</div>

### Ana Beatriz Mação de Barros Ferreira

<img className="img-equipe-foto" src="/img/equipe/ana-beatriz-macao.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/ana-beatriz-m-de-b-ferreira-44456b245"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/anamacao"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2022 - atual*

</div>

### Rebeca dos Santos Tosta

<img className="img-equipe-foto" src="/img/equipe/rebeca-tosta.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/rebeca-dos-santos-tosta-206672247"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/rtosta"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>
<a href="https://www.instagram.com/rebecasantos.tosta/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>
<a href="https://gitlab.com/r.s.tosta"> <img className="img-icon-redes" src="/img/social/gitlab.png" /> </a>

</div>


<div style={{textAlign: 'center'}}>

*2022 - atual*

</div>

## Já passaram por aqui

### Gustavo Castejon de Oliveira

<img className="img-equipe-foto" src="/img/equipe/gustavo-castejon.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/gustavo-castejon/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/gu.castejon/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

</div>


<div style={{textAlign: 'center'}}>

*2022*

</div>

### [Cintia Paulena Di Iorio](/docs/equipe#cintia-paulena-di-iório)

<img className="img-equipe-foto" src="/img/equipe/cintia-paulena.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/cíntia-di-iório-999138150/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/cintiadiiorio/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>


</div>


<div style={{textAlign: 'center'}}>

*2021 - 2022*

</div>

### [Pedro Henrique Campagna](https://labriunesp.org/docs/equipe_inativo#pedro-henrique-campagna)

<img className="img-equipe-foto" src="/img/equipe/pedro-campagna.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/pedrohcmds/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/pedrohcmds"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - 2021*

</div>


### [Júlia Silveira](/docs/equipe_inativo#júlia-silveira)

<img className="img-equipe-foto" src="/img/equipe/julia-silveira.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/julia-silveira23/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/rikamishiro"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2021 - 2022*

</div>

### [Treyce Annunciado](/docs/equipe_inativo#treyce-annunciado)

<img className="img-equipe-foto" src="/img/equipe/treyce-annunciado.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/treyce-h-r-annunciado-459680125/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="http://lattes.cnpq.br/7001164021728775"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2022*

</div>