---
id: lista
title: Lista Simplificada Projetos de Dados
sidebar_label: Lista Projetos de Dados
slug: /projetos/dados/lista
--- 

<center>
    <img src="/img/projetos/dados/logo-projetos-dados.svg" alt="centered image" />
</center>

Os projetos de dados são iniciativas que visam coletar, tratar, analisar e, quando possível, disponibilizar dados relevantes para as pesquisas em Relações Internacionais. Deste modo, neste espaço é possível encontrar projetos voltados à incorporação de tecnologias digitais para melhorar o manuseio da crescente quantidade de dados disponíveis nas redes sociais, nos meios de comunicação, nas instâncias governamentais e organismos internacionais. Para voltar para página principal dos projetos de dados, [clique aqui](https://labriunesp.org/projetos/dados/).

|Nome do Projeto de dados|Informações|Local da documentação|
|---|---|---|
|Acervo Redalint|Projeto divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/acervo-redalint/info)|
|DiáriosBR|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/diariosbr/info)|
|FullText|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/full-text/info)|
|Gov LatinAmerica|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/gov-latin-america/info)|
|Hemeroteca PEB|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/hemeroteca-peb/info)|
|IRjournalsBR|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/irjournalsbr/info)|
|MercoDocs|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/mercodocs/info)|
|NewsCloud|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/newscloud/info)|
|TweePInA|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/tweepina/info)|
|Internet e Relações Internacionais|O projeto é divulgado no site aberto. A documentação e o repositório do código estão restritos|[Local](https://labriunesp.org/docs/projetos/dados/internet-relacoes-internacionais/info)|