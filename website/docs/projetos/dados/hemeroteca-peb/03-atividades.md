---
id: atividades
title: Atividades Realizadas Hemeroteca PEB
sidebar_label: Atividades Realizadas
slug: /projetos/dados/hemeroteca-peb/atividades
---

## Atividades Realizadas

|Data|Atividades Realizadas|Participantes|
|---|---|---|
|19/06/2023|Verificação de similaridade pelo nome do arquivo pdf|Rafael, João Paulo e Thiago|
|12/06/2023|edições do arquivo “METADADOS_FINAL_copy_ajustado.json”, nas seções título_noticia e nome_arquivo_pdf com base no docs 2023-06-05-mudança_titulos_notícias_hpeb|João Paulo e Thiago|
|05/06/2023|Ajustes do código para identificação de similaridade|Rafael, João Paulo e Thiago|
|08/05/2023|Removendo arquivos repetidos nas pastas da hemeroteca|João Paulo, Rafael e Thiago|
|24/04/2023|Criação de um novo banco json apenas com as entradas selecionadas (sem as duplicatas) usando o pandas|João Paulo, Rafael e Thiago|
|17/04/2023|Seleção de duplicatas no json usando o pandas|João Paulo, Rafael e Thiago|
|03/04/2023|Identificação de duplicatas no json usando o pandas|João Paulo, Rafael e Thiago|
|20/03/2023|Organização de tarefas no gestor do gitlab, criação de código de buscas por palavras-chave no colab e ajustes no banco json da hemeroteca|João Paulo, Rafael e Thiago|
|09/02/2023|Reestruturação das pastas e arquivos do projeto|Thiago, João Paulo, Rafael|
|16/11/2022|Inserção de link no Readme e inclusão do streamlit|Thiago, João Paulo, Rafael|
|09/11/2022|Ajuste no script para fazer ocr em imagens com baixo dpi|Thiago, João Paulo, Rafael|
|26/10/2022|Início da realização do OCR em toda a base de dados json|Rafael, Thiago, João Paulo|
|19/10/2022|Preparação para realização do OCR em toda a base dados|Rafael, Thiago, João Paulo|
|28/09/2022|Vinculação das datas do banco json aos arquivos em pdf|Thiago,Rafael|
|21/09/2022|Código para vincular metadados ao xmp e às propriedades do pdf|Ana,Thiago,Rafael|
|14/09/2022|Ajustes para viabilizar o ocr e atualizar o banco json|Ana,Rafael|
|05/09/2022|OCR em português;Exclusão das pags avulsas de notícias com mais de uma pag|Ana,Rafael|
|29/08/2022|Preparação dos arquivos tifs para realização do OCR|Ana,Rafael,Thiago|
|24/08/2022|Início do ocr nos arquivos|Ana,Rafael|
|08/08/2022|Ajustes no banco json|Ana,Rafael,Thiago|
|08/08/2022|Remover sufixos dos nomes dos arquivos|Ana,Rafael,Thiago|
|04/08/2022|Retirar listas de títulos e ajustar páginas tifs faltantes|Ana,Rafael,Thiago|
|04/08/2022|Início da documentação|Ana,Rafael,Thiago|
|03/08/2022|Verificação de arquivos ausentes do banco json|Ana,Rafael,Thiago|
|28/07/2022|Mesclagem das pastas tif2 e tif3 na pasta tif|Ana,Rafael,Thiago|
|28/07/2022|Mesclagem do metadados.json com metadados02|Ana,Rafael,Thiago|
|27/07/2022|Retirada dos sufixos desnecessários dos nomes dos arquivos e títulos das notícias|Ana,Rafael,Thiago|
|27/07/2022|Padronização da sigla da Gazeta Mercantil|Ana,Rafael,Thiago|
|07/07/2022|Tentativa de inserção de metadados no banco json|Ana,Rafael,Thiago|
|06/07/2022|Estruturação do script para inserir informações do banco json a partir do nome do arquivo|Ana,Rafael,Thiago|
|30/06/2022|Revisão de arquivos duplicados do banco json|Ana,Rafael,Thiago|
|29/06/2022|Inserção dos metadados dos arquivos renomeados|Ana,Rafael,Thiago|
|23/06/2022|Continuação da renomeação manual dos arquivos|Ana,Rafael|
|22/06/2022|Renomeação manual dos arquivos sem nomes|Ana,Rafael,Thiago|
|15/06/2022|Trabalho em torno dos arquivos não inseridos no json|Ana,Rafael,Thiago|
|09/06/2022|Tratamento das datas e siglas do banco json|Ana,Rafael,Thiago|
|09/06/2022|Renomeação dos nomes dos arquivos|Ana,Rafael,Thiago|
|08/06/2022|Estrutura da renomeação dos nomes dos arquivos|Ana,Rafael,Thiago|
|08/06/2022|Mesclagem dos metadados revisados|Ana, Rafael,Thiago|
|02/06/2022|Revisão final dos metadados inseridos no banco json|Ana,Rafael,Thiago|
|01/06/2022|Início da montagem do script para renomear os nomes dos arquivos|Ana,Rafael,Thiago|
|01/06/2022|Verificação dos metadados|Ana,Rafael,Thiago|
|26/05/2022|Revisão do banco de json_mesclado|Ana, Thiago,Rafael|
|19/05/2022|Finalização do ajuste manual dos metadados|Ana,Thiago,Rafael|
|31/03/2022|Revisão das datas|Ana,Thiago e Rafael|
|29/03/2022|Revisão das datas|Ana e Rafael|
|29/03/2022|Revisão das siglas|Thiago e Rafael|
|24/03/2022|Continuação da correção: Datas, títulos,nome_jornais com problemas|Rafael,Ana e Thiago|
|22/03/2022|Correção de erros: títulos vazios, page no título e siglas com "NA"|Rafael,Ana e Thiago|
|22/03/2022|Revisão das datas erradas|Rafael,Ana e Thiago|
|17/03/2022|Montagem do banco com metadados normalizados separados dos não normalizados|Rafael,Ana,Thiago|
|17/03/2022|Verificação do tratamento das siglas | Rafael, Thiago, Ana|
|15/03/2022| Tratamento das siglas dos jornais|Rafael, Thiago, Ana|
|10/03/2022|Análise de como resolver os problemas dos metadados (data, sigla e titulo)|Rafael, Thiago|
|08/03/2022|Finalização do tratamento de encode incorreto|Rafael, Ana e Thiago|
|03/03/2022| Tratamento das informções do banco de dados json | Rafael, Thiago, Ana|
|24/02/2022| Início da verificação dos dados do banco json | Rafael, Thiago, Ana|
|22/02/2022|Finalização da primeira versão do banco json|Rafael, Thiago, Ana|
|17/02/2022|Estruturando inserção de uma entrada no banco para cada arquivo com mais de uma página|Rafael, Thiago e Ana Julia|
|15/02/2022|Inicio da inserção dos metadados no banco json|Ana Julia, Rafael, Thiago|
|10/02/2022|Início da realização do OCR|Rafael e Thiago|
|03/02/2022|Finalização da organização dos arquivos (pastas apenas com tif).|Ana,Rafael,Thiago|
|01/02/2022|Conversão pdfs em tifs.|Ana,Rafael,Thiago|
|27/01/2022|Finalização da mesclagem dos arquivos;Início da conversão dos pdfs em tif.|Ana,Rafael,Thiago|
|25/01/2022|Mesclando os arquivos digitalizados na época da Regiane e da época Ricardo/Kelly|Ana, Rafael,Thiago|
|20/01/2022|Início do tratamento dos arquivos da hemeroteca - Definindo estruturas do diretório|Ana,Rafael,Thiago|
|18/01/2022|Ajustes no ambiente virtual| Ana,Rafael|
|07/10/2021|Configuração do ambiente local (MacOs)|Ana,Rafael,Thiago|
|05/10/2021|Configuração do ambiente local (windows)|Ana,Rafael,Thiago|
|30/09/2021|Introdução e revisão sobre aspectos relacionados ao ambiente virtual (anaconda), Git/Gitlab|Ana,Rafael,Thiago|
|28/09/2021|Introdução ao python, estrutura básica de script, trabalhando com diretórios e criação do gitignore|Ana,Rafael,Thiago|
|23/09/2021 |Início a renomeação dos arquivos|Ana,Rafael,Thiago|
|21/09/2021|Utilização de branchs, tags e versionamento semântico|Ana,Rafael,Thiago|
|16/09/2021|Utilização do Markdown e do Git|Ana,Rafael,Thiago|
|14/09/2021|Criação de página de gestão de projetos no Notion|Ana,Rafael,Thiago|
|09/09/2021|Criação do repositório e do ambiente virtual do projeto|Ana,Rafael,Thiago|

## Próximas Atividades Gerais
- [ ] OCR nos arquivos (gerar pdf pesquisável)
  - [ ] ocrmypdf
  - [ ] layoutparser
  - [ ] OpenCV/Pillow
    - [Deep Learning based Super Resolution with OpenCV](https://towardsdatascience.com/deep-learning-based-super-resolution-with-opencv-4fd736678066)
    - [OpenCV Super Resolution with Deep Learning](https://www.pyimagesearch.com/2020/11/09/opencv-super-resolution-with-deep-learning/)

- [ ] Unir arquivos pdfs com mais de uma página

- [ ] Vincular metadados aos arquivos (xmp-pikepdf)

- [ ] Vincular metadados aos arquivos (data,autor e título)

- [ ] substituir arquivos da hemeroteca

- [ ] Analise de dados (pandas)

- [ ] buscar substituir arquivos pdf por html

- [ ] aprendizado de máquina


## Próximas Atividades Específicas

- 