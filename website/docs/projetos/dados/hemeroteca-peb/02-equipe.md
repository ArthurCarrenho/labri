---
id: equipe
title: Equipe da Hemeroteca PEB
sidebar_label: Equipe
slug: /projetos/dados/hemeroteca-peb/equipe
---

Abaixo são indicadas as pessoas que contribuem com o projeto

## Coordernadores do projeto

### Tullo Vigevani

<img className="img-equipe-foto" src="/img/equipe/tullo-vigevani.png"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/8414328955232709"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br/citations?user=JMLdYQIAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

<a href="https://orcid.org/0000-0001-6698-8291"> <img className="img-icon-redes" src="/img/social/orcid.png" /> </a>

</div>

### Shiguenoli Miyamoto

<img className="img-equipe-foto" src="/img/equipe/shiguenoli.png"/>


<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/2256167799437118"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br/citations?user=6LIzaZkAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

<a href="http://www.linkedin.com/in/shiguenoli-miyamoto-80500624"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

### [Marcelo Mariano](https://labriunesp.org/docs/equipe#marcelo-passini-mariano)

<img className="img-equipe-foto" src="/img/equipe/marcelo.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/3505849964932313"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>


## Colaboradores do projeto

### [Rafael de Almeida](https://labriunesp.org/docs/equipe#rafael-de-almeida)

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2021 - atual*

</div>

### Thiago Fernandes

<img className="img-equipe-foto" src="/img/equipe/thiago-fernandes.png"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/7777193801520984"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>
<a href="https://www.linkedin.com//"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2021 - atual*

</div>


## Já passaram por aqui


### Ana Motta

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>
<a href="https://www.linkedin.com//"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>



<div style={{textAlign: 'center'}}>

*2021 - 2022*

</div>
