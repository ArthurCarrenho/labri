---
id: intro
title: Documentação Hemeroteca PEB
sidebar_label: Introdução
slug: projetos/dados/hemeroteca-peb/infos/intro
---

## Introdução - Repositório

A documentação referente ao projeto da Hemeroteca de Política Externa Brasileira pode ser acessada através do repositório gitlab indicado abaixo. Nele estão disponíveis os dados e metadados json a respeito de todas as noticias listadas, as bibliotecas e programas que permitiram sua listagem e o acesso ao público, além da página Readme do projeto que contém maiores informações sobre o desenvolvimento deste.

Para acessar o *repositório do projeto*, [clique aqui](https://gitlab.com/unesp-labri/projeto/hemeroteca-peb)
