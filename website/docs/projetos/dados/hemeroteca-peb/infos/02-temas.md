---
id: temas
title: Documentação Hemeroteca PEB
sidebar_label: Temas
slug: projetos/dados/hemeroteca-peb/infos/temas
---

## Temas da Hemeroteca

Esta hemeroteca foi dividida em 12 temas:

|Tema|Nome da pasta|
|---|---|
|África|`01-brasil-africa`|
|América Latina|`02-brasil-america_latina`|
|Argentina|`03-brasil-argentina`|
|Ásia|`04-brasil-asia`|
|Diálogo Subnacional|`04-brasil-dialogo_subnacional`|
|Economia Internacional|`05-brasil-economia_internacional`|
|EUA|`06-brasil-eua`|
|Europa|`07-brasil-europa`|
|Direitos Autorais|`09-brasil-informatica_e_direitos_autorais`|
|Meio Ambiente|`10-brasil-meio_ambiente`|
|Oriente Médio|`11-brasil-oriente_medio`|
|Política Exterior|`12-brasil-politica_exterior`|