---
id: equipslabri
title: Equipamentos LabRI/UNESP
sidebar_label: Equipamentos LabRI/UNESP
slug: /projetos/sistemas/infralabriunesp/equipslabri
---

### Descrição do Parque de Equipamentos científicos da(s) Instituição(ões) Sede

Os equipamentos listados abaixo estão disponíveis e alocados no espaço do Laboratório de Relações Internacionais (LabRI/UNESP) do Departamento de Relações Internacionais da UNESP/Franca. Eles compõe um cluster computacional, baseado no PROXMOX - plataforma open source de virtualização. Ademais, destacamos, em um item à parte, o storage alocado neste cluster.


- **PVE 01**
  - AMD FX 8320E OCA CORE Black Edition
  - ASUS M5A78L-M PLUS/USB3
  - 8GB x4 DDR3 Kingston HyperX Fury 1866Mhz
  - HD - 8TB x2
  - SSD - 120GB + 240GB

- **PVE 02**
  - Intel CORE i7-3770
  - ASUS P8Z77-M
  - 8GB x4 DDR3 Corsair XMS3 1600MHz
  - HD 8TB x2
  - SSD - 240GB x2
  - NVME M2 - 120GB

- **PVE 03**
  - Servidor HP ML30
  - Intel Xeon E3-1220 V5 3.00 GHz
  - 8GB x2 DDR4 2400MHZ
  - HD 1TB x4
  - SSD - 240GB
  - NVME M2 - 120GB

- **PVE 04**
  - Itautec SM 3321
  - AMD Phenom 9650 Quad-Core
  - 4GB x4 DDR2 800Mhz
  - HD 1TB x4
  - SSD - 240GB + 120GB

- **PVE 05**
  - Servidor IBM X3100M4
  - Intel Xeon E3-1220 V2 3.10 GHz
  - 4GB x2 DDR3 Kingston, 1600MHZ ECC 
  - 8GB x2 DDR3 Kingston, 1600MHZ ECC
  - HD 1TB x4
  - SSD - 240GB + 120GB

- **PVE 06**
  - Servidor IBM X3500M4
  - Intel Xeon E5-2620 V2 2.10 GHz
  - 8GB x4 
  - HD 3TB x2
  - HD SAS 300GB x2
  - SSD - 120GB + 480GBx4

- **Storage/Armazenamento**
  - HD SATA: 50 Terabytes
  - HD SAS: 600 Gigabytes
  - SSD SATA: 1920 Gigabytes

-----

- 12 computadores Itautec
- 9 computadores HP all in One
- 5 Webcam Logitech Full HD 1080p
- 1 Web Logitech HD 720p
- 8 HeadSet C3 Tech
- 4 Headset DAZ Bluetooth
- 1 Scanner - Avision A3 Flatbed Scanner FB5000
- 1 Scanner - Plustek OpticBook 3800
- 1 Scanner - Plustek SmartOffice PS286 Plus
- 1 Scanner - HP Laser Jet M1522nf

[Informações em docs](https://docs.google.com/document/d/12Vhb2GjE3CdLrBVTCHRad4wsgo4QAcsIzB8RfnURw3w/edit)

## Material de Apoio

- [How a months-old AMD microcode bug destroyed my weekend [UPDATED]](https://archive.is/4lo5X)
