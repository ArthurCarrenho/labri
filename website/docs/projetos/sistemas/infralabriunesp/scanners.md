---
id: scanners
title: Scanners
sidebar_label: Scanners
slug: /projetos/sistemas/infralabriunesp/scanners
---

Para saber mais sobre a utilização dos scanners do LabRI/UNESP, visite a nossa [página de instruções](https://labriunesp.org/docs/projetos/ensino/processamento-imagens/intro) para pós-processamento de imagens!