---
id: itautec
title: Itautec
sidebar_label: Itautec
slug: /projetos/sistemas/infralabriunesp/bios/itautec
---


```
Base Board Information
        Manufacturer: Itautec S.A.
        Product Name: SM 3321

```


### Como acessar a BIOS

1. Ao ligar o computador, aperte consecutivamente a tecla DEL.

### Como modificar a prioridade de boot

1. Na BIOS, com as setas do teclado, vá em "Boot" no menu superior.

![itautec1](/img/projetos/sistemas/itautec1.jpeg)

2. No menu "Boot", entre em "Hard Disk Drives" e, com a tecla de "+", coloque o pendrive em primeiro.

![itautec2](/img/projetos/sistemas/itautec2.jpeg)

3. Agora, dê "Esc" para voltar ao menu "Boot" e entre em "Boot Device Priority". Selecione a opção "Hard Disk".

![itautec3](/img/projetos/sistemas/itautec3.jpeg)

4. Pressione F10 para salvar, sair da BIOS e iniciar o computador pelo pendrive.

### Como habilitar Wake On Lan

1. Na BIOS, vá no menu Advanced > Onboard Device Configuration. Habilite a opção "Onboard LAN"

![itautec4](/img/projetos/sistemas/itautec4.jpeg)

### Como habilitar Power On

1. Na BIOS, vá no menu Power >  APM Configuration

![itautec5](/img/projetos/sistemas/itautec5.jpeg)

2. Deixe a opção "Restore on AC Power Loss" em "Power On"

![itautec6](/img/projetos/sistemas/itautec6.jpeg)

### Como habilitar virtualização

1. Vá no menu Advanced > CPU Configuration e deixe "AMD Virtualization" em "Enable"

![itautec7](/img/projetos/sistemas/itautec7.jpeg)