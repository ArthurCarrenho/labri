---
id: ibm
title: Servidores IBM
sidebar_label: Servidores IBM
slug: /projetos/sistemas/infralabriunesp/bios/ibm
---



```
IBM x3100 M4
Base Board Information
        Manufacturer: IBM
        Product Name: 81Y7071
        Version: SVT-R


IBM x3500 M4
Base Board Information
        Manufacturer: IBM
        Product Name: 00AL034


```


## IBM x3500 M4

### Como acessar a BIOS

1. Plugue o pendrive com o sistema operacional desejado no computador. Tire da tomada e coloque novamente e o computador deverá ligar automaticamente. Nessa tela, aperte F1 para acessar o Setup.

![ibm1](/img/projetos/sistemas/ibm1.jpeg)

### Como modificar prioridade de boot

2. Com as setas do teclado, vá em "Boot Manager" e dê Enter. Logo depois, em "Boot from File" e selecione o pendrive plugado.

![ibm2](/img/projetos/sistemas/ibm2.jpeg)

### Como habilitar Wake On Lan

### Como habilitar Power On

### Como habilitar Virtualização

## IBM x3100 M4

### Como acessar a BIOS

1. Como o IBM x3500 M4: Plugue o pendrive com o sistema operacional desejado no computador. Tire da tomada e coloque novamente e o computador deverá ligar automaticamente. Nessa tela, aperte F1 para acessar o Setup.

### Como modificar prioridade de boot

2. Agora, o processo é parecido com uma ligeira diferença: Com as setas do teclado, vá em "Start Options", acima de "Boot Manager" e selecione "USB Storage". 

![ibm3](/img/projetos/sistemas/ibm3.jpeg)

