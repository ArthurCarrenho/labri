---
id: dell
title: DELL
sidebar_label: DELL
slug: /projetos/sistemas/infralabriunesp/bios/dell
---

- Para acessar a BIOS dos PCs Dell do LabRI/UNESP, ligue o computador e aperte consecutivamente a tecla F2.


```
System Information
        Manufacturer: Dell Inc.
        Product Name: OptiPlex 3000

```


### Como modificar a prioridade de boot

1. Com o computador desligado, plugue o pendrive. Ligue-o e acesse a BIOS.

2. Clique em Boot Configuration. Na lista, clique nas setas até o pendrive ser a primeira opção.
![bios1](/img/projetos/sistemas/bios1.jpeg)

3. Clique em "Apply Changes" e "Exit". O computador irá iniciar pelo pendrive.

### Como habilitar Power On

1. No menu lateral da BIOS, clique em "Power"

![menu-lateral-dell](/img/projetos/sistemas/Menu-lateral-dell.jpeg)

2. Selecione a opção "Power On"

![power-on-dell](/img/projetos/sistemas/power-on-dell.jpeg)

### Como habilitar Wake On Lan

1. No menu lateral da BIOS, clique em ""

2. Selecione a opção "Lan or WLAN"

![lan-dell](/img/projetos/sistemas/lan-dell.jpeg)

### Como habilitar virtualização

1. No menu lateral, clique em "Virtualization Support".

2. Habilite a primeira opção para "on"

![virtualization-dell](/img/projetos/sistemas/virtualization-dell.jpeg)