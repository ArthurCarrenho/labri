---
id: 121
title:  HPE ProLiant ML30 Gen9
sidebar_label: HP ML30 GEN9
slug: /projetos/sistemas/infralabriunesp/bios/121
---



```
Base Board Information
        Manufacturer: HP
        Product Name: ProLiant ML30 Gen9

```

A BIOS foi atualizada para [U23_2.82_04_04_2019](https://support.hpe.com/connect/s/softwaredetails?softwareId=MTX_ee67dcd89ef74e2da195fdff53)

Mensagem do boot do server:

- [[Firmware Bug]: The BIOS Has Corrupted Hw-PMU Resources](https://support.hpe.com/hpesc/public/docDisplay?docId=emr_na-c03265132)


Documentação oficial:

- [HPE ProLiant Gen9 Servers - Download Drivers, Firmware, Software, Utilities, Documentation and Guides](https://support.hpe.com/hpesc/public/docDisplay?docId=sf000044077en_us&docLocale=en_US)



## Como acessar a BIOS

1. Ao ligar o computador, pressione F11 somente uma vez e mantenha pressionado.

## Como modificar prioridade de boot

1. No menu, com as setas do teclado, dê Enter em "Boot Menu".

![121](/img/projetos/sistemas/121.jpeg)

2. Selecione o pendrive previamente plugado.