---
id: ryzen
title: RYZEN
sidebar_label: RYZEN
slug: /projetos/sistemas/infralabriunesp/bios/ryzen
---


```

Base Board Information
        Manufacturer: ASUSTeK COMPUTER INC.
        Product Name: TUF GAMING X570-PLUS_BR

```

### Como acessar a BIOS

1. Os computadores 21 e 22 acessam a BIOS pela tecla F2 ou pela tecla DEL. Ao ligar, aperte consecutivamente uma dessas teclas.

### Como modificar prioridade de boot

1. Na BIOS, no menu lateral inferior direito intitulado "Boot Priority", clique em "Boot Menu". Ou simplesmente aperte F8.

![ryzen1](/img/projetos/sistemas/ryzen1.jpeg)

2. Na tela de Boot Menu, clique no USB plugado. O computador irá realizar a inicialização pelo pendrive.

![ryzen2](/img/projetos/sistemas/ryzen2.jpeg)

### Como habilitar Power On/Wake On Lan

1. Na BIOS, no menu superior clique em Advanced Mode > APM Configuration

![ryzen3](/img/projetos/sistemas/ryzen3.jpeg)

2. Coloque a opção "Power On By PCI-E" em "Enable"

### Como habilitar Virtualização

1. Na BIOS, clique em Advanced Mode > CPU Configuration

![ryzen4](/img/projetos/sistemas/ryzen4.jpeg)

2. Coloque a opção "SVM Mode" em "Enable"

![ryzen5](/img/projetos/sistemas/ryzen5.jpeg)