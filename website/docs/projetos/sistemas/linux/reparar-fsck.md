---
id: reparar-fsck
title: Como Reparar o Disco em Linux com FSCK e Recuperar Arquivos Linux
sidebar_label: Como Reparar o Disco em Linux com FSCK e Recuperar Arquivos Linux
slug: /projetos/sistemas/linux/reparar-fsck
---

### Material de Apoio

- [Como Reparar o Disco em Linux com FSCK e Recuperar Arquivos Linux](https://recoverit.wondershare.com.br/harddrive-tips/repair-linux-disk.html)
- [FSCK](https://guialinux.uniriotec.br/fsck/)