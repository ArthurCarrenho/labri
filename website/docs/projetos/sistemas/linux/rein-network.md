---
id: rein-network
title: Como reiniciar Network Interface no Linux
sidebar_label: Como reiniciar Network Interface no Linux
slug: /projetos/sistemas/linux/rein-network
---

### Material de apoio

- [How to Restart Network Interface in Linux](https://www.cyberciti.biz/faq/linux-restart-network-interface/)