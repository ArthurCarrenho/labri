---
id: lock-screen
title: Como desabilitar a opção de lock screen para cada usuário
sidebar_label: Como desabilitar a opção de lock screen para cada usuário
slug: /projetos/sistemas/linux/lock-screen
---

### Material de Apoio

- [Como desabilitar a opção de lock screen para cada usuário](https://ubuntuforums.org/showthread.php?t=2254441)