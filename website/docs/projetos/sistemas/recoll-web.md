---
id: recollweb
title: Documentação RecollWeb
sidebar_label: RecollWeb
slug: /projetos/sistemas/linux/recoll-web
---

## Informações gerais

* Este RecollWeb foi instalado em uma VM baseada no turnkey/debian8 e seguiu as instruções das seguintes páginas:
  * [Github​ ​ do​ ​ recollWebUI​](https://github.com/koniu/recoll-webui)
  * [Algumas​ ​ instruções​ ​ que​ ​ podem​ ​ auxiliar​ ​ na​ ​ instalação​ ​ do​ ​ recollWebUI](http://www.lesbonscomptes.com/recoll/pages/recoll-webui-install-wsgi.htm)


* Todo conteúdo (pastas, subpastas e arquivos) colocado dentro do diretório /lantri/ACERVO_DIGITAL será automaticamente indexado e disponibilizado para pesquisa na interface web
* O grupo "org" esta vinculado a pasta "ACERVO_DIGITAL". Todos os usuários que fazem parte do grupo "org" podem manipular os arquivos da pasta "ACERVO_DIGITAL".
* Para  o material indexado ficar disponivel para busca via interface web não pode haver espaços nos nomes das pastas, subpastas e arquivos (utilize o shell script indicado abaixo para retirar os espaços dos arquivos de maneira automática)


### [Software e Serviços](http://tarefas.lantri.org/projects/lantri-39/wiki/Software_e_Servi%C3%A7os#2-Discos-R%C3%ADgidos-Virtuais)

**PASSO 1** Converter .vhd para .qcow2

**PASSO 2** Criar Máquina Virtual (VM) no Proxmox

**PASSO 3** Transferir arquivo convertido (qcow2)para o diretório que contem a máquina criada

```
/var/lib/vz/images/XXX
scp lantri-adm@lantrivm01.lantri.org:/home/lantri-adm/Downloads/2019-08-12-Hemeroteca.qcow2 .
```

**PASSO 4** Sobrescrever a máquina transferida em cima na VM criada no Proxmox

**PASSO 5** configurar rede e ligar vm no proxmox

### Ajuste no virtual host

*1. Renomear o arquivo /etc/apache2/sites-available/mudar.dominio.org.conf*

```
sudo mv /etc/apache2/sites-available/mudar.dominio.org.conf /etc/apache2/sites-available/nome_do_novo.domínio.org.conf
```

*2. Modificar informações do arquivo /etc/apache2/sites-available/mudar.dominio.org.conf*

```
ServerName mudar.dominio.org 
``` 
<!-- RedirectPermanent / http://mudar.dominio.org/recoll # esta linha deve ser comentada -->

<!-- # <Directory /var/www/recoll-webui-master>
                        #AuthType Basic
                        #AuthName "Area Restrita"
                        #AuthUserFile /etc/apache2/.htpasswd
                        #Require valid-user
                        WSGIProcessGroup recoll
                        Order allow,deny
                        allow from all
#                       Satisfy Any # se esta linha estiver comentada o apache pedirá o login e senha
#  </Directory>
-->



*3. Reiniciar o apache2*

```
sudo /etc/init.d/apache2 restart

ou

sudo service apache2 reload

```

----

## Ajustar index.html

1. Entrar no arquivo abaixo e trocar o nome do dominio (manter o/recoll)

```
sudo nano /var/www/recoll-webui-master/index.html
```

----

## Desabilitar e habillitar domínios no apache2

*1. Desabilitar o domino antigo (caso seja necessário)*

```
sudo a2dissite mudar.dominio.org.conf
```

*2. Habilitar o domino novo*

```
sudo a2ensite nome_do_novo.domínio.org.conf
```


*3. Reinicie o Apache 2*

```
sudo /etc/init.d/apache2 restart

ou

sudo service apache2 reload

```

----

## Ajustar o arquivo apache2.conf

1.  Entrar no arquivo abaixo:

```
sudo nano /etc/apache2/apache2.conf
```

*2. Para Desabilitar a listagem de diretórios na web procure dentro o arquivo indicado acima a parte que trata dos diretórios e acrescente "Options -Indexes":*


*3. Para ocultar  a versão do Apache no servidor acrescente no arquivo apache2.conf as seguintes linhas*

```
# Ocultar versao do apache
ServerTokens ProductOnly
ServerSignature Off
```

* Fonte: [Ocultar versão do apache no servidor](https://archive.is/fsTtK)

*4. Reiniciar o apache2*

```
sudo /etc/init.d/apache2 restart

ou

sudo service apache2 reload

```

----

## Editar layout da página

*1. Ocultar os campos "settings" e "Dates"*

* Recomendamos comentar as linhas 12, 27 e 28 do layout na pagina de busca (html). Para os usuários não alterarem as conficurações (campo "settings") e por que no campo "Date" se refere a data de insersão do arquivo no recoll e não a data de publicação do texto (isso pode gerar confusão nos usuários)

* É possivel traduzir o layout do mecanismo de busca para o português alterando os termos desta página.

*2. Para realizar as alterações indicadas acima basta editar o arquivo search.tpl:*

```
sudo nano /var/www/recoll-webui-master/views/search.tpl
```

*3. Comentar linhas referentes aos "Settings" (linha 12) e ao "Dates" (linhas 27 e 28)*

```
<!-- comment DATES

        <b>Dates</b> <small class="gray">YYYY[-MM][-DD]</small><br>
        <input name="after" value="{{query['after']}}" autocomplete="off"> &mdash; <input name="before" value="{{query['before']}}" autocomplete="off">
-->

E

<!-- comment  SETTINGS
        <a href="settings" tabindex="-1"><input type="button" value="Settings"></a>

-->

```

*4. Reiniciar o apache2*

```
sudo /etc/init.d/apache2 restart

ou

sudo service apache2 reload

```

Fonte: https://github.com/koniu/recoll-webui/issues/58

----

## Gerenciamento de usuários para autenticação no Apache2

* A autenticação no apache2 somente funcionará caso a linha "Satisfy Any" não esteja habilitada no virtualhost (ver item ajuste do virtualhost)

*1. Criar um novo usuário*

```
htpasswd /etc/apache2/.htpasswd nome_usuário
```


*2. Remover usuário*

```
htpasswd -D /etc/apache2/.htpasswd nome_usuário
```

----

## Habilitar certificado SSL


*1. Instalar o cliente Let's Encrypt*

É necessário desabilitar/comentar a seguinte linha do virtualhost na instalação e configuração do SSL (após a instalação esta linha pode ser descomentada): WSGIDaemonProcess. Ver: https://archive.is/wnuJv

```
echo 'deb http://ftp.debian.org/debian jessie-backports main' | sudo tee /etc/apt/sources.list.d/backports.list
```

```
sudo apt-get update
```

```
sudo apt-get install python-certbot-apache -t jessie-backports
```

*2. Configurar certificado SSL*


*3. Renovação automática*
```
sudo crontab -e
```

```
30 2 * * 1 /usr/bin/certbot renew >> /var/log/le-renew.log
```

Fonte: https://archive.is/ASDjJ

----

## RecollWeb (interface nova - agile)

*para mudar dominio*

```
 find /var/www -type f -exec sed -i 's/dominio.atual.br/dominio.novo.org/g' {} \;
```

*Incluir pasta para consulta*

```
/var/www/recoll-webui-master/views/consulta.tpl
```

*Incluir pasta para consulta*

*Entre datas - Comentar as linhas abaixo*

```
<!--  <span class="showHidden" data-target="#EntreDadas">Entre Datas</span> -->
```

```
<!--

<div class="col col-12 col-pc-4 hidden pt40" id="EntreDadas">
         Datas
         Depois de<input name="after" value="" autocomplete="off" placeholder="YYYY[-MM][-DD]">
         Até <input name="before" value="" autocomplete="off" placeholder="YYYY[-MM][-DD]">
</div>


-->
```

*Trocar o logo de parceiros*

* Deve ser feito em cada uma das páginas que aparecem as logos equipe:

```
/var/www/recoll-webui-master/views/footer.tpl​
/var/www/recoll-webui-master/views/instituicoesenvolvidas.tpl​
```
