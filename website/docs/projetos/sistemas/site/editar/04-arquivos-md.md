---
id: md
title: Criar arquivos .md
sidebar_label: Arquivo.md
slug: /projetos/sistemas/site/editar/md
---

O LabRI/UNESP foi construido com [Docusaurus](https://docusaurus.io), um gerador de site-estático. É uma ferramenta voltada para facilitar o gerenciamento e criação de documentação para um web-site. Caso queira saber mais sobre o Docusaurus, [clique aqui](https://docusaurus.io/pt-BR/blog/2017/12/14/introducing-docusaurus)

------

## Docs

- A ferramenta *'docs'* auxilia o usuario a organizar arquivos Markdown de modo hierarquico, utilizando a estrutura

1. Paginas individuais
2. Barras laterais
3. Versões
4. Instâncias de plug-in

Para mais informações, [clique aqui](https://docusaurus.io/docs/docs-introduction)

------

```
example.com/                                -> generated from `src/pages/index.js`

example.com/docs/intro                      -> generated from `docs/intro.md`
example.com/docs/tutorial-basics/...        -> generated from `docs/tutorial-basics/...`
...

example.com/blog/2021/08/26/welcome         -> generated from `blog/2021-08-26-welcome/index.md`
example.com/blog/2021/08/01/mdx-blog-post   -> generated from `blog/2021-08-01-mdx-blog-post.mdx`
```

------

## Criar um arquivo doc (.md)

- Crie o arquivo desejado na pasta *website>docs*, ele deverá utilizar a extensão *.md*
- Se atente para o local onde este arquivo será criado, a pasta **docs** possui um grande numero de subpastas (cadernos, equipe, geral, newsletter e projetos)
- Utilize as *doc tags* para categorizar as informações do seu arquivos. 
- Para mais informações, clique aqui[clique aqui](https://docusaurus.io/docs/create-doc)

------
```
---
id: doc-with-tags
title: A doc with tags
sidebar-label: Doc with tags
slug: tags
---
```
------

- Todo documento *doc* possui um **id** único, ou seja o nome do documento, relativo a **raiz do diretório** *docs*

- **Title** indica o título principal da página
- **Sidebar-label** indica o nome da página no menu lateral
- **Slug** indica a URL do documento. Possui um padrão de definição mas pode ser alterada. Para saber mais [clique aqui](https://docusaurus.io/docs/create-doc#doc-urls)

------