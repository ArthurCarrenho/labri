---
id: static
title: Informações Estáticas
sidebar_label: Informações Estáticas
slug: /projetos/sistemas/site/editar/static
---

Esta página tem como objetivo indicar onde se localizam as informações estáticas do site LabRI/UNESP (imagens, pdfs e afins)

------

## Static

- A pasta *website>static* contem duas subpastas: **arquivos** e **img**. A primeira se refere aos *PDFs* e a segunda as *imagens* utilizadas no site do Labri/UNESP

- Para adicionar um **PDF** a pasta "website>static>arquivos" siga o padrão *DATA-LABRI-NÚMERO DO CADERNO.pdf* como indicado na imagem a seguir

- A data descrita no nome do arquivo deve seguir o formato ANO-MÊS-DIA

![arquivos](/img/projetos/sistemas/web-redes/readme19.jpg)

- Para adicionar **imagens** (logos, fotos e ícones) entre na pasta "website>static>img" e se atente para as subpastas

![img](/img/projetos/sistemas/web-redes/readme20.jpg)

- As fotos da equipe de colaboradores e estagiários do LabRI/UNESP estão localizadas na pasta "equipe" sempre seguindo o padrão *NOME-SOBRENOME.jpg*

- É importante se atentar para a proporção das fotos da equipe (quadrado de lados iguais) para evitar que a imagem fique distorcida. Geralmente o tamanho das imagens utilizadas é 600x600 pixels, no formato jpg ou png

- Caso queira alterar as imagens da equipe de um grupo de extensão ou de pesquisa específico procure a **subpasta correspondente** no local *static>img*

- Para alterar logos e ícones do site do LabRI utilize o formato padrão **.svg** ou **.png** de imagens com o *fundo transparente*

- Os ícones das redes sociais estão localizados em *static>img>social*

------