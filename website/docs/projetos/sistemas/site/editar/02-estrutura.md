---
id: estrutura
title: Estrutura do site
sidebar_label: Estrutura
slug: /projetos/sistemas/site/editar/estrutura
---

Esta página indica como as **pastas** do site do LabRI/UNESP estão estruturados.

-----

```bash
website
├── docs (.md)
│   └── cadernos
│   └── equipe
│   └── geral
│   └── newsletter
│   └── projetos
│       └── dados
│       └── ensino
│       └── extensão
│       └── sistemas
├── cadernos
├── src
│   └── css
│   └── themes
│   │   └── customizaçao de cardenos (blog)
│   └── pages
│       └── home (index.js)
│       └── sobre (sobre.js)
│       └── grupos de extensão
│       └── grupos de pesquisa
│       └── styles.module.css
├── static
│   └── img
├── docusaurus.config.js
└── sidebar.js
```