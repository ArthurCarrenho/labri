---
id: fapesp
title: Relatório técnico para a FAPESP
sidebar_label: Relatório técnico para a FAPESP
slug: /projetos/sistemas/infraipriunesp/fapesp
---

O presente relatório técnico visa indicar os resultados alcançados através do financiamento de “Auxílio à Pesquisa - Reserva Técnica para Infra-estrutura Institucional de Pesquisa” (Processo: 19/06704-6) em relação à infraestrutura computacional da Sala de Situação e Pesquisa Avançada que serve aos pesquisadores do IPPRI/UNESP. 

Os três objetivos apontados na proposta inicial do pedido de auxílio foram alcançados. Abaixo são assinalados tais objetivos e os principais impactos que trouxeram no cotidiano das atividades de pesquisa na instituição. Ademais também é apontado o desenho final da referida sala que foi constituída a partir deste financiamento.

O primeiro objetivo era “Instalar a infraestrutura computacional básica de virtualização (Cluster Proxmox) de todos os serviços que serão utilizados no PPGRI, atendendo às necessidades de pesquisa dos pós-graduandos, professores e grupos de pesquisa”.

Foi realizada a compra de equipamentos novos e atualização em dois servidores doados que necessitavam de upgrade para que se integrassem à infraestrutura computacional planejada. Ao final da aquisição temos os seguintes equipamentos disponíveis:


- 3 Estações Multifuncionais de Trabalho (cada estação tem a configuração descrita a seguir).
  - Processador AMD Ryzen 9 3900x; 
  - Placa Mãe Asus Prime B450M-GAMING/BR; 
  - Memória RAM 4x16GB; 
  - Fonte de energia (650W);
  - Gabinete;
  - Teclado/mouse;
  - Duas Estações tem placas de vídeo GeForce GTX 750 Ti 2GB GDDR5 128bits - Bluecase; e uma estação terá uma placa de vídeo Asus Geforce GTX 1650 OC 4GB GDDR5, Phoenix. Estas placas de vídeo viabilizam a integração com os painéis de visualização indicados abaixo;
  - Os Hard Disk Drives (HDDs) e os Solid State Drives (SSDs) alocados nestas estações multifuncionais compõe o Storage Compartilhado da infraestrutura computacional construída (para detalhes ver item Storage/Armazenamento indicado abaixo);

- 2 Servidores instalados em sala refrigerada.
  - Atualização do Servidor I (HP ML150 Gen9):
    - 2x16 GB DDR4 memória RAM; 
    - gavetas para disco rígido;
    - 2 adaptador de gavetas para SSD;

  - Atualização do Servidor II (Servidor HP DL180 Gen9):
    - 1x16 GB DDR4 memória RAM;
    - 5 gavetas para disco rígido; 
    - 2 adaptador de gavetas para SSD;
    - Os Hard Disk Drives (HDDs) e os Solid State Drives (SSDs) alocados nestas estações multifuncionais compõe o Storage Compartilhado da infraestrutura computacional construída (para detalhes ver item Storage/Armazenamento indicado abaixo);

  - Painéis de Visualização e Videoconferência:
    - 1 TV 43'' LED UHD 4k Samsung 43RU7100
    - 2 Monitores LG LED 29 Ultrawide, IPS, HDMI, FreeSync - 29WK500
    - 2 WebCam Logitech C920
    - Estes painéis estão integrados as três estações multifuncionais de trabalho;



  - Storage/Armazenamento de 45 Terabytes:
    - HD: 42 terabytes
    - SSD (SATA e NVMe M.2): 3 terabytes

- 3 NoBreak de 720VA 120V:
  - Estão integrados as três estações multifuncionais de trabalho;

- 3 headset Logitech H390

*O segundo objetivo era “Prover acesso local e remoto a infraestrutura computacional instalada”*. Com a infraestrutura instalada e descrita acima se viabiliza a implementação  de um cluster computacional, baseado no sistema de virtualização Proxmox. Basicamente, um cluster é um sistema em que dois ou mais computadores (nodes) estão interconectados e realizam atividades conjuntas podendo fornecer maior capacidade de processamento, de armazenamento, alta disponibilidade de serviços entre outros aspectos que um computador isolado não suportaria. No caso específico da sala de situação e pesquisa avançada as três estações multifuncionais de trabalho, os dois servidores e mais uma máquina pré-existente são os nodes que formam o Cluster Computacional do IPPRI/UNESP. A partir disso uma série de possibilidades de suporte a pesquisa se abrem, inicialmente, o cluster está voltado para:

- Fornecer condições adequadas para a implementação da sala de situação;
- Fornecer aos pesquisadores uma Estação de Trabalho Virtual que pode ser acessada local ou remotamente e que provenha uma melhor capacidade de processamento e armazenamento de dados das pesquisas;
- Disponibilizar Acervos de Pesquisa (ver: https://hemerotecapeb.lantri.org;)
- Integração com o cluster do LabRI/UNESP localizado no campus da UNESP/Franca;

*O terceiro objetivo era “Estimular o uso contínuo de novas tecnologias de pesquisa na área de ciências humanas e sociais”*. Os pontos elencados acima viabilizados pelo Cluster IPPRI/UNESP possibilitam aos pesquisadores melhores recursos para incorporar ferramentas tecnológicas no seu cotidiano de trabalho. Abaixo indicamos alguns resultados desta melhor disponibilidade de recursos tecnológicos ao pesquisador. Grande parte dos pontos apontados a seguir estão sendo realizados em atividades conjuntas com o LabRI/UNESP.

- Coleta de dados de perfis públicos nas redes Sociais;
- Coleta de Bibliografia Acadêmica;
- Coleta de dados em sites governamentais e de instituições internacionais;
- Viabilização do tratamento de textos, em uma quantidade relativamente grande, que necessitam de um pós-processamento (ajustes dos arquivos e Optical Character Recognition - OCR);
- Formação de bases de dados e indexação integral de documentos, inicialmente,  através do Recoll Desktop Search;
- Limpeza e estruturação de dados;
- Melhoria na integração  dos trabalhos realizados na rede internacional de pesquisa Development  International Politics and Peace (DIPP) vinculada ao CAPPES/PRINT/UNESP.


Abaixo colocamos o desenho final da sala de situação pesquisa avançada:

![sala](/img/projetos/sistemas/sala.png)

[Informações em docs](https://docs.google.com/document/d/1gEEQGRWy6xD1-UpPemb-gLnFM098ZouTlzCeYmAH5PM/edit#heading=h.4gron4gegyak)