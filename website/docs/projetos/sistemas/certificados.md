---
id: certificados
title: Tutorial
sidebar_label: Tutorial
slug: /projetos/sistemas/certificados
---

## Como gerar certificados automaticamente

1. No processo serão usados o [Google Apresentações](https://www.google.com/intl/pt-BR/slides/about/), [Google Formulários](https://www.google.com/intl/pt-BR/forms/about/) e o [Google Planilhas](https://www.google.com/intl/pt-BR/sheets/about/)
1. O Apresentações será usado para criar o _template_ do certificado, onde você irá inserir os elementos gráficos e as informações gerais. 
1. Através do Google Formulários, você irá coletar as informações como e-mail, nome e etc., que irão diretamente para a planilha que servirá como base de dados para a geração de certificados
1. No Planilhas, é importante ressaltar que utilizaremos uma ferramenta (complemento) chamada [Autocrat](https://workspace.google.com/u/0/marketplace/app/autocrat/539341275670?hl=pt&pann=sheets_addon_widget), que fará a integração da planilha com o arquivo do Google Apresentações.


### Passo a passo

1. Abra o Google Apresentações e crie um template básico que servirá para o Certificado.
- Note que aqui colocamos algumas variáveis, como `<<nome>>`, `<<nome_do_evento>>`, `<<data>>` e `<<ch>>`. É importante que essas variáveis estejam entre os sinais de `<<>>`, pois é assim que o Autocrat irá "ler" as informações da planilha e convertê-las para o certificado.
- Guarde o local onde você salvou esse certificado, recomenda-se que crie uma pasta no Google Drive exclusiva para o processo como um todo.

![certificado1](/img/projetos/sistemas/certificados/certificado1.png)

2. Agora, usando o Formulários, crie um formulário padrão, onde você irá requisitar as informações dos participantes/de quem vai ter seus dados inseridos no certificado.
- O mais importante nesta parte é solicitar o e-mail, através das configurações → coletar e-mail.
- Na aba de respostas, selecione a opção para criar uma planilha, tentando coloca-la na mesma pasta do drive que você criou o formulário e a apresentação/certificado.

![Formularios](https://i.imgur.com/QiFMzft.png)

3. Em seguida, trabalharemos com a Planilha. Preste atenção nessa etapa! Aqui, na primeira linha (row), você notará que ela estará pré-preenchida com alguns dos dados que você solicitou no formulário. Se você não utilizou um formulário e quer gerar certificados de uma planilha criada por você mesmo, adicione as informações requeridas na primeira linha da mesma forma.

- **IMPORTANTE**: Na primeira linha, escreva as informações exatamente iguais as que no certificado modelo estão colocadas entre `<<>>`. Por exemplo, se no certificado modelo está escrito `<<nome>>`, na primeira linha escreva "nome", sem os sinais. E assim com todas as informações. Não precisam estar em uma ordem exata de colunas.
- Na primeira linha podem existir mais informações além daquelas colocadas no certificado. Lembre-se de adicionar uma coluna para e-mail também na primeira linha. Ela não entrará no certificado mas servirá para o envio automático deles.

![certificado2](/img/projetos/sistemas/certificados/certificado2.png)

4. A seguir, utilizaremos o serviço do Autocrat. Na planilha, vá em Extensões > Complementos > Instalar complementos e procure por Autocrat.
- Para abrir o Autocrat vá em Extensões > Autocrat > Open. Clique em "New Job" e escolha um nome para o processo. Pode ser um nome genérico, ele não será contabilizado no certificado.
- Na etapa 2, selecione no Google Drive o modelo de certificado que você criou anteriormente.

![certificado3](/img/projetos/sistemas/certificados/certificado3.png)

- A etapa 3 é onde as informações do certificado serão conectadas às informações da planilha. Em "Merge tab", coloque página 1, em "Header row" coloque 1 e "First data row" coloque 2. Coloque as informações da coluna de acordo com as informações do certificado.

![certificado4](/img/projetos/sistemas/certificados/certificado4.png)

- Na etapa 4, escolha o nome do arquivo do certificado, uma dica é utilizar o campo nome da planilha, sendo "Certificado - `<<nome>>`". Dessa forma, todos os certificados serão salvos com o nome de cada participante do evento.
- Em Type escolha PDF e selecione a opção "Multiple output mode (classic mode)" 

![certificado5](/img/projetos/sistemas/certificados/certificado5.png)

- Na etapa 5, selecione a pasta onde os certificados salvos serão gerados.
- A etapa 6 e 7 podem ser desviadas. São opcionais.

- A etapa 8 serve para caso você queira enviar os certificados para os participantes automaticamente após serem gerados. Se não, na opção de "Share doc" apenas selecione "No". Se sim, selecione "Yes" e preencha a caixa de email com os emaisl dos participantes, assunto e corpo do texto.

![certificado6](/img/projetos/sistemas/certificados/certificado6.png)

- Na etapa 9 selecione "No" em ambais as caixas e clique em "SAVE"

- Agora, para gerar os certificados é só clicar no botão de "play" e os certificados serão gerados na pasta escolhida! Não altere a planilha até o término do processo.