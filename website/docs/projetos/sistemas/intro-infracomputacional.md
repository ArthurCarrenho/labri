---
id: intro-infracomputacional
title: Introdução
sidebar_label: Introdução
slug: /projetos/sistemas/intro-infracomputacional
---

:::tip
O conteúdo desta seção reúne configurações e comandos úteis que podem auxiliar a admistração do uso dos computadores do LabRI/UNESP e do IPPRI/UNESP.
:::