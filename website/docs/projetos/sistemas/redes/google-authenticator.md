---
id: google-authenticator
title: Google Authenticator
sidebar_label: Google Authenticator
slug: /projetos/sistemas/redes/google-authenticator
---



# Material de Apoio

- [google-authenticator-libpam](https://github.com/google/google-authenticator-libpam)
- [Autenticação Euler Documentation](https://euler.cemeai.icmc.usp.br/documentacao/autenticacao) 
- [Setup multi-factor authentication for SSH in Linux](https://ostechnix.com/setup-multi-factor-authentication-for-ssh-in-linux/)
- [Setting up multi-factor authentication on Linux systems](https://www.redhat.com/sysadmin/mfa-linux)
- [How to use two-factor authentication with sudo and SSH on Linux with Google Authenticator](https://www.vultr.com/docs/how-to-use-two-factor-authentication-with-sudo-and-ssh-on-linux-with-google-authenticator/) 
- [Two-factor authentication for SSH key on Ubuntu](https://www.linuxbabe.com/ubuntu/two-factor-authentication-ssh-key-ubuntu) 


# Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto?author=rafaelrdealmeida)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/acesso-remoto)
