---
id: tutorial-gif
title: Tutorial Como fazer tutorial em gif
sidebar_label: Tutorial como fazer tutorial em gif
slug: geral/gestao/comunicacao/tutorial-gif
---

## Tutorial: Como fazer Tutorial em Gif 

### Sites e extensões utilizadas

1. [Screencastify](https://chrome.google.com/webstore/detail/screencastify-screen-vide/mmeijimgabbpbgpdklnllpncmdofkcpn): Extensão do google utilizada para gravar a tela e transformar em gif;

2. [Ezgif](https://ezgif.com/): Plataforma online de edição de Gifs;

3. [Imgur](https://imgur.com/): Plataforma online para publicar imagens/gifs.

### Como gravar a tela e gerar o Gif 

1. Instale a extensão Screencastify no Google Chrome;

2. Abra a extensão e configure as permissões necessárias;

3. Entre na tela/guia que deseja gravar e clique no logo da extensão no canto superior direito da tela;

4. Clique em "Browser tab" ou "Desktop" e pressione "Record" para começar a gravar;

![img print](https://i.imgur.com/zceCeot.png)

5. Grave automaticamente o que deseja e assim que terminar clique em "Pausar" e você será transferido para a visualização do vídeo;

6. Faça os ajustes necessários do tamanho do gif (cortar a duração) e clique para "Exportar como Gif".

![Tutorial 1](https://i.imgur.com/lZ6EKXM.gif)

### Como editar o Gif 

1. Abra o site Ezgif; 

2. Selecione a opção "Redimencionar" e faça o upload do gif; 

3. Clique na aba "cortar" e redimencione o gif da forma que desejar; 

4. Clique em "cortar" e depois no logo de "Salvar". 

![Tutorial 2](http://i.imgur.com/N2Dx5Jsh.gif)

### Como fazer o upload do Gif 

1. Entre no site Imgur;

2. Clique em 

1. Entre no site do Imgur e clique em "Criar novo post";

2. Faça o upload do gif; 

3. Clique em "Get share links";

4. Selecione e copie a opção "BBCode";

5. Escreva "![Nome](link copiado)" e apenas exclua "[img] e [/img]" do link copiado. 

![Tutorial 3](https://i.imgur.com/n9HVvD9.gif)

## Tutorial: Como fazer Tutorial em Gif no Ubuntu

### Ferramentas utilizadas

1. [Ubuntu](https://ubuntu.com/): Sistema operacional utilizado em todos os computadores do LabRI
2. [Ezgif](https://ezgif.com/): Plataforma online de edição de Gifs;
3. [Imgur](https://imgur.com/): Plataforma online para publicar imagens/gifs.

### Como gravar a tela e gerar o Gif

1. Pressione a tecla ```Print Screen``` ou inicie ```Captura de tela``` a partir da visão geral das atividades no menu iniciar
2. Clique no ícone de **filmadora** para alternar entre captura de tela e gravação de tela
3. Clique no botão do **cursor** caso queira inclui-lo na gravação de tela.
4. Escolha entre *Seleção ou Tela.*

![Seleção ou Tela](https://i.imgur.com/ILqq8EF.png)

Em Seleção você gravará uma parte da tela da sua preferência. Para fazer isso. clique e arraste a área desejada para a gravação de tela usando as alças ou o ponteiro de mira. Em Tela, você gravará a tela inteira. 

5. Para ambos os casos, clique no botão redondo vermelho para começar a gravar o que está na sua tela.
6. Um indicador vermelho é exibido no canto superior direito da tela durante a gravação, mostrando os segundos decorridos. Clique em cima para parar a gravação

![Status da Gravação](https://i.imgur.com/Zpd5dq3.png)

### Como editar o Gif (Ferramentas úteis)

1. Abra o site [*Ezgif*](https://ezgif.com/)
2. Se necessário, use uma das ferramentas que o site disponibiliza:
    ### *Cut Video*
        
    i. Faça o upload do arquivo de vídeo do seu dispositivo ou cole a URL.
        
    ii. Selecione os tempos de início e término. Você pode fazer isso manualmente ou iniciar o player de vídeo e usar o botão de ```current position``` enquanto o vídeo está sendo reproduzido. Também é possível pausar o vídeo e definir a posição atual a partir daí.
        
    iii. Clique no botão "Cortar o Vídeo!" para concluir.

    ### *Crop Video*
        
    i. Recorte as dimensões de arquivos de vídeo, removendo partes desnecessárias do vídeo.
        
    ii. Você pode utilizar a ferramenta de recorte para selecionar a parte que deseja cortar sobre a visualização do vídeo ou inserir manualmente as dimensões desejadas. 
        
    iii. Também é possível fixar a proporção de aspecto para um dos presets, tornando o vídeo quadrado, 4:3, 16:9, 3:2, e muitos outros.
        
    iv. **Aviso: às vezes, a pré-visualização do vídeo pode ter qualidade reduzida, mas isso não afetará a saída.**
        
    ### *Video to GIF* 
        
    i. Com este conversor de vídeo online, você pode fazer o upload de seus arquivos de vídeo em formatos como mp4, avi, WebM, flv, wmv e muitos outros tipos populares de vídeo e mídia, transformando-os em GIFs animados de alta qualidade.
        
    ii. A taxa de quadros (fps) é o número de quadros exibidos a cada segundo. Uma taxa de quadros mais alta proporciona uma animação mais suave e cinematográfica, aumentando assim a qualidade percebida, mas também aumenta significativamente o tamanho do arquivo. Escolha de acordo com suas necessidades.
        
    iii. Para manter o tamanho do arquivo e o tempo de processamento razoáveis, o site limita o comprimento máximo da parte que você pode selecionar para conversão (duração), dependendo da taxa de quadros escolhida. Se você deseja criar GIFs mais longos, é necessário selecionar uma taxa de quadros mais baixa. O comprimento máximo a 5 fps é de 60 segundos; a 10 fps, é reduzido para 30 segundos, e assim por diante.

### Como fazer o upload do Gif 

1. Entre no site Imgur;

2. Entre no site do Imgur e clique em "Criar novo post";

3. Faça o upload do gif; 

4. Clique em "Get share links";

4. Selecione e copie a opção "BBCode";

5. Escreva "![Nome](link copiado)" e apenas exclua "[img] e [/img]" do link copiado. 