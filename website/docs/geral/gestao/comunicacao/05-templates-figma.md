---
id: templates-figma
title: Como utilizar os templates Figma
sidebar_label: Como utilizar os templates Figma
slug: geral/gestao/comunicacao/templates-figma
---

## Templates LabRI-UNESP

Os [templates LabRI-UNESP](https://www.figma.com/file/J20M066rszJ6o93X7fJUps/Templates-LABRI-UNESP?node-id=17%3A4) encontram-se disponíveis para todos os colaboradores do laboratório. É recomendado que todos os posts nas redes sociais do LabRI (Facebook, Twitter, Instagram e Linkedin) sejam formulados a partir destes templates, para que todas as redes sigam uma _padronização visual_. 

Saiba como utilizar os templates:

## TUTORIAL (GIFS): Estilizando o template no FIGMA

### PASSO 1: Ambiente de edição

Acesse o link dos Templates LabRI-UNESP e vá para a página "[Template Instagram/...](https://www.figma.com/file/J20M066rszJ6o93X7fJUps/?node-id=0%3A1)". 

Em seguida, copie o template da Rede Social desejada: 

![passo1-a](https://i.imgur.com/yNXb94p.gif)

Após copiar, vá para a página "[Utilizar template](https://www.figma.com/file/J20M066rszJ6o93X7fJUps/?node-id=17%3A198)" e cole o template escolhido:

![passo1-b](https://i.imgur.com/QhvwNeY.gif)

### PASSO 2: Modificando informações 

Modifique o nome do documento para que o mesmo fique com o nome da sua postagem:

![passo2-a](https://i.imgur.com/vdOLMKr.gif)

Para navegar pelo conteúdo do template, é possível 1) selecionar clicando **duas vezes** na parte que deseja modificar; 2) encontrar a informação na **barra lateral**. 

![passo2-b](https://i.imgur.com/1IaBzLQ.gif)

### PASSO 3: Modificando textos

Para modificar **qualquer** texto do template, basta clicar **duas** vezes no texto da imagem: 
 
![passo3-a](https://i.imgur.com/Q1txYR8.gif)

### PASSO 4: Modificando imagens 

Para modificar **qualquer** imagem do template, abra o arquivo da imagem local: 

![passo4-a](https://i.imgur.com/o2OmR5W.gif)