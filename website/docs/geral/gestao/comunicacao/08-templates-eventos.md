---
id: templates-eventos
title: Templates para eventos e publicações
sidebar_label: Templates para eventos e publicações
slug: geral/gestao/comunicacao/templates-eventos
---

Nesta seção encontra-se tutoriais para edição dos templates de eventos e publicações no Instagram disponibilizados pelo LabRI/UNESP.

:::info

Acesse aqui os templates do LabRI: 
1. [Template para Eventos](https://www.canva.com/design/DAFBKZVyhDs/mXd6bvQRQV0FU8sxkINKmw/edit?utm_content=DAFBKZVyhDs&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton) 

2. [Template para Publicações no Instagram](https://www.canva.com/design/DAFCkuHGrzE/QsZ8nAlt_MKko9d66nvqUg/edit?utm_content=DAFCkuHGrzE&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton)

:::

---

## Como utilizar os templates do LabRI

:::tip

É necessário possuir uma conta do [Canva](https://www.canva.com) para seguir este tutorial

:::

### Passo 1: Acesse o template 

Após entrar em sua conta do Canva, acesse o link do template desejado (Eventos ou Publicações no Instagram)

### Passo 2: Duplicar o template

Localize o template que deseja editar e faça uma cópia a partir do botão "Duplicar página"

![Clicando no botão "Duplicar página"](https://i.imgur.com/KeQxLDY.gif)

### Passo 3: Modificar textos

Para modificar os textos basta dar um duplo click na caixa de texto 

![Modificando as caixas de texto](https://i.imgur.com/o5VbGt6.gif)

### Passo 4: Modificar imagens 

1. Faça upload da imagem 

2. Insira a imagem desejada no quadro de imagem

![Inserindo imagem no quadro de imagem](https://i.imgur.com/G8eO4CW.gif)

### Passo 5: Fazendo dowload do template

1. Clique no botão "Compartilhar" > "Baixar"

2. Selecione o formato desejado (jpg, png, svg)

3. Selecione a página que foi editar 

4. Clique no botão "Baixar"

![Baixando arquivo](https://i.imgur.com/IBCyM51.gif)

### Bônus: Trancando o template

Para evitar que o template seja editado por outra pessoa mas ainda assim continuar disponível na página de edição dos templates: 

1. Arraste o arquivo para a última página do documento 

2. Clique na opção "Bloquear página"

![Bloqueando página](https://i.imgur.com/XOOnjQS.gif)

## Como utilizar o Carbon 

Para poder expor linhas de códigos nas postagens do Instagram é necessário utilizar o site [Carbon](https://carbon.now.sh) e seguir as configurações utilizadas a seguir: 

### Passo 1: Arrumando as cores do layout

Para que todos os posts permaneçam padronizados, ajuste o Carbon para seguir as mesmas cores e configurações das outras postagens do site. 

Sendo assim, é necessário ajustar para que a configuração esteja em "Night Owl" e "Python" (ou qualquer linguagem que esteja sendo utilizada no código). 

![arrumando o layout](https://i.imgur.com/paN23ik.gif)

### Passo 2: Ajustes finais do layout 

É necessário retirar a opção de "sombra" da imagem e tornar o fundo transparente. 

![ajustes finais](https://i.imgur.com/eOuBmc2.gif)

### Passo 3: Download da imagem 

Para fazer o download da imagem é necessário ir em "Export", selecionar a opção "1x" e salvar em "PNG". 

![download da imagem](https://i.imgur.com/EYUcV4C.gif)

Agora é só utilizar a imagem salva na postagem desejada. 