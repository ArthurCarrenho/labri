---
id: estagio
title: Oportunidade de Estágio
sidebar_label: Estágio
slug: /geral/info/estagio
---

### Oportunidades de Estágio

:::danger ATUALMENTE, A VAGA DE ESTÁGIO REMUNERADO ESTÁ PREENCHIDA
[PARA MAIS DETALHES CLIQUE NESTE LINK](/docs/geral/info/processo-seletivo)
:::

O Laboratório de Relações Internacionais da UNESP (LabRI/UNESP) oferece uma vaga de estágio remunerado via edital. Quando o processo seletivo para esta vaga esta aberto divulgamos no sistema interno da UNESP, em nossas redes sociais, em [nosso site](/docs/geral/info/processo-seletivo) e também no site do [Departamento de Relações Internacionais (DERI)](https://www.franca.unesp.br/#!/selecaoestagiariolabri) da Unesp-Franca. 

A pessoa que ocupa a vaga remunerada pode permanecer no estágio por até dois anos.

Também incentivamos o [Estágio Voluntário](/docs/geral/info/estagio-voluntario). Acreditamos que essa oportunidade tem grandes qualidades de agregar conhecimento pessoal e profissional para quem está em busca de novos competências e habilidades. Esta forma de estágio poderá ser desenvolvida de duas maneiras: a primeira nos moldes das atividades desenvolvidas pelo estagiário remunerado; a segunda é o desenvolvimento de um conjunto de atividades visando um projeto específico.

Os estagiários voluntários que quiserem concorrer ao Estágio Remunerado terão um bônus de pontuação ao concorrerem ao edital (os detalhes serão apresentados no mesmo).

Mais informações sobre o Estágio no LabRI, entre em contato conosco pelo nosso e-mail: unesplabri@gmail.com

