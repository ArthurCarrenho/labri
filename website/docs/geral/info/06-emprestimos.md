---
id: emprestimos
title: Empréstimos de materiais
sidebar_label: Emprestimo de materiais
slug: /geral/info/emprestimos
---

Alguns equipamentos do LabRI/UNESP estão disponíveis para empréstimo. Figuram entre eles câmeras, microfones, tripés, headsets, caixas de som, entre outros.

Para realizar os empréstimos o estudante deve realizar o pedido pelo formulário abaixo. Seus dados então serão colocados na planilha de empréstimos e o estagiário será responsável pela entrega e pela devolução do material, assim como a atualização da planilha. A foto deverá ser tirada no LabRI/UNESP pelo requisitante ou pelo estagiário

- [Formulário](https://forms.gle/tdXPGtbFwSh1YDV46)
