---
id: estagio-voluntario
title: Seja um estagiário no LabRI/UNESP
sidebar_label: Estágio voluntário
slug: /geral/info/estagio-voluntario
---

Como estagiário voluntário você pode colaborar em diversas frentes, sendo elas:

  ✅ Marketing e comunicação

  ✅ Infraestrutura computacional

  ✅ Gestão de Dados

  ✅ Desenvolvimento de sistemas

  ✅ Produção de material didático

  ✅ Suporte a usuários

# Estágio voluntário de fluxo contínuo

O processo seletivo para estagiário voluntário é de fluxo contínuo, ou seja, **em qualquer período do ano o estudante pode pleitear uma vaga de estágio voluntário.** Para isso, acesse o edital abaixo:

:::tip
[Clique aqui para acessar o EDITAL e demais informações](https://docs.google.com/document/d/1xnc1quJGluvND5I4ulZ9wc3NVrepeScg7Wi-sY5AuSc/edit)
:::

# Chamadas de estágio voluntário

Além do estágio voluntário de fluxo contínuo indicado acima, o LabRI/UNESP também pode realizar alguma chamada específica para selecionar estudantes interessados em colaborar com as atividades realizadas. Abaixo são listadas as chamadas finalizadas e as abertas.

## Chamadas abertas

No momento não há chamadas de estágio voluntário abertas. Caso você tenha interesse em realizar estágio no LabRI/UNESP, se inscreva no estágio voluntário de fluxo contínuo indicado acima.

## Chamadas fechadas

### Chamada de gestão de mídias sociais

- [EDITAL PARA ESTÁGIO VOLUNTÁRIO EM GESTÃO DE MÍDIAS SOCIAIS](https://docs.google.com/document/d/1-2oKen_aUTpdfbCwoE5LAfOvpMZxBnroB_07P8tmef0/edit?usp=sharing)

- [RESULTADO DA CHAMADA](https://docs.google.com/document/d/1Wy2uSuDFQ64mfpY3UzmLLA9ibb7S7jaACoanmmI7hK4/edit?usp=sharing)