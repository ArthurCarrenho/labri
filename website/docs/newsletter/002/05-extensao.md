---
id: extensao
title: Projetos de Extensão
sidebar_label: Projetos de Extensão
slug: /newsletter/002/extensao
---


Os projetos de extensão têm por objetivo promover uma melhoria e o incentivo entre o meio acadêmico e a sociedade. Aqui, é possível localizar projetos envolvendo docentes e discentes dedicados a atividades que promovam uma divulgação científica.

Como destaque, pode-se citar: Pod-RI, Conhecer para Acolher e Cidades Saudáveis e Sustentáveis.
