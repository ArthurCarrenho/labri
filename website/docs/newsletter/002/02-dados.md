---
id: dados
title: Projetos de Dados
sidebar_label: Projetos de Dados
slug: /newsletter/002/dados
---


Os projetos de dados tem como intuito a coleta, tratamento e análise de dados relevantes para as pesquisas em Relações Internacionais. Os projetos dessa seção são voltados à incorporação de tecnologias digitais a fim de melhorar o manuseio dos dados disponíveis nas redes sociais, nos meios de comunicação, instâncias governamentais e organismos internacionais. 

Atualmente, existem 7 projetos de dados sendo executados, sendo estes: Hemeroteca PEB, NewsCloud, Diários BR, Tudo em Texto, Tweenpina, IRJournalsBR e Gov LatinAmerica. A seguir, traremos um panorama geral do que foi realizado nas últimas semanas em cada um dos projetos mencionados.

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
  <TabItem value="hemeroteca" label="Hemeroteca PEB" default>
  </TabItem>
  <TabItem value="newscloud" label="NewsCloud" default>
  </TabItem>
  <TabItem value="diáriosbr" label="DiáriosBR" default>
  </TabItem>
  <TabItem value="tudoemtexto" label="Tudo em Texto" default>
  </TabItem>
  <TabItem value="tweenpina" label="Tweenpina" default>
  </TabItem>
  <TabItem value="IRjournalsbr" label="IRJournalsBR" default>
  </TabItem>
  <TabItem value="govlatinamerica" label="Gov LatinAmerica" default>
  </TabItem>
</Tabs>