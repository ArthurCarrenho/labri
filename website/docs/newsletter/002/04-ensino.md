---
id: ensino
title: Projetos de Ensino
sidebar_label: Projetos de Ensino
slug: /newsletter/002/ensino
---


Os projetos de ensino se referem àqueles que pretendem promover oficinas, materiais de apoio e cursos para incentivar e auxiliar uma melhor incorporação das ferramentas tecnológicas nas atividades de pesquisa. Aqui, encontramos projetos que promovam um melhor intercâmbio entre as Relações Internacionais e a ciência de dados.

Os projetos de destaque são: trilha de dados, material bibliográfico, tecnologias digitais e curso de governança da internet.
