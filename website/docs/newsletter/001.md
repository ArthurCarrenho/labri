---
id: '001'
title: Newsletter LabRI/UNESP
sidebar_label: Newsletter 01
slug: /newsletter/001
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
  <TabItem value="dados" label="Dados" default>
   Os projetos de dados tem como intuito a coleta, tratamento e análise de dados relevantes para as pesquisas em Relações Internacionais. Os projetos dessa seção são voltados à incorporação de tecnologias digitais a fim de melhorar o manuseio dos dados disponíveis nas redes sociais, nos meios de comunicação, instâncias governamentais e organismos internacionais.
   Atualmente, existem 7 projetos de dados sendo executados, sendo estes: Hemeroteca PEB, NewsCloud, Diários BR, Tudo em Texto, Tweenpina, IRJournalsBR e Gov LatinAmerica. A seguir, traremos um panorama geral do que foi realizado nas últimas semanas em cada um dos projetos mencionados.

   ## NewsCloud

1. teste
2. teste2

   ## Tweepina

1. teste
2. teste2

   

  </TabItem>
  <TabItem value="sistemas" label="Sistemas">
    Os projetos de sistema tem como objetivo promover uma melhor utilização de recursos computacionais, a implementação e o desenvolvimento de software e serviços voltados para as pesquisas na área de Relações Internacionais. Aqui, os projetos estão relacionados com a construção e a manutenção de estações de trabalho multifuncionais, além de softwares e serviços utilizados e/ou mantidos pelo próprio laboratório.
    Os projetos de sistemas que se encontram ativos neste momento são: edição do site do LabRI/UNESP, cadernos LabRI, sistema de OCR automático e a infraestrutura computacional.




    


  </TabItem>
  <TabItem value="extensao" label="Extensão">
    This is a banana 🍌
  </TabItem>
  <TabItem value="ensino" label="Ensino">
    This is a banana 🍌
  </TabItem>
</Tabs>