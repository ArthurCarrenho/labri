---
slug: eventos-002
tags: [Dipp - Eventos]
title: "Regionalismo e blocos regionais: desafios atuais"
author: rafaelrdeameida
tipo_publicacao: "Eventos DIPP - 002"
---

<div align="center">

![](https://i.imgur.com/vmuwNzI.jpg)

</div>


>*O Minicurso “Regionalismo e blocos regionais: desafios atuais” foi laborado a partir do PPGRI San Tiago Dantas (UNESP, UNICAMP, PUC-SP) em parceria com a Rede DIPP, a Rede de Pesquisa em Política Externa e Regionalismo (REPRI) e o Observatório de Regionalismo (ODR)...*

<!--truncate-->

O Minicurso “Regionalismo e blocos regionais: desafios atuais” foi laborado a partir do PPGRI San Tiago Dantas (UNESP, UNICAMP, PUC-SP) em parceria com a Rede DIPP, a Rede de Pesquisa em Política Externa e Regionalismo (REPRI) e o Observatório de Regionalismo (ODR) sob coordenação das professoras Regiane Nitsch Bressan (UNIFESP) e Karina Lilia Pasquariello Mariano (UNESP).

O objetivo do Minicurso foi analisar e debater os processos de integração regional em vigência com especialistas, discutindo seus desafios atuais e perspectivas futuras. O Minicurso ocorreu via Google Meet, de 14/10 a 09/12 (às Quartas-Feiras) das 14h30 às 17h30.