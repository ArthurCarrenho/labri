Bem-vindo, estagiário(a)! Essa página tem como objetivo auxiliar você que está começando a entender melhor como funciona a edição do site.

# Website

O site do LabRI é construído com a plataforma [Docusaurus 2](https://v2.docusaurus.io/), um moderno gerador de websites.

# Passo a passo para edição do site

- Ao acessar o Gitlab, clique no menu representado por três traços na horizontal no canto superior esquerdo, clicar em: Groups > View all groups > Unesp Labri

![Groups](/img/readme1.jpg/)

![Unesp Labri](/img/readme2.jpg/)


- Na nova página, na aba “Subgroups and projects”, clicar em: sites > labri
- No menu em horizontal central da página, procurar por “Web IDE” com uma seta para baixo ao lado. Ao clicar na seta, o estagiário irá perceber que existe a opção de escolha entre “Web IDE” E “Gitpod”. Ambos são diferentes formas de edição do site. O estagiário escolhe uma delas e clica em cima da escolha.

![Editores](/img/readme3.jpg/)

- Deve ser levado em consideração que o Web IDE é uma forma de edição mais simples e generalizada, enquanto o Gitpod é para edições mais completas.

-----
- Ao **acessar Web IDE**, no menu esquerdo, clique na pasta “website”. Nela, está localizado todo o conteúdo do site do LabRI, escolha uma das pastas e abra as pastas internas para editar. Ao final da edição, clique em “Create commit...”, depois, selecione a opção "Commit to main branch" e clique em "Commit" novamente.  Em alguns minutos, o site oficial será editado.

![Commit1](/img/readme4.jpg/)

![Commit2](/img/readme5.jpg/)

-----
- Ao **acessar Gitpod**, no código localizado na metade inferior da tela na aba “Terminal”, vá até a última linha do código, dê um espaço do *$* e digite “cd website”. Após isso, vá até a nova última linha e escreva “ls”. Depois, na nova última linha, digite “yarn install $$ yarn start”. Aguarde o carregamento.

![Gitpod1](/img/readme6.jpg/)

- Com isso, foi criado um site provisório, como um “site rascunho”. O intuito é que o estagiário consiga editar esse “site rascunho” e poder visualizar o resultado antes de modificar o site original. O acesso e as eventuais modificações podem ser verificadas clicando no link http://localhost:3000/, que foi criado no código apertando Ctrl + Click.
(Editar imagem depois)

- Agora, no menu esquerdo, clique em “website”. Os novos menus abertos são as páginas do site. Para editar, abra um dos menus abertos e escolha a página.
- Ao final da edição, para que o progresso seja salvo, digite:

```
git add . && git commit -m 'inserir a mensagem aqui'
git pull origin main && git push origin main
```

- Dica: É interessante o estagiário salvar o seu progresso aos poucos e não deixar para o final da edição, para que não corra o risco de parte do processo ser perdido em eventuais bugs do computador.


## Markdown
**O que é Markdown?**

Markdown é a linguagem utilizada para a formatação de textos. Por exemplo, em programas como o Microsoft Word, você aperta um botão e o texto fica em negrito. No markdown, você adiciona caracteres específicos no texto para formatá-lo. Por exemplo, para deixar uma palavra em negrito, coloque dois asteriscos entre o texto.
É possível utilizar aplicativos de edição para markdown, mas é importante que se entenda o processo como um todo e como utilizá-lo manualmente.

**Por quê usar markdown?**

O markdown foi criado para uso em websites, portanto pode ser usado em conjunto com diversas linguagens de programação diferentes. O markdown é bem amplo, possui códigos simples e alguns mais complexos. O uso cotidiano facilita o manuseio.

### Por exemplo:

```
**Olá!**, *Clique em uma opção para continuar:* 
- [Opção 1](link da opção 1)
- [Opção 2](link da opção 2)
```
O texto acima está formatado com o markdown, o resultado dele seria esse:

**Olá!**, *Clque em uma opção para continuar:*
- [Opção 1](link)
- [Opção 2](link)


- Para se aprofundar sobre o conceito do markdown, [clique aqui](https://www.markdownguide.org/getting-started/).
- Para visualizar os códigos mais básicos, [clique aqui](https://www.markdownguide.org/basic-syntax/).
- Para visualizar códigos mais complexos, [clique aqui](https://www.markdownguide.org/extended-syntax/).
- As vezes, pode acontecer do markdown não ser o suficiente para uma formatação que você deseja. Nesse caso, [clique aqui](https://www.markdownguide.org/hacks/) para visualizar alguns códigos em HTML para auxiliá-lo nessas ocasiões.
- Caso você esteja procurando aplicativos para editar markdown ou queira saber quais programas aceitam essa linguagem, [clique aqui](https://www.markdownguide.org/tools/).
- O site do LabRI é criado no Docusaurus, para visualizar o uso do markdown especificamente no Docusaurus, [clique aqui](https://docusaurus.io/docs/markdown-features).

# Material de Apoio

## Centralizando imagens
- [Center a hyperlink or an image in Docusaurus](https://stackoverflow.com/questions/57788506/center-a-hyperlink-or-an-image-in-docusaurus)



