---
slug: labri-n0032
tags: [COVID-19, CADERNOS]
title: "Saúde Global e as relações Internacionais"
subtitulo: "Reflexões sobre o tema"
author: Gabriela Fideles Silva
author_title: Graduanda de Relações Internacionais na UNESP – Campus de Franca/SP. Bolsista Fapesp. Membro do Projeto de Extensão Universitária - “Cidades Saudáveis e Sustentáveis”; e do Projeto do Laboratório de Relações Internacionais (LabRI) - As Relações Internacionais e o Novo Coronavírus.
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 32
image_url: https://i.imgur.com/7n8doYF.jpg
descricao: Diante da pandemia do novo coronavírus (COVID-19), a Saúde Global ganhou um espaço de destaque nas agendas de Estados e Organizações Internacionais. Por muito tempo, o tema de saúde foi negligenciado pelo sistema internacional...
download_pdf: /arquivos/2020-07-08-labri-032.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/7n8doYF.jpg)

</div>

>*Diante da pandemia do novo coronavírus (COVID-19), a Saúde Global ganhou um espaço de destaque nas agendas de Estados e Organizações Internacionais...*

<!--truncate-->

Diante da pandemia do novo coronavírus (COVID-19), a Saúde Global ganhou um espaço de destaque nas agendas de Estados e Organizações Internacionais. Por muito tempo, o tema de saúde foi negligenciado pelo sistema internacional, deixou-se de discutir projetos globais de proteção e fortalecimento dos sistemas de saúde, desprezando-se o bem estar da população mundial.

Pretende-se, neste texto, trazer uma visão introdutória ao tema dos estudos de Saúde Global e demonstrar a importância dessa temática não apenas em cenários de pandemias. A atual situação da saúde do mundo mostra a necessidade de se manter constante a troca de informações e colaboração na área de saúde em escala global, além da obrigatoriedade dos Estados de refletirem sobre as dificuldades e obstáculos para o desenvolvimento da qualidade de vida e bem-estar de todos.

Discutir a atual pandemia vai além de analisar somente a crise sanitária e de saúde pública. Dentro dos estudos da Saúde Global existem elementos fundamentais das Relações Internacionais como as disputas e assimetrias de poder dos Estados nacionais, quais são os interesses por trás das decisões tomadas, qual é a agenda da saúde global, quais atores nacionais e transnacionais estão envolvidos. A determinação do rumo das políticas de saúde e quais os debates que são levantados no cenário internacional passam pela dinâmica do sistema mundial e precisam ser avaliadas dentro do contexto da pandemia.

Falar de Saúde Global é debater sobre diplomacia e cooperação em saúde, é discutir sobre saúde pública, distribuição de doenças e inequidades, além do acesso universal à atenção em saúde. Este tema engloba reconhecer quais são os atores e agendas que permeiam o debate e a governança em saúde. Existe uma necessidade cada vez maior do diálogo de problemas transnacionais de ordem médica e sanitária, interligando os diferentes setores para discussão de soluções para comunidade global como um todo.

Segundo Fortes e Ribeiro (2014), a evolução do termo saúde internacional para o de Saúde Global é marcado pela globalização. O seu advento é marcado pela constante troca de mercadorias e grande circulação de pessoas entre países de forma cada vez mais rápida, em um fluxo cada vez maior, oferecendo um risco constante de epidemias globais. A globalização trouxe tanto benefícios quanto impactos, como a interdependência planetária que trazem riscos para a saúde humana de uma forma diferenciada para os Estados, ou seja, tem-se uma distribuição desigual das doenças e agravos da saúde ao redor do mundo.

Tanto dentro do debate que se fazia na saúde internacional, quanto no debate da saúde global, desde 1970, os “contextos econômico, social e político são determinantes das condições de saúde dos indivíduos e populações” (ALMEIDA, 2018, p.02). Além disso, as relações de poder, nas mais diferentes formas, permeiam os processos decisórios e as formulações de políticas de saúde. A ampliação do conceito da saúde internacional, que é a saúde organizada apenas entre os países, para o termo saúde global teve o objetivo de se pensar em todos os temas que envolvem a governança de saúde e todos os atores envolvidos, além de Estados, tais como Organizações Internacionais, filantrópicas internacionais, indústrias farmacêuticas, dentre outras.

Como apontado, o debate da saúde global conta com a participação além dos Estados, desde Organizações Internacionais à organizações civis. Em entrevista a PUC-SP, Arthur Murta afirma que até a década de 70 a Organização Mundial da Saúde tinha uma hegemonia na determinação da agenda de saúde. Após a década de 80 o Banco Mundial ganha muita força, definindo a pauta de saúde em diversos países. Organizações filantrópicas internacionais, como a Fundação Bill e Melinda Gates, se tornaram um ator fundamental no financiamento e determinação da agenda de saúde. Para além destes, os Médicos Sem Fronteira, Cruz Vermelha, a indústria farmacêutica e blocos multilaterais são importantes nas discussões de saúde no mundo.

O Brasil desempenhou importante papel dentro da Organização Mundial da Saúde (OMS) e nos debates de saúde na América Latina. Conforme Buss e Leal (2009), o Brasil como um país emergente e de grande influência na região, se destacou durante muitos anos na diplomacia da saúde, principalmente na cooperação Sul-sul, em blocos regionais com agendas de saúdes independentes e de relevância, como a União de Nações Sul-Americanas (UNASUL) saúde e a Comunidade de Países de Língua Portuguesa (CPLP Saúde). Teve papel relevante na “cooperação estruturante de saúde” que consistia na construção de instituições estruturantes e formação de profissionais para apoiar o desenvolvimento da capacidades de saúde dos países.

O avanço da globalização e o desequilíbrio que o ser humano impos ao meio ambiente, com o desmatamento e exploração sem limites da natureza, ainda trará muitas consequências na saúde da população. Pesquisadores da área de Saúde Global, como Deisy Ventura, já afirmam que serão cada vez mais constantes epidemias de doenças zoonóticas. Os Estados precisam estar preparados para responder às emergências de saúde que serão cada vez mais constantes e dispor de mecanismos e meios para impedir que as populações mais vulneráveis sofram de forma mais intensa.

As doenças não respeitam fronteiras, por isso é preciso cada vez mais uma colaboração na área de saúde para lidar com as dificuldades e compartilhar os êxitos dos países neste tema. Muito se pode aprender com a pandemia que se está enfrentando com a COVID-19 e olhar para o passado, os erros e acertos das organizações de saúde como a OMS, e todas as conquistas que se tiveram a partir da cooperação em saúde, buscando construir um futuro mais sadio e assegurando o direito das pessoas de uma vida saudável, com a promoção do bem-estar para todos.

A pandemia demonstra a necessidade da cooperação em saúde para o enfrentamento da doença. É cada vez mais necessário o intercâmbio de informações e profissionais capacitados para produção de vacinas e medicamentos, uma troca cada vez maior de insumos médicos e o auxílio para países mais vulneráveis, já que, para superar a emergência internacional, é preciso um esforço conjunto de todos os Estados, uma vez que doenças ultrapassam fronteiras. Um país que não controle um surto de uma doença transmissível, se torna um epicentro, correndo o risco de disseminar a patologia pelo mundo.



<article></article>

### Referências Bibliográficas

ALMEIDA, Celia. Resenha: Saúde global- história, contextos e estratégias. Cad. Saúde Pública, 2018.



BUSS, Paulo Marchiori. LEAL, Maria do Carmo. Saúde global e diplomacia da saúde. Cad. Saúde Pública, Rio de Janeiro, dez, 2009.



FORTES, Paulo Antônio de Carvalho; RIBEIRO, Helena. Saúde Global em tempos de globalização. Saúde soc., São Paulo, v. 23, n. 2, p. 366-375, jun. 2014.



Chade, Jamil. Murta, Arthur. Relações Internacionais e Saúde Global: A pandemia na agenda política internacional. [Entrevista concedida a] Terra F. Budini. TVPUC, Youtube, 2020. Disponível em: https://www.youtube.com/watch?v=IVYIsXqNKa0. Acesso em: 24 jun. 2020.


