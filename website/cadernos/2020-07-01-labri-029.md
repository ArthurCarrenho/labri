---
slug: labri-n0029
tags: [COVID-19, CADERNOS]
title: "Turcomenistão e o Autoritarismo"
subtitulo: "uma relação muito além do atual Coronavírus"
author: Lívia Dutra e Gustavo Castejon
author_title: Graduandos em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 29
image_url: https://i.imgur.com/C38ZQUB.jpg
descricao: A atual crise sanitária causada pela COVID-19 tem impactado os governos e as suas respectivas esferas das mais variadas maneiras, sejam elas econômicas, políticas ou sociais. A necessidade de conter tanto o avanço da pandemia quanto a instabilidade econômica põe à prova a capacidade política...
download_pdf: /arquivos/2020-07-01-labri-029.pdf
serie: COVID-19
---
<div align="center">

![](https://i.imgur.com/C38ZQUB.jpg)  

</div>

>*A atual crise sanitária causada pela COVID-19 tem impactado os governos e as suas respectivas esferas das mais variadas maneiras, sejam elas econômicas, políticas ou sociais...*

<!--truncate-->

<div align="justify">

A atual crise sanitária causada pela COVID-19 tem impactado os governos e as suas respectivas esferas das mais variadas maneiras, sejam elas econômicas, políticas ou sociais. A necessidade de conter tanto o avanço da pandemia quanto a instabilidade econômica põe à prova a capacidade política e a própria governabilidade dos Estados. Nesse sentido, é válido analisar um país que caminha em contramão de quaisquer recomendações de segurança e saúde proposta pela Organização Mundial da Saúde (OMS), mostrando-se ser um dos destaques em reclusão, omissão de dados estatísticos e até mesmo em atos antidemocráticos. Posicionado em 179º lugar de um total de 180 países no “2020 WORLD PRESS FREEDOM INDEX” - ranking que diz respeito ao teor de liberdade dos países - o Turcomenistão carrega de uma longa data a característica de ser um país consideravelmente recluso e unilateral, o que pode justificar, em grande parte, seu duvidoso posicionamento e manejamento de recursos durante a crise. Dessarte, na tentativa de compreender as razões para o citado cenário político-ideológico, é preciso estipular uma análise contextual desse território e sua possível direta relação com as medidas governamentais - ou a falta dessas - presenciadas durante a pandemia, uma vez que esse país não tem um reconhecimento mundial significativo e sua cultura, religião, território são, por um considerável público, desconhecidas.


### Contexto Histórico do país

O Turcomenistão localiza-se na Ásia Central, fazendo fronteira com Irã, Afeganistão, Cazaquistão e Uzbequistão. Assim como seus dois últimos vizinhos listados, o país é uma ex-república soviética que emergiu após a dissolução da União das Repúblicas Socialistas Soviéticas (URSS), em 1991. Sua capital é Ashgabat, e o país é, economicamente, o mais rico comparado aos seus fronteiriços, devido às grandes reservas de gás natural - havia no fim de 2005 reservas provadas de gás de cerca de 2,9 trilhões de metros cúbicos, ficando em 12º lugar do mundo, segundo o grupo petrolífero  _British Petroleum (_BP).

O primeiro presidente da república após o desmonte da URSS foi Saparmurat Niyazov, que se manteve no poder de 1990, até 2006. Devido ao passado soviético, Niyazov herdou várias tendências políticas do governo autoritário. Uma das semelhanças ao regime era o extremo nacionalismo: distribuídas pelo país todo, mas principalmente em Ashgabat, foram construídas estátuas do presidente que tinham como objetivo implícito fazer com que o líder político se tornasse onipresente.

Em relação à religião, os governos também tiveram similitudes. A União Soviética, bastante estruturada nas ideologias marxistas, seguia o preceito de que a religião era o ópio do povo, como Vladimir Lênin – político comunista, revolucionário russo e um dos fundadores da URSS - defendia, ou seja, de acordo com esse pensamento, a religião é uma das principais alienações do homem, e assim, deve ser eliminada e perseguida. A igreja católica, na época, foi a mais perseguida pelo poder soviético. Desse modo, os mandatos de Niyazov seguiram caminhos parecidos: ele escreveu um livro religioso[^1]  e tornou obrigatória sua leitura para todos, todavia, religiões diferentes da sua foram duramente perseguidas, em principal, o evangelismo cristão. Hoje, de acordo com a Organização Não Governamental (ONG)  _Portas Abertas_, o Turcomenistão ocupa a vigésima segunda posição de cinquenta países na Classificação de Perseguição Religiosa.

Outra relevante política que ilustra muito bem o caráter do primeiro governo não soviético do Turcomenistão, foi a limitação - e quase restrição total - pelo governo de quaisquer formas de ingresso ao mundo cibernético. Em síntese, o acesso à internet foi totalmente banido do território turcomeno, sob a alegação do presidente Niyazov de que as novidades do ocidente eram "alheias à mentalidade do nosso povo".

Após Saparmurat Niyazov morrer em 2006, Kurbanguly Berdymukhamedov (KB) assumiu o cargo de presidente vencendo as eleições em fevereiro de 2007 com 89% dos votos. No entanto, havia seis candidatos nesta pesquisa, todos do Partido Democrata do Turcomenistão. Figuras exiladas da oposição turcomana foram proibidas de competir, e grupos de direitos humanos e diplomatas ocidentais condenaram a eleição como fraudada. Berdymukhamedov, portanto, foi escolhido como presidente, sendo o único candidato para o cargo e permanecendo no poder até hoje.

Decorrido esse cenário, ao longo de seu governo, o sucessor de Niyazov prometeu continuar com políticas anteriores, todavia, foram estabelecidas exceções: a internet teria acesso ilimitado, a educação seria melhorada e as pensões seriam mais altas, uma vez que Niyazov pretendia retirá-las dos idosos no ano anterior. No começo do seu primeiro mandato, Berdymukhamedov também desmontou alguns feitios de culto à personalidade de seu antecessor. Ao decorrer de seus mandatos, o presidente cumpriu com as promessas sobre as pensões, contudo, em junho de 2010, estimava-se que apenas 1,6% da população tinha acesso à internet. O desmonte do culto ao presidente anterior apenas serviu para introduzir o seu: uma nova mesquita recebeu seu nome em 2009 e as livrarias estão cheias das obras de Berdymukhamedov, além de ter fotografias suas espalhadas por hotéis, hospitais, bares e restaurantes.

Hodiernamente, uma das maiores propagandas do governo de KB e que propiciam uma intensa idealização do presidente é a imagem de sua saúde exacerbada e vigorosa. A TV do Estado o mostra com certa frequência levantando pesos na academia ou andando de bicicleta. Ainda, o governante turcomeno é posto como principal personagem das campanhas de "saúde e felicidade", nas quais aparecem funcionários do Estado vestindo uniformes idênticos fazendo seus exercícios matinais. A mensagem que o governo sempre quis mostrar desde o começo é a de que a nação está saudável e, portanto, feliz, graças ao presidente. Ocupando, no passado, o cargo de ex-dentista de Niyazov, Berdymukhamedov tornou-se ministro da Saúde do Turcomenistão em 1997 e vice-primeiro-ministro em 2001, e uma de suas tarefas foi realizar o fechamento da maioria das instalações médicas, o que levou o atendimento público à saúde ao ponto de colapso.


### Como o governo de KB vem lidando com a crise sanitária atual

Essas mensagens propagandistas são cada vez mais tratadas como farsas e inúteis devido à chegada do novo Coronavírus. De acordo com o mapa produzido pelo  _European Centre for Disease Prevention and Control_, uma agência da União Europeia, há atualmente por volta de três milhões e oitocentos mil infectados pela doença espalhados pelo mundo inteiro e aproximadamente duzentos e setenta mil mortos. A pandemia enfrentou e ainda enfrenta até mesmo as maiores potências mundiais, e quase 185 países já registraram oficialmente a presença da doença. Estima-se, conforme a BBC News, que cerca de 40 países não se manifestaram sobre casos da COVID-19 em seus territórios. Alguns, por serem ilhas remotas, de difícil acesso e com populações pequenas, como Samoa (Oceania). Outros, por serem regimes fechados e não publicarem facilmente informações, caso no qual o Turcomenistão se enquadra. Além de alegar não haver nenhum caso da doença no país, o governo turcomeno proibiu as mídias de usarem a palavra “Coronavírus” ou “COVID-19” em seus trabalhos, e os cidadãos também foram impedidos de comentar publicamente sobre o assunto.

O descaso com o alto nível de contágio da doença é notável. Enquanto países em todo o mundo como Alemanha e Espanha continuam a encorajar os indivíduos a se manterem isolados e respeitarem a quarentena, Berdymukhamedov ordenou que centenas de pessoas participassem do evento anual do país, realizado no “Dia do Cavalo”, 26 de abril, no qual ele desfilou com seus cavalos raros em uma clara demonstração vaidosa. Outro evento de aglomeração turcomeno financiado pelo governo e também realizado em plena crise sanitária mundial foi o da pedalada, para marcar o Dia Mundial da Saúde, considerado muito importante para o presidente, já que um dos seus maiores slogans, ironicamente, tem a saúde como pilar.

Por fim, as atividades do Turcomenistão estão ocorrendo normalmente: restaurantes estão funcionando como sempre, jogos de futebol continuaram a acontecer, pessoas circulam em praças públicas sem o uso de máscara, etc.

Enquanto isso, um grupo que monitora a mídia turcomena, o Chronicles Turkmenistan, informou em suas notícias que as autoridades ordenaram que os pacientes diagnosticados com COVID-19 fossem transferidos para hospitais remotos para escondê-los de funcionários da OMS.



### Como o passado histórico vem, cada vez mais, justificando ações irresponsáveis

O regime, tão preocupado com sua imagem no exterior, adota desde 1991 uma postura de isolamento político, econômico e social, e busca omitir toda e qualquer crise que aconteça no país, seja ela política, econômica ou sanitária - como observado exemplo na atualidade. O combate a essas doenças é colocado em segundo plano, não só deixando de minimizar impactos, como também aumentando-os, dado que não somente os turcomenos são afetados pelo vírus, e sim, quaisquer outros países que estabeleçam conexão com esses - como exemplo os países fronteiriços.

Essas atitudes podem ser explicadas para além do fato do governo ter caráter autoritário, nacionalista e fechado politicamente: a falta de transparência no sistema de saúde do país tem histórico. Desde a década passada até agora, as autoridades do Turcomenistão afirmam que em seu território não há nenhuma pessoa vivendo com o vírus do HIV. Dados médicos são sistematicamente manipulados e normas e protocolos internacionais não são totalmente aplicados na prática, conforme a experiência da organização médico-humanitária internacional Médicos Sem Fronteiras (MSF). Após 10 anos fornecendo assistência médica no país, o diretor médico da organização afirmou que é inegável que infecções sexualmente transmissíveis como HIV / Aids, são mais prevalentes do que os números relatados sugerem, e que o governo do Turcomenistão estaria se recusando a reconhecer esta realidade. Desse modo, os médicos turcomenos colocam em perigo diversas pessoas, as quais fazem transfusões de sangue sem a verificação de presença do vírus no corpo ou não, são ignoradas em estados críticos e muitas vezes nem são diagnosticadas correta e totalmente. Tal negligência se repete com a crise sanitária atual e demonstra ações que remetem ao passado, não sendo novidades.



### Consequências correntes

A tradição autoritária do governo turcomeno pode e já está pondo em xeque os esforços de outros países no combate à pandemia, principalmente no que se diz respeito à não transparência de dados e o relaxamento quanto às medidas protetivas propostas pela OMS. A cooperação internacional e o compartilhamento de informações, segundo o  _Jornal da USP_, são de extrema importância nesse momento de fragilidade, principalmente em países carentes de um sistema de saúde e condições econômicas favoráveis. Mesmo assim, o presidente turcomeno ainda caminha em sentido contrário.

Por conseguinte, mantida essa realidade unilateral adotada pelo Turcomenistão, poder-se-á, em um cenário futuro, presenciar um agravamento do cenário pandêmico, enfraquecendo o embate com o vírus e minando os esforços do outros países - como o isolamento social, contenção infecciosa e transparência de dados - no sentido de não cooperar internacionalmente e, ainda assim, oferecer risco de contágio dada a não adesão ao distanciamento social e as diversas outras medidas recomendadas. Consequências essas que apenas corroboram com um passado histórico de um país marcado por decisões e ações de cunho autoritário.

[^1]: Ruhnama (O Livro da Alma) é o nome da obra se Saparmurat Niyazov, muito comparado com o líder autoritário Mao Tsé Tung, revolucionário comunista que esteve no poder da China de 1949 a 1976 e que escreveu o “Livro Vermelho”, também obrigatório e religioso.



<article></article>

### Referências Bibliográficas

ABDURASULOV, Abdujalil. Coronavirus: Why has Turkmenistan reported no cases?**. BBC News,** 7 de abr. de 2020.  Disponível em: <https://www.bbc.com/news/world-asia-52186521>. Acesso em: 27 abril 2020.

A necessidade crítica de cooperação internacional durante a pandemia de covid-19**. Jornal da USP,** 8 de abr. de 2020. Disponível em: <https://jornal.usp.br/universidade/politicas-cientificas/a-necessidade-critica-de-cooperacao-internacional-durante-a-pandemia-de-covid-19/>. Acesso em: 06 maio 2020.

Após se declarar livre da covid-19, Turcomenistão retoma liga de futebol. **ISTOÉ,** 19 de abr. de 2020.  Disponível em: <https://istoe.com.br/apos-se-declarar-livre-da-covid-19-turcomenistao-retoma-liga-de-futebol/>. Acesso em: 02 maio 2020.

Coronavírus: o que você precisa saber e como prevenir o contágio. **Ministério da Saúde.** Disponível em: <https://coronavirus.saude.gov.br/>. Acesso em: 01 maio 2020.

Download today's data on the geographic distribution of COVID-19 cases worldwide**. ECDC,** 12 de maio de 2020. Disponível em: <https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide>. Acesso em: 01 maio 2020.

ESPARZA, Pablo. Como a União Soviética influenciou o surgimento e a expansão do radicalismo islâmico. **BBC News**, 12 de nov. de 2017  Disponível em: <https://www.bbc.com/portuguese/internacional-41937721>. Acesso em: 27 abril 2020.

In Turkmenistan, Whatever You Do, Don't Mention The Coronavirus. **RFE/RL's Turkmen Service,** 31 de mar. de 2020.  Disponível em: <https://www.rferl.org/a/in-turkmenistan-whatever-you-do-don-t-mention-the-coronavirus/30520255.html>. Acesso em: 27 abril 2020.

Isolado, o Turcomenistão chega, enfim, à era da internet**. G1**, 2 de set. de 2013.  Disponível em: <http://g1.globo.com/tecnologia/noticia/2013/09/isolado-o-turcomenistao-chega-enfim-era-da-internet.html>. Acesso em: 06 maio 2020.

[Lista Mundial da Perseguição 2020](https://www.google.com/url?q=https%3A%2F%2Fwww.portasabertas.org.br%2Flista-mundial%2Fpaises-da-lista&sa=D&sntz=1&usg=AFQjCNEePZecN-N5_ABHxiUojs_Q-iFYTw). **Portas Abertas.** Disponível em: <https://www.portasabertas.org.br/lista-mundial/paises-da-lista>. Acesso em: 01 maio 2020.

MILLER, Christopher.Turkmenistan: This Eccentric Dictator Continues To Deny That The Coronavirus Exists In His Country But He's Taking US Money To Fight It. **BuzzFeed News**, 1 de maio de 2020. Disponível em: <https://www.buzzfeednews.com/article/christopherm51/coronavirus-turkmenistan>. Acesso em: 27 abril 2020.

NARCIZO, Alan. 3º Encontro de Pesquisa em História: Historiografia e Fontes Históricas Graduação em História. **USC,** Bauru, p.1-15, 2015. Disponível em: <https://unisagrado.edu.br/custom/2008/uploads/wp-content/uploads/2016/09/3.-ALAN-MARCOS-MORAES-NARCIZO.pdf>. Acesso em: 27 abril 2020.

Que países e territórios ainda não têm casos confirmados de coronavírus?**. BBC,** 2 de abr. de 2020.  Disponível em: <https://www.bbc.com/portuguese/internacional-52136748>. Acesso em: 01 maio 2020.

Turcomenistão, isolado do mundo, mas com importância crescente em energia**. G1,** 21 de dez. de 2006.  Disponível em: <http://g1.globo.com/Noticias/Economia_Negocios/0,,AA1396424-9356,00-TURCOMENISTAO+ISOLADO+DO+MUNDO+MAS+COM+IMPORTANCIA+CRESCENTE+EM+ENERGIA.html>. Acesso em: 06 maio 2020.

Turcomenistão: dicas gerais de um dos países mais fechados do mundo**. Marcio no mundo,** 20 de jun. de 2016.  Disponível em: <https://www.marcionomundo.com.br/asia/turcomenistao/turcomenistao-dicas-gerais-de-um-dos-paises-mais-fechados-do-mundo/>. Acesso em: 06 maio 2020.

Turkmenistan to move coronavirus patients to remote hospitals to hide them from WHO experts. **Chronicles of Turkomenistan,** 28 de abr. de 2020  .Disponível em: <https://en.hronikatm.com/2020/04/turkmenistan-to-move-coronavirus-patients-to-remote-hospitals-to-hide-them-from-who-experts/>. Acesso em: 27 abril 2020.

Um novo regime autoritário no Turcomenistão**. Portas Abertas,** 2015 .Disponível em: <https://www.portasabertas.org.br/noticias/cristaos-perseguidos/um-novo-regime-autoritario-no-turcomenistao>. Acesso em: 02 maio 2020.
</div>