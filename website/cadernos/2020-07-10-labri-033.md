---
slug: labri-n0033
tags: [COVID-19, CADERNOS]
title: "“A pedra que canta”"
author: Cristiano Manhães
author_title: Membro do Grupo de Estudos de Defesa e Segurança Internacional (GEDES) - Bolsista do CNPq- Brasil.

tipo_publicacao: Cadernos LabRI/UNESP - Núm. 33
image_url: https://i.imgur.com/aXwSDZM.jpg
descricao: O estudo da paradiplomacia é algo recente nas Relações Internacionais, porém, regiões - principalmente fronteiriças - têm ajudado a fomentar cada vez mais o assunto, pelo fato de carregarem consigo elementos de integração regional...
download_pdf: /arquivos/2020-07-10-labri-033.pdf
serie: COVID-19
---
<div align="center">

![](https://i.imgur.com/aXwSDZM.jpg)

</div>

>*O estudo da paradiplomacia é algo recente nas Relações Internacionais, porém, regiões - principalmente fronteiriças - têm ajudado a fomentar cada vez mais o assunto, pelo fato de carregarem consigo elementos de integração regional...*

<!--truncate-->

<div align="justify">

O estudo da paradiplomacia é algo recente nas Relações Internacionais, porém, regiões - principalmente fronteiriças - têm ajudado a fomentar cada vez mais o assunto, pelo fato de carregarem consigo elementos de integração regional. É amplamente conhecido o fato de que áreas fronteiriças sofrem com a falta da presença do Estado ([Projeto de pesquisa do Ipea identifica problemas da fronteira brasileira](https://www.google.com/url?q=https%3A%2F%2Fwww.ipea.gov.br%2Fportal%2Findex.php%3Foption%3Dcom_content%26view%3Darticle%26id%3D30614&sa=D&sntz=1&usg=AFQjCNHNjbL2YMquDUE7HlJBIIa4Y_5_1g);  [Monitoramento de fronteiras ainda é frágil por falta de verbas, aponta debate](https://www.google.com/url?q=https%3A%2F%2Fwww12.senado.leg.br%2Fnoticias%2Fmaterias%2F2019%2F07%2F09%2Fmonitoramento-de-fronteiras-ainda-e-fragil-por-falta-de-verbas-aponta-debate&sa=D&sntz=1&usg=AFQjCNEmNr6JnecO2NbCl-rbFTtxghAWwA)), muito embora sejam bastante militarizadas; porém aqui se diz respeito ao desenvolvimento econômico e social, com algumas ressalvas pelo tipos de interações fronteiriças.

As fronteiras brasileiras possuem cinco tipos de interações, segundo a Proposta de Reestruturação do Programa de Desenvolvimento da Faixa de Fronteira (2005): 1) margem; 2) zona-tampão; 3) frentes; 4) capilar; e a que nos interessa como foco deste artigo: 5) sinapse. A respeito da primeira, conhecida por ter pouco contato entre as populações. A segunda, por áreas de preservação ambiental - terras protegidas e áreas indígenas, por exemplo.

A terceira, por “caracterizar frentes de povoamento” (MINT, 2005, p. 146) essas frentes podem ser: cultura, indígena e militar; a quarta “as interações podem se dar somente no nível local, [...] pode se dar através de trocas difusas entre vizinhos fronteiriços com limitadas redes de comunicação” (MINT, 2005, p. 146).

Por fim, a última e que aqui é de interesse, uma vez que é o exemplo de Foz do Iguaçu-PR. A interação do tipo de sinapse “é ativamente apoiado pelos Estados contíguos, que geralmente constroem em certos lugares de comunicação e trânsito infraestrutura especializada e operacional de suporte” (MINT, 2005, p. 146).

Embora as interações de sinapse tenham apoio do Estado, outros atores começaram a entrar no cenário, também chamados de Atores Subnacionais, como é o caso da tríplice fronteira Argentina, Brasil e Paraguai, principalmente, na cidade brasileira, Foz do Iguaçu, e na paraguaia, Ciudad del Este, devido a presença da Itaipu Binacional. Em outro momento já escrevi um artigo “[Itaipu Binacional como um ator paradiplomático na integração regional entre Brasil e Paraguai](https://www.google.com/url?q=https%3A%2F%2Fwww.congresso2019.fomerco.com.br%2Fresources%2Fanais%2F9%2Ffomerco2019%2F1570744375_ARQUIVO_557afaf8688ed3a7d9afae4f018dc0f4.pdf&sa=D&sntz=1&usg=AFQjCNFaC-hQTOJ-4HSuY0zAeEDQgmCe1g)”, cuja finalidade era mostrar que a Usina atua no fortalecimento e desenvolvimento econômico e social na região, sendo caracterizada como um “ator paradiplomático”.

<span>Itaipu não apenas têm institucionalizado a sua missão de cumprir papéis que são superiores ao de apenas uma usina, ela representa ativamente entidades federativas, de mais de um Estado, e trabalha ativamente como um ator em atividades e construções, não apenas como uma variável de cooperação internacional (MANHÃES; PINTO, p. 12, 2019).</span>

No artigo, foi elucidado também as construções realizadas pela empresa como: trincheira avenida Paraná e BR-277, mercado municipal, aeroporto internacional e a segunda ponte entre Brasil e Paraguai. Abaixo seguem duas fotos de autoria própria sobre as informações de duas obras.


<div align="center">

![](https://i.imgur.com/qtyQZAR.jpg)

![](https://i.imgur.com/nUt1x9D.jpg)

</div>

Portanto, essas ações da Itaipu Binacional configuram responsabilidade do Estado seja no âmbito do governo do Paraná ou Federal, e pelo fato da Usina investir nessas infraestruturas ela se configura como um Ator Subnacional (ou também ator paradiplomático). Sendo assim, o que de fato é a paradiplomacia?

_o envolvimento de governo subnacional nas relações internacionais, por meio do estabelecimento de contatos formais e informais, permanentes ou provisórios (ad hoc), com entidades estrangeiras públicas ou privadas, objetivando promover resultados socioeconômicos ou políticos, bem como qualquer outra dimensão externa de sua própria competência constitucional (PRIETO, 2004, p. 251, grifos meus)._



Ela pode ser dividida em tipos de paradiplomacia?

Sim, a paradiplomacia pode ser dividida em alguns enfoques, segundo Junqueira (2014): econômica, regional, transfronteiriça, transregional. A que nos chama atenção é a paradiplomacia transfronteiriça, uma vez que a Usina investe financeiramente no desenvolvimento da região, e segundo DUCHACEK (1984, pp. 18-19, apud JUNQUEIRA, 2014, p. 234) ela pode ser caracterizada por “[...] contatos transfronteiriços - institucional, formal e, acima de tudo, informal - que são preponderantemente condicionados pela proximidade e pela resultante similar na natureza de problemas comuns e suas possíveis soluções [...]”.

Por se tratar de uma paradiplomacia em áreas de fronteiras a sua característica tende a mudar um pouco, ou seja

_adquire características peculiares que se distanciam das práticas realizadas por atores cujas instituições são suficientemente sólidas e competentes para ganharem influência nas relações internacionais, [...] atende a necessidade local, a priori, utilizando as ações que visam suprir as lacunas do Estado pela falta de políticas públicas nessas regiões (NASCIMENTO, 2018, p. 55)_



Posto esses elementos supracitados, o que a Itaipu Binacional está fazendo para a cidade de Foz do Iguaçu e região (9ª Regional de Saúde do Paraná que abarca  [nove cidades](http://www.google.com/url?q=http%3A%2F%2Fwww.saude.mppr.mp.br%2Farquivos%2FFile%2Frs%2Fgd_mapa_pr.jpg&sa=D&sntz=1&usg=AFQjCNHvkRiST-FZovyrcBbM741-IzuzCg))?

Primeiramente, quando se analisa instituições públicas ou empresas privadas se faz necessária uma análise da “missão” e “visão”, que são os princípios norteadores. No caso da Itaipu Binacional:

[Missão](https://www.google.com/url?q=https%3A%2F%2Fwww.itaipu.gov.br%2Finstitucional%2Fmissao&sa=D&sntz=1&usg=AFQjCNGcz070kCTyH6hRLfutUh7GsUvHsA): Gerar energia elétrica de qualidade, com responsabilidade social e ambiental,  _impulsionando o desenvolvimento econômico_, turístico e tecnológico, sustentável, no Brasil e no Paraguai (grifos meus).

[Visão](https://www.google.com/url?q=https%3A%2F%2Fwww.itaipu.gov.br%2Finstitucional%2Fvisao&sa=D&sntz=1&usg=AFQjCNGrPeazDqPkBIKCCmgEWlibM3BBwA): Até 2020, a Itaipu Binacional se consolidará como a geradora de energia limpa e renovável com o melhor desempenho operativo e as melhores práticas de sustentabilidade do mundo,  _impulsionando o desenvolvimento sustentável e a integração regional_  (grifos meus).

Finalmente, cabe agora salientar algumas ações da empresa, elas foram divididas em duas partes: 1) ações no combate à COVID-19 e 2) ações para o pós-pandemia. Todas a notícias foram extraídas da sua  [página oficial](https://www.google.com/url?q=https%3A%2F%2Fwww.itaipu.gov.br%2Fsala-de-imprensa&sa=D&sntz=1&usg=AFQjCNHSHbyZRl4X6N-pSHXPjSrfz6geIA): - cabe uma observação: as notícias e ações são em relação à margem esquerda do rio (brasileira), ou seja, não remete ao que o lado paraguaio tem feito no combate à pandemia.

-   Algumas ações no combate à COVID-19:

1.  No dia 18 de março, a Itaipu e a Fundação de Saúde Itaiguapy – fundação responsável pela administração do Hospital Ministro Costa Cavalcanti, hospital mantido pela própria usina – firmaram um convênio de 3 milhões de dólares (aproximadamente 15 milhões de reais) e durará dez meses. O montante tem a finalidade de ser usado tanto na cidade de Foz do Iguaçu como cidades ao seu entorno, que atinge ao todo 400 mil pessoas. Já no dia 26 de março, o Hospital Costa Cavalcanti contava com uma ala exclusiva para quem está diagnosticado da COVID-19.
2.  No dia 02 de abril, a usina de Itaipu fechou um outro convênio de 4 milhões de reais, mas agora com o Estado do Paraná para a contratação de 733 bolsistas. O alvo são estudantes de enfermagem das universidades públicas do Estado, bem como a própria Universidade Federal do Paraná (UFPR) para atuarem na orientação, monitoração e auxiliar pacientes com os sintomas do novo coronavírus.
3.  No dia 20 de abril, o fundo de auxílio para o combate da pandemia foi dobrado de 250 mil dólares para 500 mil, valor destinado às entidades sem fins lucrativos. “O aumento do valor do auxílio eventual se soma a inúmeras outras medidas já adotadas pela Itaipu no enfrentamento à covid-19”, afirmou a empresa. No mesmo dia, a empresa repassou 15 respiradores ao Hospital Municipal Padre Germano Lauck, como também materiais necessários para confecção de máscaras para pessoas com vulnerabilidade social.
4.  No dia 23 de abril, a empresa forneceu cinco ônibus para que brasileiros pudessem retornar ao Brasil.
5.  No dia 27 de abril, o Hospital Costa Cavalcanti abriu espaço na sua ala exclusiva para pacientes do Sistema Único de Saúde, ou seja, “caso ocorra uma superlotação de leitos de UTI para pacientes com diagnóstico comprovado de covid-19 nos hospitais públicos da 9ª Regional de Saúde, o Costa Cavalcanti disponibilizará os leitos especializados de UTI covid-19 aos pacientes do SUS”, afirmou a Itaipu.
6.    


-   Algumas ações para o pós-pandemia:

1.  No dia 06 de maio, a Itaipu afirmou que “vai empreender todos os esforços necessários para a retomada gradual do turismo de Foz do Iguaçu e região no pós-pandemia da covid-19”. No mesmo dia, foi inaugurado o “Iguassu Connect”, cuja finalidade é promover lives com os representantes estratégicos do turismo e áreas afins para a retomada do turismo.
2.  No dia 03 de junho, a hidroelétrica anunciou uma doação de 50 mil reais, e também a iniciativa de uma live para que valores sejam arrecadados com o intuito de repassar aos trabalhadores do setor do turismo.
3.  No dia 04 de junho, a usina ilustrou o uso da tecnologia como ferramenta para o retorno das atividades turísticas. No caso, serão robôs que serão usados para desinfecção dos ambientes, e é uma parceria com o Parque Tecnológico Itaipu (PTI-PR) e a Universidade Estadual do Oeste do Paraná (Unioeste).

Sabemos que o impacto regional da construção de uma hidroelétrica é grande, principalmente, no que se diz respeito à criação do reservatório, uma vez que altera o clima da região e há necessidade de remanejar populações, como também o impacto na fauna e flora. No entanto, a Itaipu mostra na sua “missão” e “valor” que busca melhorar a região, e tem feito isso - até diria que como uma forma de se “desculpar” por conta do seu impacto. Embora, a construção de uma Usina possua de fato pontos positivos após seu início de trabalho, por isso a necessidade de se analisar os valores e missões das empresas e instituições.

_É interessante notar que uma realização dessa natureza não desperta nenhum entusiasmo (pelo menos alguma curiosidade deveria…) nos ativistas das organizações não governamentais – as notórias ONGs, estrangeiras em maioria – que se apresentam como defensores do meio ambiente e participam em pleno século XXI de campanhas financiadas do exterior para impedir a expansão da oferta de energia limpa entre nós. Basta sentir o seu desinteresse (fruto da ignorância, talvez) em comemorar o fato de que a energia limpa conduzida por milhares de quilômetros a partir da Usina de Itaipu corresponde a eliminar a sujeira de 500 mil barris de petróleo, que teriam de ser consumidos diariamente para atender à demanda nas regiões Sudeste, Sul e Centro-Oeste do Brasil e no leste paraguaio. (CARTA CAPITAL, 2013, grifos meus)._

Num período pandêmico, ainda mais no Brasil que concomitantemente surgiu-se uma crise política e instabilidade das instituições, Atores Subnacionais se mostram necessários para desafogar o papel do Estado na recuperação. E no que se diz respeito à região de Foz do Iguaçu a Itaipu se mostrará cada vez mais presente.

*Itaipu em tupi-guarani significa: “A pedra que canta”.

<article></article>

### Referências Bibliográficas

CARTA CAPITAL. **A pedra que canta**. 2013. Disponível em: https://www.cartacapital.com.br/sociedade/a-pedra-que-canta/. Acesso em: 19 de jun. 2020.



ITAIPU. **Fundo Emergencial de US$ 3 milhões para enfrentar o coronavírus já está liberado.** 2020. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/fundo-emergencial-de-us-3-milhoes-para-enfrentar-o-coronavirus-ja-esta-libe. Acesso em: 05 jun. 2020.



ITAIPU. **Hospital Costa Cavalcanti conta com ala exclusiva para portadores da COVID-19**. 2020. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/hospital-costa-cavalcanti-conta-com-ala-exclusiva-para-portadores-da-covid-. Acesso em: 05 jun. 2020.



ITAIPU. **Itaipu fará convênio de R$ 4 milhões com Estado para contratação de 733 bolsistas no combate à COVID-19**. 2020. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/itaipu-fara-convenio-de-r-4-milhoes-com-estado-para-contratacao-de-733-bols. Acesso em: 05 jun. 2020.



ITAIPU. **Itaipu dobra valor de auxílio eventual para R$ 2,7 milhões no combate ao novo coronavírus**. 2020. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/itaipu-dobra-valor-de-auxilio-eventual-para-r-27-milhoes-no-combate-ao-novo. Acesso em: 05 jun. 2020.



ITAIPU. **Respiradores, máscaras e testagem em massa: Itaipu reforça apoio a Foz no enfrentamento à COVID-19**. 2020. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/respiradores-mascaras-e-testagem-em-massa-itaipu-reforca-apoio-foz-no-enfre. Acesso em: 05 jun. 2020.



ITAIPU. **Itaipu e prefeitura de Foz participam de força-tarefa para trazer brasileiros do Paraguai**. 2020. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/itaipu-e-prefeitura-de-foz-participam-de-forca-tarefa-para-trazer-brasileir. Acesso em: 05 jun. 2020.



ITAIPU. **Hospital mantido por Itaipu tem ala de tratamento de COVID-19 para o SUS, sem custo**. 2020. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/hospital-mantido-por-itaipu-tem-ala-de-tratamento-de-covid-19-para-o-sus-se. Acesso em: 05 jun. 2020.



ITAIPU. **Itaipu terá papel fundamental na retomada do turismo em Foz do Iguaçu e região. 2020**. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/itaipu-tera-papel-fundamental-na-retomada-do-turismo-em-foz-do-iguacu-e-reg. Acesso em: 05 jun. 2020.



ITAIPU. **Itaipu dará R$ 50 mil na largada da live solidária da retomada do turismo em Foz. 2020**. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/itaipu-dara-r-50-mil-na-largada-da-live-solidaria-da-retomada-do-turismo-em. Acesso em: 05 jun. 2020.



ITAIPU. **Robôs de desinfecção de ambientes vão garantir visita segura na retomada do turismo da Itaipu**. 2020. Disponível em: https://www.itaipu.gov.br/sala-de-imprensa/noticia/robos-de-desinfeccao-de-ambientes-vao-garantir-visita-segura-na-retomada-do. Acesso em: 05 jun. 2020.



JUNQUEIRA, Cairo Gabriel Borges. **A Inserção Internacional dos Atores Subnacionais e os Processos de Integração Regional: Uma Análise da União Europeia e do Mercosul** (Dissertação de Mestrado). Universidade de Brasília (UnB): Brasília, 2014.



MANHÃES, C.; PINTO, G. **Itaipu Binacional como um ator paradiplomático na integração regional entre Brasil e Paraguai**. In: XVII Congresso Internacional do FoMerco, 2019, Foz do Iguaçu. Anais Eletrônicos, 2019.



MINT Ministério da Integração Nacional. **Proposta de Reestruturação do Programa de Desenvolvimento da Faixa de Fronteira: Bases de uma política integrada de desenvolvimento regional para a faixa de fronteira**. Brasília: MINT, 2005.



NASCIMENTO, R. F. **A Paradiplomacia Transfronteiriça: Realidade e Perspectivas das Relações entre Brasil - Bolívia e Brasil - Paraguai**. Revista Espaço e Tempo Midiáticos. Volume 03, Número 01, Ano 2018 (jan./jun.).



PRIETO, Noé Conargo. **O outro lado do novo regionalismo pós-soviético e da Ásia-Pacífico: a diplomacia federativa além das fronteiras do mundo ocidental**. In: VIGEVANI, Tullo; WANDERLEY, Luiz Eduardo; BARRETO, Maria Inês; MARIANO, Marcelo Passini (Org.), A dimensão subnacional e as relações internacionais, São Paulo: Editora da PUC/ Editora da Unesp/ Cedec/ Fapesp, 2004.

</div>