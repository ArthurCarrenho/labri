---
slug: labri-n0042
tags: [COVID-19, CADERNOS]
title: "A relação Brasil e OMS"
subtitulo: "uma análise em meio a crise multilateral"
author: Nicole Mourad Pereira e Sofia Sabbag 
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 42
image_url: https://i.imgur.com/fmLjxpc.jpg
descricao: Mediante à crise sanitária global pela qual passamos atualmente, muitos olhos se voltaram para a Organização Mundial de Saúde (OMS). Suas responsabilidades,.
download_pdf: /arquivos/2020-12-10-labri-042.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/fmLjxpc.jpg)

</div>

<figcaption align="left">Análise histórica da relação Brasil-OMS</figcaption>


>*Mediante à crise sanitária global pela qual passamos atualmente, muitos olhos se voltaram para a Organização Mundial de Saúde (OMS). Suas responsabilidades, sua atuação no combate ao vírus da Covid-19*

<!--truncate-->

<div align="justify">

Mediante à crise sanitária global pela qual passamos atualmente, muitos olhos se voltaram para a Organização Mundial de Saúde (OMS). Suas responsabilidades, sua atuação no combate ao vírus da Covid-19, seu funcionamento e muito de sua história ficam ocultados nos pânicos sociais que buscam responsabilizar algum agente pelos desbalanços de 2020. O objetivo da presente exposição é iluminar essas questões, pelo viés das Relações Internacionais. O texto será dividido em duas partes: (1) análise histórica da relação Brasil-OMS e (2) o momento geopolítico atual da instituição.



Para o começo do desenho da relação brasileira com esta organização internacional, resgata-se o contexto das reuniões de criação da Organização das Nações Unidas em 1945, quando três países advogaram em nome de uma agência de saúde internacional: China, Noruega e Brasil. Foi um dos patronos da saúde pública brasileira, Geraldo de Paula Souza, que semeou a OMS, a qual seria constituída oficialmente em 1948.

Os primeiros feitos de larga escala da Organização foram campanhas de vacinação de tuberculose, em 1950, e de erradicação da malária a partir do uso do inseticida DDT em 1955. Nota-se que a última é, até os dias atuais, a que mais mata pessoas por ano e, nesse sentido, a campanha da OMS é de suma importância. Seguindo a cronologia da década de 1950, foi eleito o segundo diretor-geral, o brasileiro Dr. Marcolino Candau, sanitarista que comandou por vinte anos a OMS (de 1953 até 1973).

Na década de 1960 e 1970, é iniciada a atuação acerca das doenças crônicas e não infecciosas como diabetes e câncer, que levaria mais tarde, em 1996, à criação da UNAIDS, programa adjunto das Nações Unidas para erradicação do HIV. Em 1980, ocorreu a histórica declaração, na Assembleia Mundial da Saúde, da erradicação da varíola, em decorrência de intensos esforços globais liderados pela OMS, os quais, atualmente, ainda servem de aprendizado em tempos de Covid-19. “A erradicação da varíola é um lembrete do que é possível quando as nações se reúnem para combater uma ameaça comum à saúde”, diz Tedros Adhanom, diretor-geral da OMS, em maio de 2020.

Entrando na década de 2000, o Brasil inicia uma posição paradigmática nas políticas de saúde globais, das quais muitas lideradas pela OMS, a notar:



Convenção-Quadro sobre Controle do Tabaco, na OMS, em 2003, quando do primeiro tratado internacional sobre Saúde Pública da história, aderido por todas as nações do mundo (Buss, 2017). O Brasil coordenou a elaboração do tratado e implementou a Convenção durante os anos seguintes;
Criação da Unitaid, em 2006, protagonizada por várias nações do mundo entre elas o Brasil. O programa visa acessibilizar medicamentos para populações pobres e logrou, assim como a Convenção do Tabaco, grande mobilização pública;
Conferência Mundial sobre Determinantes Sociais de Saúde (DSS), realizada no Rio de Janeiro, em 2011, resultado da grande participação da Fiocruz (Fundação Oswaldo Cruz) nessa área da Saúde Pública nacional e global. A conferência não só representou um grande avanço desse debate, mas reafirmou a posição histórica do Brasil de liderança e agência nos temas de Saúde.


Sendo assim, é notório os liames e vínculos próximos entre o Brasil e a arena internacional no que tange o tema da Saúde, sob a égide da OMS. As participações ativas, desde a concepção até os programas atuais da Organização, criaram um legado de uma relação profícua e construtiva rumo a uma saúde menos excludente.

### O momento atual político da OMS e a crise do multilateralismo

O momento da pandemia não poderia ser mais inconveniente para o sistema internacional: uma crise do multilateralismo tem sido analisada em diversos âmbitos acadêmicos, políticos e institucionais. Não se trata, a priori, de um amplo consenso, contudo existe um entendimento de esvaziamento e impasse que instituições internacionais enfrentam no momento. Trata-se, muito antes da pandemia, da incapacidade que as organizações internacionais apresentam de resolver problemas complexos, em uma oposição às premissas do sistema: cooperar e interagir com diversos atores da cena internacional para resolução de problemas coletivos contemporâneos. Exemplificando, é sabido que dentre as várias justificativas para esse movimento anti-bloco europeu, o BREXIT (a saída do Reino Unido da União Europeia) adveio das políticas de imigração e de refugiados da UE, ou seja, de políticas intrabloco. Também, a eleição de partidos ultraconservadores e o aumento de sua representação política na Polônia, Alemanha e Hungria são sintomáticos da retórica anti-bloco europeu.

Complementar a esse quadro, a presidência do republicano Donald Trump tem se declarado abertamente contrária ao multilateralismo e às organizações internacionais: os Estados Unidos retiraram-se da UNESCO, do NAFTA, do Conselho de Direitos Humanos da ONU e do Acordo de Paris sobre as mudanças climáticas. Além disso, na Assembleia Geral da ONU, em três discursos já realizados, os EUA adotaram posições nacionalistas e contra o chamado “globalismo”. Atualmente, Trump empreende uma campanha contra a legitimidade do Tribunal Penal Internacional, impondo medidas restritivas de coerção aos juízes internacionais.

Em julho de 2020, os EUA retiraram-se oficialmente da Organização Mundial da Saúde, em plena pandemia (cabe observar que seu presidente criticou também a Organização Mundial do Comércio nessa retirada). Como explica o professor de direito internacional da Universidade de São Paulo, Alberto de Amaral Jr.: “As perspectivas do multilateralismo são ruins a curto prazo, dado a tendência dos EUA de resolver seus problemas bilateralmente, pondo seu poder em detrimento de outros países”. Ou seja, a exemplo dos movimentos políticos europeus e dos divórcios que a atual administração dos EUA engendram, as relações Estado-Organizações Internacionais passam por uma crise de confiança, em que a cooperação internacional é vista como ineficaz e ideológica.

Na América latina, por sua vez, a crise do multilateralismo demonstra-se a partir do desmonte institucional de organismos multilaterais como a UNASUL (União das Nações Sul Americanas) e a OEA (Organização dos Estados Americanos), e principalmente no alinhamento político e ideológico do presidente brasileiro, Jair Bolsonaro, com o presidente americano, Donald Trump.

No tocante à atual política externa brasileira, como ressaltado pela carta de repúdio de diferentes agentes da diplomacia brasileira publicada no Estado de São Paulo, é “objeto de repulsa e escárnio internacional” o sectarismo dos ataques a Organização Mundial de Saúde e a consequente quebra de princípios constitucionais brasileiros, de moderação, realismo construtivo e pragmatismo, além de respeito à ciência e à participação histórica do Brasil na instituição. A pesquisadora brasileira, Deise Ventura, ressalta no painel online do Fórum Brasileiro de Política Internacional que: “a OMS não é uma organização supranacional, e sim intergovernamental [de modo que] depende da vontade de participação de todos seus 194 Estados”, ou seja, “não basta criticar uma organização internacional sem criticar os Estados que participam dela”. Isto é, as organizações internacionais vinculam-se à vontade de terceiros, tendo uma natureza secundária (Seitenfus, 2004). Além do que, analisando a atuação da instituição na pandemia, a professora é contundente em dizer que a OMS agiu na medida da possibilidade e responsabilidade, sendo notório afirmar a relação de comprimento das recomendações propostas por ela e bons resultados na contenção da pandemia. Sendo assim, cabe avaliar o vínculo com essa instituição, de forma colaborativa e pragmática, como de suma valia para a população do Brasil e de qualquer país. A saída proposta por Trump, junto das ameaças de Jair Bolsonaro a fazê-la, divorciando duas das maiores populações mundiais do zelo da OMS é de extrema contraprodução.

Como exemplo dos ataques à OMS , tem-se o chanceler brasileiro, Ernesto Araújo, recorrentemente afirmando que a OMS atua com “viés ideológico”, se referindo a grande influência chinesa no órgão e às preocupações da transparência da instituição com dados da COVID. Inicialmente, questionar dados de um regime contrário à liberdade de expressão é, dentro de um regime internacional de Direitos Humanos, sempre válido. Contudo, criticar algo e agir de forma igual constitui gritante paradoxo. Outra justificativa inválida desta análise recai sobre “rejeitar” e advogar contra a influência de uma Nação soberana sobre uma organização internacional, como se fosse digno de reprimenda. Ora, desde o final da Segunda Guerra Mundial, e especialmente após o fim da Guerra Fria, os EUA dominam metade da existência do mundo ocidental (Aaron, 2002) e , desse modo, todas suas instituições geopolíticas. Logo, não cabe invalidar a ingerência de poder chinesa no sistema, pois, isso decorre naturalmente da existência da organização internacional, que reflete a maneira pela qual as relações interestatais já ocorrem.

### Conclusão

Portanto, o referido contexto político de crise do multilateralismo vai extinguindo, aos poucos, as capacidades de colaboração em momentos críticos como a pandemia da Covid-19. Em temas globais como esse, que exige diálogo mútuo e decisões referentes a uma gama de Estados e suas populações, a posição estadunidense (e o consequente alinhamento da política externa brasileira) paulatinamente quebra os vínculos com as instituições internacionais, como na relação com a OMS à despeito de suas imensas contribuições, desde sua concepção até os dias atuais. São argumentos vãos os que deslegitimam a organização internacional. Não procede subjugar a OMS pelo âmbito racional e histórico da instituição de forma emocional-ideológica, baseando-se em alegações infundadas, desmontando a tradição diplomática brasileira e flexionando-a contra nossa própria Constituição, como a defesa da independência, dignidade do interesse nacional, busca de diálogo e cooperação internacional (Artigo 4.º da Constituição de 1988.)



### Referências Bibliográficas



AGÊNCIA BRASIL. Brasil e outros países vão pedir reforma da OMS. 2020. Disponível em: <https://agenciabrasil.ebc.com.br/internacional/noticia/2020-06/brasil-e-outros-paises-vao-pedir-reforma-da-oms> Acesso em: 15 jul. 2020.



BRASIL, Opas (org.). Erradicação da varíola: um legado de esperança para COVID-19 e outras doenças. 2020. Disponível em: <https://www.paho.org/bra/index.php?option=com_content&view=article&id=6165:erradicacao-da-variola-um-legado-de-esperanca-para-covid-19-e-outras-doencas&Itemid=812#:~:text=8%20de%20maio%20de%202020,pessoas%20somente%20no%20s%C3%A9culo%20XX>Acesso em: 11 set. 2020.



DW. Trump faz discurso nacionalista na ONU. Disponível em: <https://www.dw.com/pt-br/trump-faz-discurso-nacionalista-na-onu/a-50569222> Acesso em: 10 set. 2020.



EL PAÍS. Ultraconservadores nacionalistas vencem as eleições na Polônia. 2015. Disponível em: <https://brasil.elpais.com/brasil/2015/10/25/internacional/1445764644_924199.html?rel=listapoyo> Acesso em: 11 set. 2020.



ESTADO DE SP. A reconstrução da política externa brasileira. 2020. Disponível em: <https://politica.estadao.com.br/noticias/geral,a-reconstrucao-da-politica-externa-brasileira,70003296122> Acesso em: 11 jul. 2020.



ESTADO DE SP. O cartão vermelho de Trump para o Conselho de Direitos Humanos da ONU. 2018. Disponível em: <https://politica.estadao.com.br/blogs/fausto-macedo/o-cartao-vermelho-de-trump-para-o-conselho-de-direitos-humanos-da-onu/> Acesso em: 5 jul. 2020.



FÓRUM BRASILEIRO DE POLÍTICA INTERNACIONAL. O Brasil e a OMS. 2020. Debate virtual via Facebook. Disponível em: <https://www.facebook.com/watch/forumbrasileiropi/> Acesso em: 9 set. 2020



Jornal da USP. Políticas de Trump são sistematicamente contrárias ao multilateralismo. Disponível em: <https://jornal.usp.br/radio-usp/politicas-de-trump-sao-sistematicamente-contrarias-ao-multilateralismo/> Acesso em: 10 set. 2020.



NEXO JORNAL. Que papel o Brasil tem na OMS. E qual a consequência de deixá-la. 2020. Disponível em: <https://www.nexojornal.com.br/expresso/2020/06/14/Que-papel-o-Brasil-tem-na-OMS.-E-qual-a-consequ%C3%AAncia-de-deix%C3%A1-la> Acesso em: 11 set. 2020.



REVISTA CIÊNCIA E SAÚDE MANGUINHOS (org.). O médico brasileiro que foi diretor-geral da OMS por 20 anos. 2020. Disponível em: <http://www.revistahcsm.coc.fiocruz.br/o-medico-brasileiro-que-foi-diretor-geral-da-oms-por-20-anos/> Acesso em: 10 jul. 2020.



REVISTA ÉPOCA. BRASIL É DESTAQUE NO MUNDO POR ESCONDER DADOS DE MORTES POR COVID-19. OMS COBRA TRANSPARÊNCIA. 2020. Disponível em: <https://epoca.globo.com/brasil/brasil-destaque-no-mundo-por-esconder-dados-de-mortes-por-covid-19-oms-cobra-transparencia-24468531> Acesso em: 15 jul. 2020.



REVISTA DO INSTITUTO DE ESTUDOS ECONÔMICOS E SOCIAIS (UNESP) (org.). Crise do multilateralismo. 2019. Disponível em: <https://ieei.unesp.br/index.php/IEEI_MundoeDesenvolvimento/issue/view/4> Acesso em: 10 set. 2020.



VALOR ECONÔMICO. Trump rejeita multilateralismo em discurso na ONU. 2018. Disponível em: <https://valor.globo.com/mundo/noticia/2018/09/25/trump-rejeita-multilateralismo-em-discurso-na-onu.ghtml> Acesso em: 10 set. 2020.



VENTURA, Deisy; PEREZ, Fernanda Aguilar. Crise e reforma da organização mundial da saúde. 2014. Disponível em: <https://www.scielo.br/pdf/ln/n92/a03n92.pdf> Acesso em: 23 jun. 2020.



VEJA SAÚDE. A incrível história do brasileiro que ajudou a fundar a OMS. Disponível em: <https://saude.abril.com.br/blog/tunel-do-tempo/a-incrivel-historia-do-brasileiro-que-ajudou-a-fundar-a-oms/> Acesso em: 11 set. 2020.
</div>