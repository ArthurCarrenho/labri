---
slug: labri-n0016
tags: [COVID-19, CADERNOS]
title: "Comparação e Impactos Gerais entre Pandemias"
subtitulo: "Covid-19 x Gripe Espanhola"
author: Júlia Nogueira Lourenço, Maria Eduarda Gumiero Silveira e Tainara Cristina Ebeling
author_title: Graduandas em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 16
image_url: https://imgur.com/3uw9Yjv.png
descricao: Não é a primeira vez que o mundo vivencia uma crise de saúde. Já ocorreram outras situações epidêmicas notáveis, geralmente respiratórias, como SARS, MERS e, a mais violenta até então, a Gripe Espanhola. A pandemia...
serie: COVID-19
---

<div align="center">

![](https://imgur.com/3uw9Yjv.png)

</div>

>*Não é a primeira vez que o mundo vivencia uma crise de saúde. Já ocorreram outras situações epidêmicas notáveis, geralmente respiratórias, como SARS, MERS e, a mais violenta até então, a Gripe Espanhola...*

<!--truncate-->

<div align="justify">

Não é a primeira vez que o mundo vivencia uma crise de saúde. Já ocorreram outras situações epidêmicas notáveis, geralmente respiratórias, como SARS, MERS e, a mais violenta até então, a Gripe Espanhola. A pandemia, datada de 1918 a 1920, foi causada por uma mutação do vírus Influenza A, do subtipo H1N1, que foi altamente disseminado pela movimentação de tropas na Primeira Guerra Mundial. Além da lotação nos quartéis, os combatentes sofriam com más condições na frente de batalha e nas trincheiras, com a fome e a consequente desnutrição. Para além dos soldados, os civis dos países envolvidos na guerra também sofriam com a falta de alimentos, já que os alimentos eram primordialmente enviados às tropas. Com o fim da guerra, os soldados que voltaram para as suas cidades de origem carregaram consigo o vírus da Gripe Espanhola. Supõe-se que o primeiro caso confirmado da doença tenha sido registrado em um campo de treinamento nos Estados Unidos.

Outro agravante influente na chegada da doença a diversos países do globo, foi a Segunda Revolução Industrial. Do meio do século XIX até o fim da Segunda Guerra Mundial, o período contou com um aprimoramento dos meios de transporte e um aumento da circulação entre os países, acelerando o processo de disseminação da pandemia em relação a Peste Negra, por exemplo, quando os modais de transporte eram altamente primitivos.

Além das influências da Primeira Guerra Mundial e da Segunda Revolução industrial, o período também foi marcado pela censura da imprensa de países envolvidos no conflito, a fim de camuflar os efeitos da doença nas batalhas e nas tropas. Assim, a pandemia ficou conhecida como Gripe Espanhola pois a Espanha, que se manteve neutra na guerra, foi o único país a notificar a população sobre a realidade enfrentada. A Gripe chegou ao Brasil em setembro de 1918 com o retorno de marinheiros enviados à guerra e com os tripulantes de alguns navios, como o inglês Demerara, que esteve em Salvador, Recife e no Rio de Janeiro, sendo assim os primeiros responsáveis pela disseminação da doença no território brasileiro.

Cem anos depois, o que vivemos hoje com o novo coronavírus se assemelha muito ao enfrentado pelo mundo durante a Gripe Espanhola, como pôde ser notado por dados já citados aqui. Situações como enterros em covas coletivas e dificuldade do poder público em liderar a população não são novidades na história das pandemias da humanidade.

A desinformação em pandemias também é recorrente. Informações sem embasamento científico e remédios sem comprovação de eficácia foram amplamente divulgados. Era comum em 1918 ouvir que o vírus Influenza era uma criação dos alemães como estratégia de guerra, da mesma forma que foram levantadas suspeitas quanto a origem do coronavírus. Há quem acredite que este é uma arma biológica, criada em laboratório, um vírus propositalmente manipulado pelos chineses.

Apesar disso, o maior erro do Brasil durante a pandemia de 1918 foi, definitivamente, a demora para agir. De início não foram tomadas providências sérias por parte do poder público por subestimar o vírus. O então Diretor Geral de Saúde Pública, Carlos Seidl, dizia que a gripe vista no Brasil não era a mesma que atacava a Europa, e que esta tinha um caráter benigno (não muito diferente do que hoje, quando muitos declaram ser apenas uma “gripezinha”).

Jornais e imprensa começaram a pressionar o governo e a Direção Geral de Saúde pedindo por medidas de isolamento e quarentena. O diretor insistiu que as medidas reivindicadas não eram possíveis, não eram necessárias e tão pouco legais. Os jornais foram culpados de causar histeria e pânico desnecessários na população e em diversas regiões do país foram proibidos de divulgar informações como número de infectados e mortos.

Conforme o número de casos e de mortes aumentava, o país foi se dando conta da difícil situação. Carlos Seidl foi demitido do cargo de Diretor e substituído por Carlos Chagas.

É importante destacar que não havia em 1918 uma Organização Internacional, como existe a Organização Mundial da Saúde (OMS) atualmente, para orientar os países ante a Gripe Espanhola, mas a maioria das medidas recomendadas pelos cientistas e médicos da época foram muito parecidas com as atuais. No Brasil, bem como na maioria dos países, as recomendações foram: evitar aglomerações, tomar cuidados higiênicos com o nariz e a garganta, evitar fadiga e exercício físico, ao sentir qualquer sintoma o doente deveria ficar em repouso e evitar contato com demais pessoas, evitar causas gerais de resfriamento e os cuidados deveriam ser redobrados para com o idosos.

Essas medidas foram divulgadas em diversos jornais brasileiros como recomendações. Escolas, comércio e repartições públicas no Brasil foram fechados, a exemplo do que países como os Estados Unidos fizeram. Este, inclusive, adotou também o uso de máscara como forma de prevenir o contágio. Sistemas de saúde de todo o mundo entraram em colapso e leitos médicos foram improvisados.

A adoção destas recomendações e medidas não impediram, entretanto, o caos social por terem sido adotadas tardiamente. Remédios e alimentos foram distribuídos por prefeituras e governos estaduais, enfermarias foram improvisadas em escolas e igrejas. Sem uma legislação trabalhista, muitos empregadores deixaram de pagar salários e as pessoas, em sua grande maioria as mais pobres, retornaram ao trabalho, o que agravou a disseminação do vírus e sobrecarregou ainda mais o sistema de saúde.

Depois de meses e de um aumento rápido no número de casos criou-se uma situação inusitada no Brasil de 1918: as pessoas com medo de serem infectadas ficaram em casa. Uma medida não intencional, mas provocada pelo grande número de pessoas mortas.

No dia 24 de março de 2020 repetiu-se no discurso do Presidente da República algo que o então Diretor Carlos Seidl expressado 102 anos antes: o coronavírus foi menosprezado assim como a gripe espanhola, chamado de gripezinha, e recomendou-se que as pessoas voltassem ao trabalho e as suas rotinas.

Mesmo com exemplos contrários, os erros cometidos em 1918 em relação à gripe espanhola foram levados em conta por alguns países, como a Nova Zelândia, que demonstrou agilidade em fechar fronteiras e impôs o fechamento de serviços não essenciais no país antes mesmo de registrar alguma morte pelo novo coronavírus. Infelizmente no Brasil, ainda há uma discussão quanto a necessidade de um  _lockdown_  e de medidas rígidas de isolamento social. Os números de mortos batem recordes a cada dia. Políticos e civis continuam negando a pandemia e sua importância.

Fica evidente, ao compararmos a gripe espanhola com o novo coronavírus, que apesar da grande diferença de tempo entre as duas epidemias, ambas possuem a mesma trajetória. No entanto, ao analisarmos os números das duas pandemias, a Gripe Espanhola, que durou de 1918 a 1920, teve de 50 a 100 milhões de mortos em todo o mundo (destes aproximadamente 35 mil no Brasil) em uma época em que a saúde, o saneamento básico e a velocidade de espalhar informações eram defasados. Hoje, apesar dos números serem, até agora, menores, em torno de 281 mil mortos ao redor do globo, em um período de 6 meses, o mundo e países como o Brasil, em especial, mais uma vez, agiram erroneamente e tardaram ao tomar as precauções necessárias, mesmo com todas as facilidades que temos a nosso favor.

Uma vez que isso ocorre, as chances de diminuir o contágio são reduzidas e as mortes aumentam, o que nos leva a crer que, embora tenhamos evoluído na ciência, na comunicação, organização social, entre outras esferas, continuamos repetindo os erros do passado, menosprezando o vírus, desrespeitando recomendações de isolamento e obtendo resultados semelhantes aos de 1918 no presente.

<article></article>

### Referências Bibliográficas

ALBUQUERQUE, Cristiane. Fake news circularam na imprensa na epidemia de 1918. **Revista HCSM**, março de 2020. Disponível em: https://www.arca.fiocruz.br/bitstream/icict/40653/2/Fake%20news%20circularam%20na%20imprensa%20na%20epidemia%20de%201918%20_%20Hist%C3%B3ria%2C%20Ci%C3%AAncias%2C%20Sa%C3%BAde%20%E2%80%93%20Manguinhos.pdf.

ALVES, Gabrielle Wereniciz. Uma comparação entre a pandemia de Gripe Espanhola e a pandemia de Coronavírus. **UFRGS**, 17 de abril de 2020. Disponível em: https://www.ufrgs.br/coronavirus/base/uma-comparacao-entre-a-pandemia-de-gripe-espanhola-e-a-pandemia-de-coronavirus/.

BIERNATH, André. Quais as semelhanças entre a Covid-19 e outras pandemias do passado. **Abril**, 11 de maio de 2020. Disponível em: https://saude.abril.com.br/blog/tunel-do-tempo/semelhancas-covid-pandemias-passado/.

BISPO, Geovanna. Gripe espanhola e coronavírus: historiadores mapeiam semelhanças de como Brasil lidou com pandemias. **Jornal de Brasília**, 30 de abril de 2020. Disponível em: https://jornaldebrasilia.com.br/brasil/gripe-espanhola-e-coronavirus-historiadores-mapeiam-semelhancas-de-como-brasil-lidou-com-pandemias/.

Imagem:

https://images.app.goo.gl/bkCc1WtqrZ8EmiPeA

https://www.gov.br/pt-br/noticias/saude-e-vigilancia-sanitaria/2020/03/saiba-quando-e-eficaz-utilizar-a-mascara-de-protecao

</div>
