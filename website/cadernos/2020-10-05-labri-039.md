---
slug: labri-n0039
tags: [COVID-19, CADERNOS]
title: "O capitalismo de desastre no pós pandemia"
author: Matheus Valencia
author_title: Graduando em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 39
image_url: https://i.imgur.com/jpI9oMY.png
descricao: Depois de um grande evento traumático, como guerras, ataques terroristas, golpes de estado, crises econômicas e desastres naturais, organizações tendem a explorar a confusão das pessoas em meio a situação crítica fornecendo soluções imediatistas...
download_pdf: /arquivos/2020-10-05-labri-039.pdf
serie: COVID-19
---
<div align="center">

![](https://i.imgur.com/jpI9oMY.png)

</div>

>*Depois de um grande evento traumático, como guerras, ataques terroristas, golpes de estado, crises econômicas e desastres naturais, organizações tendem a explorar a confusão das pessoas em meio a situação crítica fornecendo soluções imediatistas...*

<!--truncate-->

<div align="justify">

Depois de um grande evento traumático, como guerras, ataques terroristas, golpes de estado, crises econômicas e desastres naturais, organizações tendem a explorar a confusão das pessoas em meio a situação crítica fornecendo soluções imediatistas, porém falhas, que sugam riquezas remanescentes e nutrem uma pequena parte da população, muitas vezes abastada e isenta de riscos. O capitalismo de desastre foi, pela primeira vez, descrito no livro  _“The Shock Doctrine”_  escrito por Naomi Klein, como uma prática que sobrevive às custas da tragédia e da penúria.



O início da pandemia do novo coronavírus alarmou nações por todo o mundo, uma crise sem precedentes responsável por atacar sistematicamente a economia, a saúde e a governança tornou-se logo um terreno fértil para que estratégias consumistas estabelecem-se com promessas irreais. Entretanto, cenários futuros não parecem promissores, a pandemia foi responsável pela queda significativa da atividade econômica ao longo dos últimos meses que refletiu numa severa queda do PIB real, dos salários, dos empregos, da produção industrial e do comércio.

Nos Estados Unidos, o Secretário do Tesouro, Steven Mnuchin, decidiu revogar os regulamentos financeiros protocolados após o último grande colapso financeiro ocorrido em 2008. A China pretende, ainda esse ano, relaxar restrições ambientais para estimular sua economia nos próximos meses, a medida não só anularia os efeitos benéficos que o isolamento social trouxe ao meio ambiente como acarretaria problemas socioeconômicos no futuro para populações afetadas pelo aumento da poluição atmosférica e consecutivamente do aquecimento global. Dessa forma, tal recuperação econômica “rápida”, prenunciada para combater os déficits resultantes da pandemia citados no parágrafo anterior, requerirá medidas negligentes e o retorno à práticas retrógradas.


<div align="center">

![](https://i.imgur.com/yn3q0t6.png)

</div>

<figcaption>Fonte: https://unsplash.com/photos/w3TwyZMlfPg_</figcaption>



A adesão ao isolamento social intensificou as necessidades particulares de um público que se restringe a efetuar compras em lojas físicas, esse “novo hábito” foi responsável por um crescimento gigantesco do e-commerce que, somente em Abril, viu suas atividades crescerem em 81% totalizando 24,5 milhões de vendas. Entre os produtos mais vendidos estão as máscaras faciais e géis antibacterianos, ambos insumos de proteção durante a pandemia que espelham uma tentativa conjunta de comércios obterem lucro a partir da venda desses itens.

Além disso, suplementos alimentares, fones de ouvido, iluminações para parede e teto, camisetas, moletons, eletrodomésticos e eletrônicos também obtiveram um crescimento substancial em suas vendas, representando o interesse dos consumidores com a manutenção de sua saúde e bem estar. A aquisição de utensílios domésticos demonstra a assimilação do mercado com seu novo dia a dia, uma nova rotina de trabalho, lazer e estudo situada completamente no conforto de seu lar.

A quarentena não só resultou no fechamento das portas de lojas físicas, ela também obrigou esses comércios a migrarem para o ambiente virtual. A ação coercitiva apressou uma etapa de enorme importância nesse processo, o preparo logístico e o estudo de mercado. Ferramentas de enorme utilidade para que um comércio conquiste espaço e clientes no disputado meio do e-commerce. Sem elas pequenos e médios empresários tendem, em virtude do medo de falência, a ingressar precariamente em uma zona com enorme concorrência e obstáculos. Por fim, empresas já consolidadas no ramo prosperam enquanto outras enfrentam danos irreparáveis.

<div align="center">

![](https://i.imgur.com/mMd41IW.png)

</div>

<figcaption>Fonte: https://unsplash.com/photos/DNn2AOy4L1Q_</figcaption>





O recesso econômico mundial era algo premeditável, a possibilidade de uma queda da atividade industrial e comercial não tardaria a chegar, entretanto a pandemia de coronavirus foi o estopim de algo muito maior. Diferente do que ocorreu em 2008, onde a eclosão da bolha do mercado imobiliário estadunidense desabrigou cerca de 20 milhões de pessoas, dessa vez a crise enfrentada é também sanitária, e apresenta desafios a serem superados não apenas nos próximos anos, mas sim nas próximas décadas. Contudo, nem todos estão fadados ao prejuízo.

Quem tem a ganhar é sobretudo as grandes empresas farmacêuticas, dispostas a comercializar em massa remédios que combatam os sintomas da Covid-19 garantindo uma receita superior ao esperado em tempos de pré-pandemia. Além disso, China e União Europeia destinam hoje milhões de dólares para iniciativas privadas que buscam desenvolver uma vacina efetiva. A ação pode parecer positiva à primeira vista, todavia, essa corrida revela um pungente sentimento nacionalista em conquistar o tão sonhado mérito por solucionar o maior desafio da atualidade.

Em meio ao pânico generalizado, lobistas de diversas áreas estão desenterrando estratégias e ideias guardadas anteriormente para atender demandas em situações como essa. Donald Trump, ao relacionar-se com esses profissionais, pretende suspender impostos sobre folhas de pagamento, acarretando numa possível ruptura do sistema previdenciário que abriria brechas para seu encerramento ou privatização. Além disso, medidas anunciadas que conduzem um subsídio emergencial a alguns dos mais ricos e tóxicos setores de sua economia, como a indústria de cruzeiros e a indústria aérea reforçam a oportunidade das mesmas lhe retornarem no futuro apoio político e financeiro.


<div align="center">

![](https://i.imgur.com/xA3LErM.png)

</div>

<figcaption>Fonte: https://unsplash.com/photos/9BXL-Vn22Do_</figcaption>



Entretanto, medidas promissoras germinaram em meio ao caos promovido pela pandemia. No Brasil vimos o avanço do Auxílio Emergencial, uma renda básica de R$ 600, possibilitada pelo Senado e sancionada mais tarde pelo Governo Federal, o projeto busca permanecer em vigência durante toda a fase de quarentena atenuando danos financeiros entre a população. Na América do Norte e, especificamente nos Estados Unidos, é discutida a ideia de um “Green New Deal” que, diferente da reforma proposta após a Crise de 1929, busca fomentar unicamente empresas auto declaradas ecológicas e que possuem atividades inerentes à danos ambientais.

Incontestavelmente, a pandemia revelou fissuras em um sistema que há décadas encaminha-se à ruína. A falta de insumos médicos, preparo político, seguros econômicos e a preponderância da desigualdade econômica, de governos autoritários e o negacionismo científico freiou a confiança de nações em seu poderio individual e as obrigou a atuar em um cenário onde a cooperação entre países tornou-se a chave para a manutenção da segurança de seus habitantes. A presença de uma vertente do capitalismo que se retroalimenta da tragédia transforma o lucro no principal inimigo da solidariedade, inanir essa prática requer reformas intensas que também também previnam episódios conceptivos para a mesma.

A história nos ensina que momentos de crise são voláteis, onde escolhas são fundamentais para decidir se o futuro da raça humana será preenchido pelas consequências do tradicionalismo ou pelos frutos de conquistas progressistas. Nas palavras de Milton Friedman, economista norte-americano, “Apenas uma crise, real - ou percebida como real - produz mudança de fato. Quando essa crise ocorre, as ações dependem de ideias que estão disponíveis no momento. Acredito que essa seja nossa principal função: desenvolver alternativas às políticas vigentes, mantê-las vivas e disponíveis até que o politicamente impossível se torne politicamente inevitável”.




<div align="center">

![](https://i.imgur.com/Rx269T1.png)

</div>

<figcaption>Fonte: https://unsplash.com/photos/1AaRGN_vyq0_</figcaption>

<article></article>

### Referências Bibliográficas



CARLA, Maria. **O CORONAVÍRUS: O PERFEITO DESASTRE PARA O CAPITALISMO DO DESASTRE**. Disponível em: https://www.sinprodf.org.br/o-coronavirus-o-perfeito-desastre-para-o-capitalismo-do-desastre/. Acesso em: 15 jun. 2020.



KLEIN, Naomi; _**The Shock Doctrine: The Rise of Disaster Capitalism**_**.** New York: Henry Holt and Company, Inc., 2008



KOKKE, Marcelo. **Capitalismo de desastre e o coronavírus: alternativas constitucionais**. Disponível em: https://domtotal.com/noticia/1432797/2020/03/capitalismo-de-desastre-e-o-coronavirus-alternativas-constitucionais/. Acesso em: 15 jun. 2020.



LARGHI, Nathália. **Compras on-line crescem na quarentena; veja produtos mais vendidos**. Disponível em: https://valorinveste.globo.com/objetivo/gastar-bem/noticia/2020/05/15/compras-on-line-crescem-na-quarentena-veja-produtos-mais-vendidos.ghtml. Acesso em: 15 jun. 2020.



MARTINS, Pedro Ivo. **Após coronavírus, pesquisa mostra crescimento das compras online de Moda**. Disponível em: https://www.ecommercebrasil.com.br/artigos/apos-coronavirus-pesquisa-mostra-crescimento-das-compras-online-de-moda/. Acesso em: 15 jun. 2020.



MORAES, Reginaldo de. **Doutrina do choque: a direita produz o caos para impor sua política**. Disponível em: https://www.cartamaior.com.br/?/Editoria/Economia-Politica/Doutrina-do-choque-a-direita-produz-o-caos-para-impor-sua-politica/7/39435. Acesso em: 15 jun. 2020.

</div>