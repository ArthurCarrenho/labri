---
slug: labri-n0027
tags: [COVID-19, CADERNOS]
title: "Como a União Europeia está lidando com a COVID-19"
author: Ana Júlia do Lago
author_title: Graduanda em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 27
image_url: https://i.imgur.com/8IUcioY.png
descricao: A União Europeia (UE) é um bloco econômico existente desde 1992, formada por 27 países integrados politica e socialmente. Dentre os objetivos e valores que compõem e norteiam tal união, é possível citar a promoção da paz e do bem-estar de seus membros...
download_pdf: /arquivos/2020-06-29-labri-027.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/8IUcioY.png)

</div>

>*A União Europeia (UE) é um bloco econômico existente desde 1992, formada por 27 países integrados politica e socialmente...*

<!--truncate-->

<div align="justify">

A União Europeia (UE) é um bloco econômico existente desde 1992, formada por 27 países integrados politica e socialmente. Dentre os objetivos e valores que compõem e norteiam tal união, é possível citar a promoção da paz e do bem-estar de seus membros; a garantia da liberdade, segurança e justiça, sem fronteiras internas; e o reforço da coesão econômica, social e territorial e a solidariedade entre os países da UE. No entanto, durante a pandemia da COVID-19, a qual teve grande impacto no continente europeu, colocou-se em questão a estabilidade da integração do bloco, suas forma de gerenciamento de crises e também alguns de seus objetivos - citados anteriormente - levando-se a uma reflexão acerca do futuro da união, com possibilidades tanto de desenvolvimento quanto de retraimento.

Em um primeiro plano analisa-se a cooperação interna ao bloco e a externa a ele no combate à pandemia. No âmbito interno, o cenário não se mostrou muito favorável, uma vez que os países da Europa eram inicialmente, os mais atingidos. Nesse viés, destaca-se a falta de coordenação e agilidade do bloco para planejar uma série de medidas que poderiam ter reduzido os impactos negativos que a pandemia gerou, de forma que muitas decisões foram diferentes para cada um dos membros, ainda que tenham sido realizadas cúpulas com os líderes visando uma melhor coordenação das ações (as quais ainda estão sendo realizadas).

Além disso, se apresenta também a questão de que muitos materiais de saúde considerados como “primeira necessidade” (como máscaras, luvas e aventais) estão em falta devido a grande demanda e produção insuficiente, de forma que, mais uma vez, pode-se notar a ausência de colaboração dos países-membros, pois a própria Comissão Europeia acabou por intensificar a vigilância de produtos nos portos e aeroportos e aumentou as restrições de exportação, buscando evitar que nações como a Alemanha, deixassem de enviar materiais para outras. Todos esses fatores citados resultam em um plano de ação pouco eficiente marcado por egoísmos nacionais.

Outra medida adotada por alguns países foi o fechamento de fronteiras internas, o que desrespeita o Acordo Schengen[^1] e a recomendação da Comissão Europeia que declara que tal decisão não é cabível, já que todos os países possuíam casos do novo coronavírus e a orientação era apenas fechar as fronteiras externas, visando a manutenção do mercado interno, sendo tal medida muito importante para a economia do bloco.

Ademais, tal decisão afetou a organização do setor da saúde dentro da UE já que, antes, era possível que muitos profissionais dessa área fossem para outros países a fim de exercerem suas funções e por outro lado, muitos pacientes podiam recorrer a hospitais e tratamentos no geral em outras nações. Dessa forma, com o impedimento da travessia, algumas cidades ficaram com falta de atendimento e leitos e impossibilidade de recorrer a outros países que estavam disponíveis nestes setores saturados.

Portanto, internamente, a crise sanitária afetou o bloco de forma que cada país passou a tomar decisões nacionalistas e se fechar cada vez mais, contrariando a ideia de uma integração. Com isso, eles tentam exercer suas respectivas soberanias mas acabam por comprometer os outros membros.

Em paralelo, observa-se a interferência externa dentro da UE, como a colaboração da Rússia realizada na Itália e da Organização do Tratado do Atlântico Norte (OTAN) na Espanha, ambas solicitações que visavam o fornecimento de mais equipamentos sanitários para a contenção da pandemia. Ademais, os dois países europeus também contaram com a ajuda da China através do envio de máscaras e outros materiais para os italianos e testes da COVID-19 para os espanhóis. Tais medidas mostram como o bloco europeu não foi capaz de assistir suficientemente todos os seus membros e esses tiveram que recorrer a auxílio externo, estreitando relações com outras nações e não seus pares estabelecidos pelo bloco. Essa postura não era esperada e vai de encontro com o objetivo - já citado anteriormente - que se refere ao reforço da coesão em diversos âmbitos, o que é notado é o enfraquecimento da solidariedade interna e o crescimento de interesses nacionalistas.

É importante ressaltar que a pandemia do novo coronavírus também trouxe prejuízos econômicos para o bloco. Foi discutido no Conselho Europeu[^2] a criação de um fundo emergencial correspondente a cerca de 500 bilhões de euros para diminuir os efeitos negativos na economia, o qual englobaria um fundo de resgate da zona do euro; um fundo de garantia do Banco Europeu de Investimento com créditos para empresas; e um fundo temporário que ajude a pagar salários de trabalhadores e assim evitar demissões, que seriam divididos entre os membros.

Tal plano teve que passar por muitos debates e inicialmente, novamente, encontravam-se divergências entre os integrantes, principalmente entre os países do norte do bloco, que se opunham a certas condições, e aos países do sul, que se mostravam favoráveis. Nesse quesito, é significativo lembrar que anos atrás, países da região sul, como Itália e Espanha, foram fortemente afetados pela crise da zona do euro, tendo até os dias atuais certos resquícios econômicos, por isso é importante que, nesse momento de crise, a União se mostre disposta a realmente cooperar e ajudar os mais afetados.

Por outro aspecto, na pandemia atual pode ainda ser observada uma oportunidade esperançosa para que a UE avance e se fortaleça caso consiga resolver os empecilhos da coordenação e passe a se organizar de forma mais eficiente, tanto no âmbito sanitário quanto no econômico, mostrando principalmente um aprofundamento na integração. No espectro da economia, surge a oportunidade de reindustrialização europeia e, consequentemente, retorno à competitividade de mercado, consolidando as potências econômicas.

Essa possibilidade se dá pois, com o cenário global abalado pós-pandemia, a UE pode voltar a investir nesse setor e se reinserir no mercado como um forte concorrente de outras potências, o que não se via desde a terceira Revolução Industrial quando a Europa, como um todo, perdeu certo protagonismo na produção e inovação tecnológica mundial, possuindo destaque com apenas algumas exceções como é o caso da Alemanha. Além disso, após a crise sanitária, é provável que haja um reordenamento global, o qual a Europa e, em especial a União Europeia, poderia assumir a liderança, segundo a diretora do centro de estudos Friends of Europe, Shada Islam[^3].

Desse modo, é possível analisar como a COVID-19 afetou e continuará afetando a União Europeia, agindo diretamente nas políticas e decisões futuras do bloco. Portanto, além das milhares de vidas perdidas devido ao vírus, perde-se também o que a UE era antes da pandemia, um bloco modelo de cooperação e ações conjuntas, deixando um futuro incerto que pode resultar em um avanço ou retraimento do União, dependendo das decisões e da forma que as mesmas serão tomadas nesse período.




[^1]: O Acordo ou Tratado Schengen delimita um espaço dentro da União Europeia onde há a livre circulação de pessoas. https://ec.europa.eu/home-affairs/sites/homeaffairs/files/e-library/docs/schengen_brochure/schengen_brochure_dr3111126_pt.pdf. Acesso em 20 maio 2020.

[^2]: UE cria fundo de emergência pós-pandemia. DW, 2020. Disponível em: https://www.dw.com/pt-br/ue-cria-fundo-de-emerg%C3%AAncia-p%C3%B3s-pandemia/a-53229430. Acesso em: 22 maio 2020.

[^3]: MIGUEL, Bernardo de. Europa trava disputa existencial contra o nacionalismo alimentado pelo coronavírus: cúpula da união europeia tenta reforçar o multilateralismo, mas divergências minam o bloco. El País. Bruxelas, p. 1-1. 01 abr. 2020. Disponível em: https://brasil.elpais.com/internacional/2020-04-01/europa-trava-disputa-existencial-contra-o-nacionalismo-alimentado-pelo-coronavirus.html. Acesso em: 22 maio 2020.


<article></article>

### Referências Bibliográficas

A UE em poucas palavras. [20--]. Disponível em: https://europa.eu/european-union/about-eu/eu-in-brief_pt. Acesso em: 20 maio 2020.

CRANE, Melinda. **Crise do coronavírus é novo teste para solidariedade na UE**: pandemia de covid-19 expõe deficiências na cooperação na área de saúde e coloca mais uma vez estados-membros diante da questão, que encontra resistência sobretudo no norte, de ajudar financeiramente países mais afetados. 2020. Disponível em: https://www.dw.com/pt-br/crise-do-coronav%C3%ADrus-%C3%A9-novo-teste-para-solidariedade-na-ue/a-53232568. Acesso em: 22 maio 2020.

GASPAR, Carlos et al. A Pandemia COVID-19: Que Impacto nas Áreas da Segurança e Defesa?. **IDN Brief**, 2020.

GIL, Andrés. Europa, incapaz de dar una respuesta ambiciosa y coordinada a la crisis del coronavirus. **El Diario.** [s. L.], p. 1-1. 21 mar. 2020. Disponível em: https://www.eldiario.es/internacional/Europa-respuesta-ambiciosa-coordinada-coronavirus_0_1007900104.html. Acesso em: 20 maio 2020.

GIMENES, Diego. Alemanha se dobra e aceita resgatar Europa em meio ao risco de cisão: após impasse, alemães e franceses selam acordo por por fundo de 500 bilhões de euros para mitigar os impactos da pandemia; tratado vai ao parlamento europeu. **Veja**, [s. L.], p. 1-1, 20 maio 2020. Disponível em: https://veja.abril.com.br/economia/alemanha-se-dobra-e-aceita-resgatar-europa-em-meio-ao-risco-de-cisao/. Acesso em: 22 maio 2020.

JULIANA, Enric. El ejército de Rusia entra en Italia; España pide ayuda a la OTAN. **La Vanguardia.** Madri, p. 1-1. 26 mar. 2020. Disponível em: https://www.lavanguardia.com/politica/20200326/48100209033/espana-coronavirus-gobierno-otan-italia-europa-epidemia.html. Acesso em: 20 maio 2020.

LINDE, Pablo; VIEJO, Manuel. OMS alerta que a falta de equipamentos de proteção põe profissionais da saúde em risco: demanda por máscaras nas farmácias espanholas cresceu 200 vezes em relação ao ano passado. : Demanda por máscaras nas farmácias espanholas cresceu 200 vezes em relação ao ano passado. **El País.** Madri, p. 1-1. 05 maio 2020. Disponível em: https://brasil.elpais.com/ciencia/2020-03-05/oms-alerta-que-a-falta-de-equipamentos-de-protecao-poe-profissionais-da-saude-em-risco.html. Acesso em: 20 maio 2020.

MERELES, Carla. **UNIÃO EUROPEIA: TUDO O QUE VOCÊ PRECISA SABER SOBRE ESSE BLOCO ECONÔMICO**. 2017. Disponível em: https://www.politize.com.br/uniao-europeia/. Acesso em: 20 maio 2020.

MIGUEL, Bernardo de. Europa trava disputa existencial contra o nacionalismo alimentado pelo coronavírus: cúpula da união europeia tenta reforçar o multilateralismo, mas divergências minam o bloco. **El País.** Bruxelas, p. 1-1. 01 abr. 2020. Disponível em: https://brasil.elpais.com/internacional/2020-04-01/europa-trava-disputa-existencial-contra-o-nacionalismo-alimentado-pelo-coronavirus.html. Acesso em: 22 maio 2020.

MIGUEL, Bernardo de; PELLICER, Lluís. Coronavírus abre outra fenda na União Europeia: alemanha e os países nórdicos impedem a ação fiscal coordenada que a frança está buscando. : Alemanha e os países nórdicos impedem a ação fiscal coordenada que a França está buscando. **El País: Alemanha e os países nórdicos impedem a ação fiscal coordenada que a França está buscando.** Bruxelas/berlim, p. 1-1. 12 mar. 2020. Disponível em: https://brasil.elpais.com/economia/2020-03-12/coronavirus-abre-outra-fenda-na-uniao-europeia.html. Acesso em: 20 maio 2020.

MIGUEL, Bernardo de. U**nião Europeia abre caminho para acordo histórico sobre fundo anticrise**. **El País.** Bruxelas, p. 1-1. 22 abr. 2020. Disponível em: https://brasil.elpais.com/economia/2020-04-22/uniao-europeia-abre-caminho-para-acordo-historico-sobre-fundo-anticrise.html. Acesso em: 20 maio 2020.

NETO, Ricardo Borges Gama. As consequências da pandemia do Covid-19 na geopolítica: notas introdutórias.

REICHLIN, Lucrezia. A covid-19 é uma oportunidade para a Europa: choque da covid-19 testará a resiliência dos sistemas de saúde pública, relações trabalhistas e mecanismos formais e informais de solidariedade em toda a ue. : Choque da covid-19 testará a resiliência dos sistemas de saúde pública, relações trabalhistas e mecanismos formais e informais de solidariedade em toda a UE. **Exame..** [s. L.], p. 1-1. 23 mar. 2020. Disponível em: https://exame.com/mundo/o-covid-19-e-uma-oportunidade-para-a-europa-2/. Acesso em: 20 maio 2020.

REDAÇÃO, da. **Perguntas e respostas: a crise financeira na Europa**. **Veja**, [s L.], p. 1-1, 08 jun. 2010. Disponível em: https://veja.abril.com.br/economia/perguntas-e-respostas-a-crise-financeira-na-europa/. Acesso em: 22 maio 2020.

**UE cria fundo de emergência pós-pandemia**. DW, 2020. Disponível em: https://www.dw.com/pt-br/ue-cria-fundo-de-emerg%C3%AAncia-p%C3%B3s-pandemia/a-53229430. Acesso em: 22 maio 2020.
</div>