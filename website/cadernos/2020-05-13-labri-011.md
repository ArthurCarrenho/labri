---
slug: labri-n0011
tags: [COVID-19, CADERNOS]
title: "Da Saúde ao Clima"
subtitulo: "Reflexos do Novo Coronavírus"
author: Hamilton Reis e Sofia Sabbag
author_title: Graduandos em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 11
image_url: https://i.imgur.com/BeHNtdo.jpg
descricao: A cooperação internacional no âmbito de contingência do aquecimento global, muito em parte sob égide da Organização das Nações Unidas (ONU), especialmente nas recentes Convenções Globais sobre Mudanças Climáticas, evidencia uma crescente tendência de colaboração internacional...
download_pdf: /arquivos/2020-05-13-labri-011.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/BeHNtdo.jpg)

</div>

>*A cooperação internacional no âmbito de contingência do aquecimento global, muito em parte sob égide da Organização das Nações Unidas (ONU), especialmente nas recentes Convenções Globais sobre Mudanças Climáticas, evidencia uma crescente tendência de colaboração internacional em prol de uma ameaça comum...*

<!--truncate-->

<div align="justify">

### Paralelos Entre as Crises Sanitária e Ambiental

A cooperação internacional no âmbito de contingência do aquecimento global, muito em parte sob égide da Organização das Nações Unidas (ONU), especialmente nas recentes Convenções Globais sobre Mudanças Climáticas, evidencia uma crescente tendência de colaboração internacional em prol de uma ameaça comum: o aumento da temperatura do planeta. A cada ano, países se reúnem para discutir o cumprimento das metas do Acordo de Paris (2015), que enuncia diversas mudanças a serem tomadas pelo governo, sociedade civil e empresas, visando, dentre várias medidas, a descarbonificação das economias. Tal cenário é exemplar para pensarmos em como os atuais governos podem enfrentar a atual crise econômica e sanitária instaurada pelo novo coronavírus no mundo, uma vez que ambas, além de representarem ameaças globais comuns, colocando em risco a integridade física da maioria dos indivíduos do planeta, poderiam ser muito mais facilmente solucionadas através de cooperação entre países.[^1][^2]

É importante compreender que crises de saúde global, como a experienciada nos dias atuais, assim como a crise ambiental estão inerentemente conectadas e são, de certa forma, decorrentes do comportamento da nossa sociedade. A influência humana no ambiente é violentamente direta[^3], apesar de se manifestar lentamente, o que pode levar a decisões equivocadas ou até mesmo a falta dessas por parte dos atores governamentais e grandes corporações. Como consta relatório do chefe do Programa Ambiental da ONU, Inger Andersen, a expansão humana rumo à habitats naturais, seja por meio da urbanização ou outros meios, implicam o contato cada vez mais próximo entre humanos e animais potenciais vetores de doenças zoonóticas (patógenos de infectam animais e humanos). Tais categorias de atividades, como expansão da agropecuária, e desflorestamento não só tornam o planeta cada vez mais quente[^4] como podem aumentar as probabilidades de infecções advindas de animais.

Portanto, constata-se que ambas crises advêm da semelhante origem: ambiental e ecológica. Se a humanidade entender que parte do seu sistema de sobrevivência perpassa manter a natureza preservada, as chances de catástrofes como a atual serão diminuídas.



### Medidas que podem ser utilizadas para as duas crises

O primeiro entendimento que pode começar a ser melhor aplicado pelo sistema internacional é o de gerenciamento de riscos a longo prazo, visto que a atual pandemia evidenciou o estresse de diversos sistemas de saúde, a inabilidade na colaboração com outros Estados e despreparo destes em relação à epidemiologia de um mundo globalizado. Tal fato ocorreu, muito em parte devido a um negligenciamento de questões de saúde globais, que já apontavam uma carência dessa área médica e sanitarista à luz do surgimento da Diplomacia de Saúde e Saúde Global[^5] no início da década. Assim, ficou evidente que esses riscos não foram contemplados pelo sistema internacional, por se tratar de algo preventivo de médio e longo prazo, da mesma forma que se aponta hoje a urgência de medidas preventivas contra o aquecimento global. Essa falta de gerência de riscos, potenciais perigos para humanidade, fica mais grave quando o Fórum Econômico Mundial, em seu Relatório Global de Riscos de 2020, apontou como ambientais as cinco maiores ameaças globais do ano, entre elas, a falha na mitigação do aquecimento global, eventos naturais extremos, perda de ecossistemas e biodiversidade, entre outras, evidenciando que as maiores ameaças poderiam surgir da má administração desses riscos ecológicos, do qual o Coronavírus também faz parte, como foi mostrado anteriormente no texto.

Ou seja, pode se afirmar que tanto a problemática da saúde quanto a do clima exigem a quebra do paradigma da irresponsabilidade dos Estados do sistema global de despriorizar uma visão mais prospectiva da realidade ambiental-ecológica atual, não se atentando a potenciais crises que podem marcar o presente e futuro. Tendo como norte o gerenciamento dos maiores riscos, reconhecimento das vulnerabilidades do sistema, da prevenção e criação de medidas de resiliência, a humanidade pode se antever e conter danos de iminentes catástrofes.

Isso leva ao segundo importante entendimento da geração atual de atores internacionais, o de que é possível rever o comportamento coletivo humano e mudá-lo radicalmente em prol de uma contingência. Exemplo que vale o destaque é o método de Supressão, reconhecido por recentes estudos da Imperial College e aplicado na Província de Hubei pelo Partido Comunista Chinês (PCC), o qual reduziu a interação humana em mais de 70%, restringindo qualquer acesso e circulação ao local. Tal medida mostrou-se extremamente eficaz na contenção do Coronavírus e, pela abrangência e coercitividade numa população tão numerosa, poderia ser considerado ‘inviável’ se não fosse pela crucial imposição da pandemia.



### Dificuldades de Cooperação

Em um primeiro momento, o relativo enredamento de governantes para a cooperação, em especial no caso europeu, prejudicou ações que poderiam dar uma melhor estimativa para o fim da crise. Esse é, possivelmente, o ensinamento mais relevante que começa a se desenhar para a comunidade estatal global, em relação ao que pode ser feito para mitigar as consequências de políticas descuidadas com o clima e com a saúde. Considerando o fenômeno da globalização, os Estados agirem por conta própria em contexto de catástrofes ambientais ou de saúde é um erro que carrega ilusão em sua constituição teórica, pois não se tratam de ecossistemas isolados: um vírus pode continuar a evoluir em outra população, sofrendo mutações e representando potencial ameaça de nova pandemia, assim como a poluição atmosférica de um país atinge outros por ocorrência das massas de ar. Assim, a problemática em um país é facilmente transbordada para outros, em um claro movimento de interdependência[^6]. Nesse sentido, é evidente que não há fronteiras geográficas para uma doença, tampouco para uma crise climática, sendo vital que a sociedade internacional reconheça e aprenda a transnacionalização desses problemas, como reflete a análise de Appleton, postada no jornal  _The Globalist_, “o progresso e prosperidade global dependem do entendimento comum de como um mundo interdependente funciona — econômica, política e socialmente”.

Ressalta-se, ainda, que grande parte da compreensão da cooperação como ferramenta para a crise atual teve como base a ideia científica popularizada por meio da expressão “achatar a curva”[^7], que se tornou viral e incentivou diversos governos a adotarem medidas de mitigação. A tarefa de transpor a cooperação observada entre os indivíduos para o nível internacional ocorre em ritmo descoordenado, sem algum tipo de liderança global capaz de direcionar uma resposta coletiva e abrangente. Tal papel, que estava historicamente ligado aos Estados Unidos da América, foi suprimido por políticas nacionalistas de Trump, introduzindo limitações ao comércio e movimento de povos a seus aliados.

É notável que, pelo menos por agora, respostas econômicas nacionalistas predominam, algo que mina a homogeneidade de abordagens de resposta à problemática. Paralelamente, a saída dos EUA do Acordo de Paris também remonta a medidas isolacionistas e anti-cooperação. Se a solidariedade em termos políticos ainda não prevaleceu, ela possivelmente surgirá como pauta em momentos críticos das crises como a falta de equipamentos e insumos do mercado médico. Dentro desse cenário, a colaboração impulsionada pela China foi especialmente interessante ao adotar medidas como a doação de respiradores, máscaras e o envio de médicos para países mais afetados. Tal tipo de posição de ajuda em face do mundo, também em adversidade, suprindo materiais necessários, só é possível através de cooperação nas áreas de produção e distribuição, e readaptação das cadeias de produção às mudanças que ocorrerem no cenário atual, o que exige um alto nível de coordenação visto a pulverização das etapas de produção. Outrossim, quanto mais efetivos forem os esforços para o desenvolvimento conjunto de uma vacina, através de colaboração técnico-científica e compartilhamento de informações, mais rapidamente os Estados poderão voltar a suas atividades econômicas.



### Considerações Finais

O coronavírus nos mostrou a escala da resposta necessária para enfrentar a crise climática, mas nenhuma das duas questões parece estar sendo manejada corretamente. A descrença e desunião causam mais danos do que a cooperação e a solidariedade, ações que devem nortear essencialmente a comunidade internacional e que serão necessárias para uma melhor administração da crise climática e seus efeitos econômicos e sociais. Apesar das crises iminentes, essa pode ser uma janela de oportunidade única para se repensar os modelos econômicos e humanos de política e sociedade que se alinhem, também, às contingências ecológicas. É necessário ouvir os avisos do planeta como emergências, e a partir disso pensar ações afirmativas de integração mundial que possam, dialeticamente, responder a tais emergências, de maneira a construir e aplicar os aprendizados para as próximas gerações. Dessa forma, quando as nações optarem por compartilhar recursos, conhecimentos e informações, recriando um modelo sustentável de sociedade, certamente pandemias e crises ambientais serão enfrentadas de maneira mais eficiente e humana.



[^1]: **Yuval Harari: Na batalha contra o coronavírus, a humanidade carece de líderes | Opinião | EL PAÍS Brasil**, disponível em: <https://brasil.elpais.com/opiniao/2020-04-13/na-batalha-contra-o-coronavirus-a-humanidade-carece-de-lideres.html>, acesso em: 29 abr. 2020.

[^2]:  **An Earth Day in the life of a plague - Covid-19 and the climate | Leaders | The Economist**, disponível em: <https://www.economist.com/leaders/2020/04/25/covid-19-and-the-climate>, acesso em: 1 maio 2020.

[^3]: JACK, Jackson, ENVIRONMENT AND SOCIETY,  _in_: , [s.l.: s.n.], 2017, p. 579–602.

[^4]:  **Emissions – Global Energy & CO2 Status Report 2019 – Analysis - IEA**, disponível em: <https://www.iea.org/reports/global-energy-co2-status-report-2019/emissions#abstract>, acesso em: 1 maio 2020.

[^5]: KICKBUSCH, Ilona; BERGER, Chantal Berger, Diplomacia da Saúde Global*,  **Revista Eletrônica de Comunicação, Informação e Inovação em Saúde**, v. 4, n. 1, 2010.

[^6]: KEOHANE, Robert O.; NYE, Joseph S.,  **Power and Interdependence**, Nova Iorque: Longman, 1989.

[^7]: BR; MARCH 16, on Specktor-Senior Writer; 2020,  **Coronavirus: What is “flattening the curve,” and will it work?**, livescience.com, disponível em: <https://www.livescience.com/coronavirus-flatten-the-curve.html>, acesso em: 1 maio 2020.



<article></article>

### Referências Bibliográficas

BR; MARCH 16, on Specktor-Senior Writer; 2020, **Coronavirus: What is “flattening the curve,” and will it work?**, livescience.com, disponível em: <https://www.livescience.com/coronavirus-flatten-the-curve.html>, acesso em: 1 maio 2020.

BRIGGS, Helen, **How the pandemic is putting the spotlight on wildlife trade**, disponível em: <https://www.bbc.com/news/science-environment-52125309>, acesso em: 1 maio 2020.

**Convenção-Quadro das Nações Unidas sobre Mudança do Clima (UNFCCC)**, disponível em: <https://www.mma.gov.br/clima/convencao-das-nacoes-unidas>, acesso em: 30 abr. 2020.

FERGUSON, N. et al, **Report 9: Impact of non-pharmaceutical interventions (NPIs) to reduce COVID19 mortality and healthcare demand**, [s.l.: s.n.], 2020.

First Person: **COVID-19 is not a silver lining for the climate, says UN Environment chief | UN News**, disponível em: <https://news.un.org/en/story/2020/04/1061082>, acesso em: 1 maio 2020.

**Has the coronavirus finally taught us how to listen to science? | MIT Office of the President**, disponível em: <http://president.mit.edu/speeches-writing/has-coronavirus-finally-taught-us-how-listen-science>, acesso em: 1 maio 2020.

**How Europe failed the coronavirus test** – POLITICO, disponível em: <https://www.politico.eu/article/coronavirus-europe-failed-the-test/>, acesso em: 1 maio 2020.

KUO, **Lily, China sends doctors and masks overseas as domestic coronavirus infections drop**, disponível em: <https://www.theguardian.com/world/2020/mar/19/china-positions-itself-as-a-leader-in-tackling-the-coronavirus>, acesso em: 1 maio 2020.

KEOHANE, Robert O.; NYE, Joseph S., **Power and Interdependence**, Nova Iorque: Longman, 1989.

KICKBUSCH, Ilona; BERGER, Chantal Berger, Diplomacia da Saúde Global*, **Revista Eletrônica de Comunicação, Informação e Inovação em Saúde**, v. 4, n. 1, 2010.

LANDLER, Mark, **A Fumbled Global Response to the Virus in a Leadership Void**, disponível em: <https://www.nytimes.com/2020/03/11/world/europe/coronavirus-leadership-trump.html>, acesso em: 1 maio 2020.

NEWBURGER, Emma, **China is donating 1,000 ventilators to help New York in coronavirus fight**, CNBC, disponível em: <https://www.cnbc.com/2020/04/04/china-is-donating-1000-ventilators-to-help-new-york-in-coronavirus-fight.html>, acesso em: 1 maio 2020, (Library Catalog: www.cnbc.com).

**Stop Blaming Globalization: Most Problems Are Homemade,** The Globalist, disponível em: <https://www.theglobalist.com/globalization-globalism-nationalism-populism-trade-inequality-education/>, acesso em: 29 abr. 2020, (Library Catalog: www.theglobalist.com).

Tackling COVID-19 Together, **Tackling COVID-19 Together**, disponível em: <https://www.globaltradealert.org/reports/51>, acesso em: 1 maio 2020.

**The Global Risks Report 2020**, disponível em: <https://wef.ch/2QfEAR9>, acesso em: 1 maio 2020.

**The Paris Agreement | UNFCCC**, disponível em: <https://unfccc.int/process-and-meetings/the-paris-agreement/the-paris-agreement>, acesso em: 29 abr. 2020.

WILSON, Audrey, **The Countries That Are Succeeding at Flattening the Curve**, disponível em: <https://foreignpolicy.com/2020/04/02/countries-succeeding-flattening-curve-coronavirus-testing-quarantine/>, acesso em: 1 maio 2020.

**Yuval Harari: Na batalha contra o coronavírus, a humanidade carece de líderes | Opinião | EL PAÍS Brasil**, disponível em: <https://brasil.elpais.com/opiniao/2020-04-13/na-batalha-contra-o-coronavirus-a-humanidade-carece-de-lideres.html>, acesso em: 29 abr. 2020.

</div>