---
slug: labri-n0043
tags: [COVID-19, CADERNOS]
title: "Sahel"
subtitulo: "couloir de tous les dangers"
author: Jonas de Paula Vieira 
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 43
image_url: https://i.imgur.com/8CknLUB.jpg
descricao: "O Sahel é uma zona (cinturão) de transição entre o Deserto do Saara (Norte da África) e a Savana (África Subsaariana)..."
download_pdf: /arquivos/2021-02-08-labri-043.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/8CknLUB.jpg)

</div>

<figcaption align="left"> (Magharebia/Creative Commons). Disponível em: 
https://memoria.ebc.com.br/2012/10/intervencao-militar-da-onu-no-mali-deve-comecar-nas-proximas-semanas</figcaption>


>*O Sahel é uma zona (cinturão) de transição entre o Deserto do Saara (Norte da África) e a Savana (África Subsaariana), estendendo-se entre o Oceano Atlântico, ao leste, e ao Mar Vermelho...*

<!--truncate-->

<div align="justify">

O Sahel é uma zona (cinturão) de transição entre o Deserto do Saara (Norte da África) e a Savana (África Subsaariana), estendendo-se entre o Oceano Atlântico, ao leste, e ao Mar Vermelho, ao oeste, o que tradicionalmente caracteriza os Estados da região por apresentarem um grande intercâmbio cultural, religioso e comercial. Entretanto, recentemente o Sahel recebeu o epíteto de “couloir de tous les dangers” (ZOUBIR, 2017, p. 134), ou corredor de todos os perigos, uma vez que o fim do regime ditatorial de Muammar al-Gaddafi na Líbia em 2011, transformou a região em palco de operações de grupos criminosos, jihadistas e rebeldes, sobretudo, no Sahel Central, composto por Níger, Mali e Burkina Faso, os quais apresentam Índice de Desenvolvimento Humano (IDH) muito baixo, respectivamente 0.377, 0.427 e 0.434 (UNDP, 2019).





Os Estados que abrangem o Sahel encontram-se fragilizados e apresentam as marcas do subdesenvolvimento, como pobreza extrema, insegurança alimentar, serviços de saneamento básico e de saúde precários, problemas climáticos (inundações, secas e tempestades de areia), grande número de refugiados e aumento dos conflitos armados (UNHCR, 2020), sobretudo com a radicalização do Islã na região (ZOUBIR, 2017). Somado a isso, a pandemia da COVID-19 adicionou mais uma ameaça ao Sahel, pois tem atingido principalmente os refugiados e os deslocados internos, que foram forçados a deixarem suas casas em decorrência dos conflitos armados ou de problemas socioambientais.

Como o Sahel Central se tornou refúgio para grupos terroristas e rebeldes, por exemplo, o Islamic State in the Greater Sahara (IS - GS), a al-Qaeda in the Islamic Maghreb (AQIM) e o Mouvement National de Libération de l'Azawad (MNLA), que operam a partir de rotas antigas de migração e comércio entre o Sahel e o Saara (ZOUBIR, 2017), o território se apresenta como uma questão de segurança internacional, sobretudo para a Europa Ocidental, devido à proximidade entre as duas regiões. Além disso, há no Sahel mais de 2,7 milhões de refugiados (UNHCR, 2020), dificultando o acesso dessas pessoas aos serviços essenciais de saúde e consequentemente ao combate à COVID-19 pelos Estados da região. Dentre estes, Burkina Faso se destaca por apresentar uma das piores infraestruturas do sistema de saúde, uma vez que teve os postos de saúde e hospitais fechados em decorrência dos conflitos, com o pessoal técnico realocado do interior para as grandes cidades do país. Logo no início da pandemia, Burkina Faso dispunha de apenas 400 kits de teste do novo coronavírus para uma população de cerca de 20 milhões de habitantes , sendo que somente três centros de saúde do país tinham capacidade de realizá-los (WILKINS, 2020).

Dessa maneira, em uma reunião virtual ocorrida em outubro de 2020 na Mesa Redonda Ministerial Internacional sobre o Sahel Central, o Alto Comissário das Nações Unidas para Refugiados, Filippo Grandi, defendeu que a Comunidade Internacional deveria agir para mitigar a crise na região, assim como o impacto da COVID-19 na mesma (UNHCR, 2020b). Para Grandi, a União Europeia deveria ter um papel de destaque na liderança dos Estados da região, assinalando para a necessidade de um novo “Plano Marshall” e a promoção de “intervenções unificadas, estratégicas e substanciais” (CLAYTON; SCHAFER, 2020, on-line, tradução minha).

Entretanto, esse posicionamento do Alto Comissário é contraditório, dado que a Operação Barkhane (2014-presente), promovida pela França e apoiada logisticamente por países europeus e pelos Estado Unidos, está em curso nos Estados conhecidos como o “G5 do Sahel”, ex-colônias francesas – Burkina Faso, Mali, Níger, Chade e Mauritânia. Essa intervenção militar tem como objetivo norteador o combate aos grupos jihadistas, criminosos e insurgentes e a manutenção da soberania dos Estados do Sahel, além disso, faz-se necessário ressaltar, que a Operação Barkhane é sucessora da Operação Serval (2013-2014), ocorrida no Mali e também encabeçada pela França. Porém, da mesma forma que a Operação Serval, a Operação Barkhane tem se mostrado ineficiente, já que a crise em toda a região foi exacerbada, e a França é acusada de defender seus interesses estratégicos na região, como a asseguração de jazidas minerais, principalmente no Níger – terceiro produtor mundial de Urânio (BEDNIK, 2008) – ou seja, levando a cabo uma guerra por recursos naturais, semelhante à ocorrida na Líbia em 2011.

Segundo Boubacar Haidara, pesquisador associado do laboratório Les Afriques dans le Monde, afirmou ao jornal francês Le Monde Diplomatique, que para os malineses “a França só intervém por interesses econômicos e estratégicos ocultos, que ela participa da desestabilização do país para legitimar sua presença, mas, acima de tudo, que ela tomou o partido dos ex-rebeldes tuaregues” (PIGEAUD, 2020, on-line). Além destas operações, a Organização das Nações Unidas (ONU) também apresenta uma missão para a manutenção da paz em curso no Mali desde 2013, a Missão de Estabilização Multidimensional Integrada das Nações Unidas no Mali (MINUSMA), que visa “garantir a segurança, estabilização e proteção de civis; apoiar o diálogo político nacional e a reconciliação; e [auxiliar] no restabelecimento da autoridade do Estado, na reconstrução do setor de segurança e na promoção e proteção dos direitos humanos [do] país” (UN PEACEKEEPING, 2020, on-line).

Para compreender a atual crise no Sahel, nesse período conhecido como “Era pós-Gaddafi” (IMAM et al., 2014) , é preciso analisar as consequências do fim do governo de Muammar al-Gaddafi na Líbia e o impacto deste nos Estados da região. Gaddafi governou a Líbia entre 1969 e 2011, tendo o seu governo chegado ao fim em outubro de 2011, quando foi capturado e executado por rebeldes durante a Guerra Civil Síria (2011) – parte dos eventos da “Primavera Árabe”. Entretanto, a morte de Gaddafi agravou os problemas de segurança na região, gerando um vácuo de poder, posto que o ditador líbio era uma peça estratégica para o equilíbrio de poder no Norte da África e no Sahel (AUSTRALIA, 2012). Dessa forma, a desestabilização do governo líbio resultou no retorno de cerca de 3000 mil mercenários tuaregues – lutaram na Guerra Civil Síria como aliados de Gaddafi – ao Sahel, o que deu início à organização política-militar do MNLA, e “contribuiu para a deterioração da situação de segurança no Mali e em outros Estados do Sahel” (AUSTRALIA, 2012, p. 2, tradução minha). Já que proclamaram a independência de Azawad, território no norte do Mali. Além disso, também houve um aumento no fluxo de contrabando de armamentos líbios nesta região, já que os grupos contrabandistas se aproveitaram da debilidade da infraestrutura de segurança dos países do Sahel para intensificarem suas operações (ZOUBIR, 2017), agravando a crise. Dentre esses armamentos, destacam-se “armas leves” e os man-portable air-defense systems (AUSTRALIA, 2012) – sistemas portáteis de defesa antiaérea com mísseis superfície-ar.

Faz-se necessário destacar o papel que os tuaregues desempenharam no Sahel após a queda de Gaddafi. O número de tuaregues é especulado em torno de três milhões, e tradicionalmente são um povo de pastores nômades que habitam uma faixa territorial entre o Deserto do Saara e o Sahel, abrangendo o norte do Níger, o sul da Argélia, o norte do Mali e parte de Burkina Faso (KEENAN, 2012). Como um povo marginalizado pelos governos destes Estados, os tuaregues promoveram diversas rebeliões (1962-1964; 1990-1995; 2007-2009) na região, entretanto, a rebelião tuaregue (2012-2013) ocorrida no território de Azawad chama a atenção, em razão do apoio militar que recebeu dos mercenários tuaregues que lutaram na Guerra Civil Síria, além do rico suprimento de armamentos contrabandeados da Líbia (AUSTRALIA, 2012).

A rebelião tuaregue de 2012, foi liderada pelo MNLA, e defendia a independência de Azawad do Mali, região esta com péssimas condições socioeconômicas, alto índice de desemprego e precários sistemas de saúde e de educação. Logo após o início da rebelião, o presidente do Mali, Amadou Toumani Touré, sofreu um golpe de Estado (2012) pelo exército malinês, o qual acusava-o de sua inércia em combater os insurgentes e o grupo jihadista Ansar Dine, que recebia apoio da AQIM e também operava a partir do norte do Mali, objetivando impor a Xaria ao país (BBC, 2012). Dessa forma, a França levou a cabo a Operação Serval (autorizada pelo Conselho de Segurança das Nações Unidas), a qual visava a restauração territorial do Mali e a necessidade de guerra global contra o terrorismo, já que os grupos jihadistas ameaçavam conquistar Bamako, capital do Mali, após estes expulsarem os separatistas do MNLA das principais cidades da região de Azawad (CHARBONNEAU, 2019).

Com certo sucesso em mitigar as forças jihadistas em Azawad, e sob a égide de conter o terrorismo internacional, a França substituiu a Operação Serval pela Operação Barkhane, a qual recebeu apoio da MINUSMA e extendeu sua atuação para os Estados do G5 do Sahel (CHARBONNEAU, 2019). Todavia, ao contrário da primeira, a Operação Barkhane é uma intervenção militar permanente, que deu autonomia (com a exceção de Mauritânia) para a França operar livremente em condições de emergência, sobretudo, no Níger e no Mali, sendo autorizada pelo Conselho de Segurança das Nações Unidas (CSNU) de utilizar “todos os meios necessários para apoiar elementos da MINUSMA (CHARBONNEAU, 2019, on-line, tradução minha).

Todavia, tanto a MINUSMA quanto a Operação Barkhane, têm sido criticadas por setores socioeconômicos desses Estados, pois além da França ser acusada de defender os seus próprios interesses na região como já apresentado no texto, ambas as intervenções normalizam e legitimam o uso da força como uma medida de “socorrer” países em situação de vulnerabilidade, mascarando uma investida neocolonial francesa e dos países do Norte Global no Sahel (PIGEAUD, 2020), a partir de um compromisso mundial, o qual modifica a gestão e governança da segurança regional e nacional, utilizando para isto de mecanismos de governança global e das redes de organizações transnacionais (com influência na política internacional), para normalizar “o uso da força com base em alegações sobre o combate ou prevenção do terrorismo e do extremismo violento” (CHARBONNEAU, 2019, on-line, tradução minha), na medida em que aumentam a influência econômica e comercial das grandes potências mundiais em regiões pertencentes à periferia global (WALLERSTEIN, 2000).

Portanto, pode-se inferir que as políticas de securitização e as intervenções militares perpetradas no Sahel não são a chave para a estabilidade da região (ZOUBIR, 2017; AUSTRALIA, 2012), e assim como ocorreu na Líbia, na qual a ONU e as grandes potências do Norte Global agravaram a crise no país, colapsando a balança de poder do Magrebe e gradualmente do Sahel com a queda de Gaddafi, que era uma espécie de “cetro para a democracia” (AUSTRALIA, 2012, p. 12, tradução minha), a influência dos Estados do G5 também tem sido subestimada. O que tem gerado uma maior adesão das populações dos Estados do Sahel aos grupos extremistas, uma vez que estes se tornam substitutos das autoridades estatais e providenciam serviços básicos de subsistência para as pessoas, ademais, outra marca da radicalização da região pode ser compreendida a partir da perda de espaço do Sufismo para o Salafismo (ZOUBIR, 2017), o qual se apresenta como uma abordagem fundamentalista do Islã.

O crescimento do jihadismo no Sahel põe fim à tolerância religiosa que existia na região, sobretudo em Burkina Faso, onde cristãos estão sendo mortos através de ataques a igrejas (BBC, 2019). Além disso, o país passa por um processo de etnização, com diferentes milícias disputando o controle de regiões estratégicas, dentre estas, destaca-se o confronto entre os koglweogo (grupo de fazendeiros de maioria étnica Mossi) e os Fulani (grupo étnico de pastores nômades); estes últimos têm sido massacrados pelos koglweogo, já que são acusados de ter ligações com grupos jihadistas (CARAYOL, 2020). Burkina Faso também sofre com a ascensão de grupos de autodefesa que aumentam a insegurança da região, pois são formadas por civis que atuam como vigilantes, objetivando fazer “justiça com as próprias mãos”, e atacam, principalmente, grupos jihadistas (CAYROL, 2020).

Com base no que foi apresentado ao longo do texto, evidencia-se que a instabilidade político-militar na Síria reverberou nos Estados do Sahel, os quais já estavam fragilizados em decorrência de crises socioeconômicas, agravando a insegurança destes, uma vez que o vácuo de poder propiciou que grupos insurgentes, criminosos e terroristas encontrassem na região condições ideais para atuarem (ausência de governabilidade). Ademais, soma-se a esses problemas a pandemia da COVID-19, a qual tem atingido, principalmente, as populações vulneráveis (refugiados e deslocados internos).

Uma nova intervenção militar proposta por Grandi não é a solução para a estabilidade da região, a exemplo da operação Barkhane e a missão de paz da ONU, MINUSMA, que mostraram-se infrutíferas, e têm sido criticadas por defenderem os interesses das grandes potências no Sahel. No entanto, há necessidade de uma mobilização da comunidade mundial para mitigar os problemas socioeconômicos e de segurança da região, além da crise sanitária agravada pela COVID-19, desde que essa mobilização seja articulada entre os Estados do G5 do Sahel e aqueles extrarregionais, e esteja em conexão com ações humanitárias e de desenvolvimento sustentável. Por fim, para que ocorra a estabilização da região, é necessário se atentar às raízes políticas e sociais dos conflitos, assim como é essencial que o enfrentamento e neutralização dos grupos armados ocorra sob uma ótica regional, a qual é capaz de compreender que estes operam em um ambiente complexo e sem respeitar os limites fronteiriços entre os Estados.


### Referências Bibliográficas

AUSTRALIA. Exploring the post-Gadaffi Repercussions in the Sahel. Australian Government: Department of Foreign Affairs and Trade, The Nordic Africa Institute, p. 1-22, jun. 2012

BBC. Mali Tuareg rebels declare independence in the north. BBC, [S.I], 06 abr. 2012. Africa. Disponível em: https://www.bbc.com/news/world-africa-17635437. Acesso em: 11 nov. 2020.

BBC. Burkina Faso's war against militant Islamists. BBC, [S.I], 30 maio 2019. Africa. Disponível em: https://www.bbc.com/news/world-africa-39279050. Acesso: 12 nov. 2020.

BEDNIK, Anna. O urânio na raiz do conflito. Le Monde Diplomatique Brasil, [S.I], 04 jul. 2008. Edições. Disponível em: https://diplomatique.org.br/o-uranio-na-raiz-do-conflito-2/. Acesso em: 11 nov. 2020.

CARAYOL, Rémi. Les milices prolifèrent au Burkina Faso. Le Monde Diplomatique, [S.I], maio 2020. Archives. Disponível em: https://www.monde-diplomatique.fr/2020/05/CARAYOL/61740. Acesso em: 05 nov. 2020.

CHARBONNEAU, Bruno. The Military Intervention in Mali and Beyond: An Interview with Bruno Charbonneau. Oxford Research Group, [S.I], 28 mar. 2019. [S.I]. Disponível em: https://www.oxfordresearchgroup.org.uk/blog/the-french-intervention-in-mali-an-interview-with-bruno-charbonneau. Acesso em: 13 nov. 2020.

CLAYTON, Jonathan; SCHAFER, Sarah. International community must act with ‘urgency’ to end crisis in Central Sahel. UNHCR, Genebra/Copenhage, 21 out. 2020. News. Disponível em: https://www.unhcr.org/news/latest/2020/10/5f8f1e26a/international-community-must-act-urgency-end-crisis-central-sahel.html. Acesso em: 03 nov. 2020.

DEVERMONT, Judd; HARRIS, Marielle. Why Mali Needs a New Peace Deal. Center for Strategic and International Studies, [S.I], 15 abr. 2020. [S.I]. Disponível em: https://www.csis.org/analysis/why-mali-needs-new-peace-deal. Acesso em: 07 nov. 2020.

IMAN, Mukhtar et al. Libya In The Post Ghadaffi Era. The International Journal of Social Sciences and Humanities Invention, [S.I], Volume 2, p. 1150-1166, 2014. 

KEENAN, Jeremy. Mali’s Tuareg rebellion: What next? Al Jazeera, [S.I], 20 mar. 2012. Opinion. Disponível em: https://www.aljazeera.com/opinions/2012/3/20/malis-tuareg-rebellion-what-next/. Acesso em: 03. dez. 2020. 

MSF. Burkina Faso: como a epidemia de COVID-19 piorou a crise humanitária. MSF, [S.I], 24 abr. 2020. Notícias. Disponível em: https://www.msf.org.br/noticias/burkina-faso-como-epidemia-de-covid-19-piorou-crise-humanitaria. Acesso em: 02 dez. 2020. 

PIGEAUD, Fanny. Basta de presença francesa na África! Le Monde Diplomatique Brasil, [S.I], 28 fev. 2020. Edições. Disponível em: https://diplomatique.org.br/basta-de-presenca-francesa-na-africa/. Acesso em: 11 nov. 2020. 

UNDP. 2019 Human Development Index Ranking. UNDP, [S.I]. [S.I]. Disponível em: http://hdr.undp.org/en/content/2019-human-development-index-ranking. Acesso em: 02 nov. 2020. 

UNHCR. UNHCR warns of mounting needs in Sahel as forced displacement intensifies. UNHCR, [S.I], 16 out. 2020. Briefing Notes. Disponível em: https://www.unhcr.org/news/briefing/2020/10/5f894b234.html. Acesso em: 03 nov. 2020. 

UN PEACEKEEPING. MINUSMA Fact Sheet. UN PEACEKEEPING, [S.I], 01 dez. 2020. [S.I]. Disponível em: https://peacekeeping.un.org/en/mission/minusma. Acesso em: 05 dez. 2020. 

WALLERSTEIN, Immanuel. The Essential Wallerstein. New York: The New Press, 2000. 

WILKINS, Henry. In Burkina Faso, COVID-19 fight complicated by war, displacement. Al Jazeera, [S.I], 16 mar. 2020. Health. Disponível em: https://www.aljazeera.com/news/2020/03/16/in-burkina-faso-covid-19-fight-complicated-by-war-displacement/. Acesso em: 12 nov. 2020. 

ZOUBIR, Yahia. Security Challenges, Migration, Instability and Violent Extremism in the Sahel. IEMed, Dossier: Geopolitical Turmoil and its Effects in the Mediterranean Region, p. 134-140, 2017. 

</div>