---
slug: labri-n0026
tags: [COVID-19, CADERNOS]
title: "Universidades Brasileiras e Estrangeiras no Combate a Pandemia"
author: Lucas Quispe e Gabriela Nayara Santos
author_title: Graduandos em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 26
image_url: https://i.imgur.com/30EK2Qk.png
descricao: As instituições de ensino superior - sejam nas diferentes áreas do conhecimento - têm cada uma a sua relevância e função no momento atual. As áreas do conhecimento referente às ciências biológicas aparecem em evidência no combate ao novo coronavírus...
download_pdf: /arquivos/2020-06-24-labri-026.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/30EK2Qk.png)

</div>

>*A partir do discutido na publicação anterior, as instituições de ensino superior - sejam nas diferentes áreas do conhecimento - têm cada uma a sua relevância e função no momento atual...*

<!--truncate-->

<div align="justify">

A partir do discutido na  [publicação anterior](https://www.google.com/url?q=https%3A%2F%2Fwww.labriunesp.org%2Fnovo-coronavirus%2Fpublica%25C3%25A7%25C3%25B5es%2Fgt-2-15062020&sa=D&sntz=1&usg=AFQjCNG8RXWFNKrVDuzXEBKe8EmdgDaemQ), as instituições de ensino superior - sejam nas diferentes áreas do conhecimento - têm cada uma a sua relevância e função no momento atual. As áreas do conhecimento referente às ciências biológicas aparecem em evidência no combate ao novo coronavírus, visto que se os cientistas dessa área conseguirem desenvolver um tratamento eficaz para todos os infectados ou/e ainda desenvolverem uma vacina contra o novo coronavírus, serão responsáveis pelo fim pandemia e toda a cadeia dela dependente, como o fim da quarentena e a consequente possível melhora da economia mundial. Observa-se, desse modo, uma maior valorização dos profissionais da saúde e cientistas a nível acadêmico.

Profissionais ligados à ciências exatas são responsáveis, por exemplo, pela elaboração de uma melhor coordenação logística para a  [construção de hospitais](https://www.google.com/url?q=https%3A%2F%2Fsaude.estadao.com.br%2Fnoticias%2Fgeral%2Cem-12-dias-china-constroi-11-hospitais-para-tratar-vitimas-do-coronavirus%2C70003185890&sa=D&sntz=1&usg=AFQjCNH0EVZAeq7x_Smo8qYBMiMfSlqZhg)  de campanha em um pequeno intervalo de tempo, trabalham com a construção de estatísticas e observam o comportamento da contaminação do vírus pelo mundo e, com isso, descrevem matematicamente o comportamento do fenômeno, o que oferece base a previsões sobre o número de infectados em determinado período de tempo em um país, sobre até quando uma quarentena seria necessária. Informações essas que, segundo o  [Professor Sérgio](https://www.google.com/url?q=https%3A%2F%2Fwww.labriunesp.org%2Fnovo-coronavirus%2Fpublica%25C3%25A7%25C3%25B5es%2Fgt-2-15062020&sa=D&sntz=1&usg=AFQjCNG8RXWFNKrVDuzXEBKe8EmdgDaemQ), são essenciais para desenvolver políticas públicas eficientes e com base científica.

Já as ciências humanas e sociais, como abordado pelo  [professor Marcelo Passini Mariano](https://www.google.com/url?q=https%3A%2F%2Fwww.labriunesp.org%2Fnovo-coronavirus%2Fpublica%25C3%25A7%25C3%25B5es%2Fgt-2-15062020&sa=D&sntz=1&usg=AFQjCNG8RXWFNKrVDuzXEBKe8EmdgDaemQ), aparecem pelo fato da pandemia não se limitar à área da saúde, como a busca pela vacina, nem em termos estatísticos, como dados referentes às porcentagens do isolamento social. Profissionais das ciências humanas e sociais têm por função nesse cenário analisá-los, já que contextos geopolíticos e sociais são diretamente afetados pela pandemia, observam o comportamento das pessoas diante do distanciamento social, como este funciona em diversas partes da cidade, como impacta a economia global e quais seriam suas consequências, os exemplos são inúmeros, além da observação comportamental de órgãos sociais, desde nações até organizações internacionais .

Com isso, é evidente que não há frente menos importante no combate à pandemia, mesmo que algumas mostrem-se mais em evidência do que outras por motivos midiáticos ou por estarmos tratando de uma crise de saúde global. Por isso, é importante estar ciente do que fazem as universidades nacionais e internacionais em um cenário de quarentena e de crise mundial e entender o motivo do por quê estar voltando a valorização das ciências e dos centros que as produzem. Nos textos a seguir, será abordado a atuação da ciência nacional e internacional, algumas das descobertas que foram/estão sendo relevantes no contexto vivenciado, contribuindo para o entendimento da frase citada pelo  [professor Carlos](https://www.google.com/url?q=https%3A%2F%2Fwww.labriunesp.org%2Fnovo-coronavirus%2Fpublica%25C3%25A7%25C3%25B5es%2Fgt-2-15062020&sa=D&sntz=1&usg=AFQjCNG8RXWFNKrVDuzXEBKe8EmdgDaemQ)  durante a entrevista: “ser cientista é a profissão do futuro’’.

### Universidades Brasileiras no Combate à Pandemia

![](https://i.imgur.com/hAD6dsd.png)

<figcaption>Fonte: Cristiano Alvarenga (UFU)</figcaption>



Diante do cenário vivenciado mundialmente por consequência da pandemia do novo coronavírus, as universidades estão lutando ativamente para a produção e aplicação de conhecimentos que possam contribuir no combate. Com foco nas Universidades brasileiras podemos observar uma contribuição significativa. Abaixo serão citados alguns conhecimentos produzidos:

-   Pesquisadores de Instituições ligadas à Universidade Federal do Rio de Janeiro como  [_Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa de Engenharia (Coppe)_  e do  _Instituto de Biologia (IB)_](https://www.google.com/url?q=https%3A%2F%2Fmarsemfim.com.br%2Funiversidades-na-frente-do-combate-ao-coronavirus%2F&sa=D&sntz=1&usg=AFQjCNEuKExR-o25YJyyO-dO8nKGS_rAyg)  trabalham no desenvolvimento de um teste mais barato, rápido e simples para a COVID-19 quando comparado ao  _PCR (Reação em Cadeia da Polimerase)_. “A iniciativa não requer infraestrutura sofisticada para ser realizada e se baseia em uma técnica cerca de quatro vezes mais barata que a empregada no teste de  _PCR_. A tecnologia poderá ser facilmente transferida para reprodução em grande escala em diversos locais. A proposta é detectar os dois tipos de anticorpos, possibilitando tanto determinar se uma pessoa com sintomas respiratórios é positiva para covid-19 quanto, por exemplo, mapear pessoas que já tenham sido infectadas anteriormente, mesmo assintomáticas.”, explica a pesquisadora e coordenadora do trabalho, Leda Castilho.
-   No intuito de desenvolver testes rápidos e em grandes quantidades, a  [_Universidade Federal de Minas Gerais_](https://www.google.com/url?q=https%3A%2F%2Fmarsemfim.com.br%2Funiversidades-na-frente-do-combate-ao-coronavirus%2F&sa=D&sntz=1&usg=AFQjCNEuKExR-o25YJyyO-dO8nKGS_rAyg) _(UFMG)_  _também segue trabalhando._ “Atualmente, ele é feito por meio de um protocolo complexo. O novo teste tem a grande vantagem de ser aplicável de forma massiva”, explicou Flávio Fonseca à  _Globo News. Ele,_  professor do  _Instituto de Ciências Biológicas da UFMG,_  ainda complementou. “A mão de obra, quem trabalha na bancada, são os alunos da  _UFMG_. Todos são bolsistas de pós-graduação ou pós-doutorado.”
-   “Cientistas brasileiras, Ester Sabino, diretora do Instituto de Medicina Tropical da USP e Jaqueline Goes de Jesus, pós-doutoranda na USP, em apenas 48 horas sequenciaram o  [genoma](http://www.google.com/url?q=http%3A%2F%2Fwww.blog.saude.gov.br%2Findex.php%2Fperguntas-e-respostas%2F54104-confira-a-entrevista-sobre-o-sequenciamento-do-coronavirus&sa=D&sntz=1&usg=AFQjCNEkwk2JeN7xmYLhunHTdRSxp26FJg) do coronavírus (COVID-19) do primeiro caso da doença confirmado no Brasil.” ([Sequenciamento do coronavírus possibilita o desenvolvimento de vacinas](http://www.google.com/url?q=http%3A%2F%2Fwww.blog.saude.gov.br%2Findex.php%2Fperguntas-e-respostas%2F54104-confira-a-entrevista-sobre-o-sequenciamento-do-coronavirus&sa=D&sntz=1&usg=AFQjCNEkwk2JeN7xmYLhunHTdRSxp26FJg)/ 2020)
-   “De olho na sobrecarga de internações que pode ocorrer no sistema de saúde, pesquisas da Escola Politécnica da USP se destacam com três projetos de  [ventiladores pulmonares](https://www.google.com/url?q=https%3A%2F%2Fjornal.usp.br%2Funiversidade%2Fusp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia%2F&sa=D&sntz=1&usg=AFQjCNHvgYdrTkP5KCgt65cdkDA_fkXa5w)  de baixo custo, todos pensados para serem fabricados de forma rápida e projetados com peças simples, que podem ser adquiridas no país.” Pesquisadores da Escola Politécnica da USP
-   “Diagnósticos (frentes de estudo na Universidade de São Paulo): a criação de  [testes rápidos](https://www.google.com/url?q=https%3A%2F%2Fjornal.usp.br%2Funiversidade%2Fusp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia%2F&sa=D&sntz=1&usg=AFQjCNHvgYdrTkP5KCgt65cdkDA_fkXa5w)  para detectar a presença do vírus, a definição sobre a existência de transmissão vertical do vírus pela gravidez e até indicadores que detectam uma insuficiência respiratória antes dela acontecer.”
-   “[Simuladores, plataformas e algoritmos](https://www.google.com/url?q=https%3A%2F%2Fjornal.usp.br%2Funiversidade%2Fusp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia%2F&sa=D&sntz=1&usg=AFQjCNHvgYdrTkP5KCgt65cdkDA_fkXa5w): A Faculdade de Ciências Farmacêuticas da USP (FCF) tem realizado vários estudos usando ferramentas computacionais aplicadas à saúde pública. Uma das pesquisas, por exemplo, analisa dados de localização de celulares para avaliar a circulação de pessoas na cidade de São Paulo a partir do início da quarentena. Os resultados podem ajudar a melhorar as políticas de distanciamento social.”([USP contra a covid-19: conheça as várias ações da Universidade para ajudar no combate à pandemia](https://www.google.com/url?q=https%3A%2F%2Fjornal.usp.br%2Funiversidade%2Fusp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia&sa=D&sntz=1&usg=AFQjCNFQWr0euUURb2gV5IJNzP48q-EZqw)/2020)



### Universidades Estrangeiras no Combate à Pandemia



O estrangeiro também produz historicamente ciência de qualidade, desde Pequim à Lisboa, ou até mesmo, mais recentemente, desde a Cidade do México até o Vale do Silício. A seguir, veremos como estão as universidades internacionais no esforço para frear a pandemia e combater o vírus:



-   No Reino Unido, na Universidade de Newcastle, foi criada [uma equipe de voluntários](https://www.google.com/url?q=https%3A%2F%2Fdoi.org%2F10.1038%2Fs41591-020-0885-5&sa=D&sntz=1&usg=AFQjCNFrQKHjsrVTjxRXjWG6FfRHPH1Z0g), contendo desde estudantes e administradores até professores da instituição, para trabalhar dentro de um dos laboratórios do local manuseando amostras de doentes e construindo materiais de proteção com o que estava disponível na faculdade, como impressoras 3D. Ainda no Reino Unido, na ‘’University of South Wales’’, conseguiu-se criar  [um teste rápido](https://www.google.com/url?q=https%3A%2F%2Fwww.bbc.com%2Fnews%2Fuk-wales-52347827&sa=D&sntz=1&usg=AFQjCNGS4EhjnsSgboxYEnoYqx2yAQwsvA)  para a verificação da doença. Os testes são portáteis e evitam a necessidade da amostra passar por laboratório antes de se obter o resultado, já que o mesmo é obtido em um período de tempo de 20 a 30 minutos após iniciada a testagem.



-   Ainda, as universidade recebem  [investimentos do Estado britânico](https://www.google.com/url?q=https%3A%2F%2Fagenciabrasil.ebc.com.br%2Finternacional%2Fnoticia%2F2020-05%2Fcoronavirus-reino-unido-anuncia-investimento-bilionario-em-vacinas&sa=D&sntz=1&usg=AFQjCNFksdnFmiKGiq_gtqSw0iKCGwN0hA)  para os esforços de chegar a uma vacina e, com isso, despertam interesses de farmacêuticas para o controle da patente e do licenciamento da vacina. O governo britânico vai investir mais 240 mi Libras Esterlinas (R$ 1,5 bilhão) em pesquisa e produção de vacinas para o combate ao novo coronavírus. O investimento em pesquisas será compartilhado entre dois centros de pesquisa em universidades, que são os candidatos mais avançados à descoberta da vacina no Reino Unido, a Universidade de Oxford e o Imperial College, e outros laboratórios. Somado ao investimento estatal, a Universidade de Oxford receberá também o financiamento da AstraZaneca para que, caso a universidade seja exitosa em sua pesquisa, a farmacêutica será a distribuidora da vacina no país.



-   Na China, ‘’Cientistas de um laboratório afiliado à Universidade de Pequim, estão testando um  [novo medicamento](https://www.google.com/url?q=https%3A%2F%2Folhardigital.com.br%2Fcoronavirus%2Fnoticia%2Fchineses-testam-medicamento-que-pode-impedir-o-avanco-da-covid-19%2F100922&sa=D&sntz=1&usg=AFQjCNEZJrImgp9Z6VoRT7BdeKpW1fTByQ)  para combate à  [Covid-19](https://www.google.com/url?q=https%3A%2F%2Folhardigital.com.br%2Fcoronavirus%2F&sa=D&sntz=1&usg=AFQjCNElOcq8QfgDJB3etNNaJU_IwD1H4A)  que tem o potencial de impedir o avanço da  [pandemia](https://www.google.com/url?q=https%3A%2F%2Folhardigital.com.br%2Fcoronavirus%2Fnoticia%2Fcomo-executivos-do-setor-de-tecnologia-projetam-o-mundo-pos-pandemia%2F99756&sa=D&sntz=1&usg=AFQjCNFFznEf3ZddPGsoRhW3wcZ_9cZTiA). Segundo seus criadores, ela pode não só reduzir o tempo necessário para recuperação dos doentes pelo fato de neutralizar o vírus, como também conferir “imunidade temporária” aos saudáveis que a recebe.’’ A droga seria feita a base de anticorpos de seres humanos que venceram a doença e daria aos pacientes imunidade de curto prazo, entre semanas até poucos meses. Projeta-se o início do uso desse medicamento no final do ano, algo que seria de grande ajuda ao combate de uma suposta segunda onda no hemisfério norte, territórios que estariam passando por sua estação mais fria do ano.



-   Os profissionais da saúde trabalham com uma doença que pode reagir de diferentes formas nos corpos dos infectados e isso atrapalha a maneira que cuidam deles. Pensando nisso, pesquisadores da área de doenças infecciosas e de análise preditiva da Universidade de Nova York estão trabalhando com  [Inteligência Artificial](https://www.google.com/url?q=https%3A%2F%2Ftheconversation.com%2Fwe-designed-an-experimental-ai-tool-to-predict-which-covid-19-patients-are-going-to-get-the-sickest-136125&sa=D&sntz=1&usg=AFQjCNFR4y01i4mSA6Vl6nezmsp1Fp2XWg)  para tentar prever como o corpo de cada paciente irá reagir à infecção: se irá ser um quadro assintomático ou se irá progredir para um mais sério. Seus estudos começaram ainda em Janeiro e, mantendo contato com cientistas chineses, conseguiram fornecer ao seu algoritmo uma base de comparação entre o estado do paciente no início da infecção e no fim dela. O instrumento de previsão pode ajudar aos profissionais de linha de frente a separar os pacientes de mais alto risco e os de menos. Junto a isso, os sintomas que o algoritmo identificou sugerem que o vírus não ataca apenas o sistema respiratório do corpo, informações que podem ser úteis para cientistas.



-   Sobre a transmissão do vírus de grávidas para seus filhos,  [pesquisadores](https://www.google.com/url?q=https%3A%2F%2Factualidad.rt.com%2Factualidad%2F346288-embarazadas-gestantes-covid-virus-bebes&sa=D&sntz=1&usg=AFQjCNF-wFpfMkYCnWEz2vsUmwO-YeUidA)  associados à Universidade de Ciência e Tecnologia de Huazhong descobriram que ele não é passado da mãe para o filho recém nascido. Os testes foram feitos com mulheres infectadas na cidade de Wuhan e em nenhum dos casos foi encontrado o vírus no recém nascido. Médicos dizem que a cesariana é a mais indicada para a ocasião de mães infectadas, apesar de que por meio de parto natural, nenhuma criança nasceu infectada durante os testes.



### Cooperação entre Universidades 



A fim de encontrar melhores resultados e combater mais rapidamente ao novo coronavírus, universidades de todo o mundo estão colaborando umas com as outras, seja compartilhando tecnologias, equipamentos, como conhecimentos já produzidos. Então, parcerias entre essas universidades estão sendo criadas trazendo benefícios mútuos e contribuindo para o fim da pandemia. Por exemplo:

“Vacina, riscos calculados: Um dos estudos em destaque para desenvolvimento da vacina para a covid-19 é feito numa parceria entre o Hospital das Clínicas da Faculdade de Medicina da USP, a Universidade de Oxford, na Inglaterra, e a Universidade de Berna, na Suíça. Ele utiliza na elaboração da vacina as  [VLPs](https://www.google.com/url?q=https%3A%2F%2Fjornal.usp.br%2Funiversidade%2Fusp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia%2F&sa=D&sntz=1&usg=AFQjCNHvgYdrTkP5KCgt65cdkDA_fkXa5w)  -  [Virus Like Particles](https://www.google.com/url?q=https%3A%2F%2Fbdigital.ufp.pt%2Fhandle%2F10284%2F5931&sa=D&sntz=1&usg=AFQjCNGrh4WYzswj1jrW5V53L_NtpVQsEg)  - que são estruturas de múltiplas proteínas que carregam características de vírus, mas excluem sua capacidade de replicação, pois não contêm o genoma viral. As VLPs têm sido usadas com sucesso em humanos para o desenvolvimento de vacinas e evitam a utilização de patógenos atenuados ou inativados, o que comprometeria a segurança.”

“Novos tratamentos, dos testes à aprovação: em um dos estudos, o Hospital das Clínicas da Faculdade de Medicina da USP, em parceria com a Universidade de Cornell, nos EUA, investiga a incidência e gravidade da covid-19 em pacientes que fazem uso contínuo de medicamentos  [antirretrovirais](https://www.google.com/url?q=https%3A%2F%2Fjornal.usp.br%2Funiversidade%2Fusp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia%2F&sa=D&sntz=1&usg=AFQjCNHvgYdrTkP5KCgt65cdkDA_fkXa5w)  ou da  [hidroxicloroquina](https://www.google.com/url?q=https%3A%2F%2Fjornal.usp.br%2Funiversidade%2Fusp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia%2F&sa=D&sntz=1&usg=AFQjCNHvgYdrTkP5KCgt65cdkDA_fkXa5w)  para o tratamento da infecção por HIV e de doenças autoimunes. Eles serão comparados às pessoas que não fazem uso de qualquer medicação com o objetivo de identificar estratégias de tratamento e prevenção contra o coronavírus.”

“Pesquisadores da Universidade Federal da Bahia (UFBA) descobriram uma forma mais rápida de identificar a presença do novo  [coronavírus](https://www.google.com/url?q=https%3A%2F%2Fexame.abril.com.br%2Fnoticias-sobre%2Fcoronavirus%2F&sa=D&sntz=1&usg=AFQjCNHOb7xsuQrPfINTZhDoaD4HXyCGrg)  no corpo. As 48 horas de espera pelo diagnóstico foram reduzidas para 3, com o uso de um equipamento chamado  [Real-Time](https://www.google.com/url?q=https%3A%2F%2Fsaude.estadao.com.br%2Fnoticias%2Fgeral%2Cpesquisadores-brasileiros-reduzem-espera-por-diagnostico-de-coronavirus-de-48-para-3-horas%2C70003190651&sa=D&sntz=1&usg=AFQjCNHf1-CHh52Qu4j4ZvbY5-PEP9wE9w). O equipamento, que custa R$ 150 mil e foi importado dos Estados Unidos em dezembro do ano passado para o Laboratório de Virologia da universidade, é capaz de verificar se o material genético (RNA) da secreção respiratória contém o gene do coronavírus.”

Cooperações entre universidades estrangeiras também estão produzindo resultados.  [Um estudo conjunto](https://www.google.com/url?q=https%3A%2F%2Fwww.bbc.com%2Fnews%2Fuk-england-tyne-52399289&sa=D&sntz=1&usg=AFQjCNEas1sCriwO_rSscnSKVRQfIf687w)  da Universidade de Newcastle e da Universidade de Santiago de Compostela está observando o esgoto de diversas regiões e, com isso, analisando a propagação do vírus em diversas localidades. Isso só é possível porque no esgoto, há material genético remanescente, por isso é possível encontrar certas quantidades do vírus e, assim, comparar as diferenças quantitativas, permitindo uma pré noção de o quanto o vírus está presente naquele lugar.



<article></article>

### Referências Bibliográficas

**Buenas noticias para las embarazadas: gestantes con covid-19 no transmiten el virus a sus bebés** Disponível em: [https://actualidad.rt.com/actualidad/346288-embarazadas-gestantes-covid-virus-bebes](https://www.google.com/url?q=https%3A%2F%2Factualidad.rt.com%2Factualidad%2F346288-embarazadas-gestantes-covid-virus-bebes&sa=D&sntz=1&usg=AFQjCNF-wFpfMkYCnWEz2vsUmwO-YeUidA).Acesso em: 21 de maio de 2020.

**Chineses testam medicamento que pode impedir o avanço da Covid-19.** Disponível em:[https://olhardigital.com.br/coronavirus/noticia/chineses-testam-medicamento-que-pode-impedir-o-avanco-da-covid-19/100922](https://www.google.com/url?q=https%3A%2F%2Folhardigital.com.br%2Fcoronavirus%2Fnoticia%2Fchineses-testam-medicamento-que-pode-impedir-o-avanco-da-covid-19%2F100922&sa=D&sntz=1&usg=AFQjCNEZJrImgp9Z6VoRT7BdeKpW1fTByQ). Acesso em: 21 de maio de 2020.

Coronavirus: University's rapid test could be used 'in weeks'. Disponível em: [https://www.bbc.com/news/uk-wales-52347827](https://www.google.com/url?q=https%3A%2F%2Fwww.bbc.com%2Fnews%2Fuk-wales-52347827&sa=D&sntz=1&usg=AFQjCNGS4EhjnsSgboxYEnoYqx2yAQwsvA). Acesso em: 19 de Maio de 2020

**Coronavírus: Reino Unido anuncia investimento bilionário em vacinas.**Disponível em: [https://agenciabrasil.ebc.com.br/internacional/noticia/2020-05/coronavirus-reino-unido-anuncia-investimento-bilionario-em-vacinas](https://www.google.com/url?q=https%3A%2F%2Fagenciabrasil.ebc.com.br%2Finternacional%2Fnoticia%2F2020-05%2Fcoronavirus-reino-unido-anuncia-investimento-bilionario-em-vacinas&sa=D&sntz=1&usg=AFQjCNFksdnFmiKGiq_gtqSw0iKCGwN0hA). Acesso em: 19 de maio de 2020.

**Coronavirus: Newcastle University tracking virus in sewage.** Disponível em:[https://www.bbc.com/news/uk-england-tyne-52399289](https://www.google.com/url?q=https%3A%2F%2Fwww.bbc.com%2Fnews%2Fuk-england-tyne-52399289&sa=D&sntz=1&usg=AFQjCNEas1sCriwO_rSscnSKVRQfIf687w). Acesso em 20 de maio de 2020.

**Em 12 dias, China constrói 11 hospitais para tratar vítimas do coronavirus**. Disponível em: [https://saude.estadao.com.br/noticias/geral,em-12-dias-china-constroi-11-hospitais-para-tratar-vitimas-do-coronavirus](https://www.google.com/url?q=https%3A%2F%2Fsaude.estadao.com.br%2Fnoticias%2Fgeral%2Cem-12-dias-china-constroi-11-hospitais-para-tratar-vitimas-do-coronavirus&sa=D&sntz=1&usg=AFQjCNGRl9Rscw0VWoLSAg6p6q1I7UBeVQ) Acesso em: 20 de abr. de 2020.

Pesquisadores brasileiros reduzem espera por diagnóstico de coronavírus de 48 para 3 horas. Disponível em: [https://saude.estadao.com.br/noticias/geral,pesquisadores-brasileiros-reduzem-espera-por-diagnostico-de-coronavirus-de-48-para-3-horas,70003190651](https://www.google.com/url?q=https%3A%2F%2Fsaude.estadao.com.br%2Fnoticias%2Fgeral%2Cpesquisadores-brasileiros-reduzem-espera-por-diagnostico-de-coronavirus-de-48-para-3-horas%2C70003190651&sa=D&sntz=1&usg=AFQjCNHf1-CHh52Qu4j4ZvbY5-PEP9wE9w). Acesso em: 13 de maio de 2020.

Sequenciamento do coronavírus possibilita o desenvolvimento de vacinas. Disponível em: [http://www.blog.saude.gov.br/index.php/perguntas-e-respostas/54104-confira-a-entrevista-sobre-o-sequenciamento-do-coronavirus](http://www.google.com/url?q=http%3A%2F%2Fwww.blog.saude.gov.br%2Findex.php%2Fperguntas-e-respostas%2F54104-confira-a-entrevista-sobre-o-sequenciamento-do-coronavirus&sa=D&sntz=1&usg=AFQjCNEkwk2JeN7xmYLhunHTdRSxp26FJg). Acesso em: 05 de maio de 2020.

Trost, M. Supporting the UK National Health Service during the COVID-19 crisis from an academic perspective. Nat Med (2020).

Universidades na frente do combate ao coronavírus. Disponível em: [https://marsemfim.com.br/universidades-na-frente-do-combate-ao-coronavirus/](https://www.google.com/url?q=https%3A%2F%2Fmarsemfim.com.br%2Funiversidades-na-frente-do-combate-ao-coronavirus%2F&sa=D&sntz=1&usg=AFQjCNEuKExR-o25YJyyO-dO8nKGS_rAyg). Acesso em: 01 de maio de 2020.

USP contra a covid-19: conheça as várias ações da Universidade para ajudar no combate à pandemia. Disponível em: [https://jornal.usp.br/universidade/usp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia/](https://www.google.com/url?q=https%3A%2F%2Fjornal.usp.br%2Funiversidade%2Fusp-contra-a-covid-19-conheca-as-varias-acoes-da-universidade-para-ajudar-no-combate-a-pandemia%2F&sa=D&sntz=1&usg=AFQjCNHvgYdrTkP5KCgt65cdkDA_fkXa5w) Acesso em: 03 de maio de 2020.

**We designed an experimental AI tool to predict which COVID-19 patients are going to get the sickest** Disponível em: [https://theconversation.com/we-designed-an-experimental-ai-tool-to-predict-which-covid-19-patients-are-going-to-get-the-sickest-136125](https://www.google.com/url?q=https%3A%2F%2Ftheconversation.com%2Fwe-designed-an-experimental-ai-tool-to-predict-which-covid-19-patients-are-going-to-get-the-sickest-136125&sa=D&sntz=1&usg=AFQjCNFR4y01i4mSA6Vl6nezmsp1Fp2XWg). Acesso em 20 de maio de 2020.
</div>