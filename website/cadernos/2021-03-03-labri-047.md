---
slug: labri-n0047
tags: [COVID-19, CADERNOS]
title: "A corrida pela vacina"
author: Pedro Rapace e Sofia Navarro
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 47
image_url: https://i.imgur.com/cZao0wP.jpg
descricao: Quando o assunto é o novo coronavírus, não importa a ótica teórica aplicada no âmbito das relações internacionais...
download_pdf: /arquivos/2021-03-03-labri-047.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/cZao0wP.jpg)

</div>

>*Quando o assunto é o novo coronavírus, não importa a ótica teórica aplicada no âmbito das relações internacionais; todas convergem para o fato de que todos os países estão em uma busca incessante por uma vacina que provoque uma volta à normalidade estrutural...*

<!--truncate-->

<div align="justify">

Quando o assunto é o novo coronavírus, não importa a ótica teórica aplicada no âmbito das relações internacionais; todas convergem para o fato de que todos os países estão em uma busca incessante por uma vacina que provoque uma volta à normalidade estrutural. O que muda é a motivação central; para o realismo torna-se uma competição, na qual o primeiro país a conquistar a vacina ganhará ainda mais poder sobre as outras nações, para o construtivismo a motivação é intrinsecamente interna e externa e seu sucesso depende fortemente de quem está no executivo do país, para o neoliberalismo considera-se todas as esferas que poderiam contribuir, dando ênfase às Organizações Internacionais e suas funções na conjuntura, e nesse sentido diversas outras teorias também expressam suas diferentes posturas.


Outro elemento que possuem em comum, é o uso da palavra “corrida”, mesmo que em diferentes significados. O aspecto geopolítico da vacina é tão inerente à ela quanto os efeitos do vírus, principalmente ao se analisar que as principais potências são as que tiveram uma vantagem inicial na produção e que fora criado um único sistema de acompanhamento, com uma fase pré-clínica, 3 fases de testes em humanos, uma fase de aprovação limitada e a fase final de final aprovação.



Como existem mais de 100 vacinas sendo desenvolvidas ao redor do mundo, e aproximadamente 45 em fase de testes clínicos em humanos, discorrer sobre e acompanhar todas essas seria algo difícil, especificamente para alguém que não é da área de biologia. Por isso, esse texto analisará algumas das vacinas mais avançadas, no mínimo na fase 3, na qual milhares de pessoas são testadas e são consideradas efetivas quando protegem no mínimo 50% dos vacinados. Além disso, será analisado como os países estão se comportando internacionalmente, e os ensinamentos que uma possível vacina trará para o sistema. Uma vez que desde o início de março, dezenas de vacinas já passaram para a fase 1, é nítido o esforço constante dos países para uma rápida resposta, entretanto as dúvidas são sempre relevantes; Caso um país obtenha sucesso, como o sistema conseguirá gerar resposta imunológica em 80 bilhões de indivíduos? Seria possível uma efetividade tão rapidamente, visto que a vacina mais rápida até hoje demorou 5 anos no processo?



A corrida pela vacina gerou respostas urgentes de países menos desenvolvidos, uma vez que muitos, como o Brasil, tiveram que alterar o processo de suas Agências Nacionais para que fosse mais simples a entrada da vacina no país. Além desse obstáculo burocrático, outro aspecto importante é o político. A tensão sino-estadunidense pode não caracterizar um conflito como a Guerra Fria, porém é inegável que a influência competitiva que exercem sobre países menores se assemelha ao período anterior. Presidentes alegando que não confiam na vacina chinesa, proibições de testes estadunidenses em nações em desenvolvimento, todos esses elementos geopolíticos dificultam ainda mais uma possível cooperação entre potências, a qual já fora discutida como possível pelos Estados Unidos e China - entretanto não basta apenas a cooperação restrita entre os países, os Estados menores também precisam ajudar.



Sendo esse o cenário exposto, entra também a questão midiática, que exerce uma função de _soft power_[^1], muitas vezes dizendo o que os dirigentes escondem. Um exemplo são os meios de comunicação estadunidenses frequentemente alegando que a vacina russa (Sputnik V) e algumas chinesas (CanSino, entre outras) foram aceleradas demais e não são confiáveis, apresentando sérios riscos por terem sido aprovadas enquanto os cientistas ainda esperavam os resultados da fase 3 dos testes. Vale ressaltar que não há vacina estadunidense na fase de aprovação, apenas na fase 3, como a da farmacêutica Moderna (a qual fora atrasada por questões de disputas de patente), a Novavax e a da Johnson & Johnson - além da AstraZaneca europeia que recebeu 1.2 bilhões de dólares dos EUA. Pode se dizer que, nesse quesito, uma competição surge entre os países, podendo ser interpretada como positiva, como motivação, ou como mais um obstáculo à cooperação, dependendo da ótica.



Postas tamanhas dificuldades e obstáculos em questão, nada mais natural que surjam dúvidas a respeito da viabilidade de uma possível cooperação a nível mundial entre os países que venha a ser efetiva. Deste modo, nada mais justo que assim como no início do texto possamos nos orientar segundo distintas maneiras de enxergar o funcionamento do meio internacional em que vivemos agora. Se formos levar em consideração a ótica do sistema-mundo capitalista de Immanuel Wallerstein por exemplo, com certeza teríamos uma resposta negativa a tal indagação. Uma vez que essa teoria postula que a divisão internacional do trabalho gera a concentração de capital e da produção especializada intensiva nos países centrais do sistema (Europa Ocidental, EUA, Japão, etc), temos a dominância destes perante os países periféricos. Como o próprio termo ‘'periferia'' sugere, material de ponta, inovações tecnológicas, pesquisas avançadas e até mesmo uma vacina para a Covid-19, que é o caso específico, seriam coisas que chegariam por último na região periférica do sistema-mundo capitalista. Isso se daria visto que durante certo período de tempo somente as nações centrais seriam realmente capazes de desenvolver a vacina, porém de nada seria vantajoso para as mesmas distribuir sem custos econômicos ou estratégicos a nível global o fruto de seu investimento com países de periferia. A perseverança do status quo é o fio condutor da teoria, e a manutenção de vantagens estratégicas ou pelo menos o usufruto delas em benefício próprio seria o caminho mais provável a vir tornar-se realidade.



Contudo, nem só de pessimismo se compõe o cenário internacional atual, e outras concepções teóricas podem ser utilizadas visando contrapor a já apresentada. Um bom exemplo disso é a teoria da interdependência complexa de Robert Keohane e Joseph Nye. Segundo ambos, atualmente chegamos em um ponto da história por meio da globalização em que os Estados se encontram unidos econômica e politicamente de tal forma que tomadas de decisão em determinada nação refletem e são sentidas no sistema internacional, e consequentemente em seus atores como um todo. A interdependência complexa funciona principalmente através da utilização de canais múltiplos de relação entre as diversas sociedades: são eles interestaduais, transgovernamentais e transnacionais. Esse tipo mais conciso de coordenação entre as instâncias governamentais dos países favorece a cooperação internacional do sistema, e gera assim uma expectativa mútua de colaboração. Além disso, se aliarmos o fator da interdependência com o advento das organizações internacionais em geral, temos uma chance concreta de cooperação entre os Estados frente a pandemia do novo Coronavírus.



A Organização das Nações Unidas (ONU) por exemplo, vem mostrando desde 1945 que tem capacidade de governança global e de resolução de conflitos; ela possui diversas agências especializadas como a Organização Mundial da Saúde, que se empenha na prevenção e tratamento de doenças, e que inclusive através de seu trabalho veio a erradicar a varíola em 1980. A organização aconselhou os governos constantemente durante toda a pandemia sobre o novo vírus, e apelou para o trabalho conjunto na produção de uma vacina e transparência de dados entre os países. Existem ainda outras inúmeras organizações internacionais atualmente, as quais atuam nos mais variados setores, inclusive da saúde e pesquisa, o que possibilita um grande potencial de ação coordenada contra a Covid-19 por parte dos membros dessas instituições.



O mundo vive uma crise epidemiológica sem precedentes, que além das centenas de milhares de perdas humanas fez sentir seus efeitos no setor econômico, o que fez com que a economia mundial retraísse e muitos países entrassem em recessão. Dada essa situação ímpar em que vivemos, podemos enxergar o mundo através da lente de perspectivas diferentes como foram apresentadas, porém é inegável a necessidade de maior cooperação internacional e uma resposta conjunta por parte dos países, uma vez que estão todos sendo afetados pela pandemia, sem exceção. É por este motivo que a vacina deve ou pelo menos deveria ser tratada como um objetivo em comum a ser atingido e posteriormente compartilhado entre todos, de forma a controlar o contágio da Covid-19. Não interessa a ninguém uma pessoa doente, nem mesmo distante em outro país: supondo que as pessoas recebam a vacina nos países desenvolvidos, mutações virais acontecem com grande freqüência, ou seja, as pessoas nos países subdesenvolvidos não podem ser simplesmente jogadas à própria sorte, pois o vírus voltaria a infectar a todos novamente. Deste modo, a cooperação frente a pandemia se faz mais que apenas uma possibilidade, ela mostrou ser uma necessidade.

Essa necessidade, como apresentada, possui diversas maneiras de ocorrer. Uma cooperação periferia-centro, onde os países menores seriam mais um campo de pesquisa, com testes em humanos, acelerando as fases - sendo a que está se vendo como mais presente na atualidade - uma cooperação centro-centro representada mais fortemente com os Estados Unidos e Reino Unido, por muitos sendo interpretada como menos efetiva, ou até mesmo uma cooperação em nível global, como a iniciativa da UNICEF de liderar a aquisição e fornecimento em nível global da vacina[^2]. Além destas três, existem inúmeras formas de desdobramento de cenários futuros, com a participação de ONGs fortes, fundações de grande importância internacional como a Bill & Melinda Gates, empresas multinacionais e governos regionais, entre outros. Como ressaltado pela teoria da interdependência, a formulação de cenários prospectivos é bem complexa e analítica nesse contexto, já que diversos órgãos assumem diversas funções que não estão pré-estabelecidas. 



 [^1]: Conceito originado por Joseph Nye (1977), que envolve três pilares: valores políticos, cultura e política externa, e como estes são representados e demonstrados na sociedade, não necessariamente pelo Estado apenas, mas por diversos órgãos. 

[^2]: IMPRENSA. UNICEF vai liderar a aquisição e o fornecimento de vacinas contra a Covid-19 na maior e mais rápida operação global desse tipo. Disponível em: https://www.unicef.org/brazil/comunicados-de-imprensa/unicef-vai-liderar-aquisicao-e-o-fornecimento-de-vacinas-contra-covid-19. Acesso em: 04 set. 2020.

<article></article>

### Referências Bibliográficas

CORUM, Jonathan; WEE, Sui-Lee; ZIMMER, Carl. Coronavirus Vaccine Tracker. New York Times Disponível em:https://www.nytimes.com/interactive/2020/science/coronavirus-vaccine-tracker.html. Acesso em: 8 out. 2020.



 IMPRENSA. A UNICEF vai liderar a aquisição e o fornecimento de vacinas contra a Covid-19 na maior e mais rápida operação global desse tipo. Disponível em: https://www.unicef.org/brazil/comunicados-de-imprensa/unicef-vai-liderar-aquisicao-e-o-fornecimento-de-vacinas-contra-covid-19. Acesso em: 04 set. 2020.



KEOHANE, R. O; NYE, J. Power and Interdependence: World Politics in Transition. Boston: Little Brown, 1977. 



REUTERS. Trump se diz disposto a trabalhar com a China por vacina. Disponível em: ttps://noticias.r7.com/internacional/trump-se-diz-disposto-a-trabalhar-com-a-china-por-vacina-21072020. Acesso em: 07 out. 2020.

</div>
