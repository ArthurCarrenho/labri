/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
 import React from 'react';
 import clsx from 'clsx';
 import {useTOCHighlight} from '@docusaurus/theme-common';
 import TOCItems from '@theme/TOCItems';
 import styles from './styles.module.css';
 
 const LINK_CLASS_NAME = 'table-of-contents__link';
 const LINK_ACTIVE_CLASS_NAME = 'table-of-contents__link--active';
 /* eslint-disable jsx-a11y/control-has-associated-label */
 
 function TOC({className, ...props}){
   return (
     <div className={clsx(styles.tableOfContents, 'thin-scrollbar', className)}>
       <TOCItems
         {...props}
         linkClassName={LINK_CLASS_NAME}
         linkActiveClassName={LINK_ACTIVE_CLASS_NAME}
       />
     </div>
   );
 }
 
 export default TOC;