import React, { useState } from 'react';
import clsx from 'clsx';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Link from "@docusaurus/Link";
import Layout from "@theme/Layout";
import BlogPostItem from '@theme/BlogPostItem';
import BlogListPaginator from '@theme/BlogListPaginator';
import { PageMetadata } from '@docusaurus/theme-common';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext, Dot } from 'pure-react-carousel';
import "pure-react-carousel/dist/react-carousel.es.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { ThemeClassNames } from "@docusaurus/theme-common";
import styles from "./styles.module.css";


function BlogListPage(props) {
  const { metadata, items, assets, truncated } = props;
  const {
    siteConfig: { title: siteTitle },
  } = useDocusaurusContext();
  const { blogDescription, blogTitle, permalink } = metadata;
  const isBlogOnlyMode = permalink === "/";
  const title = isBlogOnlyMode ? siteTitle : blogTitle;
  const [buttonVisible, setButtonVisible] = useState(false);
  const slides = [
    <header className={clsx(styles.heroBanner, styles.slide)}
      style={{ backgroundImage: "radial-gradient(circle, #051937, #233049, #3e485c, #5a616f, #787c83)" }}>
      <div className={clsx("container", styles.center)}>
        <h1 className={clsx(styles.h1HeroTitle, "hero__title")}>Apresentação</h1>
        <a href="https://labriunesp.org/docs/cadernos/intro" target="_blank"><p className={clsx(styles.pSlider, "button button--secondary")}><b>ISSN 2764-7552</b></p></a> 
        <p className={clsx(styles.pSlider)}>Notícias LANTRI são uma publicação circunscrita a divulgar os trabalhos de grupos e indivíduos vinculados ao LabRI/UNESP. Os principais objetivos desta publicação são:</p>
        <p className={clsx(styles.pSlider)}>1. Fornecer um espaço aos grupos para divulgação de seus trabalhos.</p>
        <p className={clsx(styles.pSlider)}>2. Auxiliar uma melhor comunicação dos projetos e dos produtos derivados</p>
        <p className={clsx(styles.pSlider)}>3. Estímulo à utilização de tecnologias digitais no cotidiano acadêmico</p>
        <div className={styles.button}>
          <Link className={clsx("button button--outline button-secondary button-lg")} style={{ color: "white" }} to="https://labriunesp.org/docs/cadernos/intro"
            target="_blank">
            Leia mais
          </Link>
        </div>
      </div>
    </header>,
    <div className={clsx(styles.heroBanner, styles.slide)}
      style={{ backgroundImage: "radial-gradient(circle, #051937, #233049, #3e485c, #5a616f, #787c83)" }}>
      <div className="container">
        <h1 className={clsx(styles.h1HeroTitle, "hero__title")}>Expediente</h1>
        <a href="https://labriunesp.org/docs/cadernos/intro" target="_blank"><p className={clsx(styles.pSlider, "button button--secondary")}><b>ISSN 2764-7552</b></p></a> 
        <p className={clsx(styles.pSlider)}>O editor principal é o docente que  está responsável pela coordenação do LabRI/UNESP.</p>
        <p className={clsx(styles.pSlider)}>Porém, as séries temáticas e de grupos de pesquisa terão como editor responsável o coordenador da respectiva série temática ou grupo de pesquisa.</p>
        <p className={clsx(styles.pSlider)}>- Editor principal: Marcelo Passini Mariano</p>
        <p className={clsx(styles.pSlider)}>- Assistentes editoriais: Júlia dos Santos Silveira, Cintia Iório, Rafael de Almeida</p>
        <div className={styles.button}>
          <Link className={clsx("button button--outline button-secondary button-lg")} style={{ color: "white" }} to="https://labriunesp.org/docs/cadernos/intro"
            target="_blank">
            Leia mais
          </Link>
        </div>
      </div>
    </div>,
  ]
  return (
    <Layout
      title={title}
      description={blogDescription}
      wrapperClassName={ThemeClassNames.wrapper.blogPages}
      pageClassName={ThemeClassNames.page.blogListPage}>
      <header>
        <h1 className={clsx(styles.title, "text--center margin-top--lg margin-bottom--xs")}>
          {blogTitle}
        </h1>
        <div className="text--center margin-top--md">
          <a href="/cadernos/tags" style={{ marginRight: 22 }} title="Tags">
            <img itemProp="image" className={styles.iconeHome} src="/img/blog/tags_default.png" />
          </a>
        </div>
      </header>
      <section className={styles.wrapper}>
        <CarouselProvider
          className={styles.carousel}
          totalSlides={slides.length}
          isPlaying
          interval={8000}
          dragEnabled={false}
          isIntrinsicHeight
          infinite
          naturalSlideHeight={undefined}
          naturalSlideWidth={undefined}>
          <div
            onMouseEnter={() => {
              setButtonVisible(true);
            }}
            onMouseLeave={() => {
              setButtonVisible(false);
            }}>
            <Slider>
              {slides.map((elem, idx) => (
                <Slide key={idx} index={idx}>
                  {elem}
                </Slide>
              ))}
            </Slider>
            <ButtonBack
              id="back"
              className={clsx(
                styles.button,
                styles.carouselbutton,
                styles.backbutton,
              )}
              style={{ visibility: buttonVisible ? "visible" : "hidden" }}>
              <FontAwesomeIcon icon={faChevronLeft} />
            </ButtonBack>
            <ButtonNext
              className={clsx(
                styles.button,
                styles.carouselbutton,
                styles.nextbutton,
              )}
              style={{ visibility: buttonVisible ? "visible" : "hidden" }}>
              <FontAwesomeIcon icon={faChevronRight} />
            </ButtonNext>
            <div className={styles.carouseldots}>
              {slides.map((__, idx) => (
                <Dot className={styles.carouseldot} key={idx} slide={idx} />
              ))}
            </div>
          </div>
        </CarouselProvider>
      </section>
      <main className={styles.wrapper}>
        <div className={styles.CardContainer}>
          {items.map(({ content: BlogPostContent }) => (
            <BlogPostItem
              key={BlogPostContent.metadata.permalink}
              frontMatter={BlogPostContent.frontMatter}
              assets={BlogPostContent.assets}
              metadata={BlogPostContent.metadata}
              truncated={BlogPostContent.metadata.truncated}>
              <BlogPostContent />
            </BlogPostItem>
          ))}
        </div>
        <BlogListPaginator metadata={metadata} />
      </main>
    </Layout>
  )
}


export default BlogListPage;
