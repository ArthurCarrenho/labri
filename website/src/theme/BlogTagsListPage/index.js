import React from 'react';
import BlogTagsListPage from '@theme-original/BlogTagsListPage';

export default function BlogTagsListPageWrapper(props) {
  return (
    <>
      <BlogTagsListPage {...props} />
    </>
  );
}
