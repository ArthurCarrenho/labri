import React from 'react';
import BlogListPaginator from '@theme-original/BlogListPaginator';

export default function BlogListPaginatorWrapper(props) {
  return (
    <>
      <BlogListPaginator {...props} />
    </>
  );
}
