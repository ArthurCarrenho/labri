import React from 'react';
import BlogTagsPostsPage from '@theme-original/BlogTagsPostsPage';

export default function BlogTagsPostsPageWrapper(props) {
  return (
    <>
      <BlogTagsPostsPage {...props} />
    </>
  );
}
