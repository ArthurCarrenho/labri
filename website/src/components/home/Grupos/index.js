import React from "react";
import clsx from "clsx";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.scss";

// cards inferiores (grupos pesquisa, extensão e parceiros)

export function Grupos({ title, imageUrl, description, link }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.info, "card col col--4")}>
      <div className="card-content">
        <div className="card__header">
          <h3>{title}</h3>
          {imagemInfo && (
            <div className="text__center">
              <img
                className={styles.featureImage}
                src={useBaseUrl(imageUrl)}
                alt={title}
              />
            </div>
          )}
        </div>
        <div style={{ minHeight: "8rem" }} className="card__body">
          <p>{description}</p>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>
            Acesse aqui
          </a>
        </div>
      </div>
    </div>
  );
}
