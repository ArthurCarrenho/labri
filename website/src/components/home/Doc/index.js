import React from "react";
import useBaseUrl from "@docusaurus/useBaseUrl";
import clsx from "clsx";
import styles from "./styles.module.scss";

// grid composto por duas colunas: azul (equipe e publicações) e vermelha (tecnologias digitais e oficinas/cursos)

export function Doc({ title, text, link, imageUrl, bgColor }) {
  const imagem = useBaseUrl(imageUrl);
  return (
    <div
      className={clsx(styles.doc, bgColor)}
      style={{ backgroundColor: bgColor }}
    >
      {imagem && (
        <div className={styles.ImagemDoc}>
          <img src={useBaseUrl(imageUrl)} alt={title} />
        </div>
      )}
      <aside>
        <h3>{title}</h3>
        <p>{text}</p>
        <a className="button button--secondary" href={link}>
          Saiba Mais
        </a>
      </aside>
    </div>
  );
}
