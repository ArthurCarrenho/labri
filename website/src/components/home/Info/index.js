import React from "react";
import useBaseUrl from "@docusaurus/useBaseUrl";
import clsx from "clsx";
import styles from "./styles.module.scss";

// cards superiores

export function Info({ title, description, link, imageUrl }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.info, "card col col--4")}>
      <div className="card-content">
        <div class="card__header">
          <h3>{title}</h3>
          {imagemInfo && (
            <div className="text__center">
              <img
                className={styles.featureImage}
                src={useBaseUrl(imageUrl)}
                alt={title}
              />
            </div>
          )}
        </div>
        <div className="card__body">
          <p>{description}</p>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>
            Saiba Mais
          </a>
        </div>
      </div>
    </div>
  );
}
