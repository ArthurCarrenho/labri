import React from "react";
import useBaseUrl from "@docusaurus/useBaseUrl";
import clsx from "clsx";
import styles from "./styles.module.scss";

export function SobreP({ title, logo, imgLogo, imageUrl, description, link }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.InfosP, "cardProjects")}>
      <div className="card-container">
        <div>
          <img src={imgLogo} alt={logo} width="180" height="110" />
        </div>
        <div className="card__body">
          <h4 className={clsx(styles.h4Projetos)}>{description}</h4>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>
            <b>Acesse aqui</b>
          </a>
        </div>
      </div>
    </div>
  );
}
