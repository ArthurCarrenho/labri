import React from "react";
import styles from "./styles.module.scss";
import useBaseUrl from "@docusaurus/useBaseUrl";

export function Projeto({ imageUrl, title, description, link }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>
        <h3>{title}</h3>
        <p>{description}</p>
        <a className="button button--secondary" href={link}>
          Saiba Mais
        </a>
      </div>
    </div>
  );
}
