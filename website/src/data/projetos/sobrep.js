export const sobrep = [
  {
    title: "Projetos de Ensino",
    imgLogo: "/img/projetos/ensino/logo-projetos-ensino.svg",
    description:
      "Os projetos de ensino são iniciativas voltadas à promoção de oficinas, material de apoio e cursos que incentivem e auxiliem uma melhor incorporação de ferramentas tecnológicas nas atividades de pesquisa.",
    link: "/projetos/ensino/",
  },
  {
    title: "Projetos de Sistema",
    imgLogo: "/img/projetos/logo-projetos-sistemas2.svg",
    description:
      "Os projetos de sistemas são iniciativas que visam promover uma melhor utilização  dos recursos computacionais, bem como a implementação e o desenvolvimento de software e serviços voltados para as pesquisas de Relações Internacionais. Com isso, neste espaço é possível encontrar projetos relacionados à construção e manutenção de pequenas e versáteis estações de trabalho multifuncionais que dão suporte às atividades de pesquisa, ensino e extensão. Ademais, também é possível encontrar alguns softwares e serviços utilizados e/ou mantidos pelo LabRI/UNESP.",
    link: "/projetos/sistemas",
  },
];
