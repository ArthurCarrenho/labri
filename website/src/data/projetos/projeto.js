export const dados = [
  {
    title: "Hemeroteca de Política Externa Brasileira",
    imageUrl: "./img/projetos/dados/hemeroteca-peb/logo-hemeroteca-peb-01.svg",
    description:
      "A Hemeroteca de Política Externa Brasileira contém uma seleção de matérias publicadas, no período de 1972 a 2010, por alguns jornais brasileiros dentre os quais se destacam: O Estado de S. Paulo, Folha de S. Paulo e Gazeta Mercantil. O objetivo desta Hemeroteca é permitir aos pesquisadores interessados o acesso a notícias que foram selecionadas e classificadas ao longo dos anos, de 1972 até 2010, sobre importantes acontecimentos atinentes às relações internacionais do Brasil.",
    link: "/docs/projetos/dados/hemeroteca-peb/info",
    tipo: "dados",
  },
  {
    title: "NewsCloud",
    imageUrl: "./img/projetos/dados/news-cloud/newscloud-logo.svg",
    description:
      "O projeto NewsCloud considera as notícias vinculadas pelos jornais impressos como uma importante fonte para pesquisas acadêmicas. Nesses veículos de comunicação, além de informações nacionais e internacionais relevantes, são encontrados opiniões de importantes atores políticos. Porém, o conjunto de informações de cada jornal se encontram em bases de dados distintas sem um mecanismo que viabilize uma busca agregada; a indexação integral dos dados vinculados apresenta limitações que dificultam a pesquisa avançada (utilização de operadores booleanos) através da busca por palavras-chaves, especialmente, quando selecionamos um período temporal longo e abarcamos o grande volume de informação; as informações veiculadas em formato textual não estão estruturadas, isso dificulta o cruzamento de metadados importantes (título, autor, caderno, entre outros). Devido a isso, o objetivo geral do Projeto NewsCloud é coletar, indexar, tratar e estruturar as informações veiculadas por jornais impressos. Mais especificamente, este projeto visa (1) coletar integralmente os jornais impressos selecionados, realizando o devido tratamento das informações veiculadas para uma melhor utilização dos dados para pesquisas acadêmicas; (2) subsidiar pesquisas acadêmicas que utilizam jornais impressos como fontes de informação ou objeto de estudo; (3) fornecer um instrumento básico para análise das informações veiculadas; (4) indicar possibilidades e instrumentos que auxiliem análises mais detalhadas das informações veiculadas.",
    link: "/docs/projetos/dados/newscloud/geral/info",
    tipo: "dados",
  },
  {
    title: "DiáriosBR",
    imageUrl: "img/projetos/dados/diariosbr/diariosbr.svg",
    description:
      "O projeto DiáriosBR reúne a movimentação legal dos governos federal, estadual e municipal que são publicadas nos Diários Oficiais, sendo esse fator o que os tornam uma fonte de pesquisa importante para o acompanhamento da destinação de recursos, transferência de cargos e o embasamento legal das atividades da administração pública. Os Diários são uma importante fonte tanto para pesquisas acadêmicas como também para uma melhor participação social no cotidiano da administração pública. Apesar destes dados serem públicos muitas vezes são disponibilizados em formatos, como PDF, que dificultam uma análise de dados mais aprofundada e rápida sendo necessário um grande tempo despendido para o tratamento destes dados para viabilizar uma análise mais acurada.",
    link: "/docs/projetos/dados/diariosbr/info",
    tipo: "dados",
  },
  {
    title: "Full Text",
    imageUrl: "img/projetos/dados/full-text/full-text.svg",
    description:
      "O projeto Full Text visa auxiliar o tratamento de dados, convertendo e/ou extraindo tais dados para formato textual passível de indexação e/ou melhor estruturação.Em geral, grande parte dos dados disponibilizados publicamente na internet estão em formatos que dificultam uma rápida e adequada utilização. As boas práticas indicadas nas discussões em torno de dados abertos acabam não sendo seguidas. Com isso, é comum vermos dados sendo disponibilizados no formato de imagem, de pdf ou audiovisual sem transcrição textual. A realização de pós-processamento e/ou OCR em textos disponibilizados no formato de imagem, a conversão de áudios e vídeos para o formato textual e a extração de conteúdos disponibilizados em pdfs estão entre as atividades realizadas nesse projeto.",
    link: "/docs/projetos/dados/full-text/info",
    tipo: "dados",
  },
  {
    title: "Mercodocs",
    imageUrl: "img/projetos/dados/mercodocs/mercodocs.svg",
    description:
      "O projeto MercoDocs objetiva auxiliar na coleta, tratamento e melhor disponibilização da documentação pública do Mercosul, que possui uma gama extensa e variada de documentação oficial e pública. Apesar disso, encontramos vários problemas que dificultam a adequada utilização deste material. Dentre destes problemas, podemos destacar: a plataforma que disponibiliza os documentos não indexada integralmente os arquivos; alguns metadados que apresentam inconsistência; muitos documentos estão em formato de imagem (tiff) ou em pdf não pesquisável, sendo necessário a realização de OCR para uma melhor utilização dos arquivos neste estado.",
    link: "/docs/projetos/dados/mercodocs/info",
    tipo: "dados",
  },
  {
    title: "TweePInA",
    imageUrl: "img/projetos/dados/tweepina/tweepina.svg",
    description:
      "O objetivo geral do projeto TweePInA é reunir tweets de autoridades e instituições públicas com especial destaque ao Brasil e organismos internacionais. O Twitter é uma das principais redes sociais da atualidade, sendo muito utilizado por autoridades e instituições públicas que são objetos de estudo de várias pesquisas acadêmicas. Porém, ter acesso a série histórica de tweets dessas autoridades e instituições muitas vezes é difícil. Além disso, muitos tweets acabam sendo deletados, não estando disponíveis em arquivos e/ou repositórios públicos. Mais especificamente, este projeto visa (1) auxiliar a construção de uma memória de informações de autoridades e instituições públicas divulgadas através do Twitter; (2) subsidiar pesquisas acadêmicas que utilizam o Twitter como fonte de informação de seus objetos de estudo através da disponibilização das variáveis disponibilizadas pelo Twitter; (3) fornecer um instrumento básico de análise de tweets; (4) indicar possibilidades e instrumentos que auxiliem uma análise mais detalhada dos tweets",
    link: "/docs/projetos/dados/tweepina/info",
    tipo: "dados",
  },
  {
    title: "Acervo Redalint",
    imageUrl: "img/projetos/dados/acervo-redalint/redalint.svg",
    description:
      "O projeto Acervo REDALINT surgiu com o objetivo de reunir em uma plataforma a produção científica fornecendo metadados consistentes e a indexação integral do conteúdo disponível. A produção científica de acesso aberto sobre a internacionalização da educação superior na América Latina se encontra dispersa em variados portais, há certa inconsistência nos metadados destas publicações e a indexação integral deste material é rara.",
    link: "/docs/projetos/dados/acervo-redalint/info",
    tipo: "dados",
  },
  {
    title: "IRjournalsBR",
    imageUrl: "img/projetos/dados/irjournalsbr/irjournalsbr.svg",
    description:
      "O projeto IRjournalsBR objetiva formar uma base de dados que aglutine tanto metadados mais consistentes como também forneça uma indexação integral de revistas. As revistas acadêmicas brasileiras de Relações Internacionais seguem as políticas de acesso aberto. Com isso, todo o conhecimento divulgado por estas revistas podem ser acessados gratuitamente por qualquer pessoa interessada e podem ser reutilizados sem prévia autorização dos editores e autores, desde que seja respeitada a licença de uso do Creative Commons adotado pelos respectivos periódicos. Apesar da adoção do acesso aberto ser um aspecto importante para garantir acesso universal ao conhecimento científico, parte dos metadados destas revistas apresentam inconsistências. Ademais, a indexação integral do conteúdo dessas revistas não é disponibilizada publicamente, dificultando a busca por palavras chaves na integralidade dos arquivos.",
    link: "/docs/projetos/dados/irjournalsbr/info",
    tipo: "dados",
  },
  {
    title: "GovLatinAmerica",
    imageUrl: "img/projetos/dados/gov-latam/gov-latam.svg",
    description:
      "O objetivo do projeto GovLatinAmerica é coletar dados para que possam ser utilizados em pesquisas acadêmicas diversas. Os dados públicos dos órgãos governamentais latino americanos disponíveis via web com frequência são retirados dos sites oficiais, especialmente, após a passagem de um mandato presidencial para outro.",
    link: "/docs/projetos/dados/gov-latin-america/info",
    tipo: "dados",
  },
  {
    title: "Internet e Relações Internacionais",
    imageUrl: "img/projetos/dados/internet-ri/logo-internet-ri.svg",
    description:
      "O projeto Internet e Relações Internacionais visa reunir trabalhos acadêmicos sobre a temática Tecnologias Digitais nas Relações Internacionais.",
    link: "/docs/projetos/dados/internet-relacoes-internacionais/info",
    tipo: "dados",
  },
];

export const extensao = [
  {
    title: "POD-RI",
    imageUrl: "/img/projetos/extensao/pod-ri/pod-ri.jpg",
    description:
      "O Pod-RI é um projeto feito por estudantes, no formato de podcast, iniciado como uma ideia entre amigos. Gravamos episódios com diversas temáticas das Relações Internacionais, assim como conversas com especialistas e profissionais. Além disso, produzimos conteúdos para redes sociais, onde entramos em contato mais direto com a comunidade de Relações Internacionais do Brasil.",
    link: "/pod-ri",
    tipo: "extensao",
  },
  {
    title: "Conhecer Para Acolher",
    imageUrl: "/img/conhecer-para-acolher/logo-cpa.svg",
    description:
      "O Projeto Conhecer Para Acolher é vinculado à REDE TEMÁTICA DE EXTENSÃO: Rede de Atenção ao Migrante Internacional (RAMIN/UNESP). Programa Institucional da Pró-Reitoria de Extensão Universitária e Cultura (PROEC).",
    link: "/conhecer-para-acolher",
    tipo: "extensao",
  },
  {
    title: "Cidades Saudáveis e Sustentáveis",
    imageUrl: "/img/cidades-sustentaveis/logo-ver1.svg",
    description:
      "O Projeto de Extensão Cidades Saudáveis e Sustentáveis foi criado em 2020, vinculado ao Centro Jurídico Social da Universidade Estadual Paulista, campus de Franca/SP.",
    link: "/cidades-sustentaveis",
    tipo: "extensao",
  },
  {
    title: "As Relações Internacionais e o Novo Coronavírus",
    imageUrl: "/img/projetos/extensao/labri-pandemia/labri-pandemia.svg",
    description:
      "O Projeto de extensão As Relações Internacionais e o Novo Coronavírus surgiu a partir da iniciativa do Laboratório de Relações Internacionais (LabRI) da UNESP Franca. O projeto contou com a participação de vários docentes e discentes da universidade e tinha como principal objetivo produzir conteúdos sobre a pandemia de COVID-19 sob a perspectiva das Relações Internacionais.",
    link: "/docs/projetos/extensao/covid19/intro",
    tipo: "extensao",
  },
];

export const sistemas = [
  {
    title: "Websites e Redes",
    imageUrl: "/img/projetos/sistemas/web-logo2.svg",
    description:
      "Este projeto visa reunir informações sobre a edição e manutenção dos sites mantidos pela equipe do LabRI/UNESP.",
    link: "/docs/projetos/sistemas/site/intro",
    tipo: "sistemas",
  },
  {
    title: "Infraestrutura Computacional",
    imageUrl: "/img/projetos/sistemas/logo-projetos-infra.svg",
    description:
      "Visa reunir informações sobre as tecnologias utilizadas para manter a infraestrutura de computadores do LabRI/UNESP. Dentre outras, utilizamos o Proxmox (virtualização) e PFSense (redes).",
    link: "/docs/projetos/sistemas/intro-infracomputacional",
    tipo: "sistemas",
  },
  {
    title: "Cadernos LabRI/UNESP",
    imageUrl:
      "/img/projetos/sistemas/cadernos-labri/logo-projetos-cadernos.svg",
    description:
      "O projeto reúne as publicações dos colaboradores do LabRI/UNESP, além da documentação sobre edição dos cadernos em MarkDown.",
    link: "/docs/projetos/sistemas/cadernos/info",
    tipo: "sistemas",
  },
  {
    title: "Sistema de Confecção de Certificados",
    imageUrl: "/img/projetos/sistemas/certificados/logo-certificados.svg",
    description:
      "Visa agrupar a documentação sobre o Sistema de Emissão de Certificados do Labri/UNESP.",
    link: "/docs/projetos/sistemas/certificados",
    tipo: "sistemas",
  },
  {
    title: "Sistema de OCR Automático",
    imageUrl: "/img/projetos/sistemas/servico-ocr/logo-ocr.svg",
    description:
      "Este projeto reune informações sobre o Reconhecimento Ótico de Caracteres (OCR) utilizado pelo Labri/UNESP.",
    link: "/docs/projetos/sistemas/ocr/intro",
    tipo: "sistemas",
  },
];

export const ensino = [
  {
    title: "Trilhas de Fundamentos de Tecnologias Digitais",
    imageUrl: "./img/projetos/ensino/trilha-dados/trilha-dados.svg",
    description:
      "O projeto Trilha de Dados visa reunir informações e material didático  sobre questões relativas à ciência de dados nas Relações Internacionais. Assuntos como coleta e análise de dados; fundamentos de algoritmos e programação; entre outros estão entre os tópicos que serão abordados aqui.",
    link: "/docs/projetos/ensino/trilha-dados/intro",
    tipo: "educacao",
  },
  {
    title: "Tecnologias Digitais",
    imageUrl: "/img/projetos/ensino/tec-digitais/logo-tec-digitais.svg",
    description:
      "Nesse espaço você encontrará tutoriais entre outros materiais didáticos que tem por objetivo disseminar a utilização de tecnologias digitais.",
    link: "/docs/projetos/ensino/tec-digitais/intro",
    tipo: "educacao",
  },
  {
    title: "Processar imagens que contém texto",
    imageUrl: "./img/projetos/ensino/ensino/arquivos.svg",
    description:
      "Você tem um arquivo, tirou foto ou digitalizou um texto, mas não sabe como buscar palavras chaves nele? Este arquivo é uma imagem (png, jpeg ou tiff) ou um pdf? Neste tutorial indicamos como processar imagens textuais para gerar um pdf pesquisável e, deste modo, conseguir buscar as palavras chaves contidas dentro deste arquivo.",
    link: "/docs/projetos/ensino/processamento-imagens/intro",
    tipo: "educacao",
  },
  {
    title: "Acesso Remoto",
    imageUrl: "./img/projetos/ensino/ensino/acesso-remoto.svg",
    description:
      "Aqui você encontrará informações para acesso a estação remoto de trabalho",
    link: "/docs/projetos/ensino/acesso-remoto/intro",
    tipo: "educacao",
  },
  {
    title: "Recoll",
    imageUrl: "./img/projetos/ensino/ensino/recoll.svg",
    description:
      "Aqui você encontrará como utilizar os recursos do aplicativo Recoll.",
    link: "/docs/projetos/ensino/recoll/info",
    tipo: "educacao",
  },
  {
    title: "Filezilla",
    imageUrl: "./img/projetos/ensino/ensino/filezilla.svg",
    description:
      "Aqui você aprenderá como fazer a transferência de arquivos entre dois computadores utilizando o Filezilla.",
    link: "/docs/projetos/ensino/filezilla/intro",
    tipo: "educacao",
  },
  {
    title: "Material Bibliográfico",
    imageUrl: "./img/projetos/ensino/ensino/biblio-acad.svg",
    description:
      "Aqui você encontrará indicações de material bibliográfico relacionado às Relações Internacionais e/ou tecnologias digitais.",
    link: "/docs/projetos/ensino/material-bibliografico/intro",
    tipo: "educacao",
  },
  {
    title: "Curso: Governança da Internet",
    imageUrl: "./img/projetos/ensino/curso_governanca-da-internet1.png",
    description:
      "O curso visa preencher uma lacuna nas disciplinas de Relações Internacionais e Ciências Sociais, que é o estudo da chamada Governança da Internet. Sendo um campo relativamente novo, há um interesse crescente da comunidade acadêmica e uma demanda das áreas de RI e Ciências Sociais.",
    link: "/docs/projetos/ensino/governanca/intro",
    tipo: "educacao",
  },
];
