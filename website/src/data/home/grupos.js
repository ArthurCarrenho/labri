export const grupos = [
  {
    title: "Grupos de Pesquisa",
    imageUrl: "./img/grupos/logotestepesquisa.svg",
    alt: "Logo dos grupos de pesquisa",
    link: "/grupos/pesquisa",
    description:
      "Conheça os Grupos de Pesquisa do departamento de Relações Internacionais da UNESP, campus de Franca.",
  },
  {
    title: "Grupos de Extensão",
    imageUrl: "./img/grupos/logotesteextensao.svg",
    alt: "Logo dos grupos de extensão",
    link: "/grupos/extensao",
    description:
      "Saiba mais sobre os Grupos de Extensão do departamento de Relações Internacionais da UNESP, campus de Franca.",
  },
  {
    title: "Grupos Parceiros",
    imageUrl: "./img/grupos/logotesteapoiadores.svg",
    alt: "Logo dos grupos parceiros",
    link: "/grupos/parceiros",
    description:
      "Conheça os Grupos Parceiros do Laboratório de Relações Internacionais da UNESP.",
  },
];
