export const slides = [
  {
    columns: 1,
    heroTitle: "LabRI UNESP",
    heroSubtitle: "Laboratório de Relações Internacionais - UNESP",
    linkText: "Saiba Mais",
    linkHref: "https://labriunesp.org/sobre",
  },
  {
    columns: 2,
    heroSubtitle:
      "Conheça os projetos de dados, extensão, ensino e sistemas do LabRI/UNESP.",
    linkText: "Acesse aqui",
    linkHref: "https://labriunesp.org/projetos/",
    imgHref: "https://labriunesp.org/projetos/",
    imgSrc: "./img/projetos/projetos.svg",
  },
  {
    columns: 2,
    heroSubtitle: "Acesse as publicações dos Cadernos LabRI UNESP.",
    linkText: "Acesse aqui",
    linkHref: "https://labriunesp.org/cadernos/",
    imgHref: "https://labriunesp.org/cadernos/",
    imgSrc: "./img/projetos/cadernos.svg",
  },
  {
    columns: 2,
    heroSubtitle:
      "Conheça e acesse as páginas dos grupos parceiros do LabRI UNESP.",
    linkText: "Saiba mais",
    linkHref: "https://labriunesp.org/grupos-parceiros",
    imgHref: "https://labriunesp.org/docs/geral/info/grupos",
    imgSrc: "./img/projetos/grupos.svg",
  },
  {
    columns: 2,
    heroSubtitle: "Conheça as oportunidades de estágio no LabRI UNESP.",
    linkText: "Acesse aqui",
    linkHref: "https://labriunesp.org/docs/geral/info/estagio",
    imgHref: "https://labriunesp.org/docs/geral/info/estagio",
    imgSrc: "./img/projetos/estagio.svg",
  },
];
