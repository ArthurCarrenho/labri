import React from "react";
import Layout from "@theme/Layout";
import clsx from "clsx";
import styles from "./styles.module.scss";

import { SobreP } from "../../components/projetos/SobreP";
import { sobrep } from "../../data/projetos/sobrep";

import { Sobre } from "../../components/projetos/Sobre";
import { sobre } from "../../data/projetos/sobre";

function Projetos() {
  return (
    <Layout title="Projetos">
      <main className={styles.main}>
        <header className={clsx("hero hero--primary", styles.heroBanner)}>
          <div className={styles.container}>
            <h1 className={clsx(styles.hero__title, "hero__title")}>
              Projetos
            </h1>
            <h2 className={clsx(styles.subtituloProjetos, "hero__subtitle")}>
              Conheça e contribua com os projetos do LabRI/UNESP
            </h2>
          </div>
        </header>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
            <div className={clsx(styles.h1A)}> APRESENTAÇÃO </div>
            <p className={clsx(styles.pProjetos)}>
              Parte significativa das atividades do LabRI se relacionam a
              projetos que envolvem discentes, docentes e grupos de pesquisas
              vinculados ao curso de Relações Internacionais da UNESP e do
              Programa de Pós-Graduação em Relações Internacionais San Tiago
              Dantas (UNESP, UNICAMP, PUC-SP). Tais iniciativas não são projetos
              de pesquisa, mas sim projetos que visam fornecer suporte a
              pesquisa, por exemplo através do auxílio na construção e
              manutenção de bases de dados. Para deixar mais clara esta
              distinção os projetos mais diretamente relacionados à pesquisa são
              denominados como “projetos de dados”.
            </p>
            <p className={clsx(styles.pProjetos)}>
              Além deste tipo de projeto o LabRI/UNESP também conduz e/ou
              auxilia projetos de ensino, extensão e de sistemas. Tais projetos
              são quatro frentes de trabalho importantes dentre as atividades
              realizadas no LabRI/UNESP. A principal característica comum no
              desenvolvimento de tais projetos é a utilização de tecnologias
              digitais e a preferência pela utilização de software livre e/ou de
              código aberto.
            </p>
            <p className={clsx(styles.pProjetos)}>
              Tais projetos podem ser denominados, de maneira mais geral, como
              projetos integrados ou projetos guarda chuva, pois são grandes
              frentes de trabalho que servem para organizar e articular melhor
              as atividades em que o LabRI/UNESP está envolvido. Tais projetos
              se dividem em subprojetos desenvolvidos em parceria com discentes,
              docentes, grupos de pesquisa entre outros parceiros e
              colaboradores. Veja abaixo como os projetos integrados estão
              estruturados e quais são seus subprojetos.
            </p>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {sobre.map((props, idx) => (
                <Sobre key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {sobrep.map((props, idx) => (
                <SobreP key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Projetos;
