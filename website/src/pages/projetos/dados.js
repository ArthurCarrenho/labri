import React from "react";
import Layout from "@theme/Layout";
import clsx from "clsx";
import styles from "./styles.module.scss";
import { Projeto } from "../../components/projetos/Projeto";
import { dados } from "../../data/projetos/projeto";

function Projetos_dados() {
  return (
    <Layout title="Projetos de Dados">
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className={styles.hero__title}>Projetos</h1>
          <h2 className={styles.subtituloProjetos}>
            Conheça e contribua com os projetos do LabRI/UNESP
          </h2>
        </div>
      </header>
      <section className={styles.content}>
        <div className={clsx(styles.container)}>
          <div className={clsx(styles.h1A)}> APRESENTAÇÃO </div>
          <p className={clsx(styles.pProjetos)}>
            Abaixo estarão expostos os projetos nos quais o LabRI participa,
            assim como informações essenciais sobre esses projetos. Para acessar
            a lista simplificada dos projetos de dados{" "}
            <a href="https://labriunesp.org/docs/projetos/dados/lista">
              clique aqui
            </a>
            .
          </p>
          <p>
            Os projetos do LabRI são divididos conforme seus objetivos. são
            eles:{" "}
          </p>
          <div className="row tipos_projeto">
            <div className="col col--4">
              <h3>
                <a href="/projetos/sistemas/">Projetos de Sistemas</a>
              </h3>
            </div>
            <div className="col col--4">
              <h3>
                <a href="/projetos/ensino/">Projetos de Ensino</a>
              </h3>
            </div>
            <div className="col col--4">
              <h3>
                <a href="/projetos/extensao/">Projetos de Extensão</a>
              </h3>
            </div>
          </div>
        </div>
      </section>
      <main className={styles.main}>
        <section className={styles.content}>
          {dados && dados.length > 0 && (
            <section className={styles.projetos}>
              <div className={styles.container}>
                {dados.map((props, idx) => (
                  <Projeto key={idx} {...props} />
                ))}
              </div>
            </section>
          )}
        </section>
      </main>
    </Layout>
  );
}

export default Projetos_dados;
