import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesAcesso from "./styles.acesso.module.css";
import styles from "../styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const intro = [
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Acesso Remoto via Chrome Remote Desktop",
  },
];

const info = [
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Acesso Remoto via Windows",
    imgLogo: "/img/projetos/ensino/ensino/acesso-remoto/windows-logo.svg",
    description: "Tutorial em 10 passos de como configurar o acesso remoto utilizando o sistema operacional Windows.",
    link: "/projetos/ensino/acesso-remoto/windows"
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Acesso Remoto via Linux",
    imgLogo: "/img/projetos/ensino/ensino/acesso-remoto/linux-logo.svg",
    description: "Tutorial em 9 passos de como configurar o acesso remoto utilizando o sistema operacional Linux.",
    link: "/projetos/ensino/acesso-remoto/linux"
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Acesso Remoto via Mac OS",
    imgLogo: "/img/projetos/ensino/ensino/acesso-remoto/mac-os-logo.svg",
    description: "Tutorial em 11 passos de como configurar o acesso remoto utilizando o sistema operacional Mac OS.",
    link: "/projetos/ensino/acesso-remoto/mac-os" 
  },
];

function Info({title, description, logo, imgLogo, imageUrl, link}){
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.Info, "card col col--4")}>
      <div className="card-content">
        <div className="card__header"> 
          <h3>{title}</h3>
        </div>
        <div className={clsx(stylesAcesso.Info, "text__center")}>
          <img src={imgLogo} alt={logo} />
        </div>
        <div className="card__body">
          <p>{description}</p>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>Acesse aqui</a>
        </div>
      </div>
    </div>
  );
}

function Intro({ title, projeto, text }) {
  return (
    <div className={clsx("hero hero--primary", stylesAcesso.heroBannerPod)}>
      <div className={stylesAcesso.Intro}>
        <h1>{title}</h1>
      </div>
    </div>
  );
}

function Acesso() {
  return (
    <Layout title="Acesso Remoto via Chrome Remote Desktop">
      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
            <div className="row">
              {info.map((props, idx) => (
                <Info key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Acesso;
