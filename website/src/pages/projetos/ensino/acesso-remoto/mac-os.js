import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesAcesso from "./styles.acesso.module.css";
import styles from "../styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const intro = [
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Acesso Remoto via Mac OS",
  },
];

const tutorial = [
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Acesso Remoto via Mac OS",
    imgLogo: "/img/projetos/ensino/ensino/acesso-remoto/mac-os.png",
    text: "Tutorial em 11 passos de como configurar o acesso remoto utilizando o sistema operacional Mac OS.",
  },
];

const passos = [
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 1",
    link: "https://www.xquartz.org",
    text: "Acesse o site oficial do xquartz de como instalar o programa para poder continuar o tutorial."
},
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 2",
    link: "https://youtu.be/tK3j3mbFnuc?t=380",
    link2:
      "https://docs.google.com/document/d/139Fv5kHrcog6eeTvU_fdSI7-Il3Kz3N3Hd1OLYo7mqE/edit?usp=sharing",
    text: "Assista o vídeo tutorial de como instalar o X2GO client Mac OS ou assista ao vídeo tutorial completo."
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 3",
    link: "https://youtu.be/4ENyiSBrFvA",
    text: "Assista o vídeo tutorial ou leia o documento de como fazer a configuração de acesso via X2GO",
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 4",
    link: "https://youtu.be/t75ifvdZ_FQ",
    text: "Assista o vídeo tutorial de como habilitar o acesso via Chrome Remote Desktop. Atenção: Caso apareça alguma tela pedindo senha de administrador feche a respectiva tela e continue o processo normalmente.",
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 5",
    link: "https://youtu.be/PZIpTyzdOLY",
    text: "Assista o vídeo tutorial de como fazer a verificação da configuração do teclado."
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 6",
    link: "https://www.youtube.com/watch?v=j7PB9l5RTD4",
    text: "Assista o vídeo tutorial de como fazer para mudar a senha para uma senha definitiva (pessoal e intransferível).",
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 7",
    link: "https://youtu.be/HwSHInzmX_I",
    text: "Assista o vídeo tutorial de como resolver falha na conexação no acesso remoto. ",
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 8",
    link: "https://youtu.be/XIu8DVqasMw",
    text: "Assista o vídeo de apresentação geral da estação de trabalho remota.",
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 9",
    link: "https://youtu.be/2QOuXXEP_TY",
    text: "Assista o vídeo tutorial de como utilizar o Chrome Remote Desktop.",
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 10",
    link: "https://youtu.be/92qqi_gqQqs",
    text: "Assista o vídeo tutorial de como encerrar a sessão do X2GO.",
  },
  {
    projeto: "acesso-remoto - labri-unesp",
    title: "Passo 11",
    link: "https://youtu.be/FoLP_juWlCI",
    text: "Assista o vídeo tutorial de como encerrar a sessão do Chrome Remote Desktop. ",
  },
];

function Intro({ title, projeto, text }) {
  return (
    <div className={clsx("hero hero--primary", stylesAcesso.heroBannerPod)}>
      <div className={stylesAcesso.Intro}>
        <h1>{title}</h1>
      </div>
    </div>
  );
}

function Tutorial({
  title,
  text,
  projeto,
  logo,
  imgLogo,
  imageUrl,
  link,
  link2,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div>
      <div className={clsx(stylesAcesso.Tutorial)}>
        <div className={stylesAcesso.textIntro}>
          Instruções para auxiliar as configurações iniciais de acesso a máquina
          denominada lantrivm01 que está destinada a hospedar o usuário
          específico de cada pesquisador. É a partir desta máquina que você
          poderá realizar suas atividades individuais de pesquisa e coleta de
          dados na estação de trabalho remoto. Após a realização da configuração
          indicada abaixo, o acesso se dará via sua conta do google através da
          utilização do navegador chrome. Caso você tenha dúvidas ou
          dificuldades de configurar seu computador ou notebook envie um email
          para:{" "}
          <i>
            <u>unesplabri@gmail.com</u>
          </i>
          . Caso não consigamos resolver as dificuldades por email podemos
          marcar uma videoconferência para realizada a configuração em conjunto
          com você.{" "}
        </div>
        <h2>{title}</h2>
        <div className="card__body">
          <img src={imgLogo} alt={logo} />
          <h4>{text}</h4>
        </div>
      </div>
    </div>
  );
}

function Passos({
  text,
  title,
  passo,
  imgPasso,
  imageUrl,
  link,
  link2,
  projeto,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div>
      <div className={clsx(stylesAcesso.Passos)}>
            <h2>{title}</h2>
            <h4>{text}</h4>
          <h4><a className="button button--secondary" href={link}>Acesse aqui</a></h4>
        </div>
    </div>
  );
}

function MacOS() {
  return (
    <Layout title="Acesso Remoto via Mac OS">
      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(stylesAcesso.Windows, styles.container)}>
            <div className="row">
              {tutorial.map((props, idx) => (
                <Tutorial key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {passos.map((props, idx) => (
                <Passos key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default MacOS;
