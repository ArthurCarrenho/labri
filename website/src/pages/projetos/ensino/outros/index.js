import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesOutros from "./styles.outros.module.css";
import styles from "../styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const intro = [
  {
    projeto: "outros - labri-unesp",
    title: "Outros Tutoriais",
  },
];

const bibliografia = [
    {
        projeto: "outros - labri-unesp",
        title: "Fontes de Bibliografia Acadêmica",
        imgLogo: "/img/projetos/ensino/ensino/outros/1.png",
        description: (
            <>
            <li>Dicas <i><a href="https://www.youtube.com/watch?v=aiGW-TZ73nA">SciHub e LibGen</a></i></li>
            <li><a href="https://www.youtube.com/watch?v=vQAL5RdCa08">Acesso de Base de Dados</a>: PROQUEST, JSTOR, Periódico CAPES. Disponibilizado pelo <i>Canal LabRI</i>.</li>
            </>
        )
    },
];

const mapa = [
    {
        projeto: "outros - labri-unesp",
        title: "O que é MindMup?",
        description: (
            <>
            <p>É um programa online construido através do JavaScript e que tem o intuito de facilitar o desenvolvimento de mapeamentos mentais pessoais ou compartilhados. O programa possui plano gratuito e não exige login </p>
            <li><a href="https://www.mindmup.com">Acesse aqui</a></li>
            </>
        ),
        imgLogo: "/img/projetos/ensino/ensino/outros/2.png",
        text1: "Tutoriais disponibilizados pelo Canal Lantri",
        text2: (
            <>
            <li><a href="https://www.youtube.com/watch?v=gtSe4U58FJs">Vídeo tutorial</a> sobre <i>Atalhos</i></li>
            <li><a href="https://www.youtube.com/watch?v=rA4DYyqn_kE">Vídeo tutorial</a> sobre como <i>inserir links, notas e anexos.</i></li>
            <li><a href="https://www.youtube.com/watch?v=jEt7aQqYeWw">Vídeo tutorial</a> sobre <i>temas e cores</i></li>
            <li><a href="https://www.youtube.com/watch?v=HEs__WdubEE">Vídeo tutorial</a> sobre <i>instalação e compartilhamento.</i></li>
            </>
        )
    },
];

const docs = [
    {
        projeto: "outros - labri-unesp",
        title: "Vídeos para melhor utilizar o Google Docs",
        description: (
            <>
            <li><a href="https://youtu.be/tsvo0Kn3ixw">Vídeo tutorial</a> sobre <i>introdução e estilo de parágrafos.</i></li>
            <li><a href="https://youtu.be/3hFbdFde2yY">Vídeo tutorial</a> sobre <i>estilos de título, localizar/substituir, sumário e cabeçalhos.</i></li>
            <li><a href="https://youtu.be/C63HOyfSyvo">Vídeo tutorial</a> sobre <i>atalhos.</i></li>
            <li><a href="https://youtu.be/cFkPC3C8f5Y">Vídeo tutorial</a> sobre <i>importação/exportação, configuração da página, afins.</i></li>
            <li><a href="https://youtu.be/sR3MGpxFx_0">Vídeo tutorial</a> sobre <i>trabalho coletivo.</i></li>
            </>
        ),
        imgLogo: "/img/projetos/ensino/ensino/outros/3.png",
        text: (
            <>
            <p>Todos os vídeos foram disponibilizados pelo <a href="https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ">Canal Lantri</a> na plataforma do YouTube</p>
            </>
        )
    },
];

function Intro({ title }) {
  return (
    <div
      className={clsx("hero hero--secondary", stylesOutros.heroBannerOutros)}
    >
      <div className={clsx(stylesOutros.Intro)}>
        <h1>{title}</h1>
      </div>
    </div>
  );
}

function Bibliografia({title, description, logo, imgLogo, imageUrl}){
    const imagemInfo = useBaseUrl(imageUrl);
    return(
        <div>
            <div className={clsx(stylesOutros.Tutorial)}>
                <div className={clsx(stylesOutros.textIntro)}>
                    <h2>TUTORIAL: BIBLIOGRAFIA ACADÊMICA</h2>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className={clsx(stylesOutros.pPassos, "col col--6")}>
                        <img src={imgLogo} alt={logo} />
                    </div>
                    <div className={clsx(stylesOutros.pPassos, "col col--6")}>
                        <h2>{title}</h2>
                        <p>{description}</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

function Mapa({title, description, text1, text2, logo, imgLogo, imageUrl}){
    const imagemInfo = useBaseUrl(imageUrl);
    return(
        <div>
            <div className={clsx(stylesOutros.Tutorial)}>
                <div className={clsx(stylesOutros.textIntro)}>
                    <h2>TUTORIAL: MINDMUP</h2>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className={clsx(stylesOutros.pPassos, "col col--6")}>
                        <h2>{title}</h2>
                        <p>{description}</p>
                        <h2>{text1}</h2>
                        <p>{text2}</p>
                    </div>
                    <div className={clsx(stylesOutros.pPassos2, "col col--6")}>
                        <img src={imgLogo} alt={logo} />
                    </div>
                </div>
            </div>
        </div>
    )
}

function Docs({title, description, text, logo, imgLogo, imageUrl}){
    const imagemInfo = useBaseUrl(imageUrl);
    return(
        <div>
            <div className={clsx(stylesOutros.Tutorial)}>
                <div className={clsx(stylesOutros.textIntro)}>
                    <h2>GOOGLE DOCS: dicas e tutoriais</h2>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className={clsx(stylesOutros.pPassos2, "col col--6")}>
                        <img src={imgLogo} alt={logo} />
                    </div>
                    <div className={clsx(stylesOutros.pPassos, "col col--6")}>
                        <h2>{title}</h2>
                        <p>{description}</p>
                        <h4>{text}</h4>
                    </div>
                </div>
            </div>
        </div>
    )
}

function Outros() {
  return (
    <Layout title="Outros tutoriais">
      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
            <div className="row">
              {bibliografia.map((props, idx) => (
                <Bibliografia key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {mapa.map((props, idx) => (
                <Mapa key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {docs.map((props, idx) => (
                <Docs key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Outros;
