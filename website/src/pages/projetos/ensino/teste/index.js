import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesTeste from "./styles.teste.module.css";
import styles from "../styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";


const intro = [
    {
        projeto: "teste - labri-unesp",
        title: "Página Teste"
    },
];

const tutorial = [
    {
        projeto: "teste - labri-unesp",
        title: "Tutorial Teste",
        imgLogo: "/img/projetos/ensino/ensino/recoll/tutorial1.svg"
    },
];

const passos = [
    {
        projeto: "teste - labri-unesp",
        title: "Passo 1",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue lacinia aliquet. Sed ornare consequat venenatis. Suspendisse potenti. Curabitur finibus, metus vitae ultricies vulputate, ipsum ligula auctor sapien, et hendrerit lorem ex ac metus. Donec quis aliquet nibh.",
        imgLogo1: "/img/projetos/ensino/ensino/recoll/tutorial2.svg",
        title2: "Passo 2",
        description2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue lacinia aliquet. Sed ornare consequat venenatis. Suspendisse potenti. Curabitur finibus, metus vitae ultricies vulputate, ipsum ligula auctor sapien, et hendrerit lorem ex ac metus. Donec quis aliquet nibh.",
        imgLogo2: "/img/projetos/ensino/ensino/recoll/tutorial3.svg"
    },
    {
        projeto: "teste - labri-unesp",
        title: "Passo 3",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue lacinia aliquet. Sed ornare consequat venenatis. Suspendisse potenti. Curabitur finibus, metus vitae ultricies vulputate, ipsum ligula auctor sapien, et hendrerit lorem ex ac metus. Donec quis aliquet nibh.",
        imgLogo1: "/img/projetos/ensino/ensino/recoll/tutorial4.svg",
        title2: "Passo 4",
        description2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue lacinia aliquet. Sed ornare consequat venenatis. Suspendisse potenti. Curabitur finibus, metus vitae ultricies vulputate, ipsum ligula auctor sapien, et hendrerit lorem ex ac metus. Donec quis aliquet nibh.",
        imgLogo2: "/img/projetos/ensino/ensino/recoll/tutorial5.svg"
    }
];

function Intro({title}){
    return (
      <div className={clsx("hero hero--primary", stylesTeste.heroBannerPod)}>
        <div className={stylesTeste.Intro}>
          <h1>{title}</h1>
        </div>
      </div>
    );
  }

function Tutorial({title, logo, imgLogo, imageUrl}){
    const imagemInfo = useBaseUrl(imageUrl);
    return(
        <div>
            <div className={clsx(stylesTeste.Tutorial)}>
                <div className={clsx(stylesTeste.textIntro)}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue lacinia aliquet. Sed ornare consequat venenatis. Suspendisse potenti. Curabitur finibus, metus vitae ultricies vulputate, ipsum ligula auctor sapien, et hendrerit lorem ex ac metus. Donec quis aliquet nibh. 
                </div>
                <h2>{title}</h2>
                <div className="card__body">
                    <img src={imgLogo} alt={logo} />
                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue lacinia aliquet. Sed ornare consequat venenatis. Suspendisse potenti. Curabitur finibus, metus vitae ultricies vulputate, ipsum ligula auctor sapien, et hendrerit lorem ex ac metus. Donec quis aliquet nibh. </h4>
                </div>
                <div className={clsx(stylesTeste.textIntro)}>
                    <h2>PASSO A PASSO</h2>
                </div>
            </div>
        </div>
    )
}

function Passos({title, title2, description, description2, logo1, imgLogo1, logo2, imgLogo2, imageUrl}){
    const imagemInfo = useBaseUrl(imageUrl);
    return(
        <div>
            <div className="container">
                <div className="row">
                    <div className={clsx(stylesTeste.pPassos, "col col--6")}>
                        <img src={imgLogo1} alt={logo1} />
                    </div>
                    <div className={clsx(stylesTeste.pPassos, "col col--6")}>
                        <h2>{title}</h2>
                        <p>{description}</p>
                    </div>
                    <div className={clsx(stylesTeste.pPassos2, "col col--6")}>
                        <h2>{title2}</h2>
                        <p>{description2}</p>
                    </div>
                    <div className={clsx(stylesTeste.pPassos2, "col col--6")}>
                    <img src={imgLogo2} alt={logo2} />
                    </div>
                </div>
            </div>
        </div>
    )
}

function Teste(){
    return(
        <Layout title="Página Teste">
            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>
            <main>
                <section className={styles.content}>
                    <div className={clsx(styles.container)}>
                        <div className="row">
                            {tutorial.map((props, idx) => (
                                <Tutorial key={idx} {...props} />
                            ))}
                        </div>
                    </div>


                    <div className={clsx(styles.container)}>
                        <div className="row">
                            {passos.map((props, idx) => (
                                <Passos key={idx} {...props} />
                            ))}
                        </div>
                    </div>
                </section>
            </main>
        </Layout>
    )
}

export default Teste; 