import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesRecoll from "./styles.recoll.module.css";
import styles from "../styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const intro = [
  {
    projeto: "recoll - labri-unesp",
    title: "Instruções de Utilização do Recoll",
  },
];

const tutorial = [
  {
    projeto: "recoll - labri-unesp",
    title: "O que é o Recoll?",
    imgLogo: "/img/projetos/ensino/ensino/recoll/inicio.png",
    text: "É um indexador integral de documentos textuais (arquivos pdf, doc, docx, odt, html, epub entre outros), ou seja, este programa viabiliza a pesquisa de todas as palavras contidas em arquivos de texto. Basicamente, o Recoll tem um comportamento similar às buscas realizadas pelo Google. Atualmente as principais bases de dados dos grupos de pesquisa parceiros do LabRI/UNESP utilizam o Recoll para indexar e pesquisar seus dados.",
  },
];

const passos = [
  {
    projeto: "recoll - labri-unesp",
    title: "Tutorial 1",
    text: "Assista o vídeo tutorial com informações de primeiro acesso e bases de dados disponíveis.",
    link: "https://youtu.be/RejrfdIhj2o",
    imgLogo: "/img/projetos/ensino/ensino/recoll/tutorial1.svg"
  },
  {
    projeto: "recoll - labri-unesp",
    title: "Tutorial 2",
    text: "Assista o vídeo tutorial para conferir as dicas de utilização.",
    link: "https://youtu.be/a0_tWA1SsK8",
    imgLogo: "/img/projetos/ensino/ensino/recoll/tutorial2.svg",
  },
  {
    projeto: "recoll - labri-unesp",
    title: "Tutorial 3",
    text: "Assista o vídeo tutorial com instruções de como atualizar as Bases disponíveis.",
    link: "https://youtu.be/62x-Yer2hyg",
    imgLogo: "/img/projetos/ensino/ensino/recoll/tutorial3.svg",
  },
  {
    projeto: "recoll - labri-unesp",
    title: "Tutorial 4",
    text: "Assista o vídeo tutorial para saber como filtrar busca nos índices pelas subpastas dos dados indexados. Dica: parâmetro para o filtro: dir:/pasta_específica_dos_dados",
    link: "https://youtu.be/WFmF0b5mvyk",
    imgLogo: "/img/projetos/ensino/ensino/recoll/tutorial4.svg"
  },
  {
    projeto: "recoll - labri-unesp",
    title: "Tutorial 5",
    text: "Assista os vídeos tutoriais (vídeo tutorial ) sobre o recurso de pesquisa avançada disponibilizados pelo Canal Lantri.",
    link: "https://youtu.be/Am1O-b1ZPVs",
    imgLogo: "/img/projetos/ensino/ensino/recoll/tutorial5.svg"
  },
];

const final = [
  {
    projeto: "recoll - labri-unesp",
    link: "https://youtu.be/MK_-2XjynPg",
  },
];

function Intro({ title, projeto }) {
  return (
    <div className={clsx("hero hero--primary", stylesRecoll.heroBannerPod)}>
      <div className={stylesRecoll.Intro}>
        <h1>{title}</h1>
      </div>
    </div>
  );
}

function Tutorial({
  title,
  text,
  projeto,
  logo,
  imgLogo,
  imageUrl,
  link,
  link2,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div>
      <div className={clsx(stylesRecoll.Tutorial)}>
        <div className={stylesRecoll.textIntro}>
          Caso você tenha dúvidas ou dificuldades de configurar seu computador
          ou notebook envie um email para:{" "}
          <u>
            <i>unesplabri@gmail.com</i>
          </u>
          . Caso não consigamos resolver as dificuldades por email podemos
          marcar uma videoconferência para realizar a configuração em conjunto
          com você.
        </div>
        <h2>{title}</h2>
        <div className="card__body">
          <img src={imgLogo} alt={logo} />
          <h4>{text}</h4>
        </div>
        <div className={clsx(stylesRecoll.textIntro)}>
          <h2>Vídeos Tutoriais</h2>
          <h4>Assista os vídeos tutoriais para poder utilizar o recoll.</h4>
        </div>
      </div>
    </div>
  );
}

function Passos({ title, text, logo, imgLogo, imageUrl, link}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.Info, "card col col--6")}>
      <div className="card-content">
        <div className="card__header">
          <h2>{title}</h2>
        </div>
        <div className={clsx(stylesRecoll.Info, "text__center")}>
          <img src={imgLogo} alt={logo} />
        </div>
        <div className="card__body">
          <h6>{text}</h6>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>Acesse Aqui</a>
        </div>
      </div>
      </div>
  );
}

function Final({ link }) {
  return (
    <div>
      <div className={clsx(stylesRecoll.Tutorial)}>
        <div className={clsx(stylesRecoll.textIntro)}>
          <h2>Informações:</h2>
          <h6>
            <i>
              Dica: assista o <a href={link}>vídeo tutorial</a>
            </i>
          </h6>
          <h4>
            A visualização das notícias ocorre a partir do preview do recoll
            (localizado do lado do botão "open") ou será necessário habilitar o
            modo off-line do navegador em que as notícias são abertas. Se não
            fizer isso em vez de acessar o arquivo da notícia que está
            localizado na "estação remota de trabalho" você será direcionada
            para o site do jornal e este só dará acesso a notícia em questão
            caso você seja assinante.
          </h4>
        </div>
      </div>
    </div>
  );
}

function Recoll() {
  return (
    <Layout title="Instruções de Utilização do Recoll">
      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
            <div className="row">
              {tutorial.map((props, idx) => (
                <Tutorial key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {passos.map((props, idx) => (
                <Passos key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {final.map((props, idx) => (
                <Final key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Recoll;
