import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";



const intro = [
  {
    projeto: "repri - labri-unesp",
    logo: "/img/repri/repri-logo.png",
    link: "/repri",
    link1: "/repri",
    link2: "/repri/institucional",
    link3: "/repri/publicacoes",
    link4: "/repri/projetos",
    link5: "/repri/noticias",
    link6: "/repri/equipe"
  }
]

const emconstrucao = [
  {
    projeto: "repri - labri-unesp",
    img: "/img/repri/pag-em-construcao-2.svg"
  }
]

function EmConstrucao({ img }) {
  return (
    <div className={clsx(styles.colunaSemFundo, styles.center)}>
      <div className="row">
        <div className="col col--12">
          <img className={clsx(styles.emConstrucao)} src={img} alt="Página em Construção" />
        </div>
      </div>
    </div>
  )
}


function Intro({ logo, link, link1, link2, link3, link4, link5, link6 }) {
  return (
    <div className={clsx(styles.heroBanner, "hero hero--primary")}>
      <div className="row">
        <div className="col col--4">
          <div className={clsx(styles.LogoRepri)}>
            <a href={link}>
              <img src={logo} alt="Logo do REPRI" />
            </a>
          </div>
        </div>
        <div className={clsx(styles.Menu, "col col--8")}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}>
              <a href={link1}>home</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link2}>institucional</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link3}>publicações</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link4}>projetos</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link5}>notícias</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link6}>equipe</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function Projetos() {
  return (
    <Layout title="Projetos | REPRI - Rede de Pesquisa em Política Externa e Regionalismo">
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>

      <main>
        <section className="content">

          <div>
            <div className={clsx(styles.row, "row")}>
              {emconstrucao.map((props, idx) => (
                <EmConstrucao key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>


    </Layout>
  );
}

export default Projetos;
