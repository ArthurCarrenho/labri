import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";




const intro = [
    {
        projeto: "sobre - labri-unesp",
        imgIntro: "/img/cidades-sustentaveis/logo-ver1.svg"
    }
]

const menu = [
    {
        projeto: "sobre - labri-unesp",
        link1: "/cidades-sustentaveis",
        link2: "/cidades-sustentaveis/sobre",
        link3: "/cidades-sustentaveis/projeto",
        link4: "/cidades-sustentaveis/publicacoes",
        link5: "/cidades-sustentaveis/contato"
    }
]

const primeiracoluna = [
    {
        projeto: "sobre - labri-unesp",
        title: "SOBRE A EQUIPE",
        description: (
            <>
            O Projeto de Extensão Cidades Saudáveis e Sustentáveis foi criado em 2020, vinculado ao Centro Jurídico Social da Universidade Estadual Paulista, campus de Franca/SP. Assim, com o objetivo de difundir o conhecimento sobre essa temática, fortalecer a interlocução da universidade e a sociedade, juntamente com a finalidade de explorar o campo da inovação e das políticas públicas dos entes federativos , o grupo de extensão é composto por 16 discentes, entre eles graduandos e pós graduandos em Direito, Relações Internacionais e Arquitetura e Urbanismo, sob coordenação do Prof. Dr. Daniel Damásio Borges.
            </>
        ),
        imgFoto: "/img/cidades-sustentaveis/quem-somos-pag1.svg"
    }
]

const tituloexmembros = [
    {
        projeto: "sobre - labri-unesp",
        title: "Ex-membros"
    }
]

const exmembros = [
    {
        projeto: "sobre - labri-unesp",
        nome: "Prof. Dr. Daniel Damásio Borges",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            <p>
            Professor associado de direito internacional público da Faculdade de Ciências Humanas e Sociais da UNESP, campus Franca/SP, sendo vinculado aos programas de graduação em direito e em relações internacionais e de pós-graduação em direito desta instituição.  
            <span> Possui graduação em direito pela Faculdade de Direito da Universidade de São Paulo (2001), mestrado em direito internacional pela Faculdade de Direito da Universidade de São Paulo (2005) e pela Universidade Paris I (Panthéon-Sorbonne - 2006), doutorado em direito pela Universidade Paris I (Panthéon-Sorbonne - 2011) e livre-docência em direito internacional público pela Faculdade de Direito da Universidade de São Paulo (2017).</span>
            </p>
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Alichelly  Ventura",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            <p>
            Ex-assistente de campo do Alto Comissariado das Nações Unidas para Refugiados, escritório Região Norte do Brasil. Advogada na organização Avocats Sans Frontières. Especialista em Direitos Humanos pela American University. Mestre em Direito Ambiental pela Universidade do Estado do Amazonas. Membro da Comissão de Relações Internacionais da OAB/AM.
            <span> Coordenadora do Grupo de Estudos do Sistema Interamericano de Direitos Humanos (GESIDH). Professora da graduação e pós-graduação na ESA OAB/AM. Editora da Revista Liberdades do IBCCRIM e parecerista nas revistas  INTER da UFRJ e da Revista Científica da UESB. Coordenadora de Linha de Pesquisa do Direito Internacional Sem Fronteiras. Membro do grupo LEPADIA/UFRJ. Bolsista no curso de formação da Youth Climate Leaders.</span> 
            <span> Atualmente é pós-graduanda em Direitos Humanos pela PUC/RS e em Direito Constitucional e Direitos Humanos pelo CEI.</span>
            </p>
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "André Luiz Pereira Spinieli",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Mestre em Direito pela Universidade Estadual Paulista (UNESP/Franca). Especialista em Direitos Humanos pela Faculdade de Ciências e Tecnologias de Campos Gerais (FACICA/CEI). Especialista em Direito Constitucional pela Faculdade CERS. Bacharel em Filosofia pelo Instituto Santo Tomás de Aquino (ISTA/Belo Horizonte). Licenciado em Filosofia pelo Centro Universitário Claretiano (CEUCLAR/Batatais). Bacharel em Direito pela pela Faculdade de Direito de Franca
            (FDF/Franca). Professor de História da Filosofia Contemporânea no Instituto Agostiniano de
            Filosofia (IAF/Franca). Professor de Filosofia (PEB II) na EMEB Amélio de Paulo Coelho.
            Pesquisador do Laboratório de Estudos e Pesquisas Avançadas em Direito Internacional
            Ambiental (LEPADIA/UFRJ). Pesquisador e Professor Coordenador da linha "Direito Internacional
            Ambiental" no Grupo de Pesquisa em Direito Internacional (GPDI/UFRJ). Pesquisador e Professor
            Coordenador da linha "Direitos Humanos no Sistema Internacional" do Observatório de Direitos
            Humanos e Direitos Fundamentais (ODHDF/UCAM). Pesquisador do Núcleo de Estudos de
            Políticas Públicas (NEPPs/UNESP).
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Angélica Cardoso",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Direito pela Universidade Estadual Paulista (UNESP), campus Franca/SP. É estagiária da Procuradoria Geral da Prefeitura Municipal de Restinga e professora de inglês.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Arthur de Paula Zoadelli",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando em História pela Universidade Estadual Paulista (UNESP), campus Franca/SP. Ex-Vereador Jovem na Câmara Municipal de vereadores da cidade de Sumaré/SP.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Bárbara Canaves Domingos",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em direito pela Unesp no período de 2022 a 2026.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Beatriz Buccioli",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda de Direito na Universidade Estadual Paulista (UNESP), campus Franca/SP no período de 2018 a 2022. Bolsista de Extensão Universitária (BEU) pelo projeto de extensão "Cidades Saudáveis e Sustentáveis" e estagiária em escritório de advocacia.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Betina Rodrigues Yagi",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Direito na Universidade Estadual Paulista (UNESP), campus Franca/SP. Membro
            do Grupo de Estudos e Pesquisa em Propriedade Intelectual e Desenvolvimento
            Econômico-Social (GEPPIDES). Atuou como estagiária no escritório de advocacia Saad Diniz
            Advogados Associados de 2019 a 2021. Atualmente em programa de intercâmbio da Unesp na
            Université de Bordeaux, na França.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Carlos Eduardo Felix",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando em Direito pela Universidade Estadual Paulista, campus de Franca/SP. Técnico em Informática pelo centro Paula Souza, ETEC Bento Quirino
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Daniela Antunes Chierice Davanso",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Advogada, especialista em gestão jurídica de empresas (UNESP/Franca), em Direito e Processo
            do trabalho (UNIDERP) e em Direito Médico (IPEBJ/FVM); mestranda em Direito (UNESP/Franca);
            pesquisadora; professora e palestrante.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Danielle Pelicer de Mesquita França",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Sou graduanda em Relações Internacionais em meu 4º ano, participo da empresa júnior Orbe Consultoria Internacionais no setor de Marketing e no projeto Governamental onde desenvolvemos pesquisas em paradiplomacia, internacionalização de cidades sob a egide das ODS da ONU. Integro o grupo de extensão MKI Marketing Internacional na comissão de estudos e no projeto empresarial. Sou técnica de Multimídia pelo Centro Paulo Souza na ETEC Jornalista Roberto Marinho.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Eduarda Queiroz Fonte",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Direito pela Universidade Estadual Paulista (UNESP), campus Franca/SP, no período de 2019 a 2023. É estagiária no Tribunal de Justiça de São Paulo, na vara do Juizado Especial Cível.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Eduardo Damasceno e Silva",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando em Direito na Faculdade de Ciências Humanas e Sociais, da Universidade Estadual
            Paulista "Júlio de Mesquita Filho". Atualmente, sou Coordenador de Inteligência de Mercado na
            EJUR Soluções Jurídicas.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Gabriela Fideles",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda de Relações Internacionais pela UNESP - Faculdade de Ciência Humanas e Sociais, Campus de Franca/SP. Formada em Técnico em Enfermagem pelo Centro Paula Souza. Bolsista FAPESP no projeto de extensão: Cidades saudáveis e sustentáveis – políticas públicas urbanas em matéria de meio ambiente e saúde. Pesquisa na área de saúde global, meio ambiente e direitos humanos. Participa do Projeto As Relações Internacionais e o Novo Coronavírus, do Laboratório de Relações Internacionais (LabRI).
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Gabriela Giaqueto Gomes",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Doutoranda em Direito pela Universidade Estadual Paulista "Júlia de Mesquista Filho" (UNESP). Mestra e Graduada em Direito pela UNESP. Diretora do Núcleo do Instituto Brasileiro de Direito das Famílias (IBDFAM) de Franca/SP. Advogada (OAB/SP 430.249). Professora. Revisora de textos acaDêmicos. Conciliadora no Tribunal de Justiça do Estado de Minas Gerais. ASsistente jurídica no Tribunal de Justiça do Estado de Minas Gerais. 
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Giovanna Harpia Terra Oliveira",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Relações Internacionais pela Universidade Estadual Paulista, campus de
            Franca/SP no período de 2020 a 2023. Técnica em Informática pelo Instituto Federal do Sul de
            minas, formada em 2019. Foi integrante do Projeto Indígena, em 2018, e do Projeto Político, em
            2019, pelo IF Sul de Minas. Participou de intercâmbio cultural em Londres (Reino Unido) em
            fevereiro de 2020 e em Córdoba (Argentina) em fevereiro de 2022. Possui nível avançado na
            língua inglesa e intermediários nas línguas espanhola e francesa.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Gustavo Rodrigues",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Mestrando em Direito pelo Programa de Pós-Graduação da Faculdade de Ciências Humanas e Sociais (FCHS) da Universidade Estadual Paulista (UNESP), campus de Franca-SP. Bacharel em Direito pela Faculdade de Ciências Humanas e Sociais-UNESP-campus de Franca (2018). Pesquisador nas áreas de direito digital e direito econômico, com ênfase em criptomoedas, blockchains e fintechs. Atualmente, advogado.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Isabela Menezes",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Relações Internacionais pela Universidade Estadual Paulista, campus de Franca/SP. Integrante do Núcleo de Estudos de Políticas Públicas (NEPPs)
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Júlia Jacob Alonso",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda de Direito na Universidade Estadual Paulista (UNESP), campus Franca/SP no período
            de 2019 a 2023. Bolsista Fapesp de iniciação científica na área de direitos fundamentais das
            crianças e adolescentes, publicidade infantil, liberdade de expressão e direitos humanos.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Júlia Lourenço",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Relações Internacionais pela Universidade Estadual Paulista, campus de Franca/SP. Integrante do grupo de pesquisa "As Relações Internacionais e o Novo Coronavírus". Possui formação em inglês em nível pós avançado com certificação da Universidade de Cambridge.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Leonardo Eiji Kawamoto",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Bacharel, mestre e doutorando em Direito pela UNESP. Professor bolsista de Direito Ambiental e
            Direito Agrário na Faculdade de Ciências Humanas e Sociais (FCHS) - UNESP - câmpus de
            Franca. Bolsista da Coordenação para Aperfeiçoamento de Pessoal de Ensino Superior (CAPES).
            Pesquisador nas áreas de direito ambiental, direito do comércio internacional, direito econômico
            e produção e consumo sustentáveis. Atualmente pesquisa cadeias globais de valores e
            sustentabilidade.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Letícia de Paula Souza",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduada em Arquitetura e Urbanismo pela Universidade Federal de Uberlândia (UFU), no período
            de 2016 a 2021/2. Em 2019, desenvolveu pesquisa na área de estudos urbanos, com ênfase em
            cidades na contemporaneidade, dispersão e fragmentação, pelo Programa de Iniciação Científica
            Voluntária (PIVIC) da Universidade Federal de Uberlândia (UFU).
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Maria Eduarda Totina",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Relações Internacionais pela Universidade Estadual Paulista (UNESP), campi de
            Franca, no período de 2021 a 2024. Participo da empresa júnior de R.I (Orbe) na área de
            marketing e projetos governamentais.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Marina Vilhena",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em relações internacionais pela FCHS UNESP campus de Franca. Membro do grupo
            de estudos em política e direito ambiental internacional; Conselheira Fiscal do Centro Acadêmico
            João Cabral de Melo Neto; Membro dos grupos Extensão Núcleo Agrário Terra e Raiz, núcleo
            de ensino Abrindo Fronteiras, Núcleo de estudos da tutela penal e educação em direitos
            humanos, e do Grupo de alfabetização Paulo Freire.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Matheus Eduardo Lazari Valencia",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando em Relações Internacionais pela UNESP/FCHS, com conclusão prevista para 2023. Coordenador de imprensa no Centro Acadêmico de Relações Internacionais (CARI), presidente do Comitê de Ação Cultural (CAC) e diretor empresarial do Grupo de Estudos em Marketing Internacional (MKI).
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Matheus Guimarães Ferrete",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando em Direito pela Universidade Estadual Paulista "Júlio Mesquita Filho", câmpus de Franca/SP. Atualmente é estagiário da prefeitura de Franca/SP, atuando no Procon municipal.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Manuela Martins Kassab",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda recém-ingressada em Relações Internacionais pela Universidade Estudual Paulista, campus de Franca/SP.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Maylon Claudino",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando em Direito pela Faculdade de Direito de Franca - FDF. Pesquisador pelo programa PIBIC da FDF 2019/2020, membro do Grupo de Estudos e Pesquisa em Propriedade Intelectual e Desenvolvimento Econômico-Social ( GEPPIDES) - UNESP. Fundador e atualmente presidente do Cursinho Popular da Faculdade de Direito de Franca. Estagiário na Procuradoria Geral do Município de Franca e estagiário no escritório de advocacia CCS - Advocacia Empresarial e Digital.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Natália Ayumi Garcia Yoshida",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda do 3º ano de Relações Internacionais pela Universidade Estadual Paulista, campus de Franca/SP. Em 2017, foi monitora na ETEC Profº André Bogasian em parceria com a Universidade de São Paulo (USP) e Instituto Federal. E em 2018, foi palestrante na IV Mostra de Estágio pela FEUSP. Atualmente é membro do grupo de extensão Cidades Saudáveis e Sustentáveis. 
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Nathália Batista Vasques",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Relações Internacionais pela UNESP/Franca, primeiro período do curso.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Pedro Cardoso da Cruz Bueno",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando em Direito pela Universidade Estadual Paulista (UNESP), campus Franca/SP.
            Integrante desde 2021 do grupo de extensão, estudos e pesquisa de Constituição, Democracia e
            cidadania (NEPECC).
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Sabrina de Puiz",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda do quarto ano de Relações Internacionais pela Universidade Estadual Paulista, campus de Franca/SP. Foi membro da AIESEC Franca em 2021, trabalhando no setor de Relações Internacionais com intercâmbios voluntários relacionados as ODS (Objetivos de Desenvolvimento Sustentável da ONU).
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Sarah de Jesus Silva dos Santos",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduanda em Direito pela Universidade Estadual Paulista (UNESP), no câmpus de Franca - SP.
            Voluntária no projeto #TmjUNICEF, do UNICEF Brasil.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Tiago Figueiredo Teles Faria",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Estudante do Ensino Médio na Etec Dr. Júlio Cardoso, em Franca/SP. Bolsista de Iniciação
            Científica Junior.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Vinicius Guimarães",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando de Relações Internacionais na UNESP - Faculdade de Ciências Humanas e Socias, Campus Franca/SP. Membro do Projeto do Laboratório de Relações Internacionais (LabRI): "As Relações Internacionais e o Novo Coronavírus".
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Vitório Aflalo",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando em Relações Internacionais pela Universidade Estadual Paulista, campus de
            Franca/SP. Pesquisador pelo Núcleo de Estudos de Políticas Públicas e pelo Observatório de
            Políticas Públicas.
            </>
        )
    },
]

const titulomembrosatuais = [
    {
        projeto: "css - labri-unesp",
        title: "Membros"
    }
]

const membrosatuais = [
    {
        projeto: "sobre - labri-unesp",
        nome: "Vinicius Guimarães",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Graduando de Relações Internacionais na UNESP - Faculdade de Ciências Humanas e Socias, Campus Franca/SP. Membro do Projeto do Laboratório de Relações Internacionais (LabRI): "As Relações Internacionais e o Novo Coronavírus".
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Tiago Figueiredo Teles Faria",
        imgFoto: "/img/cidades-sustentaveis/foto-membros2.svg",
        sobre: (
            <>
            Estudante do Ensino Médio na Etec Dr. Júlio Cardoso, em Franca/SP. Bolsista de Iniciação
            Científica Junior.
            </>
        )
    },
    {
        projeto: "sobre - labri-unesp",
        nome: "Vitório Aflalo",
        imgFoto: "https://blog.unyleya.edu.br/wp-content/uploads/2017/12/saiba-como-a-educacao-ajuda-voce-a-ser-uma-pessoa-melhor.jpeg",
        sobre: (
            <>
            Graduando em Relações Internacionais pela Universidade Estadual Paulista, campus de
            Franca/SP. Pesquisador pelo Núcleo de Estudos de Políticas Públicas e pelo Observatório de
            Políticas Públicas.
            </>
        )
    },
]

function MembrosAtuais({nome, imgFoto, sobre}){
    return(
        <div className={clsx(styles.containerCards, "col col--4")}>
            <div className={clsx(styles.Cards)}>
                <h2 className={clsx(styles.h2Membros)}>{nome}</h2>
                <img src={imgFoto} alt="Foto do membro do projeto" className={clsx(styles.IconsMembros)} />
                <p className={clsx(styles.pCSS)}>{sobre}</p>
            </div>
        </div>
    )
}

function ExMembros({imgFoto, foto, nome, sobre}){
    return(
        <div className={clsx(styles.containerCards, "col col--4")}>
            <div className={clsx(styles.Cards)}>
                <h2 className={clsx(styles.h2Membros)}>{nome}</h2>
                <img src={imgFoto} alt="Foto do membro do projeto" className={clsx(styles.IconsMembros)} />
                <p className={clsx(styles.pCSS)}>{sobre}</p>
            </div>
        </div>
    )
}

function TituloMembrosAtuais({title}){
    return(
        <div className="container">
            <h2 className={clsx(styles.h2ExMembros)}>{title}</h2>
        </div>
    )
}

function TituloMembros({title}){
    return(
        <div className="container">
            <h2 className={clsx(styles.h2ExMembros)}>{title}</h2>
        </div>
    )
}

function PrimeiraColuna({title, description, imgFoto, foto}){
    return(
        <div className={clsx(styles.containerSobre)}>
            <div className="row">
                <div className={clsx(styles.sobre, "col col--6")}>
                    <img src={imgFoto} alt="Ilustração da Equipe" />
                </div>
                <div className="col col--6">
                    <h2 className={clsx(styles.h2CSS)}>{title}</h2>
                    <p className={clsx(styles.pCSS)}>{description}</p>
                </div>
            </div>
        </div>
    )
}

function Menu({link1, link2, link3, link4, link5}){
    return(
        <div className={clsx(styles.Menu)}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}><a href={link1}>HOME</a></li>
            <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
            <li className={clsx(styles.liLista)}><a href={link3}>PROJETO</a></li>
            <li className={clsx(styles.liLista)}><a href={link4}>PUBLICAÇÕES</a></li>
            <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
          </ul>
        </div>
    )
}

function Intro({imgIntro, projeto}){
    return(
        <div className={clsx(styles.heroBanner)}>
        <div className={styles.LogoCSS}>
            <img src={imgIntro} alt="Logo Cidades Saudáveis e Sustentáveis" />
        </div>
    </div>
    )
}

function Sobre(){
    return(
    <Layout title="Cidades Saudáveis e Sustentáveis">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <header className={clsx(styles.heroBanner)}>
            <div>
            {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
            ))}
            </div>
        </header>
        <main className={clsx(styles.main)}>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className="row">
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className="row">
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className="row">
              {titulomembrosatuais.map((props, idx) => (
                <TituloMembrosAtuais key={idx} {...props} />
              ))}
            </div>
          </div>
          
          <div className={clsx(styles.container)}>
          <div className="row">
              {membrosatuais.map((props, idx) => (
                <MembrosAtuais key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className="row">
              {tituloexmembros.map((props, idx) => (
                <TituloMembros key={idx} {...props} />
              ))}
            </div>
          </div>


          <div className={clsx(styles.container)}>
          <div className="row">
              {exmembros.map((props, idx) => (
                <ExMembros key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
        </main>
    </Layout>
    )
}

export default Sobre; 
