import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";

const intro = [
    {
        projeto: "sobre - labri-unesp",
        imgIntro: "/img/cidades-sustentaveis/logo-ver1.svg"
    }
]

const menu = [
    {
        projeto: "css - labri-unesp",
        link1: "/cidades-sustentaveis",
        link2: "/cidades-sustentaveis/sobre",
        link3: "/cidades-sustentaveis/projeto",
        link4: "/cidades-sustentaveis/publicacoes",
        link5: "/cidades-sustentaveis/contato"
    }
]

const mapa = [
    {
        projeto: "contato - labri-unesp",
        teste: "Testando Mapa embed"
    }
]

const contatos = [
    {
      projeto: "css - labri-unesp",
      titulo1: "Contatos",
      email: (<>E-mail: projetodeextensaocss@gmail.com </>),
      redesSociais01: "Instagram: @projeto_css",
      redesSociais02: "Facebook: https://www.facebook.com/cidades.ss",
      imgFoto1: "/img/cidades-sustentaveis/franca-sp.png"
    }
  ]

function Contatos({titulo1,email, redesSociais01, redesSociais02, imgFoto1}){
    return(
      <div className={clsx(styles.containerColuna1)}>
        <div className={clsx(styles.row, "row")}>
          <div className="col col--4">
            <h2 className={clsx(styles.h2CSS)}>{titulo1}</h2>
            <p className={clsx(styles.pCSS)}>{email}</p>
            <p className={clsx(styles.pCSS)}>{redesSociais01}</p>
            <p className={clsx(styles.pCSS)}>{redesSociais02}</p>
          </div>
          <div className="col col--8">
            <img src={imgFoto1} alt="Cidade de Franca" className={clsx(styles.coluna1)} />
          </div>
        </div>
      </div>
    )
  }

function Menu({link1, link2, link3, link4, link5}){
    return(
        <div className={clsx(styles.Menu)}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}><a href={link1}>HOME</a></li>
            <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
            <li className={clsx(styles.liLista)}><a href={link3}>PROJETO</a></li>
            <li className={clsx(styles.liLista)}><a href={link4}>PUBLICAÇÕES</a></li>
            <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
          </ul>
        </div>
    )
}

function Intro({projeto, imgIntro}){
    return(
      <div className={clsx(styles.heroBanner)}>
        <div className={styles.LogoCSS}>
            <img src={imgIntro} alt="Logo Cidades Saudáveis e Sustentáveis" /> 
        </div>
    </div>
    )
}


function Contato(){
    return(
        <Layout title="Cidades Saudáveis e Sustentáveis">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
        <header className={clsx(styles.heroBanner)}>
            <div>
            {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
            ))}
            </div>
        </header>
        <main className={clsx(styles.main)}>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className="row">
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
          </div>
          </div>
          <div className={clsx(styles.container)}>
          <div className={clsx(styles.row, "row")}>
              {contatos.map((props, idx) => (
                <Contatos key={idx} {...props} />
              ))}
            </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default Contato;