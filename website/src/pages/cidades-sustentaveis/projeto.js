import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";

const intro = [
    {
        projeto: "sobre - labri-unesp",
        imgIntro: "/img/cidades-sustentaveis/logo-ver1.svg"
    }
]

const menu = [
    {
        projeto: "css - labri-unesp",
        link1: "/cidades-sustentaveis",
        link2: "/cidades-sustentaveis/sobre",
        link3: "/cidades-sustentaveis/projeto",
        link4: "/cidades-sustentaveis/publicacoes",
        link5: "/cidades-sustentaveis/contato"
    }
]

const conteudo = [
    {
        projeto: "css - labri-unesp",
        title1: "CONHECENDO NOSSO PROJETO",
        description1: (
            <>
            O Projeto de Extensão Cidades Saudáveis e Sustentáveis: políticas públicas em matéria de meio ambiente e saúde em uma abordagem de direito internacional e comparado foi criado em 2020, vinculado ao Centro Jurídico Social da Universidade Estadual Paulista, campus de Franca/SP.
            </>
        ),
        imgFoto1: "/img/cidades-sustentaveis/sobre-projeto.svg",
        title2: "Sobre o nosso projeto",
        description2: (
            <>
            Tendo em vista a estreita relação entre saúde e meio ambiente urbano, a Organização Mundial da Saúde (OMS) tem desenvolvido por meio de políticas públicas, em especial no âmbito urbano, a promoção e proteção da saúde e do meio ambiente. Assim, partindo dessa premissa, o objetivo do projeto de extensão é difundir o conhecimento acerca dessas políticas com uma abordagem crítica. Dessa forma, com a metodologia participativa, por meio de cursos, seminários, rodas de conversa junto aos membros da sociedade civil e aos gestores públicos, dando voz para que apresentem suas visões sobre o tema do projeto, isto é, enfatizando o diálogo entre sociedade e a universidade. 
            </>
        ),
        imgFoto2: "/img/cidades-sustentaveis/sobre-projeto2.svg",
        description3: (
            <>
            Partindo desse princípio, a pesquisa permitirá o levantamento e identificação de dados referentes à relação entre políticas públicas urbanas e saúde pública, tendo como objeto de estudo a cidade de Franca/SP. Logo, a partir dessa coleta de dados e divulgação por meio de oficinas e seminários, tem-se como resultado a visão prática entre os problemas que suscitam as políticas públicas municipais em prol da saúde e do meio ambiente, como também, a disseminação do conhecimento para os diferentes setores da sociedade civil, contribuindo para um olhar mais crítico e aprofundado sobre a temática e as abordagens disso dentro da própria cidade. 
            </>
        )
    }
]

function Conteudo({title1, title2, description1, description2, description3, imgFoto1, imgFoto2, foto1, foto2}){
    return(
        <div className={clsx(styles.containerSobre)}>
            <div className="row">
                <div className={clsx(styles.sobre, "col col--6")}>
                    <img src={imgFoto1} alt="Ilustração" />
                </div>
                <div className="col col--6">
                    <h2 className={clsx(styles.h2CSS)}>{title1}</h2>
                    <p className={clsx(styles.pCSS)}>{description1}</p>
                </div>
                <div className="col col--6">
                    <h2 className={clsx(styles.h2CSS)}>{title2}</h2>
                    <p className={clsx(styles.pCSS)}>{description2}</p>
                </div>
                <div className={clsx(styles.sobre, "col col--6")}>
                    <img src={imgFoto2} alt="Ilustração" />
                </div>  
                <div className={clsx(styles.sobre, "col col--12")}>
                    <p className={clsx(styles.pCSS)}>{description3}</p>
                </div> 
            </div>
        </div>
    )
}

function Menu({link1, link2, link3, link4, link5}){
    return(
        <div className={clsx(styles.Menu)}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}><a href={link1}>HOME</a></li>
            <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
            <li className={clsx(styles.liLista)}><a href={link3}>PROJETO</a></li>
            <li className={clsx(styles.liLista)}><a href={link4}>PUBLICAÇÕES</a></li>
            <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
          </ul>
        </div>
    )
}

function Intro({imgIntro, projeto}){
    return(
        <div className={clsx(styles.heroBanner)}>
        <div className={styles.LogoCSS}>
            <img src={imgIntro} alt="Logo Cidades Saudáveis e Sustentáveis" />
        </div>
    </div>
    )
}

function Projeto(){
    return(
        <Layout title="Cidades Saudáveis e Sustentáveis">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            
        <header className={clsx(styles.heroBanner)}>
            <div>
            {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
            ))}
            </div>
        </header>
        <main className={clsx(styles.main)}>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className="row">
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className="row">
              {conteudo.map((props, idx) => (
                <Conteudo key={idx} {...props} />
              ))}
            </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default Projeto;