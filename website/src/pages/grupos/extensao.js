import React from "react";
import Layout from "@theme/Layout";
import styles from "../styles.module.scss";
import clsx from "clsx";
import useBaseUrl from "@docusaurus/useBaseUrl";

const projetos = [
  {
    title: "Grupo de Estudos e Extensão em Marketing Internacional (MkI)",
    imageUrl: "/img/grupos/mki_square.png",
    description: (
      <>
        "o Grupo de Estudos e Extensão em Marketing Internacional (MKI)
        constitui um grupo institucionalizado e vinculado aos programas de
        extensão universitária da UNESP Franca. O MKI realiza não só atividades
        teóricas provenientes da prática acadêmica, mas também ações com a
        comunidade por meio de projetos, estudos e eventos ligados ao marketing
        e ao marketing internacional."
      </>
    ),
    pessoas: "Paula Regina de Jesus Pinsetta Pavarina",
    website: "https://www.mkimarketinginternacional.com/",
    rede1: "https://www.facebook.com/MKIFRANCA/about/",
    rede1Img: "/img/social/facebook.svg",
    rede2: "",
    rede2Img: "",
  },
  {
    title: "Observatório de Política Exterior Brasileira (OPEx)",
    imageUrl: "/img/grupos/opex_square.png",
    description: (
      <>
        O Observatório de Política Exterior (OPEx) é um projeto de informação
        semanal executado pelo Grupo de Estudos de Defesa e Segurança
        Internacional (GEDES), do Centro de Estudos Latino-americanos (CELA) da
        Universidade Estadual Paulista “Júlio de Mesquita Filho” (UNESP), campus
        de Franca. Mais informações, clique aqui.
      </>
    ),
    pessoas: "Eduardo Mei",
    rede1: "https://www.facebook.com/riodepoliticaexterior/",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/2229235579487472",
    rede2Img: "/img/social/dip.png",
  },
  {
    title: "Observatório Sul-Americano de Defesa e Forças Armadas",
    imageUrl: "/img/grupos/observatorio_lam.png",
    description: (
      <>
        "O Observatório Sul-Americano de Defesa e Forças Armadas é um projeto de
        informação semanal executado pelo Grupo de Estudos de Defesa e Segurança
        Internacional (GEDES) da Universidade Estadual Paulista “Júlio de
        Mesquita Filho” (UNESP) em conjunto com o Instituto de Ciência Política
        da Universidade da República, Uruguai; o Programa de Pesquisa Forças
        Armadas e Sociedade do Centro de Estudos Estratégicos do Chile; o
        Programa de Pesquisas sobre Forças Armadas e Sociedade da Universidade
        Nacional de Quilmes, Argentina; o Grupo de Pesquisa em Segurança e
        Defesa da Universidade Nacional da Colômbia; o Controle Cidadão para a
        Segurança, a Defesa e a Força Armada Nacional, Venezuela; o Grupo de
        Estudos Comparados em Política Externa e Defesa da Universidade Federal
        de Sergipe (UFS) e o Observatório das Fronteiras do Platô das Guianas
        (OBFRON) da Universidade Federal do Amapá (Unifap)."
      </>
    ),
    pessoas: "Hector Luis Saint Pierre",
    rede1: "https://www.facebook.com/observatoriodedefesa/",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/2229235579487472",
    rede2Img: "/img/social/dip.png",
  },
  {
    title: "Empresa Junior de Relações Internacionais (ORBE)",
    imageUrl: "/img/grupos/orbe.png",
    description: (
      <>
        "A ORBE é uma Empresa Júnior de Relações Internacionais, vinculada à
        Universidade Estadual Paulista - "Júlio de Mesquita Filho" (Unesp) na
        cidade de Franca. A EJ se originou no ano 2003 e é composta por
        estudantes de relações internacionais do primeiro ao quarto ano que
        estejam interessados em ter uma experiência corporativa no meio
        universitário. Desde então, oferece consultoria aos três setores da
        sociedade, com o objetivo de viabilizar a internacionalização destes e
        também, gerar experiência profissional e conhecimento acadêmico para os
        alunos."
      </>
    ),
    website: "https://www.orbe-ri.com/",
    RedesSociais:
      "https://www.facebook.com/pages/ORBE-Empresa-J%C3%BAnior-de-Rela%C3%A7%C3%B5es-Internacionais-Unesp-Franca/150743048328626",
    rede1Img: "/img/social/facebook.svg",
    rede2: "",
    rede2Img: "",
  },
];

function Projeto({ imageUrl, title, description, website }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>
        <h3>{title}</h3>
        <p>
          {" "}
          <b> Descrição: </b> {description}
        </p>
        <br></br>
        <a class="button button--secondary" href={website}>
          Saiba Mais
        </a>
      </div>
    </div>
  );
}

function Extensao() {
  return (
    <Layout title="Grupos de Extensão">
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className={styles.hero__title}>Grupos de Extensão</h1>
          <h2 className={styles.subtituloProjetos}>
            Conheça os grupos de extensão do departamento de Relações
            Internacionais da UNESP/Franca
          </h2>
        </div>
      </header>
      <main className={styles.main}>
        <section className={styles.content}>
          {projetos && projetos.length > 0 && (
            <section className={styles.projetos}>
              <div className={styles.container}>
                {projetos.map((props, idx) => (
                  <Projeto key={idx} {...props} />
                ))}
              </div>
            </section>
          )}
        </section>
      </main>
    </Layout>
  );
}

export default Extensao;
