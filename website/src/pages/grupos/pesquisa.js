import React from "react";
import Layout from "@theme/Layout";
import styles from "../styles.module.scss";
import clsx from "clsx";
import useBaseUrl from "@docusaurus/useBaseUrl";

const projetos = [
  {
    title: "Núcleo de Estudos em Políticas Públicas (NEPPs)",
    imageUrl: "/img/grupos/nepps_square.png",
    description: (
      <>
        O Núcleo de Estudos de Políticas Públicas (NEPPs) é composto por
        docentes, pesquisadores e estudantes de diferentes áreas do
        conhecimento: Ciência Política, Serviço Social, Sociologia, Ciências
        Econômicas, Geografia, Direito, Demografia e Relações Internacionais.
        Seu principal objetivo é de fortalecer o debate sobre as políticas
        públicas no âmbito da Universidade e fora dela, assim como, realizar
        trabalhos práticos dentro das suas linhas de pesquisa. Desde seu início
        o Núcleo tem se fortalecido por meio da realização de várias pesquisas
        nas suas diversas linhas. Mas o trabalho do NEPPs não se restringe a
        estas iniciativas de pesquisa. Há um esforço para a promoção da
        discussão teórica regular que, ao mesmo tempo, em que baliza as
        pesquisas realizadas, aprofunda o debate de temas que têm chamado a
        atenção da área. É com este intuito que tem se promovido seminários
        bianuais a partir de temas pertinentes ao campo das Políticas Públicas."
        (Fonte: Dados do Grupo junto ao CNPQ)
      </>
    ),
    pessoas: "Regina Claudia Laisner, Paula Regina de Jesus Pinsetta Pavarina ",
    website: "https://neppsfranca.wixsite.com/nepps",
    rede1:
      "https://www.facebook.com/NEPPs-N%C3%BAcleo-de-Estudos-em-Pol%C3%ADticas-P%C3%BAblicas-da-UNESP-Franca-141044582734794/",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/4514725451786971",
    rede2Img: "/img/social/dip.png",
  },
  {
    title: "Grupo de Estudos de Defesa e Segurança Internacional (GEDES)",
    imageUrl: "/img/grupos/gedes_square.png",
    description: (
      <>
        Definir conceitos relevantes para a analise estratégica e para a
        formulação de políticas públicas nas áreas de Paz, Missões de Paz, de
        Defesa e de Segurança Internacional. Formar quadros para analise e
        formulação de políticas ou assessorar aos que devem decidir sobre as
        mesmas. Aproximar os estudos acadêmicos dos formuladores de políticas na
        área da Defesa e Segurança e das Forças Armadas. Fornecer para os
        tomadores de decisão nas políticas públicas análises técnicas sobre a
        situação e perspectivas da cooperação no âmbito da Defesa com os países
        vizinhos." (Fonte: Dados do Grupo junto ao CNPQ)
      </>
    ),
    pessoas:
      "Héctor Luis Saint-Pierre, Suzeley Kalil Mathias, Eduardo Mei, Fernanda Mello Sant'Anna, Luís Alexandre Fuccille e Samuel Alves Soares",
    website: "http://www.gedes.org.br/",
    rede1: "https://www.facebook.com/GEDESORG/",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/2229235579487472",
    rede2Img: "/img/social/dip.png",
  },
  {
    title:
      "Grupo de Estudos em Tecnologias de Defesa e a Evolução do Pensamento Estratégico (GETED)",
    imageUrl: "/img/grupos/geted.png",
    description: (
      <>
        "Sob a perspectiva multidisciplinar, como grupo de pesquisa permanente e
        aberto, seu objetivo é analisar o impacto que tecnologias de defesa
        provocam na relação do Estado com o espaço, permitindo mudanças
        estruturais na Ciência Política, no Direito, na Geopolítica, nas
        Relações Internacionais e nos Estudos estratégicos. Desta forma, se
        analisará o desenvolvimento e a operação de novas tecnologias de defesa
        e também de tecnologias de uso dual em cenários estratégicos, cuja
        potencialidade impacte em mudanças estruturais na relação dos Estados
        com as velhas e novas ameaças em terra, no mar, no ar e no mundo
        virtual. Diante destas mudanças estruturais, se verificará e se
        compreenderá a obtenção de ganhos estratégicos por partes dos Estados. O
        Grupo se utilizará de conhecimentos multidisciplinares que vão desde as
        Relações Internacionais, Geopolítica e História até Tecnologia da
        Informação, Robótica e Ciência Aeroespacial." (Fonte: Dados do Grupo
        junto ao CNPQ)
      </>
    ),
    pessoas: "Luís Alexandre Fuccille, Eduardo Mei",
    website: "",
    rede1: "",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/2479572547131023",
    rede2Img: "/img/social/dip.png",
  },
  {
    title: "Núcleo de estudos linguísticos e culturais",
    imageUrl: "/img/grupos/nelc.png",
    pessoas: "Elizabete Sanches Rocha",
    website: "",
    rede1: "https://www.facebook.com/GEDESORG/",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/2479572547131023",
    rede2Img: "/img/social/dip.png",
  },
  {
    title: "Grupo de Elaboração de Cenários Prospectivos",
    imageUrl: "/img/grupos/cenarios_square.png",
    description: (
      <>
        "O Grupo de Elaboração de Cenários Prospectivos foi criado com o intuito
        de elaborar cenários prospectivos, tendo como campo de ação a política
        internacional. Desenvolve atividades de pesquisa de aprofundamento sobre
        a temática de cenários, e, precipuamente, ocupa-se em estabelecer
        critérios, definir horizontes temporais e elaborar exercícios de
        prospecção que visam propor rotas de ação política no presente tendo
        como balizamento perspectivas de futuro. O Grupo tem ainda como foco
        importante o estudo sobre questões metodológicas e epistemológicas
        referidas à construção de cenários, adotando uma postura crítica em
        relação à técnicas pretensamente neutras." (Fonte: Dados do Grupo junto
        ao CNPQ)
      </>
    ),
    pessoas:
      "Samuel Alves Soares, Fernanda de Mello Sant'Anna, Héctor Luis Saint-Pierre, Paula Regina de Jesus Pinsetta Pavarina e Suzeley Kalil Mathias",
    website: "http://cenariosprospectivos.wixsite.com/site-cenarios",
    rede1: "https://www.facebook.com/CenariosProspectivos/",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/3223742826971606",
    rede2Img: "/img/social/dip.png",
  },
  {
    title: "Grupo de Estudos em Política e Direito Ambiental Internacional",
    imageUrl: "/img/grupos/direito_ambiental.png",
    pessoas: "Fernanda de Mello Sant'Anna",
    rede1: "",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/0109822021593138",
    rede2Img: "/img/social/dip.png",
  },

  {
    title:
      "Laboratório de Novas Tecnologias de Pesquisa em Relações Internacionais (LANTRI)",
    imageUrl: "/img/grupos/lantri_square.png",
    description: (
      <>
        "O objetivo geral deste grupo é a realização de pesquisas em Relações
        Internacionais, mantendo a preocupação constante em aprimorar os
        aspectos básicos da metodologia do trabalho científico em ciências
        humanas e sociais, buscando novos instrumentais e procedimentos técnicos
        a fim de proporcionar maior intensificação do uso das tecnologias de
        informação e comunicação no cotidiano dos pesquisadores e nos trabalhos
        realizados em grupo." (Fonte: Dados do Grupo junto ao CNPQ)
      </>
    ),
    pessoas: "Marcelo Passini Mariano e Gabriel Cepaluni",
    rede1: "https://www.facebook.com/lantriunesp",
    rede1Img: "/img/social/facebook.svg",
    rede2: "http://dgp.cnpq.br/dgp/espelhogrupo/1286639006327641",
    rede2Img: "/img/social/dip.png",
  },
];

function Projeto({
  imageUrl,
  title,
  description,
  pessoas,
  instituicoesEnvolvidas,
  financiamento,
  link,
  website,
  rede1,
  rede1Img,
  rede2,
  rede2Img,
}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>
        <h3>{title}</h3>
        <p>
          {" "}
          <b> Descrição: </b> {description}
        </p>
        <p>
          {" "}
          <b> Docentes do DERI envolvidos: </b> {pessoas}
        </p>
        <div className={styles.redesSociais}>
          <a href={website}>
            <img
              className={styles.loginho}
              src={useBaseUrl("/img/social/site.svg")}
              alt={website}
            />
          </a>
          <a href={rede1}>
            <img
              className={styles.loginho}
              src={useBaseUrl(rede1Img)}
              alt={rede1}
            />
          </a>
          <a href={rede2}>
            <img
              className={styles.loginho}
              src={useBaseUrl(rede2Img)}
              alt={rede2}
            />
          </a>
        </div>
      </div>
    </div>
  );
}

function Pesquisa() {
  return (
    <Layout title="Grupos de Pesquisa">
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className={styles.hero__title}>Grupos de Pesquisa</h1>
          <h2 className={styles.subtituloProjetos}>
            Conheça os grupos de pesquisa do departamento de Relações
            Internacionais da UNESP/Franca
          </h2>
        </div>
      </header>
      <main className={styles.main}>
        <section className={styles.content}>
          {projetos && projetos.length > 0 && (
            <section className={styles.projetos}>
              <div className={styles.container}>
                {projetos.map((props, idx) => (
                  <Projeto key={idx} {...props} />
                ))}
              </div>
            </section>
          )}
        </section>
      </main>
    </Layout>
  );
}

export default Pesquisa;
