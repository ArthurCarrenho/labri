import React, { Component } from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.observatorio.module.css";
import stylesSass from "./styles.novo.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";



const intro = [
    {
        projeto: "gepdai - labri-unesp",
        imgIntro: "/img/gepdai/capa-2.svg"
    }
]

const menu = [
    {
        projeto: "gepdai - labri-unesp",
        link1: "/gepdai",
        link2: "/gepdai/sobre",
        link3: "/gepdai/equipe",
        link4: "/gepdai/observatorio",
        link5: "/gepdai"
    }
]

const navbar = [
    {
        projeto: "gepdai - labri-unesp",
        link1: "#sobre",
        link2: "#membros",
        link3: "#",
        link4: "#",
        link5: "#"
    }
]

const sobre = [
    {
        projeto: "gepdai - labri-unesp",
        title: "Conheça o Observatório",
        imgFoto: "/img/gepdai/sobre.svg",
        texto: (
            <>
            <p>O Observatório consectetur adipiscing elit. Nam maximus ultricies dignissim. Aliquam auctor pulvinar 
            urna, luctus imperdiet ante volutpat vitae. Sed pulvinar sapien sapien, vel feugiat nibh mattis 
            congue. Nunc vel enim suscipit eros finibus semper. Maecenas hendrerit est eu urna efficitur, id 
            imperdiet nisl dignissim.</p>  
            Aliquam auctor pulvinar urna, luctus imperdiet ante volutpat vitae. Sed pulvinar sapien sapien, 
            vel feugiat nibh mattis congue. Nunc vel enim suscipit eros finibus semper. Maecenas hendrerit est
            eu urna efficitur, id imperdiet nisl dignissim.  
            </>
        )
    }
]

const membros = [
    {
        projeto: "gepdai labri-unesp",
        title: "Membros"
    }
]

const membrosinfo = [
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon3.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon2.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon3.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon2.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
]


function MembrosInfo({nome, imgFoto, foto, rede1, rede2, linkRede, linkRede2, imgLogo, imgLogo2}){
    return(
        <div className={clsx(styles.Info, "card col col--4")}>
          <div className="card-content">
            <div className="card__header">
              <div className={clsx(styles.Info, "text__center")}>
                <img src={imgFoto} alt="Foto do membro da equipe do Observatório do GEPDAI" />
              </div>
            </div>
            <div className="card__body">
              <h2 className={clsx(styles.h2Gepdai)}>{nome}</h2>
            </div>
            <div className={clsx(styles.Icons, "card__footer")}>
              <img src={imgLogo} alt="Logo da Rede Social 1" href={linkRede} />
              <img src={imgLogo2} alt="Logo da Rede Social 2" href={linkRede2} />
            </div>
          </div>
          <a className="button button--secondary" href="#nav">Voltar para o menu</a>
        </div>
    )
}

function Membros({title, imgFoto, foto, imgLogo, imgLogo2, rede1, rede2, linkRede, linkRede2}){
    return(
        <div className="container">
            <div className={clsx(styles.row, "row")}>
                <div className="col col--12">
                    <h2 id="membros" className={clsx(styles.h2Gepdai)}>
                        {title}
                    </h2>
                </div>
            </div>
        </div>
    )
}

function Sobre({title, imgFoto, foto, texto}){
    return(
        <div className={clsx(styles.containerNav)}>
            <div className={clsx(styles.row, "row")}>
                <div className="col col--12">
                    <h2 id="sobre" className={clsx(styles.h2Gepdai)}>
                        {title}
                    </h2>
                </div>
                <div className="col col--8">
                    <p className={clsx(styles.pConhecer)}>{texto}</p>
                    <a className="button button--secondary" href="#nav">Voltar para o menu</a>
                </div>
                <div className="col col--4">
                    <img className={clsx(styles.conhecerSobre)} src={imgFoto} alt="Ilustração sobre GEPDAI" />
                </div>
            </div>
        </div>
    )
}

function NavBar({link1, link2, link3, link4, link5}){
    return(
        <div id="nav" className={clsx(styles.NavBar)}>
            <ul className={clsx(styles.ulNavBar)}>
                <li className={clsx(styles.liNavBar)}><a href={link1}>SOBRE</a></li>
                <li className={clsx(styles.liNavBar)}><a href={link2}>MEMBROS</a></li>         
                <li className={clsx(styles.liNavBar)}><a href={link3}>PODCAST</a></li>         
                <li className={clsx(styles.liNavBar)}><a href={link4}>MAPA</a></li>         
                <li className={clsx(styles.liNavBar)}><a href={link5}>BOLETIM</a></li>                 
            </ul>
        </div>
    ) 
}

function Menu({link1, link2, link3, link4, link5}){
    return(
        <div className={clsx(styles.Menu)}>
            <ul className={clsx(styles.ulLista)}>
              <li className={clsx(styles.liLista)}><a href={link1}>INÍCIO</a></li>
              <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
              <li className={clsx(styles.liLista)}><a href={link3}>EQUIPE</a></li>
              <li className={clsx(styles.liLista)}><a href={link4}>OBSERVATÓRIO</a></li>
              <li className={clsx(styles.liLista)}><a href={link5}>ARTIGOS</a></li>
            </ul>
        </div>)
}

function Intro({imgIntro, projeto}){
    return(
            <div className={clsx("hero hero--primary", styles.heroBanner)}>
                <div className={styles.LogoGepdai}>
                <img src={imgIntro} alt="Logo GEPDAI" />
            </div>
            </div>
    )
}

function Observatorio(){
    const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        }

    return(
        <Layout title="Observátorio GEPDAI">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div>
              {intro.map((props, idx) => (
                <Intro key={idx} {...props} />
              ))}
            </div>
          </header>
          <main className={clsx(styles.main)}>
              <section className={styles.content}>
                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.row, "row")}>
                    {menu.map((props, idx) => (
                    <Menu key={idx} {...props} />
                    ))}
                    </div>
                </div>

                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.row, "row")}>
                    {navbar.map((props, idx) => (
                    <NavBar key={idx} {...props} />
                    ))}
                    </div>
                </div>

                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.row, "row")}>
                    {sobre.map((props, idx) => (
                    <Sobre key={idx} {...props} />
                    ))}
                    </div>
                </div>

                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.row, "row")}>
                    {membros.map((props, idx) => (
                    <Membros key={idx} {...props} />
                    ))}
                    </div>
                </div>

                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.row, "row")}>
                    {membrosinfo.map((props, idx) => (
                    <MembrosInfo key={idx} {...props} />
                    ))}
                    </div>
                </div>
            </section>
            </main>
        </Layout>
    )
}

export default Observatorio;