import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import useBaseUrl from "@docusaurus/useBaseUrl";


const intro = [
  {
    projeto: "gepdai - labri-unesp",
    imgIntro: "/img/gepdai/capa-2.svg",
  },
];

const menu = [
  {
    projeto: "gepdai - labri-unesp",
    link1: "/gepdai",
    link2: "/gepdai/sobre",
    link3: "/gepdai/equipe",
    link4: "/gepdai/observatorio",
    link5: "/gepdai"
  }
]

const primeiracoluna = [
  {
    projeto: "gepdai - labri-unesp",
    titulo1: "Conheça a Equipe do Gepdai",
    texto1: (
      <>
        A nossa Equipe conta com consectetur adipiscing elit. Nam maximus ultricies dignissim. Aliquam auctor pulvinar urna, luctus imperdiet ante volutpat vitae.
      </>
    ), 
    imgFoto1: "/img/gepdai/time.svg"
  }
]

const segundacoluna = [
  {
    projeto: "gepdai - labri-unesp",
    titulo: "Conheça nossa história",
    imgFoto: "/img/gepdai/sobre-pag1.svg",
    link: "/sobre",
    texto: (
      <>
        O Grupo de Estudo em Política e Direito Ambiental Internacional (GEPDAI) se dedica a analisar e compreender os temas da Política e do Direito Ambiental em uma perspectiva das Relações Internacionais.
      </>
    )
  },
  {
    projeto: "gepdai - labri-unesp",
    titulo: "Conheça o observatório",
    imgFoto: "/img/gepdai/sobre-pag2.svg",
    link: "/observatorio",
    texto: (
      <>
        O Observatório consectetur adipiscing elit. Nam maximus ultricies dignissim. Aliquam auctor pulvinar urna, luctus imperdiet ante volutpat vitae.
      </>
    )
  }
]

const carousel = [
  {
    projeto: "gepdai - labri-unesp",
    imgFoto1: "/img/gepdai/slide1.svg",
    link1: "/gepdai",
    imgFoto2: "/img/gepdai/slide2.svg",
    link2: "/gepdai",
    imgFoto3: "/img/gepdai/slide3.svg",
    link3: "/gepdai",
    imgFoto4: "/img/gepdai/slide4.svg",
    link4: "/gepdai",
  },
]

const formulario = [
  {
    projeto: "gepdai - labri-unesp",
    texto: "Faça parte da nossa equipe",
    texto2: "Participe dos nossos eventos",
    imgFoto: "/img/gepdai/teste.png"
  }
]

function Formulario({ texto, texto2, imgFoto, foto }) {
  return (
    <div className="container">
      <div className={clsx(styles.row, "row")}>
        <div className="col col--6">
          <h2 className={clsx(styles.h2Gepdai)}>
            {texto}
          </h2>
          <form action="#" method="get" className={clsx(styles.formulario)}>
            <p className={clsx(styles.pFormulario)}>E-mail institucional:</p>
            <input type="email" className="formulario__campo" />
            <a href="#" className={clsx(styles.pLinkformulario)}>
              Enviar
            </a>
          </form>

          <h2 className={clsx(styles.h2Gepdai)}>
            {texto2}
          </h2>
          <form action="#" method="get" className={clsx(styles.formulario)}>
            <p className={clsx(styles.pFormulario)}>E-mail institucional:</p>
            <input type="email" className="formulario__campo" />
            <a href="#" className={clsx(styles.pLinkformulario)}>
              Enviar
            </a>
          </form>
        </div>
        <div className={clsx(styles.SobreCards, "col col--6")}>
          <img src={imgFoto} alt="Ilustração" />
        </div>
      </div>
    </div>
  )
}

function Carousel({ imgFoto1, link1, imgFoto2, link2, imgFoto3, link3, imgFoto4, link4 }) {
  const settings =
  {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  }

  return (
    <div className="container">
      <div className={clsx(styles.row, "row")}>
        <div className="col col--12">
          <h2 className={clsx(styles.h2Gepdai)}>ÚLTIMOS POSTS</h2>
        </div>
      </div>
      <Slider {...settings}>
        <div>
          <img src={imgFoto1} alt="Imagem do primeiro slide" href={link1} />
        </div>
        <div>
          <img src={imgFoto2} alt="Imagem do segundo slide" href={link2} />
        </div>
        <div>
          <img src={imgFoto3} alt="Imagem do terceiro slide" href={link3} />
        </div>
        <div>
          <img src={imgFoto4} alt="Imagem do quarto slide" href={link4} />
        </div>
      </Slider>
    </div>
  )
}

function SegundaColuna({ titulo, texto, imgFoto, foto, link }) {
  return (
    <div className={clsx(styles.info, "card col col--6")}>
      <div className="card-content">
        <div className="card__header">
          <h2 className={clsx(styles.h2Gepdai)}>{titulo}</h2>
        </div>
        <div className={clsx(styles.SobreCards, "card__body")}>
          <img src={imgFoto} alt="Ilustração do card" />
        </div> 
        <div className="card__footer">
          <p className={clsx(styles.pTextos)}>{texto}</p>
          <a href={link} className="button button--secondary">Saiba mais</a>
        </div>
      </div>
    </div>
  )
}

function PrimeiraColuna({ titulo1, texto1, imgFoto1, foto1 }) {
  return (
    <div className={clsx(styles.containerColuna1)}>
      <div className={clsx(styles.row, "row")}>
        <div className="col col--6">
          <h2 className={clsx(styles.h2Gepdai)}>{titulo1}</h2>
          <p className={clsx(styles.pTextos)}>{texto1}</p>
          <a className="button button--secondary" href="/equipe">Saiba mais</a>
        </div>
        <div className={clsx(styles.SobreEquipe, "col col--6")}>
          <img src={imgFoto1} alt="Ilustração Equipe GEPDAI" />
        </div>
      </div>
    </div>
  )
}

function Intro({ projeto, imgIntro }) {
  return (
    <div className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className={styles.LogoGepdai}>
        <img src={imgIntro} alt="Logo GEPDAI" className={clsx(styles.LogoGepdai)} />
      </div>
    </div>
  );
}

function Menu({ link1, link2, link3, link4, link5 }) {
  return (
    <div className={clsx(styles.Menu)}>
      <ul className={clsx(styles.ulLista)}>
        <li className={clsx(styles.liLista)}><a href={link1}>INÍCIO</a></li>
        <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
        <li className={clsx(styles.liLista)}><a href={link3}>EQUIPE</a></li>
        <li className={clsx(styles.liLista)}><a href={link4}>OBSERVATÓRIO</a></li>
        <li className={clsx(styles.liLista)}><a href={link5}>ARTIGOS</a></li>
      </ul>
    </div>
  )
}

function Gepdai() {
  return (
    <Layout title="GEPDAI">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <head>
        <link
          rel="stylesheet"
          type="text/css"
          charset="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"
        />
      </head>

      <header className={clsx(styles.heroBanner, "hero hero--primary")}>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main className={clsx(styles.main)}>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {segundacoluna.map((props, idx) => (
                <SegundaColuna key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>
    </Layout>
  );
}

export default Gepdai;
