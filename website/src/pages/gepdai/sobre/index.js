import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.sobre.module.css";
import stylesSass from "./styles.novo.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";


const menu = [
    {
        projeto: "gepdai - labri-unesp",
        link1: "/gepdai",
        link2: "/gepdai/sobre",
        link3: "/gepdai/equipe",
        link4: "/gepdai",
        link5: "/gepdai"
    } 
]

const intro = [
    {
        projeto: "gepdai - labri-unesp",
        imgIntro: "/img/gepdai/capa-2.svg"
    }
]

const conhecer = [
  {
    projeto: "gepdai - labri-unesp",
    imgSobre: "/img/gepdai/sobre-pag1.svg",
    title: "Conheça o GEPDAI",
    text: (
      <>
      O Grupo de Estudo em Política e Direito Ambiental Internacional (GEPDAI) se dedica a analisar e compreender os temas da Política e do Direito Ambiental em uma perspectiva das Relações Internacionais. O GEPDAI promove reuniões quinzenais que trazem as discussões de maneira horizontal entre os membros e docentes participantes resultando em reflexões e produções a respeito da dicotomia Sociedade - Meio Ambiente, tema de alta relevância no contexto global atual.
      </>
    )
  }
]

const atividades = [
  {
    projeto: "gepdai - labri-unesp",
    title: "Atividades do grupo",
    text1: (
      <>
      <p>
      Em (ANO) foi realizado consectetur adipiscing elit. Nam maximus ultricies dignissim. Aliquam auctor pulvinar urna, luctus imperdiet ante volutpat vitae. Sed pulvinar sapien sapien, vel feugiat nibh mattis congue. Nunc vel enim suscipit eros finibus semper. Maecenas hendrerit est eu urna efficitur, id imperdiet nisl dignissim.
      </p>
      </>
    ),
    imgAtiv1: "/img/gepdai/sobre-pag2.svg",
    text2: (
      <>
      <p>
      Em (ANO) foi realizado consectetur adipiscing elit. Nam maximus ultricies dignissim. Aliquam auctor pulvinar urna, luctus imperdiet ante volutpat vitae. Sed pulvinar sapien sapien, vel feugiat nibh mattis congue. Nunc vel enim suscipit eros finibus semper. Maecenas hendrerit est eu urna efficitur, id imperdiet nisl dignissim.
      </p>
      </>
    ),
    imgAtiv2: "/img/gepdai/sobre-pag2.svg",
  }
]

const historico = [
  {
    projeto: "gepdai - labri-unesp",
    title: "História do Gepdai",
    imgHist: "/img/gepdai/sobre-pag1.svg",
    text: (
      <>
      O grupo surge em [ano de surgimento] com a proposta de [objetivo do grupo]. compreendendo que [contextualização que o grupo se insere] com isso se busca [aspirações]. Através de uma visão crítica, o grupo faz uso de aparato teórico de livros e textos de autores da área, como também a análise documental a cerca de casos pontuais.
      </>
    )
  }
]

function Intro({imgIntro, projeto}){
    return(
        <div className={clsx("hero hero--primary", styles.heroBanner)}>
            <div className={styles.LogoGepdai}>
            <img src={imgIntro} alt="Logo GEPDAI" />
        </div>
        </div>
    )
}

function Conhecer({imgSobre, title, text, sobre}){
  return(
    <div className={clsx(styles.containerS)}>
      <div className={clsx(stylesSass.row, "row")}>
        <div className="col col--12">
        <h2 className={clsx(styles.h2Gepdai)}>{title}</h2>
        </div>
        <div className="col col--4">
          <img src={imgSobre} alt="Ilustração do sobre o GEPDAI" className={clsx(styles.conhecerSobre)} />
        </div>
        <div className="col col--8">
          <p className={clsx(styles.pTextos)}>{text}</p>
        </div>
      </div>
    </div>
  )
}

function Atividades({imgAtiv1, imgAtiv2, title, text1, text2, ativ1, ativ2}){
  return(
    <div className={clsx(styles.containerS)}> 
      <div className={clsx(stylesSass.row, "row")}>
        <div className="col col--12">
          <h2 className={clsx(styles.h2Gepdai)}>{title}</h2>
        </div>
        <div className={clsx(styles.Atividades, "col col--6")}>
          <p className={clsx(styles.pTextos)}>{text1}</p>
        </div>
        <div className={clsx(styles.Atividades, "col col--6")}>
          <img src={imgAtiv1} alt="Ilustração da atividade" />
        </div>
        <div className={clsx(styles.Atividades, "col col--6")}>
          <img src={imgAtiv2} alt="Ilustração da atividade" />
        </div>
        <div className={clsx(styles.Atividades, "col col--6")}>
          <p className={clsx(styles.pTextos)}>{text2}</p>
        </div>
      </div>
    </div>
  )
}

function Historico({title, imgHist, hist, text}){
  return(
    <div className={clsx(styles.containerS)}>
      <div className={clsx(stylesSass.row, "row")}>
        <div className="col col--12">
          <h2 className={clsx(styles.h2Gepdai)}>{title}</h2>
        </div>
        <div className="col col--8">
          <p className={clsx(styles.pTextosH)}>{text}</p>
        </div>
        <div className="col col--4">
          <img src={imgHist} alt="Ilustração do histórico" className={clsx(styles.Hist)} />
        </div>
      </div>
    </div>
  )
}

function Menu({link1, link2, link3, link4, link5}){
    return(
        <div className={clsx(styles.Menu)}>
            <ul className={clsx(styles.ulLista)}>
              <li className={clsx(styles.liLista)}><a href={link1}>INÍCIO</a></li>
              <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
              <li className={clsx(styles.liLista)}><a href={link3}>EQUIPE</a></li>
              <li className={clsx(styles.liLista)}><a href={link4}>OBSERVATÓRIO</a></li>
              <li className={clsx(styles.liLista)}><a href={link5}>ARTIGOS</a></li>
            </ul>
        </div>
    )
}


function Sobre(){ 
  return(
    <Layout title="GEPDAI">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      
      <header className={clsx(styles.heroBanner, "hero hero--primary")}>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header> 
      <main className={clsx(styles.main)}>
      <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className={clsx(stylesSass.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className={clsx(stylesSass.row, "row")}>
              {conhecer.map((props, idx) => (
                <Conhecer key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className={clsx(stylesSass.row, "row")}>
              {atividades.map((props, idx) => (
                <Atividades key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className={clsx(stylesSass.row, "row")}>
              {historico.map((props, idx) => (
                <Historico key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  )
}


export default Sobre;