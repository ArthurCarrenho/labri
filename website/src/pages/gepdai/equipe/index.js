import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.equipe.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";
 
const menu = [
    {
        projeto: "gepdai - labri-unesp",
        link1: "/gepdai",
        link2: "/gepdai/sobre",
        link3: "/gepdai/equipe",
        link4: "/gepdai/observatorio",
        link5: "/gepdai"
    } 
]

const intro = [
    {
        projeto: "gepdai - labri-unesp",
        imgIntro: "/img/gepdai/capa-2.svg"
    }
]

const sobreequipe = [
    {
        projeto: "gepdai - labri-unesp",
        title: "Equipe GEPDAI",
        text: (
            <>
            A nossa Equipe conta com consectetur adipiscing elit. Nam maximus ultricies dignissim. Aliquam auctor pulvinar urna, luctus imperdiet ante volutpat vitae. Sed pulvinar sapien sapien, vel feugiat nibh mattis congue. Nunc vel enim suscipit eros finibus semper. Maecenas hendrerit est eu urna efficitur, id imperdiet nisl dignissim.  
            </>
        ),
        imgEquipe: "/img/gepdai/equipe2.png",
        title2: "Membros atuais"
    }
]

const membrosatuais = [
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon3.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon2.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon2.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon3.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon2.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon3.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do membro",
        imgFoto: "/img/gepdai/icon.png",
        linkRede: "/gepdai",
        imgLogo: "/img/social/linkedin.svg",
        linkRede2: "/gepdai",
        imgLogo2: "/img/social/facebook.svg"
    },
]

function MembrosAtuais({nome, imgFoto, foto, rede1, rede2, linkRede, linkRede2, imgLogo, imgLogo2}){
    return(
        <div className={clsx(styles.Info, "card col col--4")}>
          <div className="card-content">
            <div className="card__header">
              <div className={clsx(styles.Info, "text__center")}>
                <img src={imgFoto} alt="Foto do membro da equipe GEPDAI" />
              </div>
            </div>
            <div className="card__body">
              <h2 className={clsx(styles.h2Gepdai)}>{nome}</h2>
            </div>
            <div className={clsx(styles.Icons, "card__footer")}>
              <img src={imgLogo} alt="Foto do logo da Rede Social" href={linkRede} />
              <img src={imgLogo2} alt="Foto do logo da Rede Social" href={linkRede2} />
            </div>
          </div>
        </div>
    )
}

function SobreEquipe({title, text, imgEquipe, equipe, title2}){
    return(
        <div className={clsx(styles.containerS)}>
            <div className={clsx(styles.row, "row")}>
                <div className="col col--12">
                    <h2 className={clsx(styles.h2Gepdai)}>{title}</h2>
                </div>
                <div className="col col--6">
                    <p className={clsx(styles.pSobreEquipe)}>{text}</p>
                </div>
                <div className="col col--4">
                    <img src={imgEquipe} alt="Ilustração da Equipe GEPDAI" className={styles.SobreEquipe} />
                </div>
                <div className="col col--12">
                    <h2 classBame={clsx(styles.h2Gepdai)}>{title2}</h2>
                </div>
            </div>
        </div>
    )
}

function Menu({link1, link2, link3, link4, link5}){
    return(
        <div className={clsx(styles.Menu)}>
            <ul className={clsx(styles.ulLista)}>
              <li className={clsx(styles.liLista)}><a href={link1}>INÍCIO</a></li>
              <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
              <li className={clsx(styles.liLista)}><a href={link3}>EQUIPE</a></li>
              <li className={clsx(styles.liLista)}><a href={link4}>OBSERVATÓRIO</a></li>
              <li className={clsx(styles.liLista)}><a href={link5}>ARTIGOS</a></li>
            </ul>
        </div>)
}

function Intro({imgIntro, projeto}){
    return(
            <div className={clsx("hero hero--primary", styles.heroBanner)}>
                <div className={styles.LogoGepdai}>
                <img src={imgIntro} alt="Logo GEPDAI" />
            </div>
            </div>
    )
}

function Equipe(){
    return(
        <Layout title="Equipe GEPDAI">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            
          <header className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div>
              {intro.map((props, idx) => (
                <Intro key={idx} {...props} />
              ))}
            </div>
          </header>
          <main className={clsx(styles.main)}>
              <section className={styles.content}>
                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.row, "row")}>
                    {menu.map((props, idx) => (
                    <Menu key={idx} {...props} />
                    ))}
                    </div>
                </div>

                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.row, "row")}>
                    {sobreequipe.map((props, idx) => (
                    <SobreEquipe key={idx} {...props} />
                    ))}
                    </div>
                </div>

                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.row, "row")}>
                    {membrosatuais.map((props, idx) => (
                    <MembrosAtuais key={idx} {...props} />
                    ))}
                    </div>
                </div>

              </section>
          </main>
        </Layout>
    )
}

export default Equipe;