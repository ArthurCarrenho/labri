import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";



const intro = [
    {
        projeto: "lantri - labri-unesp",
        logo: "/img/lantri/logo-nome.svg",
        link: "/lantri",
        link1: "/lantri/noticias",
        link2: "/lantri/pesquisas",
        link3: "/lantri/publicacoes",
        link4: "https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ",
        link5: "/lantri/dados",
    }
]


const primeiracoluna = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Documentos de Trabalho LANTRI",
        texto1: (
            <>
                <center><b>Apresentação</b></center><br />
                O Laboratório de Novas Tecnologias de Pesquisa em Relações Internacionais (LANTRI) busca integrar a utilização sistemática de Novas Tecnologias de Informação e Comunicação (TICs) com as pesquisas acadêmicas em Relações Internacionais, privilegiando ferramentas de pesquisa Livres (Free Software) ou de Código Aberto (Open Source). Esta integração é realizada através do desenvolvimento de pesquisas que tem como foco central as Relações Internacionais do Brasil ou apresentem temáticas com impactos significativos sobre o Brasil. A partir dos aspectos citados acima, o Cadernos LANTRI tem como objetivo divulgar os resultados dos trabalhos realizados pelos integrantes e colaboradores do LANTRI. Deste modo, aqui será possível encontrar monografias, dissertações de mestrado, teses de doutorado, relatórios finais de pesquisas, artigos apresentados em congressos científicos, entre outros.
            </>
        ),
        texto2: (
            <>
                <b>Expedientes</b><br /><br />
                <b>Editor</b><br />
                Marcelo Passini Mariano (UNESP/CEDEC)<br /><br />
                <b>Assistência Editorial</b><br />
                Bárbara Carvalho Neves (UNESP)<br />
                Jaqueline Trevisan Pigatto (UNESP)<br />
                Rafael Augusto Ribeiro de Almeida (UNESP/CEDEC)<br /><br />
                <b>Periodicidade</b><br />
                Irregular <br /><br />
                <b>This work is licensed under a Creative Commons Attribution 4.0 International License.</b><br /><br />
                <a href="#"><b>Normas para publicação</b></a><br /><br />
                <a href="#"><b>Termos de Uso do Acervo do site do LANTRI (Clique aqui)</b></a><br /><br />
                <b>Página Oficial</b><br />
                www.labriunesp.org/lantri/documentos-lantri<br /><br />
                <b>Endereço</b><br />
                <i>Universidade Estadual Paulista</i><br />
                <i>"Julio de Mesquita Filho"</i><br />
                Campus Franca<br />
                Av. Eufrásia Monteiro Petráglia, 900<br />
                Jd. Dr. Antonio Petráglia -14409-160 - Franca, SP, Brasil.
            </>
        )
    }
]

const downloads = [
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_c275cc6b63da4fb0a8ca6517eb96ac01.pdf",
        nome: "Manual de boas práticas para vídeo-conferências e aulas online, 2020.",
        texto: (
            <>
                Tutorial Google Meet.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_975b82e0dc1a4820ad2c9b8ec3cf6d7b.pdf",
        nome: "Documentos LANTRI, N° 4, dezembro de 2019.",
        texto: (
            <>
                A Política Externa de Bolsonaro: uma amostra dos termos “Itamaraty” e “Globalismo” presentes nos discursos de Jair Bolsonaro e Ernesto Araújo, de janeiro a outubro, No Software “Antconc” - Julia de Souza Borba Gonçalves.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_c83833a1acc44ba08e7f7ee4fb9ce500.pdf",
        nome: "Documentos LANTRI, N° 3, junho de 2017.",
        texto: (
            <>
                Bitcoin e Blockchain: conceitos, termos técnicos, uso e lógica para as Relações Internacionais - Jaqueline Trevisan Pigatto e Norberto Filho.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_58085df0fac1400ea5b517476cf5d86b.pdf",
        nome: "Documentos LANTRI, N° 2, dezembro de 2016.",
        texto: (
            <>
               Programa de visualização gráfica de dados: Tableau Public - Gessica Carmo
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_ed868a7f6b514886b034e25584c98ec0.pdf",
        nome: "Documentos LANTRI, N° 1, dezembro de 2016.",
        texto: (
            <>
               Tutorial Antconc – software para a realização de análises qualitativas - Julia de Souza Borba Gonçalves.
            </>
        )
    },
]

function Downloads({ img, link, nome, texto }) {
    return (
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">
                <div className={clsx(styles.icone, "col col--3")}>
                    <a href={link} target="_blank"><img className={clsx(styles.icone)} src={img} alt="Download do Texto" /></a>
                </div>
                <div className="col col--9">
                    <a href={link} target="_blank"><h4 className={clsx(styles.h4Link)}>{nome}</h4></a>
                    <a href={link} target="_blank"><p className={clsx(styles.texto)}>{texto}</p></a>
                </div>
            </div>
        </div>
    )
}

function PrimeiraColuna({ titulo, texto1, texto2 }) {
    return (
        <div className={clsx(styles.colunaComFundo, "container")}>
            <div className="row">

                <div className="col col--6">
                    <h2 className={clsx(styles.h2Titulo)}>{titulo}</h2>
                    <p className={clsx(styles.paragrafo2)}>{texto1}</p>
                </div>

                <div className="col col--6">
                    <p className={clsx(styles.paragrafo)}>{texto2}</p>
                </div>

            </div>
        </div>
    )
}


function Intro({ logo, link, link1, link2, link3, link4, link5, rede1, rede2, rede3 }) {
    return (
        <div className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div className="row">
                <div className="col col--2">
                    <div className={clsx(styles.LogoLantri)}>
                        <a href={link}><img src={logo} alt="Logo do LANTRI-UNESP" /></a>
                    </div>
                </div>
                <div className={clsx(styles.Menu, "col col--10")}>
                    <ul className={clsx(styles.ulLista)}>
                        <li className={clsx(styles.liLista)}><a href={link1}>notícias e artigos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link2}>pesquisas</a></li>
                        <li className={clsx(styles.liLista)}><a href={link3}>publicações</a></li>
                        <li className={clsx(styles.liLista)}><a href={link4} target="_blank">vídeos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link5}>base de dado</a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.facebook.com/lantriunesp" target="_blank"><img src="/img/lantri/facebook-logo.svg" alt="logo do facebook" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ" target="_blank"><img src="/img/lantri/youtube-logo.svg" alt="logo do youtube" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://twitter.com/LANTRI_unesp" target="_blank"><img src="/img/lantri/twitter-logo.svg" alt="logo do twitter" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}


function Documentos({ }) {
    return (
        <Layout title="LANTRI-UNESP">
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main>
                <section className="content">
                    <div className="container">

                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>

                        <div className={clsx(styles.row, "row")}>
                            {downloads.map((props, idx) => (
                                <Downloads key={idx} {...props} />
                            ))}
                        </div>

                    </div>
                </section>
            </main>
        </Layout>
    )
}

export default Documentos;