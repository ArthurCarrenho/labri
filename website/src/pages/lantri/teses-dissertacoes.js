import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";



const intro = [
    {
        projeto: "lantri - labri-unesp",
        logo: "/img/lantri/logo-nome.svg",
        link: "/lantri",
        link1: "/lantri/noticias",
        link2: "/lantri/pesquisas",
        link3: "/lantri/publicacoes",
        link4: "https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ",
        link5: "/lantri/dados",
    }
]

const primeiracoluna = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Teses, Dissertações e TCCs",
        texto: "Aqui você encontrará teses e dissertações de membros vinculados ao LANTRI."
    }
]

const downloads = [
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://repositorio.ufu.br/handle/123456789/24789",
        texto: (
            <>
                O Brasil e o multilateralismo comercial: a política externa brasileira em relação à OMC entre 2003 e 2008. Ano de obtenção: 2019.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_1243923608434d37922e9972d0eee103.pdf",
        texto: (
            <>
                Política externa brasileira e a integração da infraestrutura na América do Sul: uma análise a partir dos mecanismos IIRSA/COSIPLAN. Ano de obtenção: 2019.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_8ae979348e86442b8870656ccd0fab37.pdf",
        texto: (
            <>
                Brasil e a Aliança do Pacífico: visões em disputa na integração regional? Ano de obtenção: 2019.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-https://www.teses.usp.br/teses/disponiveis/8/8131/tde-14032013-121719/pt-br.php" /* sem link no site do lantri */,
        texto: (
            <>
                O Brasil e as negociações no sistema GATT/OMC: uma análise da Rodada Uruguai e da Rodada Doha, Ano de obtenção: 2012.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://www.teses.usp.br/teses/disponiveis/8/8131/tde-14032013-121719/pt-br.php",
        texto: (
            <>
                Processo Decisório de Política Externa e Coalizões Internacionais: as posições do Brasil na OMC,Ano de Obtenção: 2010.
            </>
        )
    },
]

function Downloads({ img, link, nome, texto }) {
    return (
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">
                <div className={clsx(styles.icone, "col col--3")}>
                    <a href={link} target="_blank"><img className={clsx(styles.icone)} src={img} alt="Download do Texto" /></a>
                </div>
                <div className="col col--9">
                    <a href={link} target="_blank"><p className={clsx(styles.texto)}>{texto}</p></a>
                </div>
            </div>
        </div>
    )
}


function PrimeiraColuna({ imagem, titulo, texto }) {
    return (
        <div className={clsx(styles.colunaComFundo, "container")}>
            <div className="row">
                <div className="col col--12">
                    <h2 className={clsx(styles.h2Titulo)}>{titulo}</h2>
                    <p className={clsx(styles.paragrafo)}>{texto}</p>
                </div>
            </div>
        </div>
    )
}


function Intro({ logo, link, link1, link2, link3, link4, link5, rede1, rede2, rede3 }) {
    return (
        <div className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div className="row">
                <div className="col col--2">
                    <div className={clsx(styles.LogoLantri)}>
                        <a href={link}><img src={logo} alt="Logo do LANTRI-UNESP" /></a>
                    </div>
                </div>
                <div className={clsx(styles.Menu, "col col--10")}>
                    <ul className={clsx(styles.ulLista)}>
                        <li className={clsx(styles.liLista)}><a href={link1}>notícias e artigos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link2}>pesquisas</a></li>
                        <li className={clsx(styles.liLista)}><a href={link3}>publicações</a></li>
                        <li className={clsx(styles.liLista)}><a href={link4} target="_blank">vídeos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link5}>base de dado</a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.facebook.com/lantriunesp" target="_blank"><img src="/img/lantri/facebook-logo.svg" alt="logo do facebook" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ" target="_blank"><img src="/img/lantri/youtube-logo.svg" alt="logo do youtube" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://twitter.com/LANTRI_unesp" target="_blank"><img src="/img/lantri/twitter-logo.svg" alt="logo do twitter" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}


function TesesDissertacoes({ }) {
    return (
        <Layout title="LANTRI-UNESP">
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main>
                <section className="content">
                    <div className="container">

                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>

                        <div className={clsx(styles.row, "row")}>
                            {downloads.map((props, idx) => (
                                <Downloads key={idx} {...props} />
                            ))}
                        </div>

                    </div>
                </section>
            </main>
        </Layout>
    )
}

export default TesesDissertacoes;