import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";



const intro = [
    {
        projeto: "lantri - labri-unesp",
        logo: "/img/lantri/logo-nome.svg",
        link: "/lantri",
        link1: "/lantri/noticias",
        link2: "/lantri/pesquisas",
        link3: "/lantri/publicacoes",
        link4: "https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ",
        link5: "/lantri/dados",
    }
]

const primeiracoluna = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Livros e Artigos",
        texto: "Trabalhos publicados por pesquisadores do LANTRI"
    }
]

const downloads = [
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "http://ojs.ufgd.edu.br/index.php/moncoes/article/view/8723",
        texto: (
            <>
                MARIANO, M.; PIGATTO, J.; ALMEIDA, R. Atores internacionais e poder cibernético: o papel das transnacionais na era digital. Monções: Revista de Relações Internacionais da UFGD, Dourados, v.7. n.13, jan./jun. 2018.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://revistas.uniandes.edu.co/doi/full/10.7440/colombiaint92.2017.02",
        texto: (
            <>
                FUCCILLE, Alexandre; MARIANO Marcelo Passini; RAMANZINI JÚNIOR, Haroldo; ALMEIDA, Rafael Augusto Ribeiro de . The government of Dilma Rousseff and South America. Brazil’s actions in the UNASUR (2011-2014). Colombia Internacional, [s. l.], n. 92, p. 43–72, 2017
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_e9106f87501d42d1a75dc36444f353b2.pdf",
        texto: (
            <>
                MARIANO, M. P.; RAMANZINI JR, Haroldo. ; ALMEIDA, Rafael A. R. . O Brasil entre os Contenciosos e Coalizões Agrícolas no Sistema GATT/OMC. Monções: Revista de Relações Internacionais da UFGD, v. 4, p. 27, 2015.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_f9e03f2080104cd3a41b3adf97909b7d.pdf",
        texto: (
            <>
                DESIDERA NETO, W. A. ; MARIANO, M. P. ; PADULA, R. ; HALLACK, M. C. M. ; BARROS, P. S. . Relações do Brasil com a América do Sul após a Guerra Fria: política externa, integração, segurança e energia. Texto para Discussão (IPEA. Brasília), v. 2023, p. 7-85, 2015
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://www.teses.usp.br/teses/disponiveis/8/8131/tde-02032010-174055/publico/HAROLDO_RAMANZINI_JUNIOR.pdf",
        texto: (
            <>
                MARIANO, M. P.; Ramanzini Jr., Haroldo ; ALMEIDA, Rafael A. R. . O Brasil e a integração na América do Sul: uma análise dos últimos dez anos (2003-2013). Relações Internacionais (Lisboa), v. 1, p. 123, 2014.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://www.academia.edu/27515879", /* sem link no site do lantri*/
        texto: (
            <>
                MARIANO, M. P.; RAMANZINI JR, Haroldo. ; ALMEIDA, Rafael A. R . O Brasil e a Integração Sul-Americana. Anuario de la Integración Regional de América Latina y el Gran Caribe, v. 10, p. 381-398, 2014.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://repositorio.unesp.br/handle/11449/194590", /* sem link no site do lantri*/
        texto: (
            <>
                Ramanzini Jr., Haroldo ; MARIANO, M. P. . Brazil Construction of the Negotiating Position in the Doha Round of the WTO and the G-20: Domestic Pressures and the. Journal of World Trade, v. 6, p. 1203-1224, 2013.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_131367dd60eb406088ac3f51856b715b.pdf",
        texto: (
            <>
                MARIANO, M. P.; Ramanzini Jr., Haroldo . Uma análise das limitações estruturais do Mercosul a partir das posições da Política Externa Brasileira. Revista de Sociologia e Política (UFPR. Impresso), v. 20, p. 23-41, 2012.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "#", /* sem link no site do lantri */
        texto: (
            <>
                MARIANO, M. P.; Ramanzini Jr., Haroldo . Structural Limitations of the Mercosur: an analysis based on the brazilian foreign policy positions. The Latin Americanist (Orlando, Fla.), v. 56, p. 161180-180, 2012.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://periodicos.fclar.unesp.br/perspectivas/article/view/1452/1145", /* sem link no site do lantri */
        texto: (
            <>
                MARIANO, M. P.; MARIANO, Karina Lilia Pasquariello . A formulação da política externa brasileira e as novas lideranças políticas regionais. Perspectivas : Revista de Ciências Sociais (UNESP. Araraquara. Impresso), v. 33, p. 1-30, 2008.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://05a76dca-0fbf-49d8-bc92-402beca0f36d.filesusr.com/ugd/0dcb04_a2e5e5d1f264412787086ad8ca2e7a9d.pdf",
        texto: (
            <>
                MARIANO, M. P.; VIGEVANI, Tullo . A Alca e a Política Externa Brasileira. Caderno CEDEC (São Paulo), São Paulo, v. 74, 2005.
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "https://static.scielo.org/scielobooks/2f3jk/pdf/mariano-9788568334638.pdf", /* sem link no site do lantri */
        texto: (
            <>
                MARIANO, M. P.. A política externa brasileira e a integração regional: uma análise a partir do Mercosul. 1. ed. São Paulo: Editora Unesp, 2015. 268p .
            </>
        )
    },
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/download-button.png",
        link: "http://brasilnomundo.org.br/wp-content/uploads/2015/08/057_Livro.Politica.Externa.Brasileira.Cooperacao.Sul-Sul.e.Negociacoes.Internacionais_Ramanzini.Ayerbe.pdf", /* link quebrado no site do lantri */
        texto: (
            <>
                RAMANZINI JR, Haroldo. ; MARIANO, M. P. ; ALMEIDA, Rafael A. R. . As diferentes dimensões da cooperação Sul-Sul na política externa brasileira. In: Haroldo Ramanzini Júnior; Luis Fernando Ayerbe. (Org.). Política externa brasileira, cooperação sul-sul e negociações internacionais. 1ed.São Paulo: Cultura Acadêmica, 2015, v. , p. 13-49.
            </>
        )
    },
]

function Downloads({ img, link, nome, texto }) {
    return (
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">
                <div className={clsx(styles.icone, "col col--3")}>
                    <a href={link} target="_blank"><img className={clsx(styles.icone)} src={img} alt="Download do Texto" /></a>
                </div>
                <div className="col col--9">
                    <a href={link} target="_blank"><p className={clsx(styles.texto)}>{texto}</p></a>
                </div>
            </div>
        </div>
    )
}


function PrimeiraColuna({ imagem, titulo, texto }) {
    return (
        <div className={clsx(styles.colunaComFundo, "container")}>
            <div className="row">
                <div className="col col--12">
                    <h2 className={clsx(styles.h2Titulo)}>{titulo}</h2>
                    <p className={clsx(styles.paragrafo)}>{texto}</p>
                </div>
            </div>
        </div>
    )
}


function Intro({ logo, link, link1, link2, link3, link4, link5, rede1, rede2, rede3 }) {
    return (
        <div className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div className="row">
                <div className="col col--2">
                    <div className={clsx(styles.LogoLantri)}>
                        <a href={link}><img src={logo} alt="Logo do LANTRI-UNESP" /></a>
                    </div>
                </div>
                <div className={clsx(styles.Menu, "col col--10")}>
                    <ul className={clsx(styles.ulLista)}>
                        <li className={clsx(styles.liLista)}><a href={link1}>notícias e artigos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link2}>pesquisas</a></li>
                        <li className={clsx(styles.liLista)}><a href={link3}>publicações</a></li>
                        <li className={clsx(styles.liLista)}><a href={link4} target="_blank">vídeos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link5}>base de dado</a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.facebook.com/lantriunesp" target="_blank"><img src="/img/lantri/facebook-logo.svg" alt="logo do facebook" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ" target="_blank"><img src="/img/lantri/youtube-logo.svg" alt="logo do youtube" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://twitter.com/LANTRI_unesp" target="_blank"><img src="/img/lantri/twitter-logo.svg" alt="logo do twitter" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}


function LivrosArtigos({ }) {
    return (
        <Layout title="LANTRI-UNESP">
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main>
                <section className="content">
                    <div className="container">

                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>

                        <div className={clsx(styles.row, "row")}>
                            {downloads.map((props, idx) => (
                                <Downloads key={idx} {...props} />
                            ))}
                        </div>

                    </div>
                </section>
            </main>
        </Layout>
    )
}

export default LivrosArtigos;