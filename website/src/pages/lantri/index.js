import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";

const intro = [
    {
        projeto: "lantri - labri-unesp",
        logo: "/img/lantri/logo-nome.svg",
        link: "/lantri",
        link1: "/lantri/noticias",
        link2: "/lantri/pesquisas",
        link3: "/lantri/publicacoes",
        link4: "https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ",
        link5: "/lantri/dados",
    }
]

const primeiracoluna = [
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/logo.svg",
        titulo: "Seja bem vindo!",
        texto: (
            <>
                O LANTRI busca integrar a utilização sistemática de Novas Tecnologias de Informação e Comunicação (TICs) com as pesquisas acadêmicas em Relações Internacionais, privilegiando ferramentas de pesquisa Livres (Free Software) ou de Código Aberto (Open Source). <br /><br />
                Esta integração é realizada através do desenvolvimento de pesquisas que tem como foco central as Relações Internacionais do Brasil ou apresentem temáticas com impactos significativos sobre o Brasil.
            </>
        ),
        link1: "#"
    }
]

const segundacoluna = [
    {
        projeto: "lantri - labri-unesp",
        img: "/img/lantri/contato.svg",
        link1: "#",
        link2: "#",
        link3: "#",
        titulo: "Assine a Newsletter do LANTRI"
    }
]

const ferramentaspesquisa = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Ferramentas de Pesquisa",
        texto: (
            <>
                Neste espaço, você encontrará as ferramentas de pesquisas utilizadas no LANTRI.
            </>
        ),
        link: "/lantri/ferramentas-pesquisa",
        img: "/img/lantri/pesquisar1.svg"
    }
]

const pesquisas = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Pesquisas",
        texto: (
            <>
                Neste espaço, você encontrará as linhas de pesquisas desenvolvidas atualmente e as já concluidas pelo LANTRI.
            </>
        ),
        link: "/lantri/pesquisas",
        img: "/img/lantri/pesquisar2.svg"
    }
]

const publicacoes = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Publicações",
        texto: (
            <>
                Neste espaço, você encontrará publicações, teses e dissertações, além de livros e artigos escritos pelos pesquisadores do LANTRI
            </>
        ),
        link: "/lantri/publicacoes",
        img: "/img/lantri/publicacoes.svg"
    }
]

function Publicacoes({ img, titulo, texto, link }) {
    return (
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">

                <div className={clsx("col col--6")}>
                    <img className={clsx(styles.logoHome)} src={img} alt="ilustração" />
                </div>

                <div className="col col--6">
                    <h2 className={clsx(styles.h2Home)}>{titulo}</h2>
                    <p className={clsx(styles.texto)}>{texto}</p>
                    <a className={clsx(styles.botaoInfo, "button button--secondary")} href={link} target="_blank">Acesse Aqui</a>
                </div>

            </div>
        </div>
    )
}

function Pesquisas({ img, titulo, texto, link }) {
    return (
        <div className={clsx(styles.colunaComFundo, "container")}>
            <div className="row">

                <div className="col col--6">
                    <h2 className={clsx(styles.h2Home2)}>{titulo}</h2>
                    <p className={clsx(styles.texto2, styles.alinhar)}>{texto}</p>
                    <a className={clsx(styles.botaoSegundaColuna, "button button--secondary")} href={link} target="_blank">Acesse Aqui</a>
                </div>

                <div className={clsx(styles.logoHome, "col col--6")}>
                    <img src={img} alt="ilustração" />
                </div>

            </div>
        </div>
    )
}

function FerramentasPesquisa({ img, titulo, texto, link }) {
    return (
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">

                <div className={clsx(styles.logoHome, "col col--6")}>
                    <img src={img} alt="ilustração" />
                </div>

                <div className="col col--6">
                    <h2 className={clsx(styles.h2Home)}>{titulo}</h2>
                    <p className={clsx(styles.texto)}>{texto}</p>
                    <a className={clsx(styles.botaoInfo, "button button--secondary")} href={link} target="_blank">Acesse Aqui</a>
                </div>

            </div>
        </div>
    )
}

function SegundaColuna({ img, link1, link2, link3, link4, titulo }) {
    return (
        <div className={clsx(styles.colunaComFundo, "container")}>
            <div className="row">
                <div className={clsx(styles.botoes, "col col--4")}>
                    <p className={clsx(styles.p)}><a className={clsx(styles.botaoSegundaColuna, "button button--secondary")} href={link1}>Contato</a></p>
                    <p className={clsx(styles.p)}><a className={clsx(styles.botaoSegundaColuna, "button button--secondary")} href={link2}>Equipe</a></p>
                    <p className={clsx(styles.p)}><a className={clsx(styles.botaoSegundaColuna, "button button--secondary")} href={link3}>Parceiros</a></p>
                </div>

                <div className="col col--4">
                    <img src={img} alt="Ilustração de Contato" />
                </div>

                <div className="col col--4">
                    <h2 className={clsx(styles.h2Azul)}>{titulo}</h2>
                </div>
            </div>
        </div>
    )
}

function PrimeiraColuna({ img, texto, titulo, link1 }) {
    return (
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">
                <div className="col col--6">
                    <h2 className={clsx(styles.h2Lantri)}>{titulo}</h2>
                    <p className={clsx(styles.texto)}>{texto}</p>
                    <a className={clsx(styles.botaoInfo, "button button--secondary")} href={link1} target="_blank">Saiba Mais</a>
                </div>
                <div className={clsx(styles.logo, "col col--6")}>
                    <img src={img} alt="ilustração" />
                </div>
            </div>
        </div>
    )
}

function Intro({ logo, link, link1, link2, link3, link4, link5, rede1, rede2, rede3 }) {
    return (
        <div className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div className="row">
                <div className="col col--2">
                    <div className={clsx(styles.LogoLantri)}>
                        <a href={link}><img src={logo} alt="Logo do LANTRI-UNESP" /></a>
                    </div>
                </div>
                <div className={clsx(styles.Menu, "col col--10")}>
                    <ul className={clsx(styles.ulLista)}>
                        <li className={clsx(styles.liLista)}><a href={link1}>notícias e artigos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link2}>pesquisas</a></li>
                        <li className={clsx(styles.liLista)}><a href={link3}>publicações</a></li>
                        <li className={clsx(styles.liLista)}><a href={link4} target="_blank">vídeos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link5}>base de dado</a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.facebook.com/lantriunesp" target="_blank"><img src="/img/lantri/facebook-logo.svg" alt="logo do facebook" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ" target="_blank"><img src="/img/lantri/youtube-logo.svg" alt="logo do youtube" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://twitter.com/LANTRI_unesp" target="_blank"><img src="/img/lantri/twitter-logo.svg" alt="logo do twitter" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

function Home() {
    return (
        <Layout title="LANTRI-UNESP">
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main>
                <section className="content">
                    <div className="container">
                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className="container">
                        <div className={clsx(styles.row, "row")}>
                            {segundacoluna.map((props, idx) => (
                                <SegundaColuna key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className="container">
                        <div className={clsx(styles.row, "row")}>
                            {ferramentaspesquisa.map((props, idx) => (
                                <FerramentasPesquisa key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className="container">
                        <div className={clsx(styles.row, "row")}>
                            {pesquisas.map((props, idx) => (
                                <Pesquisas key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className="container">
                        <div className={clsx(styles.row, "row")}>
                            {publicacoes.map((props, idx) => (
                                <Publicacoes key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                </section>
            </main>

        </Layout>
    )
}

export default Home;