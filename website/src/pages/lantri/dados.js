import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";

const intro = [
    {
        projeto: "lantri - labri-unesp",
        logo: "/img/lantri/logo-nome.svg",
        link: "/lantri",
        link1: "/lantri/noticias",
        link2: "/lantri/pesquisas",
        link3: "/lantri/publicacoes",
        link4: "https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ",
        link5: "/lantri/dados",
    }
]

const primeiracoluna = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Bases de Dados",
    }
]


const segundacoluna = [
    {
        projeto: "lantri - labri-unesp",
        logo: "/img/hemeroteca-peb/hemeroteca-logo.svg",
        titulo: "Hemeroteca de Política Externa Brasileira",
        texto: (
            <>
                A Hemeroteca Política Externa Brasileira é um banco de dados que contém uma seleção de matérias publicadas por dois jornais brasileiros - O Estado de S. Paulo e Gazeta Mercantil - entre 1973 e 2011. <br />
                Com aproximadamente 30 mil notícias que tratam de diversos temas da política exterior do Brasil, essa Hemeroteca esta dividida em 13 temáticas...
            </>
        ),
        link: "/docs/projetos/dados/hemeroteca-peb/intro"
    }
]

function SegundaColuna({logo, titulo, texto, link}) {
    return(
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">
                <div className="col col--6">
                    <img src={logo} alt="Logo da Hemeroteca de Política Externa Brasileira" />
                </div>
                <div className="col col--6">
                    <h2 className={clsx(styles.h2Titulo2)}>{titulo}</h2>
                    <p className={clsx(styles.texto)}>{texto}</p>
                    <a href={link} target="_blank" className={clsx(styles.botaoInfo, "button button--secondary")}>Acesse aqui</a>
                </div>
            </div>
        </div>
    )
}


function PrimeiraColuna({ imagem, titulo, texto }) {
    return (
        <div className={clsx(styles.colunaComFundo, "container")}>
            <div className="row">
                <div className="col col--12">
                    <h2 className={clsx(styles.h2Titulo)}>{titulo}</h2>
                </div>
            </div>
        </div>
    )
}

function Intro({ logo, link, link1, link2, link3, link4, link5, rede1, rede2, rede3 }) {
    return (
        <div className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div className="row">
                <div className="col col--2">
                    <div className={clsx(styles.LogoLantri)}>
                        <a href={link}><img src={logo} alt="Logo do LANTRI-UNESP" /></a>
                    </div>
                </div>
                <div className={clsx(styles.Menu, "col col--10")}>
                    <ul className={clsx(styles.ulLista)}>
                        <li className={clsx(styles.liLista)}><a href={link1}>notícias e artigos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link2}>pesquisas</a></li>
                        <li className={clsx(styles.liLista)}><a href={link3}>publicações</a></li>
                        <li className={clsx(styles.liLista)}><a href={link4} target="_blank">vídeos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link5}>base de dado</a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.facebook.com/lantriunesp" target="_blank"><img src="/img/lantri/facebook-logo.svg" alt="logo do facebook" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ" target="_blank"><img src="/img/lantri/youtube-logo.svg" alt="logo do youtube" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://twitter.com/LANTRI_unesp" target="_blank"><img src="/img/lantri/twitter-logo.svg" alt="logo do twitter" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

function Dados() {
    return (
        <Layout title="LANTRI-UNESP">
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main>
                <section className="content">
                    <div className="container">

                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>

                        <div className={clsx(styles.row, "row")}>
                            {segundacoluna.map((props, idx) => (
                                <SegundaColuna key={idx} {...props} />
                            ))}
                        </div>

                    </div>

                </section>
            </main>
        </Layout>
    )
}


export default Dados;