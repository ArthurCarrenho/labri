import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";

const intro = [
    {
        projeto: "lantri - labri-unesp",
        logo: "/img/lantri/logo-nome.svg",
        link: "/lantri",
        link1: "/lantri/noticias",
        link2: "/lantri/pesquisas",
        link3: "/lantri/publicacoes",
        link4: "https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ",
        link5: "/lantri/dados",
    }
]

const primeiracoluna = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Pesquisas",
        texto: "Nesta página você encontrará as pesquisas desenvolvidas pelo LANTRI."
    }
]

const segundacoluna = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Linhas de Pesquisa",
        texto: (
            <>
                Neste espaço esta concentrado as linhas de pesquisas desenvolvidas pelo  LANTRI atualmente, e concluidas.<br />
                <p><li>Novos Métodos e Técnicas de Pesquisa em Ciências Humanas e Sociais.</li></p>

                <p><li>Processos de Integração Regional</li></p>

                <p><li>Política Externa Brasileira</li></p>

                <p><li>Negociações Comerciais Internacionais</li></p>

                <p><li>Novos Atores</li></p>

                <p><li>Novos Temas nas Relações Internacionais</li></p>

            </>
        ),
        imagem: "/img/lantri/ideias.png"
    }
]

const pesquisasandamento = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Pesquisas em Andamento",
        subtitulo1: (
            <>
                O Brasil e a Integração Sul-americana: a Unasul e as novas dimensões da integração (2013 - Atual)
            </>
        ),
        texto1: (
            <>
                <li>
                    A presente pesquisa, na perspectiva da política externa brasileira, busca contribuir para o entendimento da atual dinâmica da integração regional da América do Sul, representada pela formação da UNASUL, a partir da análise dos acordos e objetivos relacionados a três temas específicos e inter - relacionados: segurança, integração física e financiamento da integração. Esses três temas envolvem dilemas importantes de cooperação, que buscaremos identificar, ao mesmo tempo, lidar com essas questões torna-se fundamental, pois, são conseqüências do objetivo de promover maior cooperação e interconexão entre os países; além de serem temas próprios das novas dimensões que a integração assume nos anos 2000. Pretendemos averiguar se o sentido da cooperação na América do Sul mudou com a formação da UNASUL, e, em que medida é possível a viabilização de um processo de integração regional que tem como objetivo o fortalecimento da autonomia nacional.
                </li>

                <li>
                    Instituições Envolvidas:
                    <li>Universidade Federal de Uberlândia (UFU) - Institução Principal</li>
                    <li>Universidade Estadual Paulista (UNESP) via Laboratório de Novas Tecnologias de Pesquisa em Relações Interncionais (LANTRI).</li>
                </li>

                <li>
                    Financiador:
                    <li>Conselho Nacional de Desenvolvimento Científico e Tecnológico (CNPq).</li>
                </li>
            </>
        ),
        subtitulo2: (
            <>
                Inserção Regional e suas consequencias para a política externa: uma análise do Brasil e da Índia (2015 - Atual)
            </>
        ),
        texto2: (
            <>
                <li>
                    Procuraremos entender se o ambiente regional em que um Estado está localizado molda a sua visão de mundo. Se sim, como esse processo ocorre e quais as consequências para o conjunto da política externa? A partir da análise da atuação do Brasil e da Índia, na América do Sul e no Sul da Ásia, respectivamente, este projeto buscará construir uma teoria para responder à questão colocada.
                </li>

                <li>
                    Instituições Envolvidas:
                    <li>Universidade Federal de Uberlândia (UFU) - Institução Principal</li>
                    <li>Universidade Estadual Paulista (UNESP) via Laboratório de Novas Tecnologias de Pesquisa em Relações Interncionais (LANTRI).</li>
                </li>

                <li>
                    Financiador:
                    <li>Conselho Nacional de Desenvolvimento Científico e Tecnológico (CNPq).</li>
                </li>
            </>
        )
    }
]

const pesquisasconcluidas = [
    {
        projeto: "lantri - labri-unesp",
        titulo: "Pesquisas Concluídas",
        subtitulo1: (
            <>
            O Brasil e as Negociações Sobre Agricultura no Sistema GATT/OMC: a experiência brasileira em coalizões e contenciosos agrícolas 2012 - 2016
            </>
        ),
        texto1: (
            <>
            <b>Descrição</b>: Esta proposta de pesquisa busca compreender melhor como a participação do Brasil no sistema GATT/OMC influenciou a política externa brasileira. Para tanto, a análise está centrada na atuação brasileira nas coalizões agrícolas, a partir das experiências vividas no Grupo de Cairns (Rodada Uruguai) e na coalizão G-201 (Rodada Doha), assim como nos contenciosos agrícolas, com especial atenção ao caso do Algodão (Brasil x EUA) e ao caso do açúcar (Brasil x União Europeia). A participação do país nas coalizões agrícolas e nos contenciosos fornecem elementos empíricos importantes para compreender o comportamento brasileiro com relação às negociações multilaterais. O enfoque teórico utilizado procura explorar melhor o processo de aprendizagem oriundo dessas experiências, contemplando instrumentos analíticos advindos de abordagens racionalistas, como as institucionalistas e as intergovernamentalistas, com preocupações construtivistas a respeito do processo de internalização de normas e procedimentos originados a partir das interações que ocorrem nos espaços de cooperação internacional. Trata-se, portanto, de uma expansão significativa da abordagem teórico conceitual e do trabalho empírico que foi realizada na pesquisa ?A Posição Brasileira no G-20: Política Externa e Pressões Domésticas?2, realizada com o apoio do CNPq e finalizada no ano de 2010.. <br />
            <b>Financiador(es)</b>: Universidade Estadual Paulista Júlio de Mesquita Filho - Outra / CNPQ - Auxílio financeiro.
            </>
        ),
        subtitulo2: (
            <>
            Estrutura Socioeconômica e Políticas para a Integração da América do Sul - Integração da Infraestrutura sul-americana 2011 - 2013
            </>
        ),
        texto2: (
            <>
            <b>Financiador(es)</b>: Instituto de Pesquisa Econômica Aplicada - DF - Bolsa.
            </>
        ),
        subtitulo3: (
            <>
            Metodologia do Trabalho Científico em Ciências Humanas e Sociais e as Novas Tecnologias da Informação e Comunicação 2011 - 2012
            </>
        ),
        texto3: (
            <>
            <b>Descrição</b>: UNESP - Edital nº 005/2011-PROPE Em geral, a área das Ciências Sociais e Humanas no Brasil apresenta certa dificuldade para incorporar instrumentais provenientes das novas tecnologias de informação e comunicação no cotidiano das atividades de pesquisa. Na atualidade, existem soluções tecnológicas, de software, hardware e serviços, que são de baixo custo e demandam pouco tempo de treinamento por parte dos usuários, mas que não foram desenvolvidas especificamente para as finalidades de pesquisa científica. Desta forma, o objetivo geral desta proposta será analisar aspectos básicos da metodologia do trabalho científico em ciências humanas e sociais a fim de buscar novos instrumentais e procedimentos técnicos, proporcionando maior intensificação do uso das tecnologias de informação e comunicação no cotidiano dos pesquisadores e nos trabalhos realizados em grupos de pesquisa. Este projeto pretende, portanto, diminuir a distância entre as novas soluções tecnológicas e o dia-a-dia do trabalho científico, sem perder de vista as condições da produção da pesquisa e do conhecimento no Brasil e, simultaneamente, fortalecer essa linha de pesquisa na Unesp - Campus de Franca. <br />
            <b>Financiador(es)</b>: Universidade Estadual Paulista Júlio de Mesquita Filho - Auxílio financeiro.
            </>
        ),
        subtitulo4: (
            <>
            A Posição Brasileira no G-20: Política Externa e Pressões Domésticas 2008 - 2010 
            </>
        ),
        texto4: (
            <>
            <b>Financiador(es)</b>: Conselho Nacional de Desenvolvimento Científico e Tecnológico - Auxílio financeiro.
            </>
        )
    }
]

function PesquisasConcluidas({titulo, subtitulo1, subtitulo2, subtitulo3, subtitulo4, texto1, texto2, texto3, texto4}){
    return(
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">
                <div className="col col--12">
                    <h2 className={clsx(styles.h2Titulo2)}>{titulo}</h2>
                    <h4 className={clsx(styles.h4Texto2)}>{subtitulo1}</h4>
                    <p className={clsx(styles.texto)}>{texto1}</p>
                    <h4 className={clsx(styles.h4Texto2)}>{subtitulo2}</h4>
                    <p className={clsx(styles.texto)}>{texto2}</p>
                    <h4 className={clsx(styles.h4Texto2)}>{subtitulo3}</h4>
                    <p className={clsx(styles.texto)}>{texto3}</p>
                    <h4 className={clsx(styles.h4Texto2)}>{subtitulo4}</h4>
                    <p className={clsx(styles.texto)}>{texto4}</p>
                </div>
            </div>            
        </div>
    )
}

function PesquisasAndamento({ titulo, subtitulo1, subtitulo2, texto1, texto2 }) {
    return (
        <div className={clsx(styles.colunaComFundo, styles.containerAzul, "container")}>
            <div className="row">
                <div className="col col--12">
                    <h2 className={clsx(styles.h2Titulo)}>{titulo}</h2>
                    <h4 className={clsx(styles.h4Texto)}>{subtitulo1}</h4>
                    <p className={clsx(styles.texto2)}>{texto1}</p>
                    <h4 className={clsx(styles.h4Texto)}>{subtitulo2}</h4>
                    <p className={clsx(styles.texto2)}>{texto2}</p>
                </div>
            </div>
        </div>
    )
}

function SegundaColuna({ titulo, texto, imagem }) {
    return (
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">
                <div className="col col--8">
                    <h2 className={clsx(styles.h2Lantri)}>{titulo}</h2>
                    <p className={clsx(styles.texto)}>{texto}</p>
                </div>

                <div className={clsx(styles.ilustracao, "col col--4")}>
                    <img src={imagem} alt="Ilustração da página de pesquisas" />
                </div>
            </div>
        </div>
    )
}

function PrimeiraColuna({ imagem, titulo, texto }) {
    return (
        <div className={clsx(styles.colunaComFundo, "container")}>
            <div className="row">
                <div className="col col--12">
                    <h2 className={clsx(styles.h2Titulo)}>{titulo}</h2>
                    <p className={clsx(styles.paragrafo)}>{texto}</p>
                </div>
            </div>
        </div>
    )
}

function Intro({ logo, link, link1, link2, link3, link4, link5, rede1, rede2, rede3 }) {
    return (
        <div className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div className="row">
                <div className="col col--2">
                    <div className={clsx(styles.LogoLantri)}>
                        <a href={link}><img src={logo} alt="Logo do LANTRI-UNESP" /></a>
                    </div>
                </div>
                <div className={clsx(styles.Menu, "col col--10")}>
                    <ul className={clsx(styles.ulLista)}>
                        <li className={clsx(styles.liLista)}><a href={link1}>notícias e artigos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link2}>pesquisas</a></li>
                        <li className={clsx(styles.liLista)}><a href={link3}>publicações</a></li>
                        <li className={clsx(styles.liLista)}><a href={link4} target="_blank">vídeos</a></li>
                        <li className={clsx(styles.liLista)}><a href={link5}>base de dado</a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.facebook.com/lantriunesp" target="_blank"><img src="/img/lantri/facebook-logo.svg" alt="logo do facebook" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://www.youtube.com/channel/UCXvFZ4WwZ-_cX0JstexPwUQ" target="_blank"><img src="/img/lantri/youtube-logo.svg" alt="logo do youtube" /></a></li>
                        <li className={clsx(styles.liLogos)}><a href="https://twitter.com/LANTRI_unesp" target="_blank"><img src="/img/lantri/twitter-logo.svg" alt="logo do twitter" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

function Pesquisas() {
    return (
        <Layout title="LANTRI-UNESP">
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main>
                <section className="content">
                    <div className="container">

                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>

                        <div className={clsx(styles.row, "row")}>
                            {segundacoluna.map((props, idx) => (
                                <SegundaColuna key={idx} {...props} />
                            ))}
                        </div>

                        <div className={clsx(styles.row, "row")}>
                            {pesquisasandamento.map((props, idx) => (
                                <PesquisasAndamento key={idx} {...props} />
                            ))}
                        </div>

                        <div className={clsx(styles.row, "row")}>
                            {pesquisasconcluidas.map((props, idx) => (
                                <PesquisasConcluidas key={idx} {...props} />
                            ))}
                        </div>

                    </div>

                </section>
            </main>
        </Layout>
    )
}


export default Pesquisas;