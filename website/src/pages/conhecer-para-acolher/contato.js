import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
    {
        projeto: "cpa - labri-unesp",
        imgIntro: "/img/conhecer-para-acolher/header-cpa.png"
    }
]

const menu = [
    {
        projeto: "cpa - labri-unesp",
        link1: "/conhecer-para-acolher",
        link2: "/conhecer-para-acolher/sobre",
        link3: "/conhecer-para-acolher/objetivos",
        link4: "/conhecer-para-acolher/equipe",
        link5: "/conhecer-para-acolher/contato",
    }
]

const primeiracoluna = [
    {
        projeto: "cpa - labri-unesp",
        titulo: "CONTATO",
        subtitulo: (
            <>
            Como entrar em contato com a nossa equipe
            </>
        ),
        imagem: "/img/conhecer-para-acolher/contato.svg"
    }
]

function PrimeiraColuna({titulo, subtitulo, imagem}){
    return(
        <div className={clsx(styles.containerMain, "container")}>
            <div className={clsx(styles.row, "row")}>
                <div className="col col--8">
                    <h2 className={clsx(styles.h2CPA)}>
                        {titulo}
                    </h2>
                    <p className={clsx(styles.descricao)}>
                        {subtitulo}
                    </p>
                </div>

                <div className={clsx(styles.logoCPA, "col col--4")}>
                    <img src={imagem} alt="Ilustração" />
                </div>
            </div>
        </div>
    )
}


function Menu({ link1, link2, link3, link4, link5 }) {
    return (
        <div className={clsx(styles.Menu)}>
            <ul className={clsx(styles.ulLista)}>
                <li className={clsx(styles.liLista)}><a href={link1}>INICIO</a></li>
                <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
                <li className={clsx(styles.liLista)}><a href={link3}>OBJETIVOS</a></li>
                <li className={clsx(styles.liLista)}><a href={link4}>EQUIPE</a></li>
                <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
            </ul>
        </div>
    )
}

function Intro({ projeto, imgIntro }) {
    return (
        <div className={clsx(styles.heroBanner)}>
            <div className={styles.HeaderCPA}>
                <img src={imgIntro} alt="Header e Logo do Projeto Conhecer Para Acolher" />
            </div>
        </div>
    );
}


function Contato(){
    return (
        <Layout title="Contato | Conhecer Para Acolher">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header className={clsx(styles.heroBanner)}>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main className={clsx(styles.main)}>
                <section className={styles.content}>
                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {menu.map((props, idx) => (
                                <Menu key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>
                    </div>
                </section>
            </main>
        </Layout>
        )
}


export default Contato;