import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
    {
        projeto: "cpa - labri-unesp",
        imgIntro: "/img/conhecer-para-acolher/header-cpa.png"
    }
]

const menu = [
    {
        projeto: "cpa - labri-unesp",
        link1: "/conhecer-para-acolher",
        link2: "/conhecer-para-acolher/sobre",
        link3: "/conhecer-para-acolher/objetivos",
        link4: "/conhecer-para-acolher/equipe",
        link5: "/conhecer-para-acolher/contato",
    }
]

const primeiracoluna = [
    {
        projeto: "cpa - labri-unesp",
        imagem: "/img/conhecer-para-acolher/equipe.svg",
        titulo: "EQUIPE",
        subtitulo: (
            <>
            Conheça as pessoas por trás do projeto
            </>
        )
    }
]

const membros = [
    {
        projeto: "cpa - labri-unesp",
        icon: "/img/conhecer-para-acolher/perfil.svg",
        nome: (
            <>
            Profa. dra. elizabete sanches rocha
            </>
        ),
        descricao: (
            <>
            Coordernadora
            </>
        ),
        icon2: "/img/conhecer-para-acolher/perfil.svg",
        nome2:(
            <>
            profa. dra. fernanda mello sant’anna
            </>
        ),
        descricao2: (
            <>
            Coordernadora
            </>
        )
    },
    {
        projeto: "cpa - labri-unesp",
        icon: "/img/conhecer-para-acolher/perfil.svg",
        nome: (
            <>
            nome do(a) aluno(a)
            </>
        ),
        descricao: (
            <>
            Membro / Graduando(a) de NOME DO CURSO 
            </>
        ),
        icon2: "/img/conhecer-para-acolher/perfil.svg",
        nome2: (
            <>
            nome do(a) aluno(a)
            </>
        ),
        descricao2: (
            <>
            Membro / Graduando(a) de NOME DO CURSO 
            </>
        ),
    },
    {
        projeto: "cpa - labri-unesp",
        icon: "/img/conhecer-para-acolher/perfil.svg",
        nome: (
            <>
            nome do(a) aluno(a)
            </>
        ),
        descricao: (
            <>
            Membro / Graduando(a) de NOME DO CURSO 
            </>
        ),
        icon2: "/img/conhecer-para-acolher/perfil.svg",
        nome2: (
            <>
            nome do(a) aluno(a)
            </>
        ),
        descricao2: (
            <>
            Membro / Graduando(a) de NOME DO CURSO 
            </>
        ),
    },
]

const apoio = [
    {
        projeto: "cpa - labri-unesp",
        titulo: "APOIO",
        subtitulo: (
            <>
            Departamento de Relações Internacionais e Centro Jurídico Social da FCHS, da UNESP de Franca
            </>
        ),
        imagem: "/img/conhecer-para-acolher/imagem-logo.svg"
    }
]

function Apoio({titulo, subtitulo, imagem}){
    return(
        <div className={clsx(styles.containerMain, "container")}>
            <div className={clsx(styles.row, "row")}>
                <div className="col col--8">
                    <h2 className={clsx(styles.h2CPA)}>
                        {titulo}
                    </h2>
                    <p className={clsx(styles.descricao)}>
                        {subtitulo}
                    </p>
                </div>

                <div className={clsx(styles.logoCPA, "col col--4")}>
                    <img src={imagem} alt="Ilustração" />
                </div>
            </div>
        </div>
    )
}

function Membros({icon, nome, descricao, icon2, nome2, descricao2}){
    return(
        <div className="container">
            <div className={clsx(styles.row, "row")}>
                <div className={clsx(styles.icone, "col col--2")}>
                    <img src={icon} alt="Icone do Membro da equipe" />
                </div>

                <div className="col col--4">
                    <h4 className={clsx(styles.h4Equipe)}>
                        {nome}
                    </h4>
                    <p className={clsx(styles.descricao)}>
                        {descricao}
                    </p>
                </div>

                <div className={clsx(styles.icone, "col col--2")}>
                    <img src={icon2} alt="Icone do Membro da equipe" />
                </div>

                <div className="col col--4">
                    <h4 className={clsx(styles.h4Equipe)}>
                        {nome2}
                    </h4>
                    <p className={clsx(styles.descricao)}>
                        {descricao2}
                    </p>
                </div>
            </div>
        </div>
    )
}

function PrimeiraColuna({imagem, titulo, subtitulo}){
    return(
        <div className="container">
            <div className={clsx(styles.row, "row")}>
                <div className={clsx(styles.logoCPA, "col col--4")}>
                    <img src={imagem} alt="Ilustração da Equipe do Projeto" />
                </div>

                <div className="col col--8">
                    <h2 className={clsx(styles.h2CPA)}>
                        {titulo}
                    </h2>

                    <p className={clsx(styles.descricao)}>
                        {subtitulo}
                    </p>
                </div>
            </div>

        </div>
    )
}


function Menu({ link1, link2, link3, link4, link5 }) {
    return (
        <div className={clsx(styles.Menu)}>
            <ul className={clsx(styles.ulLista)}>
                <li className={clsx(styles.liLista)}><a href={link1}>INICIO</a></li>
                <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
                <li className={clsx(styles.liLista)}><a href={link3}>OBJETIVOS</a></li>
                <li className={clsx(styles.liLista)}><a href={link4}>EQUIPE</a></li>
                <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
            </ul>
        </div>
    )
}

function Intro({ projeto, imgIntro }) {
    return (
        <div className={clsx(styles.heroBanner)}>
            <div className={styles.HeaderCPA}>
                <img src={imgIntro} alt="Header e Logo do Projeto Conhecer Para Acolher" />
            </div>
        </div>
    );
}


function Equipe(){
    return (
        <Layout title="Equipe | Conhecer Para Acolher">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header className={clsx(styles.heroBanner)}>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main className={clsx(styles.main)}>
                <section className={styles.content}>
                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {menu.map((props, idx) => (
                                <Menu key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {membros.map((props, idx) => (
                                <Membros key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {apoio.map((props, idx) => (
                                <Apoio key={idx} {...props} />
                            ))}
                        </div>
                    </div>
                </section>
            </main>
        </Layout>
        )
}


export default Equipe;