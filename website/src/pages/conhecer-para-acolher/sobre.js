import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";



const intro = [
    {
        projeto: "cpa - labri-unesp",
        imgIntro: "/img/conhecer-para-acolher/header-cpa.png"
    }
]

const menu = [
    {
        projeto: "cpa - labri-unesp",
        link1: "/conhecer-para-acolher",
        link2: "/conhecer-para-acolher/sobre",
        link3: "/conhecer-para-acolher/objetivos",
        link4: "/conhecer-para-acolher/equipe",
        link5: "/conhecer-para-acolher/contato",
    }
]

const primeiracoluna = [
    {
        projeto: "cpa - labri-unesp",
        imagem: "/img/conhecer-para-acolher/pagina-sobre-1.svg",
        titulo: "CONHECER PARA ACOLHER",
        subtitulo: "Mapeamento de migrantes e refugiados na cidade de Franca",
        texto: (
            <>
                Projeto vinculado à REDE TEMÁTICA DE EXTENSÃO: Rede de Atenção ao Migrante Internacional (RAMIN/UNESP). <br />
                Programa Institucional da Pró-Reitoria de Extensão Universitária e Cultura (PROEC).
            </>
        )
    }
]

const ods = [
    {
        projeto: "cpa - labri-unesp",
        titulo: "INDICAÇÃO DOS OBJETIVOS DE DESENVOLVIMENTO SUSTENTÁVEL (ODS) DA AGENDA 2030 - ONU",
        img1: "/img/conhecer-para-acolher/ods-4.png",
        descricao1: (
            <>
                Garantir o acesso à educação inclusiva, de qualidade e equitativa, e promover oportunidades de aprendizagem ao longo da vida para todos.
            </>
        ),
        img2: "/img/conhecer-para-acolher/ods-10.png",
        descricao2: (
            <>
                Reduzir as desigualdades no interior dos países e entre países.
            </>
        ),
        img3: "/img/conhecer-para-acolher/ods-16.png",
        descricao3: (
            <>
                Promover sociedades pacíficas e inclusivas para o desenvolvimento sustentável, proporcionar o acesso à justiça para todos e construir instituições eficazes, responsáveis e inclusivas a todos os níveis.
            </>
        )
    }
]

const justificativa = [
    {
        projeto: "cpa - labri-unesp",
        imagem: "/img/conhecer-para-acolher/pagina-sobre-2.svg",
        titulo: "POR QUE O PROJETO EXISTE",
        subtitulo: "Justificativa para sua criação",
        texto: (
            <>
                A região do município de Franca-SP não possui dados oficiais sobre a população de migrantes internacionais e refugiados e nem políticas de atenção e acolhimento a este público. Considerando o processo de interiorização das/os refugiadas/os e a demanda crescente em torno dessa realidade desafiadora para os municípios, a universidade pode contribuir para realizar esta pesquisa e pensar em práticas e abordagens acolhedoras para estas pessoas, com  atenção para o acesso aos direitos humanos inalienáveis e ao processo de adaptação em um diferente ambiente cultural e social. Ademais, é uma oportunidade sem precedentes de pensarmos coletivamente, universidade e setores públicos da cidade, em políticas públicas dirigidas para essa população em situação de vulnerabilidade.
            </>
        )
    }
]

const publicoalvo = [
    {
        projeto: "cpa - labri-unesp",
        titulo: "PARA QUEM É O GRUPO",
        subtitulo: "Público-alvo",
        texto: (
            <>
            Migrantes internacionais e refugiados.
            </>
        )
    }
]

function PublicoAlvo({ titulo, subtitulo, texto }) {
    return (
        <div className="container">
            <div className={clsx(styles.row, "row")}>
                <div className="col col--12">
                    <h2 className={clsx(styles.h2)}>
                        {titulo}
                    </h2>
                    <h4 className={clsx(styles.h4CPA)}>
                        {subtitulo}
                    </h4>
                </div>

                <div className="col col--12">
                    <p className={clsx(styles.justificativa)}>
                        {texto}
                    </p>
                </div>
            </div>
        </div>
    )
}

function Justificativa({ imagem, titulo, subtitulo, texto }) {
    return (
        <div className="container">
            <div className={clsx(styles.row, "row")}>

                <div className="col col--8">
                    <h2 className={clsx(styles.h2CPA)}>{titulo}</h2>
                    <h4 className={clsx(styles.h4CPA)}>{subtitulo}</h4>
                </div>

                <div className={clsx(styles.logoCPA, "col col--4")}>
                    <img src={imagem} alt="Ilustração" />
                </div>

                <div className="col col--12">
                    <p className={clsx(styles.justificativa)}>
                        {texto}
                    </p>
                </div>

            </div>
        </div>
    )
}

function ODS({ titulo, descricao1, descricao2, descricao3, img1, img2, img3 }) {
    return (
        <div className={clsx(styles.containerMain, "container")}>
            <div className={clsx(styles.row, "row")}>
                <div className="col col--12">
                    <h2 className={clsx(styles.h3)}>
                        {titulo}
                    </h2>
                </div>

                <div className={clsx(styles.logoODS, "col col--4")}>
                    <img src={img1} alt="Objetivo 4: Educação de Qualidade" />
                </div>
                <div className="col col--8">
                    <p className={clsx(styles.sobreObjetivos)}>
                        {descricao1}
                    </p>
                </div>

                <div className="col col--8">
                    <p className={clsx(styles.sobreObjetivos)}>{descricao2}</p>
                </div>
                <div className={clsx(styles.logoODS, "col col--4")}>
                    <img src={img2} alt="Objetivo 10: Redução das Desigualdades" />
                </div>

                <div className={clsx(styles.logoODS, "col col--4")}>
                    <img src={img3} alt="Objetivo 16: Paz, Justiça e Instituições Eficazes" />
                </div>
                <div className="col col--8">
                    <p className={clsx(styles.sobreObjetivos)}>
                        {descricao3}
                    </p>
                </div>
            </div>
        </div>
    )
}

function PrimeiraColuna({ imagem, titulo, subtitulo, texto }) {
    return (
        <div className="container">
            <div className={clsx(styles.row, "row")}>

                <div className={clsx(styles.logoCPA, "col col--4")}>
                    <img src={imagem} alt="Ilustração" />
                </div>

                <div className="col col--8">
                    <h2 className={clsx(styles.h2CPA)}>
                        {titulo}
                    </h2>
                    <h4 className={clsx(styles.h4CPA)}>
                        {subtitulo}
                    </h4>
                    <p className={clsx(styles.paragrafo)}>
                        {texto}
                    </p>
                </div>

            </div>
        </div>
    )
}

function Menu({ link1, link2, link3, link4, link5 }) {
    return (
        <div className={clsx(styles.Menu)}>
            <ul className={clsx(styles.ulLista)}>
                <li className={clsx(styles.liLista)}><a href={link1}>INICIO</a></li>
                <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
                <li className={clsx(styles.liLista)}><a href={link3}>OBJETIVOS</a></li>
                <li className={clsx(styles.liLista)}><a href={link4}>EQUIPE</a></li>
                <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
            </ul>
        </div>
    )
}

function Intro({ projeto, imgIntro }) {
    return (
        <div className={clsx(styles.heroBanner)}>
            <div className={styles.HeaderCPA}>
                <img src={imgIntro} alt="Header e Logo do Projeto Conhecer Para Acolher" />
            </div>
        </div>
    );
}


function Sobre() {
    return (
        <Layout title="Sobre | Conhecer Para Acolher">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header className={clsx(styles.heroBanner)}>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main className={clsx(styles.main)}>
                <section className={styles.content}>
                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {menu.map((props, idx) => (
                                <Menu key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {ods.map((props, idx) => (
                                <ODS key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {justificativa.map((props, idx) => (
                                <Justificativa key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {publicoalvo.map((props, idx) => (
                                <PublicoAlvo key={idx} {...props} />
                            ))}
                        </div>
                    </div>
                </section>
            </main>
        </Layout>
    )
}


export default Sobre;