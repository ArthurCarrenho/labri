import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "../styles.module.scss";
import stylesPod from "./styles.pod-ri.module.css";
import LogoPodRI from "@site/static/img/projetos/extensao/pod-ri/pod-ri.svg";
import useBaseUrl from "@docusaurus/useBaseUrl";

const menu = [
  {
    projeto: "pod-ri - labri-unesp",
    link1: "/pod-ri",
    link2: "/pod-ri/newsletter",
  },
];

const intro = [
  {
    projeto: "pod-ri - labri-unesp",
    imgIntro: "/img/projetos/extensao/pod-ri/pod-ri.svg",
  },
];

const sobre = [
  {
    projeto: "pod-ri - labri-unesp",
    description:
      "O Pod-RI é um projeto feito por estudantes, no formato de podcast, iniciado como uma ideia entre amigos. Gravamos episódios com diversas temáticas das Relações Internacionais, assim como conversas com especialistas e profissionais. Além disso, produzimos conteúdos para redes sociais, onde entramos em contato mais direto com a comunidade de Relações Internacionais do Brasil.",
    imgSobre: "./img/projetos/extensao/pod-ri/sobre.png",
  },
];

const eppod = [
  {
    projeto: "pod-ri - labri-unesp",
    nome: "#3.12: Trabalhando com Assistência de Refugiados com Miguel Pachioni",
    description:
      "Bem vindos a mais um episódio do Pod-RI! Neste episódio conversamos com Miguel Pachioni sobre sua experiência trabalhando na ACNUR e sobre o importante tema da assistência de refugiados.",
    imgEps: "./img/projetos/extensao/pod-ri/EPs-pod.jpg",
    info: "Todos os episódios do Pod-RI e plataformas em que estão disponíveis encontram-se aqui:",
  },
];

const membro = [
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Ailton Salvadori",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-ailton.png",
    rede1: "https://www.instagram.com/ailtonsalva/",
    imgRede1: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    rede2: "https://www.facebook.com/ailton.salvadori",
    imgRede2: "./img/projetos/extensao/pod-ri/fb-logo.svg",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Bianca Cintra da Costa Antunes",
    rede1: "https://www.facebook.com/bianca.antunes.758",
    imgRede1: "./img/projetos/extensao/pod-ri/fb-logo.svg",
    rede2: "https://www.facebook.com/bianca.antunes.758",
    imgRede2: "./img/projetos/extensao/pod-ri/fb-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-bianca.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Bruno Cesar David Ruy",
    rede1: "https://www.instagram.com/brunoruyyy/",
    imgRede1: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    rede2: "https://www.facebook.com/bruno.ruy.99",
    imgRede2: "./img/projetos/extensao/pod-ri/fb-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-bruno.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Danielle Elis Alves Valdivia",
    rede1: "https://www.linkedin.com/in/daniellevaldiviaunesp/",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.facebook.com/danielle.a.valdivia/",
    imgRede2: "./img/projetos/extensao/pod-ri/fb-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-danielle.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Enzo Mendes Golfetti",
    rede1: "https://www.linkedin.com/in/enzogolfetti/",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.linkedin.com/in/enzogolfetti/",
    imgRede2: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-enzo.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Gustavo Pasqueta",
    rede1: "https://www.linkedin.com/in/gustavo-pasqueta/",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.instagram.com/gustapasqueta/",
    imgRede2: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-gustavo.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Leonardo Pagano Landucci",
    rede1: "http://lattes.cnpq.br/1338768819301424",
    imgRede1: "./img/projetos/extensao/pod-ri/lattes-logo.png",
    rede2: "http://lattes.cnpq.br/1338768819301424",
    imgRede2: "./img/projetos/extensao/pod-ri/lattes-logo.png",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-leonardo.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Samuel Davis Domingues Lima",
    rede1: "https://www.linkedin.com/in/samuel-davis-domingues-lima-61843b1b3",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.instagram.com/samueldavisd/",
    imgRede2: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-samuel.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Sofia Navarro Aguiar",
    rede1: "http://lattes.cnpq.br/8344699371908069",
    imgRede1: "./img/projetos/extensao/pod-ri/lattes-logo.png",
    rede2: "https://www.linkedin.com/in/sofia-navarro-aguiar-23a435a3/",
    imgRede2: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-sofia.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Stephany Anicezio de Souza Primo",
    rede1: "https://www.instagram.com/stephyanicezio/?hl=pt-br",
    imgRede1: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    rede2: "https://www.linkedin.com/in/stephanyanicezio",
    imgRede2: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-stephany.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Victoria dos Anjos Gois",
    rede1: "https://www.linkedin.com/in/victoriagois/",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.instagram.com/torigois/",
    imgRede2: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-victoria.png",
  },
];

const redessociais = [
  {
    projeto: "pod-ri - labri-unesp",
    imgRedes: "./img/projetos/extensao/pod-ri/redes-sociais.svg",
    description: "ACOMPANHE O Pod-RI NAS REDES SOCIAIS",
    info: "Siga nossas redes sociais para ficar por dentro de conteúdos exclusivos e saber antecipadamente sobre o lançamento de novos episódios.",
  },
];

const membron = [
  {
    projeto: "pod-ri - labri-unesp",
    title: "Equipe",
  },
];

const exmembrot = [
  {
    projeto: "pod-ri - labri-unesp",
    title: "Já passaram por aqui",
  },
];

const exmembro = [
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Nome do Ex-Membro",
    rede1: "/pod-ri",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "/pod-ri",
    imgRede2: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    sobre: "Fez parte de nossa equipe entre 2017-2020",
    imgMembro: "/img/gepdai/icon.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Nome do Ex-Membro",
    rede1: "/pod-ri",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "/pod-ri",
    imgRede2: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    sobre: "Fez parte de nossa equipe entre 2017-2020",
    imgMembro: "/img/gepdai/icon3.png",
  },
];

function Intro({ projeto, imgIntro }) {
  return (
    <div className={clsx(stylesPod.heroBannerPod, "hero hero--primary")}>
      <div className={stylesPod.LogoPod}>
        <img src={imgIntro} alt="Logo Pod-RI" />
      </div>
    </div>
  );
}

function Sobre({
  description,
  projeto,
  imgSobre,
  imageUrl,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div>
      <div className="container">
        <div className={clsx(styles.row, "row")}>
          <div className="col col--6">
            <img src={imgSobre} alt="Ilustração Sobre Pod-RI"></img>
          </div>
          <div className={clsx(stylesPod.pSobrePod, "col col--6")}>
            <p>{description}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

function EpPod({ description, imageUrl, imgEps, EpsPod, info, nome }) {
  const ImagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.pEpPod)}>
      <h3>Episódios Disponíveis</h3>
      <div className="container">
        <div className={clsx(styles.row, "row")}>
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <img src={imgEps} alt="Capa do episódio do Pod-RI"></img>
          </div>
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <h3>{nome}</h3>
            <p>{description}</p>
            <h3>
              <a
                href="https://anchor.fm/pod-ri/episodes/Pod-RI-3-12---Trabalhando-com-Assistncia-de-Refugiados-com-Miguel-Pachioni-e12qg3i"
                target="_blank"
              >
                Acesse Agora
              </a>
            </h3>
          </div>
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <p>{info}</p>
            <h3>
              <a href="https://linktr.ee/podri" target="_blank">
                Linktr.ee
              </a>
            </h3>
          </div>
        </div>
      </div>
    </div>
  );
}

function MembroN({ title }) {
  return (
    <div className="container">
      <div className={clsx(stylesPod.Centralizar)}>
        <h2 className={clsx(stylesPod.h2T)}>{title}</h2>
      </div>
    </div>
  );
}

function Membro({
  imageUrl,
  nome,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
  imgMembro,
  membro,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.pEquipe2, "col col--6")}>
      <h3>{nome}</h3>
      <div className={clsx(stylesPod.imagemEquipe)}>
        <img src={imgMembro} alt="Foto do membro do Pod-RI" />
      </div>
      <div className={stylesPod.RedesLogo}>
        <a href={rede1} target="_blank">
          <img src={imgRede1} alt="Logo da Rede Social 1" />
        </a>
        <a href={rede2} target="_blank">
          <img src={imgRede2} alt="Logo da Rede Social 2" />
        </a>
      </div>
    </div>
  );
}

function ExMembroT({ title }) {
  return (
    <div className={clsx(stylesPod.Centralizar)}>
      <h2 className={clsx(stylesPod.h2ExM)}>{title}</h2>
    </div>
  );
}

function ExMembro({
  imageUrl,
  nome,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
  imgMembro,
  membro,
  sobre,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.pEquipe2, "col col--6")}>
      <h3>{nome}</h3>
      <p className={clsx(stylesPod.pExmembro)}>{sobre}</p>
      <div className={clsx(stylesPod.imagemEquipe)}>
        <img src={imgMembro} alt="Foto do ex-membro da equipe do Pod-RI" />
      </div>
      <div className={stylesPod.RedesLogo}>
        <a href={rede1} target="_blank">
          <img src={imgRede1} alt="Logo da Rede Social 1" />
        </a>
        <a href={rede2} target="_blank">
          <img src={imgRede2} alt="Logo da Rede Social 2" />
        </a>
      </div>
    </div>
  );
}

function RedesSociais({ imageUrl, redes, info, imgRedes, description }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.containerR)}>
      <div className={clsx(styles.row, "row")}>
        <div className="col col--12">
          <img src={imgRedes} alt="Ilustração Redes Sociais" />
        </div>
        <div className="col col--12">
          <h2 className={clsx(stylesPod.h2)}>{description}</h2>
          <div className={stylesPod.pSociais}>
            <h4>{info}</h4>
          </div>
          <a
            className="button button--secondary"
            href="https://www.instagram.com/podr_i/"
            target="_blank"
          >
            Instagram
          </a>
          <a
            className="button button--secondary"
            href="https://www.facebook.com/podcastRI/"
            target="_blank"
          >
            Facebook
          </a>
          <a
            className="button button--secondary"
            href="https://twitter.com/podr_i"
            target="_blank"
          >
            Twitter
          </a>
          <a
            className="button button--secondary"
            href="https://www.linkedin.com/company/pod-ri/"
            target="_blank"
          >
            Linkedin
          </a>
        </div>
      </div>
    </div>
  );
}

function Menu({ link1, link2, link3, link4, link5 }) {
  return (
    <div className={clsx(stylesPod.Menu)}>
      <ul className={clsx(stylesPod.ulLista)}>
        <li className={clsx(stylesPod.liLista)}>
          <a href={link1}>HOME</a>
        </li>
        <li className={clsx(stylesPod.liLista)}>
          <a href={link2}>NEWSLETTER</a>
        </li>
      </ul>
    </div>
  );
}

function SobrePod() {
  return (
    <Layout title="Sobre Pod-RI">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <header className={clsx(stylesPod.heroBannerPod, "hero hero--primary")}>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main className={clsx(stylesPod.pagCor)}>
        <section className={styles.content}>
          <div className={clsx(stylesPod.Sobre, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Sobre, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {sobre.map((props, idx) => (
                <Sobre key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Sobre, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {eppod.map((props, idx) => (
                <EpPod key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {membron.map((props, idx) => (
                <MembroN key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {membro.map((props, idx) => (
                <Membro key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {exmembrot.map((props, idx) => (
                <ExMembroT key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {exmembro.map((props, idx) => (
                <ExMembro key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.RedesSociais, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {redessociais.map((props, idx) => (
                <RedesSociais key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default SobrePod;
