import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "../styles.module.scss";
import stylesPod from "./styles.pod-ri.module.css";
import LogoPodRI from "@site/static/img/projetos/extensao/pod-ri/pod-ri.svg";
import useBaseUrl from "@docusaurus/useBaseUrl";

const intro = [
  {
    projeto: "pod-ri - labri-unesp",
    imgIntro: "./img/pod-ri/logo-sembg-pod-ri.svg",
    rede1: "./img/pod-ri/rede-1.svg",
    rede2: "./img/pod-ri/rede-2.svg",
    rede3: "./img/pod-ri/rede-3.svg",
    rede4: "./img/pod-ri/rede-4.svg",
    rede5: "./img/pod-ri/rede-5.svg",
  },
];

const sobre = [
  {
    projeto: "pod-ri - labri-unesp",
    description:
      "O Pod-RI é um projeto feito por estudantes, no formato de podcast, iniciado como uma ideia entre amigos. Gravamos episódios com diversas temáticas das Relações Internacionais, assim como conversas com especialistas e profissionais. Além disso, produzimos conteúdos para redes sociais, onde entramos em contato mais direto com a comunidade de Relações Internacionais do Brasil.",
    imgSobre: "./img/pod-ri/primeira-coluna.svg",
  },
];

const eppod = [
  {
    projeto: "pod-ri - labri-unesp",
    nome: "#3.12: Trabalhando com Assistência de Refugiados com Miguel Pachioni",
    description:
      "Bem vindos a mais um episódio do Pod-RI! Neste episódio conversamos com Miguel Pachioni sobre sua experiência trabalhando na ACNUR e sobre o importante tema da assistência de refugiados.",
    imgEps: "./img/projetos/extensao/pod-ri/EPs-pod.jpg",
    info: "Todos os episódios do Pod-RI e plataformas em que estão disponíveis encontram-se aqui:",
  },
];

const membro = [
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Ailton Salvadori",
    imgMembro: "./img/pod-ri/equipe-ailton-1.png",
    rede1: "https://www.facebook.com/ailton.salvadori",
    imgRede1: "./img/pod-ri/logo-fb-cor.png",
    rede2: "https://www.instagram.com/ailtonsalva/",
    imgRede2: "./img/pod-ri/logo-ig-cor.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Bianca Cintra da Costa Antunes",
    rede1: "https://www.facebook.com/bianca.antunes.758",
    imgRede1: "./img/pod-ri/logo-fb-cor.png",
    rede2: "https://instagram.com/bia_antunes92?utm_medium=copy_link",
    imgRede2: "./img/pod-ri/logo-ig-cor.png",
    imgMembro: "./img/pod-ri/equipe-bianca-1.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Bruno Cesar David Ruy",
    rede1: "https://www.facebook.com/bruno.ruy.99",
    imgRede1: "./img/pod-ri/logo-fb-cor.png",
    rede2: "https://www.instagram.com/brunoruyyy/",
    imgRede2: "./img/pod-ri/logo-ig-cor.png",
    imgMembro: "./img/pod-ri/equipe-bruno-1.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Danielle Elis Alves Valdivia",
    rede1: "https://www.linkedin.com/in/daniellevaldiviaunesp/",
    imgRede1: "./img/pod-ri/logo-linkedin-cor.png",
    rede2: "https://www.facebook.com/danielle.a.valdivia/",
    imgRede2: "./img/pod-ri/logo-fb-cor.png",
    imgMembro: "./img/pod-ri/equipe-danielle-1.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Gustavo Pasqueta",
    rede1: "https://www.linkedin.com/in/gustavo-pasqueta/",
    imgRede1: "./img/pod-ri/logo-linkedin-cor.png",
    rede2: "https://www.instagram.com/gustapasqueta/",
    imgRede2: "./img/pod-ri/logo-ig-cor.png",
    imgMembro: "./img/pod-ri/equipe-gustavo-1.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Leonardo Pagano Landucci",
    rede1: "http://lattes.cnpq.br/1338768819301424",
    imgRede1: "./img/pod-ri/logo-lattes-cor.png",
    rede2: "https://instagram.com/leo_landucci28?utm_medium=copy_link",
    imgRede2: "./img/pod-ri/logo-ig-cor.png",
    imgMembro: "./img/pod-ri/equipe-leonardo-1.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Samuel Davis Domingues Lima",
    rede1: "https://www.linkedin.com/in/samuel-davis-domingues-lima-61843b1b3",
    imgRede1: "./img/pod-ri/logo-linkedin-cor.png",
    rede2: "https://www.instagram.com/samueldavisd/",
    imgRede2: "./img/pod-ri/logo-ig-cor.png",
    imgMembro: "./img/pod-ri/equipe-samuel-1.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Sofia Navarro Aguiar",
    rede1: "http://lattes.cnpq.br/8344699371908069",
    imgRede1: "./img/pod-ri/logo-lattes-cor.png",
    rede2: "https://www.linkedin.com/in/sofia-navarro-aguiar-23a435a3/",
    imgRede2: "./img/pod-ri/logo-linkedin-cor.png",
    imgMembro: "./img/pod-ri/equipe-sofia-1.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Stephany Anicezio de Souza Primo",
    rede1: "https://www.linkedin.com/in/stephanyanicezio",
    imgRede1: "./img/pod-ri/logo-linkedin-cor.png",
    rede2: "https://www.instagram.com/stephyanicezio/?hl=pt-br",
    imgRede2: "./img/pod-ri/logo-ig-cor.png",
    imgMembro: "./img/pod-ri/equipe-stephany-1.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Victoria dos Anjos Gois",
    rede1: "https://www.linkedin.com/in/victoriagois/",
    imgRede1: "./img/pod-ri/logo-linkedin-cor.png",
    rede2: "https://www.instagram.com/torigois/",
    imgRede2: "./img/pod-ri/logo-ig-cor.png",
    imgMembro: "./img/pod-ri/equipe-victoria-1.png",
  },
];

const redessociais = [
  {
    projeto: "pod-ri - labri-unesp",
    imgRedes: "./img/pod-ri/social-media.png",
    description: "ACOMPANHE O Pod-RI NAS REDES SOCIAIS",
    info: "Siga nossas redes sociais para ficar por dentro de conteúdos exclusivos e saber antecipadamente sobre o lançamento de novos episódios.",
  },
];

const membron = [
  {
    projeto: "pod-ri - labri-unesp",
    title: "Equipe",
  },
];

const exmembrot = [
  {
    projeto: "pod-ri - labri-unesp",
    title: "Já passaram por aqui",
  },
];

const exmembro = [
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Nome do Ex-Membro",
    rede1: "/pod-ri",
    imgRede1: "./img/pod-ri/logo-linkedin-cor.png",
    rede2: "/pod-ri",
    imgRede2: "./img/pod-ri/logo-linkedin-cor.png",
    sobre: "Fez parte de nossa equipe entre 2017-2020",
    imgMembro: "./img/pod-ri/exemplo-icon.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Nome do Ex-Membro",
    rede1: "/pod-ri",
    imgRede1: "./img/pod-ri/logo-linkedin-cor.png",
    rede2: "/pod-ri",
    imgRede2: "./img/pod-ri/logo-linkedin-cor.png",
    sobre: "Fez parte de nossa equipe entre 2017-2020",
    imgMembro: "./img/pod-ri/exemplo-icon.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Nome do Ex-Membro",
    rede1: "/pod-ri",
    imgRede1: "./img/pod-ri/logo-linkedin-cor.png",
    rede2: "/pod-ri",
    imgRede2: "./img/pod-ri/logo-linkedin-cor.png",
    sobre: "Fez parte de nossa equipe entre 2017-2020",
    imgMembro: "./img/pod-ri/exemplo-icon.png",
  },
];

const carousel = [
  {
    projeto: "pod-ri - labri-unesp",
    img1: "/img/projetos/extensao/pod-ri/pod-ri.svg",
    link1: "#",
    img2: "/img/projetos/extensao/pod-ri/pod-ri.svg",
    link2: "https://labriunesp.org/pod-ri/newsletter",
  },
];

const primeiracoluna = [
  {
    projeto: "pod-ri - labri-unesp",
    titulo: "Sobre o Pod-RI",
    texto:
      "O Pod-RI é um projeto feito por estudantes, no formato de podcast, iniciado como uma ideia entre amigos. Gravamos episódios com diversas temáticas das Relações Internacionais, assim como conversas com especialistas e profissionais. Além disso, produzimos conteúdos para redes sociais, onde entramos em contato mais direto com a comunidade de Relações Internacionais do Brasil.",
    img: "./img/pod-ri/primeira-coluna3.svg",
  },
];

const segundacoluna = [
  {
    projeto: "pod-ri - labri-unesp",
    titulo: "Sobre o Pod-RI",
    texto:
      "Nossa equipe é formada por 10 graduandos em Relações Internacionais da UNESP/Franca.",
    img: "./img/pod-ri/membros-equipe.svg",
  },
];

const infos = [
  {
    projeto: "pod-ri - labri-unesp",
    link1: "https://open.spotify.com/show/6m87Jl9P8aTwoMYDlPvxax",
    link2: "https://podcasts.apple.com/br/podcast/pod-ri/id1503693769",
    link3: "https://anchor.fm/pod-ri",
    link4:
      "https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy8xNmUxYWE4MC9wb2RjYXN0L3Jzcw==",
    link5: "https://www.breaker.audio/pod-ri",
    link6: "https://overcast.fm/itunes1503693769/pod-ri",
    link7: "https://pca.st/1riysphk",
    link8: "https://radiopublic.com/podri-WeYB0E",
    link9: "https://www.youtube.com/channel/UCPjk8jku95pIEzijvqC8rPg",
  },
];

const newsletter = [
  {
    projeto: "pod-ri - labri-unesp",
    titulo: "Acervo",
    link: "https://labriunesp.org/pod-ri/newsletter",
    img: "./img/pod-ri/newsletter-pod-ri.svg",
    texto: "Receba a newsletter por email",
    link2:
      "https://docs.google.com/forms/d/e/1FAIpQLSfiuA9qkJiHnvBz_jym5V1yWh_oP-0MunMv5UEsAbSTAM_NPQ/viewform",
  },
];

const menu = [
  {
    projeto: "pod-ri - labri-unesp",
    link1: "#sobre",
    link2: "#newsletter",
    link3: "#equipe",
    link4: "#redes-sociais",
  },
];

function Menu({ link1, link2, link3, link4, link5 }) {
  return (
    <div className={clsx(stylesPod.Menu)}>
      <ul className={clsx(stylesPod.ulLista)}>
        <li className={clsx(stylesPod.liLista)}>
          <a href={link1}>SOBRE</a>
        </li>
        <li className={clsx(stylesPod.liLista)}>
          <a href={link2}>NEWSLETTER</a>
        </li>
        <li className={clsx(stylesPod.liLista)}>
          <a href={link3}>EQUIPE</a>
        </li>
        <li className={clsx(stylesPod.liLista)}>
          <a href={link4}>REDES SOCIAIS</a>
        </li>
      </ul>
    </div>
  );
}

function PrimeiraColuna({ texto, img, titulo2, link, img2 }) {
  return (
    <div className="container">
      <div className="row">
        <div id="sobre" className={clsx(stylesPod.Info, "col col--6")}>
          <img src={img} alt="Ilustração do card" />
        </div>
        <div className="col col--6">
          <p className={clsx(stylesPod.pSobrePod)}>{texto}</p>
        </div>
      </div>
    </div>
  );
}

function Infos({
  link1,
  link2,
  link3,
  link4,
  link5,
  link6,
  link7,
  link8,
  link9,
}) {
  return (
    <div className={clsx(stylesPod.infos)}>
      <div className="col col--12">
        <h2>Onde escutar os episódios do Pod-RI</h2>
      </div>
      <div className="col col--12">
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link1}
          target="_blank"
        >
          Spotify
        </a>
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link2}
          target="_blank"
        >
          Apple Podcasts
        </a>
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link3}
          target="_blank"
        >
          Anchor
        </a>
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link4}
          target="_blank"
        >
          Google Podcasts
        </a>
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link5}
          target="_blank"
        >
          Breaker
        </a>
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link6}
          target="_blank"
        >
          Overcasts
        </a>
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link7}
          target="_blank"
        >
          RocketCasts
        </a>
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link8}
          target="_blank"
        >
          Radio Public
        </a>
        <a
          className={clsx(stylesPod.botaoInfo, "button button--secondary")}
          href={link9}
          target="_blank"
        >
          Youtube
        </a>
      </div>
    </div>
  );
}

function SegundaColuna({ texto, img, titulo2, link, img2 }) {
  return (
    <div className="container">
      <div className="row">
        <div className="col col--12">
          <h3 id="equipe">Conheça a Equipe do Pod-RI</h3>
          <p className={clsx(stylesPod.pEquipe)}>{texto}</p>
        </div>
      </div>
    </div>
  );
}

function Newsletter({ titulo, link, img, texto, link2 }) {
  return (
    <div className="container">
      <div className="row">
        <div className="col col--5">
          <h2 id="newsletter" className={clsx(stylesPod.h2Newsletter)}>
            {texto}
          </h2>
          <a
            className={clsx(stylesPod.newsletterBotao)}
            href={link2}
            target="_blank"
          >
            Cadastre-se aqui
          </a>
        </div>
        <div className={clsx(stylesPod.imagemNews, "col col--4")}>
          <img src={img} alt="Ilustração do card" />
        </div>
        <div className="col col--3">
          <h2 className={clsx(stylesPod.h2Newsletter)}>{titulo}</h2>
          <p className={clsx(stylesPod.pAcervo)}>
            Todas as edições da Newsletter
          </p>
          <a
            className={clsx(stylesPod.newsletterBotao)}
            href={link}
            target="_blank"
          >
            Acesse aqui
          </a>
        </div>
      </div>
    </div>
  );
}

function Carousel({ img1, link1, img2, link2 }) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div className="container">
      <div className={clsx(styles.row, "row")}>
        <div className="col col--12">
          <h2 className={clsx(styles.h2T)}>ACESSE AQUI</h2>
        </div>
      </div>
      <Slider {...settings}>
        <div>
          <img
            src={img1}
            alt="Clique para acessar a página inicial do Pod-RI"
            href={link1}
          />
        </div>
        <div>
          <img
            src={img2}
            alt="Clique para acessar a página do Newsletter do Pod-RI"
            href={link2}
          />
        </div>
      </Slider>
    </div>
  );
}

function Intro({ projeto, imgIntro, rede1, rede2, rede3, rede4, rede5 }) {
  return (
    <div className={clsx(stylesPod.heroBannerPod, "hero hero--primary")}>
      <div className="row">
        <div className={clsx(stylesPod.LogoPod)}>
          <img src={imgIntro} alt="Logo Pod-RI" />
        </div>
        <div className={clsx(stylesPod.RedesLogo, "col col--6")}>
          <h4 className={clsx(stylesPod.h4Pod, stylesPod.h4Header)}>
            Acompanhe o Pod-RI nas Redes Sociais
          </h4>
          <a href="https://www.instagram.com/podr_i/" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede1}
              alt="Instagram do Pod-RI"
            />
          </a>
          <a href="https://www.facebook.com/podcastRI/" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede2}
              alt="Facebook do Pod-RI"
            />
          </a>
          <a href="https://twitter.com/podr_i" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede3}
              alt="Twitter do Pod-RI"
            />
          </a>
          <a href="https://www.linkedin.com/company/pod-ri/" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede4}
              alt="LinkedIn do Pod-RI"
            />
          </a>
          <a href="https://linktr.ee/podri" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede5}
              alt="Linktr.ee do Pod-RI"
            />
          </a>
        </div>
      </div>
    </div>
  );
}

function Sobre({
  description,
  projeto,
  imgSobre,
  imageUrl,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div>
      <div className="container">
        <div className={clsx(styles.row, "row")}>
          <div className="col col--6">
            <img src={imgSobre} alt="Ilustração Sobre Pod-RI"></img>
          </div>
          <div className={clsx(stylesPod.pSobrePod, "col col--6")}>
            <p>{description}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

function EpPod({ description, imageUrl, imgEps, EpsPod, info, nome }) {
  const ImagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.pEpPod)}>
      <h3>Episódios Disponíveis</h3>
      <div className="container">
        <div className={clsx(styles.row, "row")}>
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <img src={imgEps} alt="Capa do episódio do Pod-RI"></img>
          </div>
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <h3>{nome}</h3>
            <p>{description}</p>
            <h3>
              <a
                href="https://anchor.fm/pod-ri/episodes/Pod-RI-3-12---Trabalhando-com-Assistncia-de-Refugiados-com-Miguel-Pachioni-e12qg3i"
                target="_blank"
              >
                Acesse Agora
              </a>
            </h3>
          </div>
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <p>{info}</p>
            <h3>
              <a href="https://linktr.ee/podri" target="_blank">
                Linktr.ee
              </a>
            </h3>
          </div>
        </div>
      </div>
    </div>
  );
}

function Membro({
  imageUrl,
  nome,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
  imgMembro,
  membro,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.pEquipe2, "card col col--4")}>
      <div className="card-content">
        <div className="card__header">
          <h4 className={clsx(stylesPod.h4Pod)}>{nome}</h4>
        </div>
        <div className="card__body">
          <div className={clsx(stylesPod.imagemEquipe)}>
            <img src={imgMembro} alt="Foto do membro do Pod-RI" />
          </div>
        </div>
        <div className={clsx(stylesPod.RedesLogoM, "card__footer")}>
          <a href={rede1} target="_blank">
            <img src={imgRede1} alt="Logo da Rede Social 1" />
          </a>
          <a href={rede2} target="_blank">
            <img src={imgRede2} alt="Logo da Rede Social 2" />
          </a>
        </div>
      </div>
    </div>
  );
}

function ExMembroT({ title }) {
  return (
    <div className="col col--12">
      <h2 className={clsx(stylesPod.h2ExM)}>{title}</h2>
    </div>
  );
}

function ExMembro({
  imageUrl,
  nome,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
  imgMembro,
  membro,
  sobre,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.pEquipe2, "card col col--4")}>
      <div className="card-content">
        <div className="card__header">
          <h3>{nome}</h3>
        </div>
        <div className="card__body">
          <p className={clsx(stylesPod.pExmembro)}>{sobre}</p>
          <div className={clsx(stylesPod.imagemEquipe)}>
            <img src={imgMembro} alt="Foto do ex-membro da equipe do Pod-RI" />
          </div>
        </div>
        <div className="card__footer">
          <div className={clsx(stylesPod.RedesLogoM)}>
            <a href={rede1} target="_blank">
              <img src={imgRede1} alt="Logo da Rede Social 1" />
            </a>
            <a href={rede2} target="_blank">
              <img src={imgRede2} alt="Logo da Rede Social 2" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

function RedesSociais({ imageUrl, redes, info, imgRedes, description }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div id="redes-sociais" className={clsx(stylesPod.containerR)}>
      <div className={clsx(styles.row, "row")}>
        <div className="col col--12">
          <img src={imgRedes} alt="Ilustração Redes Sociais" />
        </div>
        <div className="col col--12">
          <h2 className={clsx(stylesPod.h2)}>{description}</h2>
          <div className={stylesPod.pSociais}>
            <h4>{info}</h4>
          </div>
          <a
            className="button button--secondary"
            href="https://www.instagram.com/podr_i/"
            target="_blank"
          >
            Instagram
          </a>
          <a
            className="button button--secondary"
            href="https://www.facebook.com/podcastRI/"
            target="_blank"
          >
            Facebook
          </a>
          <a
            className="button button--secondary"
            href="https://twitter.com/podr_i"
            target="_blank"
          >
            Twitter
          </a>
          <a
            className="button button--secondary"
            href="https://www.linkedin.com/company/pod-ri/"
            target="_blank"
          >
            Linkedin
          </a>
        </div>
      </div>
    </div>
  );
}

function SobrePod() {
  return (
    <Layout title="Sobre Pod-RI">
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <head>
        <link
          rel="stylesheet"
          type="text/css"
          charset="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"
        />
      </head>

      <header className={clsx(stylesPod.heroBannerPod, "hero hero--primary")}>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(stylesPod.Sobre)}>
            <div className={clsx(styles.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Sobre, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Sobre)}>
            <div className={clsx(styles.row, "row")}>
              {infos.map((props, idx) => (
                <Infos key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Sobre, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {newsletter.map((props, idx) => (
                <Newsletter key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {segundacoluna.map((props, idx) => (
                <SegundaColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {membro.map((props, idx) => (
                <Membro key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {exmembrot.map((props, idx) => (
                <ExMembroT key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {exmembro.map((props, idx) => (
                <ExMembro key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.RedesSociais, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {redessociais.map((props, idx) => (
                <RedesSociais key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default SobrePod;
