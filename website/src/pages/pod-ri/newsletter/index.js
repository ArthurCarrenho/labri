import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.news.module.css"
import stylesPod from "./styles.pod-ri.module.css";
import LogoPodRI from "@site/static/img/projetos/extensao/pod-ri/pod-ri.svg";
import useBaseUrl from "@docusaurus/useBaseUrl";

const intro = [
  {
    projeto: "pod-ri - labri-unesp",
    imgIntro: "/img/pod-ri/logo-sembg-pod-ri.svg",
    rede1: "/img/pod-ri/rede-1.svg",
    rede2: "/img/pod-ri/rede-2.svg",
    rede3: "/img/pod-ri/rede-3.svg",
    rede4: "/img/pod-ri/rede-4.svg",
    rede5: "/img/pod-ri/rede-5.svg",
  },
];

const menu = [
  {
    projeto: "newsletter - labri-unesp",
    link1: "/pod-ri",
    link2: "/pod-ri/newsletter",
  },
];

const primeiracoluna = [
  {
    projeto: "publicacoes - labri-unesp",
    title: "Newsletter Pod-RI",
  },
];

const edicoes = [
  {
    projeto: "publicacoes - labri-unesp",
    titulo: "Edição Julho 2022",
    sobre: (
      <>
        Nesta edição traremos os últimos acontecimentos da geopolítica mundial: Renúncia de Boris Johnson;
        O que ocorre no Sri Lanka; Assassinato do Ex-Primeiro ministro japonês; Putin agradece a Ergogan; Estados
        Unidos envia representante especial para a Etiópia; Pod-Cobertura; Para além de guerras...
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
    link: "https://drive.google.com/file/d/1aK1byHZPfS3TGpXsCZ-xqalRVna_pXom/view?usp=sharing",
  },
  {
    projeto: "publicacoes - labri-unesp",
    titulo: "Edição Junho 2022",
    sobre: (
      <>
        Nesta edição traremos os últimos acontecimentos da geopolítica mundial: Epidemia de varíola assusta o planeta;
        Caso Bruno Pereira e Dom Philips; Cúpula das Américas 2022; Eleições na Colômbia; Suprema Corte EUA e Aborto;
        Pod-Cobertura; Para além de guerras; Suge-RI...
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
    link: "https://drive.google.com/file/d/1UV1YwJTxn4AAojatY2PGieAnL8o-Voaz/view?usp=sharing",
  },
  {
    projeto: "publicacoes - labri-unesp",
    titulo: "Edição Maio 2022",
    sobre: (
      <>
        Nesta edição especial comentaremos sobre o ressurgimento de velhos conhecidos da medicina e sobre os
        últimos acontecimentos da geopolítica mundial: Um velho conhecido; Aborto nos EUA; Como está a guerra na Ucrânia;
        Reunião dos BRICS; OMS lança novo alerta; Pod-Cobertura; Para além de guerras...
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
    link: "https://drive.google.com/file/d/1XeC6lhyeu4yrdzvoR2PVt6YGEGG5Lfii/view?usp=sharing",
  },
  {
    projeto: "publicacoes - labri-unesp",
    titulo: "Edição Abril 2022",
    sobre: (
      <>
        Nesta edição especial comentaremos sobre alguns dos principais órgãos internacionais e como está o 
        balanço dos países neles: Copa do Mundo no Qatar; O massacre russo; Russua fights back; Os problemas dos
        fluxos migratórios; O Brasil e a OCDE; Macron vence na França; Nicarágua sai da OCDE; Pod-Cobertura; Para 
        além de guerras; Suge-RI... 
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
    link: "https://drive.google.com/file/d/15QpiqxxyDvi0-F8cZ3gvuQogVeJpEJxs/view?usp=sharing",
  },
  {
    projeto: "publicacoes - labri-unesp",
    titulo: "Edição Março 2022",
    sobre: (
      <>
        Nesta edição especial comentaremos sobre os impactos que anos de pandemia e guerras têm sobre a economia e
        política mundial: O futuro em cripto? Quarto choque do petróleo? Nova agenda global;
        Presidente da Honduras extraviado para os EUA; Pacto nuclear iraniano;
        Pod-cobertura eleitoral; Para além de guerras; Suge-RI...
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
    link: "https://drive.google.com/file/d/1USGDd4upkA7N0Hmp3vKOJyuOaSib01kO/view",
  },
  {
    projeto: "publicacoes - labri-unesp",
    titulo: "Edição Fevereiro 2022",
    sobre: (
      <>
        Nesta edição especial comentaremos sobre o ano de 2022 e as tensões que estão ocorrendo no planeta: O que acontece
        no Cazaquistão? Ucrânia vs Rússia; Encontro de Bolsonora com Putin; China e sua posição no conflito da Ucrânia; Para
        além das guerras; Suge-RI...
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
    link: "https://drive.google.com/file/d/10-ho8fFpAHl66HJynZwh0A8JLBXu91Zm/view",
  },
  {
    projeto: "publicacoes - labri-unesp",
    titulo: "Edição Abril 2021",
    sobre: (
      <>
        Continuidade dos conflitos no Afeganistão; Mudanças climáticas; Variante
        Delta pelo mundo; Crise migratória na fronteira americana; O legado de
        Merkel
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
    link: "https://drive.google.com/file/d/1cju88J26MD3cKXyiR4YZ1iVMIKc5tFw4/view?usp=sharing",
  },
];

function SegundaColuna({ titulo, imgFoto, foto, description, link }) {
  return (
    <div className={clsx(styles.containerCards, "col col--4")}>
      <div className={clsx(styles.Cards)}>
        <h2 className={clsx(styles.h2Segundo)}>{titulo}</h2>
        <img
          src={imgFoto}
          alt="Imagem das edições passadas do Newsletter do Pod-RI"
          className={clsx(styles.SobreCards)}
        />
        <p className={clsx(styles.pCaixas)}>{description}</p>
        <a className="button button--secondary" href={link} target="_blank">
          Download
        </a>
      </div>
    </div>
  );
}

function Edicoes({ titulo, sobre, imgFoto, link }) {
  return (
    <div className={clsx(styles.containerEdicoes)}>
      <div className="row">
        <div className="col col--8">
          <h2 className={clsx(styles.h2Edicoes)}>{titulo}</h2>
          <p className={clsx(styles.pEdicoes)}>{sobre}</p>
        </div>
        <div className={clsx(styles.IconPDF, "col col--4")}>
          <a href={link} target="_blank">
            <img src={imgFoto} alt="Documento em PDF para Download" />
          </a>
        </div>
      </div>
    </div>
  );
}

function PrimeiraColuna({ title }) {
  return (
    <div className={clsx(styles.containerSobre)}>
      <div className="row">
        <div className="col col--12">
          <h2 className={stylesPod.h2Newsletter}>{title}</h2>
        </div>
      </div>
    </div>
  );
}

function Menu({ link1, link2 }) {
  return (
      <div className={clsx(stylesPod.Menu)}>
        <ul className={clsx(stylesPod.ulLista)}>
          <li className={clsx(stylesPod.liLista)}>
            <a href={link1}>HOME</a>
          </li>
          <li className={clsx(stylesPod.liLista)}>
            <a href={link2}>NEWSLETTER</a>
          </li>
        </ul>
      </div>
  );
}

function Intro({ projeto, imgIntro, rede1, rede2, rede3, rede4, rede5 }) {
  return (
    <div className={clsx(stylesPod.heroBannerPod, "hero hero--primary")}>
      <div className="row">
        <div className={clsx(stylesPod.LogoPod)}>
          <img src={imgIntro} alt="Logo Pod-RI" />
        </div>
        <div className={clsx(stylesPod.RedesLogo, "col col--6")}>
          <h4 className={clsx(stylesPod.h4Pod, stylesPod.h4Header)}>
            Acompanhe o Pod-RI nas Redes Sociais
          </h4>
          <a href="https://www.instagram.com/podr_i/" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede1}
              alt="Instagram do Pod-RI"
            />
          </a>
          <a href="https://www.facebook.com/podcastRI/" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede2}
              alt="Facebook do Pod-RI"
            />
          </a>
          <a href="https://twitter.com/podr_i" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede3}
              alt="Twitter do Pod-RI"
            />
          </a>
          <a href="https://www.linkedin.com/company/pod-ri/" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede4}
              alt="LinkedIn do Pod-RI"
            />
          </a>
          <a href="https://linktr.ee/podri" target="_blank">
            <img
              classsName={clsx(stylesPod.RedesLogo)}
              src={rede5}
              alt="Linktr.ee do Pod-RI"
            />
          </a>
        </div>
      </div>
    </div>
  );
}

function Newsletter() {
  return (
    <Layout title="Newsletter Pod-RI">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <header className={clsx(stylesPod.heroBannerPod, "hero hero--primary")}>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main className={clsx(styles.pagCor)}>
        <section className={styles.content}>
          <div className={clsx(styles.Sobre, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.Sobre, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.Sobre, styles.container)}>
            <div className={clsx(styles.row, "row")}>
              {edicoes.map((props, idx) => (
                <Edicoes key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Newsletter;
