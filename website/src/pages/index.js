import React, { useState } from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  Dot,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import styles from "./styles.module.scss";
import { Doc } from "../components/home/Doc";
import { Info } from "../components/home/Info";
import { Grupos } from "../components/home/Grupos";
import { docsAzul, docsVermelho } from "../data/home/docs";
import { SectionSlide } from "../components/home/SectionSlide";
import { infos } from "../data/home/infos";
import { grupos } from "../data/home/grupos";
import { slides } from "../data/home/slides";

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  const [buttonVisible, setButtonVisible] = useState(false);

  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <head>
        <link
          href="https://fonts.googleapis.com/css?family=Montserrat"
          rel="stylesheet"
        ></link>
      </head>
      <header className={styles.wrapper}>
        <CarouselProvider
          className={styles.carousel}
          totalSlides={slides.length}
          isPlaying
          interval={3000}
          dragEnabled={false}
          isIntrinsicHeight
          infinite
          naturalSlideHeight={undefined}
          naturalSlideWidth={undefined}
        >
          <div
            onMouseEnter={() => {
              setButtonVisible(true);
            }}
            onMouseLeave={() => {
              setButtonVisible(false);
            }}
          >
            <Slider>
              {slides.map((slide, idx) => (
                <Slide key={idx} index={idx}>
                  <SectionSlide
                    columns={slide.columns}
                    heroTitle={slide.heroTitle}
                    heroSubtitle={slide.heroSubtitle}
                    linkText={slide.linkText}
                    linkHref={slide.linkHref}
                    imgHref={slide.imgHref}
                    imgSrc={slide.imgSrc}
                  />
                </Slide>
              ))}
            </Slider>
            <ButtonBack
              id="back"
              className={clsx(
                styles.button,
                styles.carouselbutton,
                styles.backbutton
              )}
              style={{ visibility: buttonVisible ? "visible" : "hidden" }}
            >
              <FontAwesomeIcon icon={faChevronLeft} />
            </ButtonBack>
            <ButtonNext
              className={clsx(
                styles.button,
                styles.carouselbutton,
                styles.nextbutton
              )}
              style={{ visibility: buttonVisible ? "visible" : "hidden" }}
            >
              <FontAwesomeIcon icon={faChevronRight} />
            </ButtonNext>
            <div className={styles.carouseldots}>
              {slides.map((__, idx) => (
                <Dot className={styles.carouseldot} key={idx} slide={idx} />
              ))}
            </div>
          </div>
        </CarouselProvider>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(styles.flexEstagioProjetos, styles.container)}>
            {infos.map((props, idx) => (
              <Info key={idx} {...props} />
            ))}
          </div>
          <div className={clsx(styles.grid, styles.gridItem)}>
            <div style={{ display: "flex", flexDirection: "column" }}>
              {docsAzul.map((props, idx) => (
                <Doc key={idx} {...props} />
              ))}
            </div>

            <div style={{ display: "flex", flexDirection: "column" }}>
              {docsVermelho.map((props, idx) => (
                <Doc key={idx} {...props} />
              ))}
            </div>
          </div>
          <section className={styles.contentFooter}>
            <div className={clsx(styles.flexEstagioProjetos, styles.container)}>
              {grupos.map((props, idx) => (
                <Grupos key={idx} {...props} />
              ))}
            </div>
          </section>
        </section>
      </main>
    </Layout>
  );
}
export default Home;
