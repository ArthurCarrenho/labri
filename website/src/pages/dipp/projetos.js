import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
  {
    projeto: "dipp - labri-unesp",
    logo: "/img/dipp/logo-dipp-pagina-inicial.svg",
    link: "/dipp",
    link1: "/dipp",
    link2: "/dipp/institucional",
    link3: "/dipp/noticias",
    link4: "/dipp/publicacoes",
    link5: "/dipp/projetos",
    link6: "/dipp/equipe",
    link7: "/dipp/cursos"
  },
];

const primeiracoluna = [
  {
    projeto: "dipp - labri-unesp",
    titulo: "Projetos e Pesquisas"
  }
]

const projetospesquisas = [
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/projetos.png",
    nome: "Programa Institucional de Internacionalização - Redes de Pesquisa Internacionais (CAPES-Print-UNESP - Edital PROPG/UNESP/2019)",
    texto: (
      <>
      O projeto da Rede DIPP (Development, International Politics and Peace) se baseia na construção de uma rede que 
      seja geograficamente ampliada (inicialmente em três continentes, mas visando ampliar para a Ásia futuramente) e 
      capaz de oferecer abordagens interdisciplinares sobre comportamentos políticos e institucionalidade no âmbito 
      das relações internacionais. Como tal, a Rede DIPP oferece um enquadramento ideal para diálogos transnacionais e
      transdisciplinares para melhor compreender a interação entre os comportamentos políticos atuais e seus impactos 
      sobre a constituição de sistemas de governança na área ambiental e de direitos humanos, na construção de projetos 
      de regionalismo voltados para a promoção do desenvolvimento e da segurança regional entre sociedades plurais. 
      Geograficamente, a rede reunirá inicialmente especialistas nacionais e internacionais de quinze instituições em 
      três continentes (seis na Europa, quatro na América do Norte, quatro na América do Sul e uma na Oceania). 
      Academicamente, os vários centros de pesquisa e universidades, assim como seus pesquisadores e professores 
      seniores e juniores, na rede, fornecem o know-how multidisciplinar necessário para desenvolver os objetivos da 
      Rede DIPP voltados para compreender os processos políticos, culturais, econômicos e sociais, atuais e históricos 
      que impactam no desenvolvimento da atual ordem global. A Rede Development, International Politics and Peace visa 
      promover pesquisa, ensino e extensão sobre essas temáticas, captando adequadamente a interação entre as 
      características emergentes na sociedade contemporânea e a construção de sistemas de governança nacionais e 
      internacionais para lidar com os desafios e oportunidades decorrentes desse novo contexto sociopolítico. A Rede 
      DIPP é composta pelas seguintes IES: Programa de Pós-Graduação em Relações Internacionais San Tiago Dantas ? 
      UNESP-UNICAMP-PUC-SP; Universidad Nacional de La Plata; Universidad Nacional de Colombia; Universidad Complutense 
      de Madrid; Universidad Autònoma de Barcelona; German Institute of Global and Area Studies; Universidad de la 
      República Uruguay; Columbia University; Katholieke Universiteit Leuven; Universidade de Coimbra; Universidad 
      Nacional Autónoma de México; Georgetown University; University Calgary; Neoma Business School e University of 
      Queensland.
      </>
    )

  }
]


function ProjetosPesquisas({img, nome, texto}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
          <div className="row">
              <div className={clsx(styles.icone, "col col--2")}>
                  <img className={clsx(styles.icone)} src={img} alt="Download do Texto" />
              </div>
              <div className="col col--10">
                  <h2 className={clsx(styles.h2Titulo)}>{nome}</h2>
                  <p className={clsx(styles.paragrafos)}>{texto}</p>
              </div>
          </div>
      </div>
  )
}

function PrimeiraColuna({titulo}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className="row">
        <div className="col col--12">
          <h2 className={clsx(styles.h2Dipp)}>
            {titulo}
          </h2>
        </div>
      </div>
    </div>
  )
}

function Intro({ logo, link, link1, link2, link3, link4, link5, link6, link7 }) {
  return (
    <div className={clsx(styles.heroBanner, "hero hero--primary")}>
      <div className="row">
        <div className="col col--4">
          <div className={clsx(styles.LogoDipp)}>
            <a href={link}>
              <img src={logo} alt="Logo do DIPP" />
            </a>
          </div>
        </div>
        <div className={clsx(styles.Menu, "col col--8")}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}>
              <a href={link1}>home</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link2}>institucional</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link3}>notícias</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link4}>publicações</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link5}>projetos</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link6}>equipe</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link7}>cursos</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function Projetos() {
  return (
    <Layout title="Projetos – DIPP">
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>

      <main>
        <section className="content">

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {projetospesquisas.map((props, idx) => (
                <ProjetosPesquisas key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>
    </Layout>
  );
}

export default Projetos;
