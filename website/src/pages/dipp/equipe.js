import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
  {
    projeto: "dipp - labri-unesp",
    logo: "/img/dipp/logo-dipp-pagina-inicial.svg",
    link: "/dipp",
    link1: "/dipp",
    link2: "/dipp/institucional",
    link3: "/dipp/noticias",
    link4: "/dipp/publicacoes",
    link5: "/dipp/projetos",
    link6: "/dipp/equipe",
    link7: "/dipp/cursos"
  },
];

const primeiracoluna = [
  {
    projeto: "dipp - labri-unesp",
    titulo: "Equipe DIPP"
  }
]

const membros = [
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/karina-mariano.png",
    nome: "Karina Lilia Pasquariello Mariano",
    sobre: (
      <>
      Possui graduação em Ciências Sociais pela Universidade de São Paulo (1992), mestrado em Ciência Política pela 
      Universidade de São Paulo (1996) e doutorado em Ciências Sociais pela Universidade Estadual de Campinas (2001). 
      Atualmente é professora adjunta da Universidade Estadual Paulista Júlio de Mesquita Filho, na qual participa do 
      Laboratório de Política e Governo e coordena o Grupo de Estudos Interdisciplinares em Cultura e Desenvolvimento 
      (GEICD), que integra a Rede de Pesquisa em Política Externa e Regionalismo (REPRI). Integra o Grupo de Reflexión 
      sobre Integración y Desarrollo en América Latina y Europa (GRIDALE). É bolsista Produtividade do CNPq e desde 1992 
      é pesquisadora da Equipe de Relações Internacionais do Centro de Estudos de Cultura Contemporânea (CEDEC), a qual 
      coordena desde 2016. Tem experiência na área de Ciência Política, com ênfase em Integração Regional, atuando 
      principalmente nos seguintes temas: Mercosul, teorias de integração, União Europeia, relações internacionais, 
      sindicatos, parlamentos regionais e democracia.
      </>
    ),
    rede1: "/img/dipp/lattes.png",
    rede2: "/img/dipp/orcid.png",
    link1: "http://lattes.cnpq.br/6935085820144171",
    link2: "https://orcid.org/0000-0002-4559-918X",
  }
]

function Membros({img, nome, sobre, rede1, rede2, link1, link2}){
  return(
    <div>
      <div className={clsx(styles.colunaSemFundo, "container")}>
          <div className="row">
              <div className="col col--2">
                  <img className={clsx(styles.membroIcon)} src={img} alt="Download do Texto" />
              </div>
              <div className="col col--10">
                  <h2 className={clsx(styles.h2Titulo)}>{nome}</h2>
                  <p className={clsx(styles.paragrafos)}>{sobre}</p>
              </div>
              <div className={clsx(styles.redes, "col col--12")}>
                <a href={link1} target="_blank"><img className={clsx(styles.redeIcon)} src={rede1} alt="Rede Social" /></a>
                <a href={link2} target="_blank"><img className={clsx(styles.redeIcon)} src={rede2} alt="Rede Social" /></a>
              </div>
          </div>
      </div>
    </div>
  )
}

function PrimeiraColuna({titulo}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className="row">
        <div className="col col--12">
          <h2 className={clsx(styles.h2Dipp)}>
            {titulo}
          </h2>
        </div>
      </div>
    </div>
  )
}

function Intro({ logo, link, link1, link2, link3, link4, link5, link6, link7 }) {
  return (
    <div className={clsx(styles.heroBanner, "hero hero--primary")}>
      <div className="row">
        <div className="col col--4">
          <div className={clsx(styles.LogoDipp)}>
            <a href={link}>
              <img src={logo} alt="Logo do DIPP" />
            </a>
          </div>
        </div>
        <div className={clsx(styles.Menu, "col col--8")}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}>
              <a href={link1}>home</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link2}>institucional</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link3}>notícias</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link4}>publicações</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link5}>projetos</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link6}>equipe</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link7}>cursos</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function Equipe() {
  return (
    <Layout title="Equipe – DIPP">
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>

      <main>
        <section className="content">

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {membros.map((props, idx) => (
                <Membros key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>
    </Layout>
  );
}

export default Equipe;
