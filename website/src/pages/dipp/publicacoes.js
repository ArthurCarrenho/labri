import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
  {
    projeto: "dipp - labri-unesp",
    logo: "/img/dipp/logo-dipp-pagina-inicial.svg",
    link: "/dipp",
    link1: "/dipp",
    link2: "/dipp/institucional",
    link3: "/dipp/noticias",
    link4: "/dipp/publicacoes",
    link5: "/dipp/projetos",
    link6: "/dipp/equipe",
    link7: "/dipp/cursos"
  },
];

const primeiracoluna =  [
  {
    projeto: "dipp - labri-unesp",
    titulo: "Publicações",
    descricao: (
      <>
      Leia as publicações dos membros da DIPP
      </>
    )
  }
]

const downloads = [
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/download.png",
    link: "http://dx.doi.org/10.1590/1807-01912019252377",
    nome: (
      <>
      Mariano, Karina L. Pasquariello; LUCIANO, BRUNO THEODORO ; SANTOS, LUCAS BISPO DOS . Parlamentos regionais nas 
      negociações comerciais: o Parlamento Europeu e o do Mercosul no acordo União Europeia-Mercosul. <b>OPINIÃO PÚBLICA</b>, 
      v. 25, p. 377-400, 2019.
      </>
    )
  }
]


function Downloads({ img, link, nome }) {
  return (
      <div className={clsx(styles.colunaSemFundo, "container")}>
          <div className="row">
              <div className={clsx(styles.icone, "col col--2")}>
                  <a href={link} target="_blank"><img className={clsx(styles.icone)} src={img} alt="Download do Texto" /></a>
              </div>
              <div className="col col--10">
                  <a href={link} target="_blank"><p className={clsx(styles.publicacoes)}>{nome}</p></a>
              </div>
          </div>
      </div>
  )
}

function PrimeiraColuna({titulo, descricao}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className="row">
        <div className="col col--12">
          <h2 className={clsx(styles.h2Dipp)}>
            {titulo}
          </h2>
          <p className={clsx(styles.descricao)}>
            {descricao}
          </p>
        </div>
      </div>
    </div>
  )
}


function Intro({ logo, link, link1, link2, link3, link4, link5, link6, link7 }) {
  return (
    <div className={clsx(styles.heroBanner, "hero hero--primary")}>
      <div className="row">
        <div className="col col--4">
          <div className={clsx(styles.LogoDipp)}>
            <a href={link}>
              <img src={logo} alt="Logo do DIPP" />
            </a>
          </div>
        </div>
        <div className={clsx(styles.Menu, "col col--8")}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}>
              <a href={link1}>home</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link2}>institucional</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link3}>notícias</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link4}>publicações</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link5}>projetos</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link6}>equipe</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link7}>cursos</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function Publicacoes() {
  return (
    <Layout title="Publicações – DIPP">
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>

      <main>
        <section className="content">

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {downloads.map((props, idx) => (
                <Downloads key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>
    </Layout>
  );
}

export default Publicacoes;
