import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
  {
    projeto: "dipp - labri-unesp",
    logo: "/img/dipp/logo-dipp-pagina-inicial.svg",
    link: "/dipp",
    link1: "/dipp",
    link2: "/dipp/institucional",
    link3: "/dipp/noticias",
    link4: "/dipp/publicacoes",
    link5: "/dipp/projetos",
    link6: "/dipp/equipe",
    link7: "/dipp/cursos"
  },
];

const primeiracoluna = [
  {
    projeto: "dipp - labri-unesp",
    titulo: "Institucional"
  }
]

const quemsomos = [
  {
    projeto: "dipp - labri-unesp",
    titulo: "Quem Somos",
    subtitulo: "Nossos objetivos",
    texto: (
      <>
      O projeto da <b>Rede DIPP (Development, International Politics and Peace)</b> se baseia na construção de uma 
      rede que seja geograficamente ampliada (inicialmente em três continentes, mas visando ampliar para a Ásia 
      futuramente) e capaz de oferecer abordagens interdisciplinares sobre comportamentos políticos e 
      institucionalidade no âmbito das relações internacionais. Como tal, a <b>Rede DIPP</b> oferece um enquadramento ideal 
      para diálogos transnacionais e transdisciplinares para melhor compreender a interação entre os comportamentos 
      políticos atuais e seus impactos sobre a constituição de sistemas de governança na área ambiental e de direitos 
      humanos, na construção de projetos de regionalismo voltados para a promoção do desenvolvimento e da segurança 
      regional entre sociedades plurais. <br /><br />

      A rede DIPP visa promover pesquisa, ensino e extensão sobre essas temáticas, captando adequadamente a interação 
      entre as características emergentes na sociedade contemporânea e a construção de sistemas de governança nacionais
      e internacionais para lidar com os desafios e oportunidades decorrentes desse novo contexto sociopolítico.
      </>
    )
  }
]

const instituicoes = [
  {
    projeto: "dipp - labri-unesp",
    titulo: "Instituições Envolvidas",
    texto: (
      <>
        <b>
          <li>Universidade Estadual Paulista (UNESP)</li>
          <li>Universidade Estadual de Campinas (UNICAMP)</li>
          <li>Pontifícia Universidade Católica de São Paulo (PUC-SP)</li>
          <li>Programa de Pós-graduação em Relações Internacionais San Tiago Dantas (UNESP, UNICAMP e PUC-SP)</li>
          <li>Universidad de la República Uruguay (UDELAR)</li>
          <li>Universidad Nacional de la Plata</li>
          <li>Universidad Nacional de Colombia</li>
          <li>Universidad Nacional Autonoma de México</li>
          <li>Universidade de Coimbra</li>
          <li>Universitat Autònoma de Barcelona</li>
          <li>Universidad Complutense de Madrid</li>
          <li>Neoma Business School</li>
          <li>Katholieke Universiteit Leuven</li>
          <li>German Institute of Global and Area Studies</li>
          <li>Columbia University</li>
          <li>Georgetown University</li>
          <li>The University of Queensland</li>
          <li>University of Calgary</li>
        </b>
      </>
    )
  }
]

const outros = [
  {
    projeto: "dipp - labri-unesp",
    titulo: "Nossa Equipe",
    texto: (
      <>
      Para conhecer nossa equipe <a href="/dipp/equipe">clique aqui</a>
      </>
    )
  },

  {
    projeto: "dipp - labri-unesp",
    titulo: "Financiamento e Apoio",
    texto: (
      <>
      Coordenação de Aperfeiçoamento de Pessoal de Nível Superior (CAPES)      
      </>
    )
  },
]

const logos = [
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/unesp-logo.gif",
    link: "https://www2.unesp.br/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/unicamp-logo.png",
    link: "https://www.unicamp.br/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/pucsp-logo.png",
    link: "https://www5.pucsp.br/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/std-logo.png",
    link: "https://www.santiagodantas-ppgri.org/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/capes-logo.jpg",
    link: "https://www.gov.br/capes/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/udelar-logo.png",
    link: "https://udelar.edu.uy/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/unpl-logo.jpeg",
    link: "https://unlp.edu.ar/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/ucolombia-logo.jpeg",
    link: "https://unal.edu.co/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/unam-logo.jpeg",
    link: "https://www.unam.mx/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/ucoimbra-logo.png",
    link: "https://www.uc.pt/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/uab-logo.png",
    link: "https://www.uab.cat/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/ucm-logo.jpg",
    link: "https://www.ucm.es/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/neoma-logo.jpg",
    link: "https://neoma-bs.com/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/kuleuven-logo.png",
    link: "https://www.kuleuven.be/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/giga-logo.png",
    link: "https://www.giga-hamburg.de/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/columbiau-logo.png",
    link: "https://www.columbia.edu/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/georgetown-logo.jpg",
    link: "https://www.georgetown.edu/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/queensland-logo.png",
    link: "https://www.uq.edu.au/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/calcary-logo.jpeg",
    link: "https://www.ucalgary.ca/"
  },
]

const rede = [
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/network-dipp.svg"
  }
]

function Rede({img}){
  return(
    <div className={clsx(styles.colunaSemFundo, styles.center)}>
      <div className="row">

        <div className="col col--12">
          <img className={clsx(styles.modelo)} src={img} alt="Rede de Instituições Vinculadas ao DIPP" />
        </div>

      </div>
    </div>
  )
}

function Outros({titulo, texto}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className="row">
        <div className="col col--12">
          <h2 className={clsx(styles.h2Titulo)}>
            {titulo}
          </h2>
          <p className={clsx(styles.paragrafos)}>
            {texto}
          </p>
        </div>
      </div>
    </div>
  )
}

function Logos({img, link}){
  return(
    <div className={clsx(styles.colunaGaleria, "col col--2")}>
      <div className="row">
          <a href={link} target="_blank">
            <img className={clsx(styles.logoIcon)} src={img} alt="Logo" />
          </a>
      </div>
    </div>
  )
}

function Instituicoes({titulo, texto}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className="row">
        <div className="col col--12">
          <h2 className={clsx(styles.h2Titulo)}>
            {titulo}
          </h2>
          <p className={clsx(styles.paragrafos)}>
            {texto}
          </p>
        </div>
      </div>
    </div>
  )
}

function QuemSomos({titulo, subtitulo, texto}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className="row">
        <div className="col col--12">
          <h2 className={clsx(styles.h2Titulo)}>
            {titulo}
          </h2>
          <h3 className={clsx(styles.subtitulo)}>
            {subtitulo}
          </h3>
          <p className={clsx(styles.paragrafos)}>
            {texto}
          </p>
        </div>
      </div>
    </div>
  )
}

function PrimeiraColuna({titulo}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className="row">
        <div className="col col--12">
          <h2 className={clsx(styles.h2Dipp)}>
            {titulo}
          </h2>
        </div>
      </div>
    </div>
  )
}

function Intro({ logo, link, link1, link2, link3, link4, link5, link6, link7 }) {
  return (
    <div className={clsx(styles.heroBanner, "hero hero--primary")}>
      <div className="row">
        <div className="col col--4">
          <div className={clsx(styles.LogoDipp)}>
            <a href={link}>
              <img src={logo} alt="Logo do DIPP" />
            </a>
          </div>
        </div>
        <div className={clsx(styles.Menu, "col col--8")}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}>
              <a href={link1}>home</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link2}>institucional</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link3}>notícias</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link4}>publicações</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link5}>projetos</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link6}>equipe</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link7}>cursos</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function Institucional() {
  return (
    <Layout title="Institucional – DIPP">
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>

      <main>
        <section className="content">

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {quemsomos.map((props, idx) => (
                <QuemSomos key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {instituicoes.map((props, idx) => (
                <Instituicoes key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {rede.map((props, idx) => (
                <Rede key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {outros.map((props, idx) => (
                <Outros key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>
    </Layout>
  );
}

export default Institucional;
