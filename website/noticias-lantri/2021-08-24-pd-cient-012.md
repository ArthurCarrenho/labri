---
slug: producao-cientifica-012
tags: [Lantri - Produção]
title: "Mercosur-Pacific Alliance Convergence: Moving Forward or Moving Nowhere?"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 012"
image_url: https://i.imgur.com/Eqc0R0b.png
descricao: "A pesquisadora do LANTRI, Julia de Souza Borba Gonçalves, publicou no E-International Relations um artigo intitulado 'Mercosur-Pacific Alliance Convergence: Moving Forward or Moving Nowhere?'..."
---

<div align="center">

![](https://i.imgur.com/Eqc0R0b.png)

</div>


>A pesquisadora do LANTRI, Julia de Souza Borba Gonçalves, publicou no E-International Relations um artigo intitulado "Mercosur-Pacific Alliance Convergence: Moving Forward or Moving Nowhere?"...*

<!--truncate-->

A pesquisadora do LANTRI, Julia de Souza Borba Gonçalves, publicou no E-International Relations um artigo intitulado "Mercosur-Pacific Alliance Convergence: Moving Forward or Moving Nowhere?". 


Se interessa pelo tema? Para ter acesso ao artigo, basta clicar aqui. 