---
slug: lantri-tec-004
tags: [Lantri - TEC]
title: "Connected Papers"
author: Equipe LANTRI
tipo_publicacao: "Produção TEC - 004"
image_url: https://i.imgur.com/fuPx37U.png
descricao: "Você já ouviu falar do Connected Papers? Essa ferramenta gratuita e muito útil ajuda pesquisadores e cientistas a encontrarem produções acadêmicas relevantes para seu campo de trabalho, por meio das várias conexões entre elas..."
---

<div align="center">

![](https://i.imgur.com/fuPx37U.png)

</div>


>Você já ouviu falar do Connected Papers? Essa ferramenta gratuita e muito útil ajuda pesquisadores e cientistas a encontrarem produções acadêmicas relevantes para seu campo de trabalho, por meio das várias conexões entre elas...*

<!--truncate-->

Você já ouviu falar do Connected Papers? Essa ferramenta gratuita e muito útil ajuda pesquisadores e cientistas a encontrarem produções acadêmicas relevantes para seu campo de trabalho, por meio das várias conexões entre elas.


Através da ferramenta torna-se possível explorar papéis conectados em um gráfico visual simples e intuitivo. Deste modo o usuário pode ter certeza de não ter perdido um artigo importante, pode descobrir os trabalhos anteriores e derivados mais relevantes, assim como organizar a bibliografia necessária para sua tese.


Mais detalhes podem ser encontrados no site oficial, [clicando aqui](https://www.connectedpapers.com/).