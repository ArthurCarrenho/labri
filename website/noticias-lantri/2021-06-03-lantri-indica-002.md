---
slug: lantri-indica-002
tags: [Lantri - Indica]
title: "Biblioteca de Botucatu oferece Programa de Capacitação e Treinamento em Pesquisa"
author: Equipe LANTRI
tipo_publicacao: "Lantri Indica - 002"
image_url: https://i.imgur.com/HRqYivP.png
descricao: "A Biblioteca de Botucatu está oferecendo um Programa de Capacitação e Treinamento em Pesquisa no formato Webinar. Dentre os cursos oferecidos, estão elaboração de estratégia de busca, revisão sistemática e gerenciador de referências, temas que podem ser úteis a pesquisas em diversos campos do saber..."
---

<div align="center">

![](https://i.imgur.com/HRqYivP.png)

</div>


>A Biblioteca de Botucatu está oferecendo um Programa de Capacitação e Treinamento em Pesquisa no formato Webinar. Dentre os cursos oferecidos, estão elaboração de estratégia de busca, revisão sistemática e gerenciador de referências, temas que podem ser úteis a pesquisas em diversos campos do saber...*

<!--truncate-->

A Biblioteca de Botucatu está oferecendo um Programa de Capacitação e Treinamento em Pesquisa no formato Webinar. Dentre os cursos oferecidos, estão elaboração de estratégia de busca, revisão sistemática e gerenciador de referências, temas que podem ser úteis a pesquisas em diversos campos do saber. Confira o calendário dos encontros [clicando aqui](https://www.btu.unesp.br/#!/sobre/biblioteca/biblio-ensina/webinars/).