---
slug: lantri-noticias-006
tags: [Lantri - Notícias]
title: "Missionários da tecnologia e deslumbrados em geral agora apostam na revolução do Big Data"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 006"
image_url: https://i.imgur.com/LhawZR7.jpg
descricao: "O mundinho corporativo é novidadeiro. De tempos em tempos divisa inovações, ou pseudoinovações, para movimentar os negócios ou manter a charrua nas trilhas..."
---

<div align="center">

![](https://i.imgur.com/LhawZR7.jpg)

</div>


>*O mundinho corporativo é novidadeiro. De tempos em tempos divisa inovações, ou pseudoinovações, para movimentar os negócios ou manter a charrua nas trilhas...*

<!--truncate-->

O mundinho corporativo é novidadeiro. De tempos em tempos divisa inovações, ou pseudoinovações, para movimentar os negócios ou manter a charrua nas trilhas. Vale mais a ficção que o fato. O assunto do momento atende pela alcunha de Big Data. Como noutras febres, trata-se de conceito incerto e vago, mas capaz de evocar sonhos de grandeza, que é, afinal, o que importa. Para mais informações [clique aqui](http://www.cartacapital.com.br/revista/917/miragens-digitais).