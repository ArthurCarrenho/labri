---
slug: producao-cientifica-006
tags: [Lantri - Produção]
title: "Desintegração econômica e fragmentação da governança regional na América do Sul em tempos de Covid-19"
author: Equipe Lantri
tipo_publicacao: "Produção Cientica - 006"
image_url: https://i.imgur.com/I6MGScY.png
descricao: "A pesquisadora do Lantri Júlia de Souza Borba Gonçalvez produziu o artigo intitulado ''Desintegração econômica e fragmentação da governança regional na América do Sul em tempos de Covid-19''..."
---

<div align="center">

![](https://i.imgur.com/I6MGScY.png)

</div>


>A pesquisadora do Lantri Júlia de Souza Borba Gonçalvez produziu o artigo intitulado ''Desintegração econômica e fragmentação da governança regional na América do Sul em tempos de Covid-19''...*

<!--truncate-->

A pesquisadora do Lantri Júlia de Souza Borba Gonçalvez produziu o artigo intitulado ''Desintegração econômica e fragmentação da governança regional na América do Sul em tempos de Covid-19''.



O objetivo deste artigo é analisar os efeitos da pandemia da Covid-19 sobre o comércio e a governança da América do Sul. A hipótese é que a crise gerada pela Covid-19 acentuou as tendências anteriores de desintegração econômica e fragmentação política. Analisa-se como organizações regionais da África, América Central, América do Sul e Europa responderam à Covid-19. No caso da América do Sul, avalia-se o papel do Foro para o Progresso e Desenvolvimento da América do Sul (Prosul) para oferecer respostas à crise, considerando as iniciativas desde o surgimento da crise, sua agenda para Saúde, as interações entre os governos e seu arcabouço institucional. Avalia-se a dinâmica comercial intrarregional, com ênfase nas exportações e importações do Brasil com os países vizinhos. Conclui-se que há uma espiral entre desintegração comercial e fragmentação da governança, que se retroalimenta e tende a prolongar a crise na América do Sul mais que em outras regiões do mundo.


Para ter acesso completo ao artigo, basta [clicar aqui](http://repositorio.ipea.gov.br/handle/11058/10337). 