---
slug: lantri-tec-005
tags: [Lantri - TEC]
title: "Ferramentas de suporte a pesquisa: Zotero"
author: Equipe LANTRI
tipo_publicacao: "Produção TEC - 005"
image_url: https://i.imgur.com/ooj2IvP.png
descricao: "Você já ouviu falar do Zotero? O Zotero é um software livre e aberto, voltado para gestão e compartilhamento de referências, que visa facilitar a elaboração de trabalhos acadêmicos e científicos, como teses, dissertações, trabalhos de conclusão de curso e artigos científicos..."
---

<div align="center">

![](https://i.imgur.com/ooj2IvP.png)

</div>


>Você já ouviu falar do Zotero? O Zotero é um software livre e aberto, voltado para gestão e compartilhamento de referências, que visa facilitar a elaboração de trabalhos acadêmicos e científicos, como teses, dissertações, trabalhos de conclusão de curso e artigos científicos...*

<!--truncate-->

Você já ouviu falar do Zotero? O Zotero é um software livre e aberto, voltado para gestão e compartilhamento de referências, que visa facilitar a elaboração de trabalhos acadêmicos e científicos, como teses, dissertações, trabalhos de conclusão de curso e artigos científicos.


Quer ver mais sobre essa ferramenta? Basta [clicar aqui](https://ericbrasiln.github.io/ferramentas_digitais_UNILAB/aula3-zotero.html!