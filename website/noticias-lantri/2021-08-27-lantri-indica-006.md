---
slug: lantri-indica-006
tags: [Lantri - Indica]
title: "Usando I.A. para encontrar preconceitos na I.A."
author: Equipe LANTRI
tipo_publicacao: "Lantri Indica - 006"
image_url: https://i.imgur.com/XNiPFxw.png
descricao: "A pesquisadora do Lantri Jaqueline Trevisan Pigatto indicou um texto do jornal The New York Times, chamado 'Using A.I. to Find Bias in A.I.', onde o autor Cade Metz explica um pouco sobre a interessante temática..."
---

<div align="center">

![](https://i.imgur.com/XNiPFxw.png)

</div>


>A pesquisadora do Lantri Jaqueline Trevisan Pigatto indicou um texto do jornal The New York Times, chamado "Using A.I. to Find Bias in A.I.", onde o autor Cade Metz explica um pouco sobre a interessante temática...*

<!--truncate-->

Você gosta de Inteligência Artificial? 


A pesquisadora do Lantri Jaqueline Trevisan Pigatto indicou um texto do jornal The New York Times, chamado "Using A.I. to Find Bias in A.I.", onde o autor Cade Metz explica um pouco sobre a interessante temática.


Para ter acesso ao texto completo, basta [clicar aqui](https://www.nytimes.com/2021/06/30/technology/artificial-intelligence-bias.html?campaign_id=158&emc=edit_ot_20210630&instance_id=34229&nl=on-tech-with-shira-ovide&regi_id=72671086&segment_id=62167&te=1&user_id=19c318b6afc41ab34aa0ca6750518233).