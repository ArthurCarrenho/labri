---
slug: lantri-noticias-007
tags: [Lantri - Notícias]
title: "O que é a 4ª revolução industrial - e como ela deve afetar nossas vidas"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 007"
image_url: https://i.imgur.com/UZAvQC2.jpg
descricao: "No final do século 17 foi a máquina a vapor. Desta vez, serão os robôs integrados em sistemas ciberfísicos os responsáveis por uma transformação radical..."
---

<div align="center">

![](https://i.imgur.com/UZAvQC2.jpg)

</div>


>*No final do século 17 foi a máquina a vapor. Desta vez, serão os robôs integrados em sistemas ciberfísicos os responsáveis por uma transformação radical...*

<!--truncate-->

No final do século 17 foi a máquina a vapor. Desta vez, serão os robôs integrados em sistemas ciberfísicos os responsáveis por uma transformação radical. E os economistas têm um nome para isso: a quarta revolução industrial, marcada pela convergência de tecnologias digitais, físicas e biológicas. [Saiba mais](http://www.bbc.com/portuguese/geral-37658309).