---
slug: lantri-indica-003
tags: [Lantri - Indica]
title: "Vice presidente do facebook rebate criticas e anuncia mudança na plataforma"
author: Equipe LANTRI
tipo_publicacao: "Lantri Indica - 003"
image_url: https://i.imgur.com/voEukW8.png
descricao: "O texto de Carlos Affonso de Souza comenta as recentes declarações de Nick Clegg, vice-presidente de assuntos globais do Facebook, acerca do funcionamento de algoritmo na plataforma..."
---

<div align="center">

![](https://i.imgur.com/voEukW8.png)

</div>


>O texto de Carlos Affonso de Souza comenta as recentes declarações de Nick Clegg, vice-presidente de assuntos globais do Facebook, acerca do funcionamento de algoritmo na plataforma...*

<!--truncate-->

O texto de Carlos Affonso de Souza comenta as recentes declarações de Nick Clegg, vice-presidente de assuntos globais do Facebook, acerca do funcionamento de algoritmo na plataforma. Assim, debate o papel das redes sociais nas atuais sociedades, frente a radicalizações e polarizações políticas, a necessidade de transparência e de educação do próprio usuário. Para ter acesso a coluna, basta [clicar aqui](https://www.uol.com.br/tilt/colunas/carlos-affonso-de-souza/2021/04/29/vice-presidente-do-facebook-rebate-criticas-e-anuncia-mudanca-na-plataforma.htm).
