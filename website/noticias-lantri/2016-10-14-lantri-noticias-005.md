---
slug: lantri-noticias-005
tags: [Lantri - Notícias]
title: "25 atalhos universais para qualquer navegador"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 005"
image_url: https://i.imgur.com/k0R28FO.jpg
descricao: "Muitas vezes, não importando qual é o navegador utilizado, teclas de atalho facilitam a vida de muitas pessoas..."
---

<div align="center">

![](https://i.imgur.com/k0R28FO.jpg)

</div>


>*Muitas vezes, não importando qual é o navegador utilizado, teclas de atalho facilitam a vida de muitas pessoas...*

<!--truncate-->

Muitas vezes, não importando qual é o navegador utilizado, teclas de atalho facilitam a vida de muitas pessoas. Mesmo assim, existem vários atalhos que são compartilhados por todos os browsers do mercado e que não são de conhecimento dos seus usuários. Para mais detalhes [clique aqui](http://www.tecmundo.com.br/tutorial/25336-25-atalhos-universais-para-qualquer-navegador.htm).