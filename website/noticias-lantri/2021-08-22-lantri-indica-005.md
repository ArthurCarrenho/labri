---
slug: lantri-indica-005
tags: [Lantri - Indica]
title: "Fundação Fernando Henrique Cardoso disponibiliza linha do tempo de 1985 a 2018"
author: Equipe LANTRI
tipo_publicacao: "Lantri Indica - 005"
image_url: https://i.imgur.com/8RAbj21.png
descricao: "Fundação FHC disponibiliza site com linhas do tempo dos temas Direitos das Mulheres, Educação, Política Ambiental, Questão Ambiental, Questão Racial, Reforma Agrária e Saúde..."
---

<div align="center">

![](https://i.imgur.com/8RAbj21.png)

</div>


>Fundação FHC disponibiliza site com linhas do tempo dos temas Direitos das Mulheres, Educação, Política Ambiental, Questão Ambiental, Questão Racial, Reforma Agrária e Saúde...*

<!--truncate-->

Fundação FHC disponibiliza site com linhas do tempo dos temas Direitos das Mulheres, Educação, Política Ambiental, Questão Ambiental, Questão Racial, Reforma Agrária e Saúde. Os acontecimentos são separados por presidente, contendo uma breve introdução, memórias e outros materiais do Acervo.


Se interessou? Basta [clicar aqui](https://linhasdotempo.fundacaofhc.org.br/).