---
slug: lantri-indica-004
tags: [Lantri - Indica]
title: "Mapeamento de empresas de tecnologia chinesas"
author: Equipe LANTRI
tipo_publicacao: "Lantri Indica - 004"
image_url: https://i.imgur.com/p0H1SiK.png
descricao: "Site Mapping China’s Tech Giants mapeia expansão internacional das principais empresas tecnologia chinesa, dentre elas Alibaba, Huawei e Baidu..."
---

<div align="center">

![](https://i.imgur.com/p0H1SiK.png)

</div>


>Site Mapping China’s Tech Giants mapeia expansão internacional das principais empresas tecnologia chinesa, dentre elas Alibaba, Huawei e Baidu...*

<!--truncate-->

Site [Mapping China’s Tech Giants](https://chinatechmap.aspi.org.au/#/homepage) mapeia expansão internacional das principais empresas tecnologia chinesa, dentre elas Alibaba, Huawei e Baidu.

Lançado em abril de 2019 e relançado em junho de 2021, o site agora inclui 27 empresas e mais de 3800 inscrições. Cada entrada é preenchida com até 15 categorias de dados, totalizando mais de 38.000 pontos de dados.