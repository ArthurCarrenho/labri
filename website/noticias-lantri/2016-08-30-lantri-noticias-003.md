---
slug: lantri-noticias-003
tags: [Lantri - Notícias]
title: "Diplomata da OMC vai debater entraves do Mercosul em fórum de agricultura"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 003"
image_url: https://i.imgur.com/NUgwoQi.jpg
descricao: "Diplomata da OMC vai debater entraves do Mercosul em fórum de agricultura..."
---

<div align="center">

![](https://i.imgur.com/NUgwoQi.jpg)

</div>


>*Diplomata da OMC vai debater entraves do Mercosul em fórum de agricultura...*

<!--truncate-->

Diplomata da OMC vai debater entraves do Mercosul em fórum de agricultura. O diplomata brasileiro na Organização Mundial do Comércio (OMC), Celso de Tarso Pereira, vai debater os entraves do Mercosul no 4º Fórum de Agricultura da América do Sul, a exemplo das disputas agrícolas e do bloco sul-americano. O fórum ocorre em Curitiba, nos dias 25 e 26 de agosto, e vai reunir representantes de todos os elos da cadeia produtiva regional. Para acessar a noticia completa [clique aqui](http://economiasc.com.br/diplomata-da-omc-vai-debater-entraves-do-mercosul-em-forum-de-agricultura/). 