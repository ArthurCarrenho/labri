---
slug: producao-cientifica-024
tags: [Lantri - Produção]
title: "Internet governance is what global stakeholders make of it: a tripolar approach"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 024"
image_url: https://i.imgur.com/jcShxoG.png
descricao: "Este documento procura identificar a configuração da governação da Internet no final de 2021, centrada na disputa sobre a representação e legitimidade institucionais em que se encontram as grandes potências regionais..."
---

<div align="center">

![](https://i.imgur.com/jcShxoG.png)

</div>



>*Este documento procura identificar a configuração da governação da Internet no final de 2021, centrada na disputa sobre a representação e legitimidade institucionais em que se encontram as grandes potências regionais...*

<!--truncate-->

Este documento procura identificar a configuração da governação da Internet no final de 2021, centrada na disputa sobre a representação e legitimidade institucionais em que se encontram as grandes potências regionais. Os Estados Unidos, a União Europeia, e a China compõem um acordo tripolar, cada um avançando um modo distinto de governação com características únicas que aqui nos esforçamos por analisar. (Tradução nossa). 


Se interessa pelo tema? Para ter acesso ao artigo, basta [clicar aqui](https://www.scielo.br/j/rbpi/a/ycQxVpnr5DtfRCbd3BqQSkr/abstract/?format=html&lang=en).