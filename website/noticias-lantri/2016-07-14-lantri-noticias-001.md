---
slug: lantri-noticias-001
tags: [Lantri - Notícias]
title: "II Simpósio Internacional: Pensar e Repensar a América Latina (PROLAM/USP)"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 001"
image_url: https://i.imgur.com/rYotBbO.jpg
descricao: "Entre os dias 17 e 21 de outubro de 2015 ocorrerá a segunda edição do Simpósio Internacional “Pensar e Repensar a América Latina” organizado pelo PROLAM/USP..."
---

<div align="center">

![](https://i.imgur.com/rYotBbO.jpg)

</div>


>*Entre os dias 17 e 21 de outubro de 2015 ocorrerá a segunda edição do Simpósio Internacional “Pensar e Repensar a América Latina” organizado pelo PROLAM/USP...*

<!--truncate-->

Entre os dias 17 e 21 de outubro de 2015 ocorrerá a segunda edição do Simpósio Internacional “Pensar e Repensar a América Latina” organizado pelo PROLAM/USP (para maiores informações [clique aqui](http://sites.usp.br/prolam/)).


Dentre as inúmeras atividades previstas, mini-cursos, conferências, mesas redondas, entre outros, destacamos o Seminário de Pesquisa 6 “Estados e Atores Institucionais de Integração Regional” (para maiores informações [clique aqui](http://sites.usp.br/prolam/seminario-de-pesquisa-6/)) que esta sob coordenação de três pesquisadores integrantes da Rede de Pesquisa em Política Externa e Regionalismo (REPRI): Regiane Nitsch Bressan, Clarissa Correa Neto Ribeiro e Camilla Silva Geraldello.