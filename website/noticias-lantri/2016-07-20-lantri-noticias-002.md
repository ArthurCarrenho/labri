---
slug: lantri-noticias-002
tags: [Lantri - Notícias]
title: "VIII Jornadas Europeias – Europa em tempos de crise"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 002"
image_url: https://i.imgur.com/HKahGCT.jpg
descricao: "Entre 19 e 21 de setembro de 2016, ocorrerá as VIII Jornadas Europeias organizadas pelo Departamento de Direito Internacional e Comparado da USP e pela Cátedra Martius de Estudos Alemães e Europeus.Diplomata da OMC vai debater entraves do Mercosul em fórum de agricultura..."
---

<div align="center">

![](https://i.imgur.com/HKahGCT.jpg)

</div>


>*Entre 19 e 21 de setembro de 2016, ocorrerá as VIII Jornadas Europeias organizadas pelo Departamento de Direito Internacional e Comparado da USP e pela Cátedra Martius de Estudos Alemães e Europeus...*

<!--truncate-->

Entre 19 e 21 de setembro de 2016, ocorrerá as VIII Jornadas Europeias organizadas pelo Departamento de Direito Internacional e Comparado da USP e pela Cátedra Martius de Estudos Alemães e Europeus. Segundo os organizadores o “tema deste ano será a crise na Europa e da União Europeia e suas muitas facetas, tais como a crise econômica e financeira, a crise de refugiados, o terrorismo e conflitos religiosos, protestos sociais, a ascensão do populismo de direita, as crises constitucionais na Hungria e na Polônia, a crise do modelo social europeu, o ‘Brexit’ e a crise de identidade(...)”. Até o dia 05 de agosto de 2016 estará aberta a chamada para apresentação de trabalhos que versam sobre a temática deste evento (para maiores informações [clique aqui](http://dwih.com.br/pt-br/oportunidades/chamada-de-trabalhos-para-oitavas-jornadas-europeias))