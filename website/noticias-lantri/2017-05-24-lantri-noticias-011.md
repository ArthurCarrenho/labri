---
slug: lantri-noticias-011
tags: [Lantri - Notícias]
title: "Armazenamento na 'névoa' elimina os riscos do armazenamento na 'nuvem'"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 011"
image_url: https://i.imgur.com/OrUrJMS.jpg
descricao: "Duas especialistas em ciência da informação italianas estão propondo um novo conceito para o armazenamento remoto e distribuído de documentos que pode manter todos os benefícios da computação em nuvem,..."
---

<div align="center">

![](https://i.imgur.com/OrUrJMS.jpg)

</div>


>*Duas especialistas em ciência da informação italianas estão propondo um novo conceito para o armazenamento remoto e distribuído de documentos que pode manter todos os benefícios da computação em nuvem,...*

<!--truncate-->

''Duas especialistas em ciência da informação italianas estão propondo um novo conceito para o armazenamento remoto e distribuído de documentos que pode manter todos os benefícios da computação em nuvem, mas sem os problemas de segurança envolvidos em colocar seus documentos sensíveis em um único servidor remoto.''

 

Para acesso a noticia completa, [clique aqui](http://www.inovacaotecnologica.com.br/noticias/noticia.php?artigo=armazenamento-nevoa-risco-armazenamento-nuvem)