---
slug: producao-cientifica-007
tags: [Lantri - Produção]
title: "Pesquisadora do Lantri avalia a atuação do BID em artigo"
author: Equipe Lantri
tipo_publicacao: "Produção Cientica - 007"
image_url: https://i.imgur.com/8VgSrjI.png
descricao: "A pesquisadora do Lantri Bárbara Carvalho Neves produziu um artigo sobre a atuação do BID na Argentina e no Brasil de 1960 a 2017, para o jornal de estudos globais Meridiano 47..."
---

<div align="center">

![](https://i.imgur.com/8VgSrjI.png)

</div>


>A pesquisadora do Lantri Bárbara Carvalho Neves produziu um artigo sobre a atuação do BID na Argentina e no Brasil de 1960 a 2017, para o jornal de estudos globais Meridiano 47...*

<!--truncate-->

A pesquisadora do Lantri Bárbara Carvalho Neves produziu um artigo sobre a atuação do BID na Argentina e no Brasil de 1960 a 2017, para o jornal de estudos globais Meridiano 47.


O objetivo deste artigo é avaliar a atuação do BID a partir das diretrizes da política externa norte-americana com foco na dinâmica dos investimentos regionais para seus principais contribuidores sul-americanos: Brasil e Argentina.


Para ter acesso completo ao artigo, basta [clicar aqui](https://periodicos.unb.br/index.php/MED/article/view/32226).