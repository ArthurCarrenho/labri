---
slug: producao-cientifica-027
tags: [Lantri - Produção]
title: "Regionalismo, crise venezuelana e a pandemia do COVID-19"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 027"
image_url: https://i.imgur.com/InNoXvB.png
descricao: "A convergência política na América do Sul, vivenciada entre 1998 e 2013, marcada por um cenário de crescimento econômico e destacado incentivo por parte dos governos sul-americanos em cooperar e formar espaços para a integração..."
---

<div align="center">

![](https://i.imgur.com/InNoXvB.png)

</div>



>*A convergência política na América do Sul, vivenciada entre 1998 e 2013, marcada por um cenário de crescimento econômico e destacado incentivo por parte dos governos sul-americanos em cooperar e formar espaços para a integração...*

<!--truncate-->

A convergência política na América do Sul, vivenciada entre 1998 e 2013, marcada por um cenário de crescimento econômico e destacado incentivo por parte dos governos sul-americanos em cooperar e formar espaços para a integração, não resultou no aprofundamento dos processos regionais existentes. Tal período, conhecido como regionalismo pós-hegemônico, ainda que compartilhando objetivos e valores em comum, levou à criação de diferentes instituições que ampliaram a segmentação regional ao serem incapazes de se consolidar diante das outras estruturas já existentes. Dessa maneira, o principal problema aqui discutido é a existência de um espaço regional fragmentado. Ao questionar como as instituições regionais sul-americanas respondem às demandas nacionais e regionais em um cenário fragmentado, este artigo busca analisar e avaliar os impactos da fragmentação regional em cenários críticos que demandam ações conjuntas e respostas institucionais regionais para resolver problemas que ultrapassam as fronteiras nacionais, em especial a crise Venezuelana e a pandemia do Covid-19. Através de análise bibliográfica recente, assim como a partir do acompanhamento de notícias, e fontes jornalísticas e documentos oficiais dos blocos regionais sul-americanos existentes, este artigo discute que em um cenário institucionalmente fragmentado, as instituições regionais encontram-se limitadas para dar respostas às demandas existentes.


Se interessa pelo tema? Para ter acesso ao artigo na íntegra, basta [acessar aqui](https://seer.ufrgs.br/ConjunturaAustral/article/view/110583). 