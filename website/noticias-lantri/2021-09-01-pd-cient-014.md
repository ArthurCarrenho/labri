---
slug: producao-cientifica-014
tags: [Lantri - Produção]
title: "O modelo Steps-to-War: panorama de um programa de pesquisa"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 014"
image_url: https://i.imgur.com/kfAys67.png
descricao: "O pesquisador do LANTRI Pedro Diniz Rocha produziu para os periódicos da UNB um artigo intitulado de ''O modelo Steps-to-War: panorama de um programa de pesquisa''..."
---

<div align="center">

![](https://i.imgur.com/kfAys67.png)

</div>


>O pesquisador do LANTRI Pedro Diniz Rocha produziu para os periódicos da UNB um artigo intitulado de ''O modelo Steps-to-War: panorama de um programa de pesquisa''...*

<!--truncate-->

Confira mais uma produção científica do grupo!


O pesquisador do LANTRI Pedro Diniz Rocha produziu para os periódicos da UNB um artigo intitulado de ''O modelo Steps-to-War: panorama de um programa de pesquisa''. 


Para ter acesso completo ao artigo, basta [clicar aqui](https://periodicos.unb.br/index.php/MED/article/view/29035).