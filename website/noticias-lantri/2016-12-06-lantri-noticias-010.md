---
slug: lantri-noticias-010
tags: [Lantri - Notícias]
title: "'Trump pode ser bom para a Europa' Diz Boris Johnson."
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 010"
image_url: https://i.imgur.com/vmwTVUv.jpg
descricao: "'Donald Trump, como já disse, é um negociador. Acho que isso pode ser uma coisa boa para a Grã-Bretanha e para a Europa e é sobre isso que precisamos de nos concentrar hoje', disse Boris Johnson..."
---

<div align="center">

![](https://i.imgur.com/vmwTVUv.jpg)

</div>


>*“Donald Trump, como já disse, é um negociador. Acho que isso pode ser uma coisa boa para a Grã-Bretanha e para a Europa e é sobre isso que precisamos de nos concentrar hoje”, disse Boris Johnson...*

<!--truncate-->

“Donald Trump, como já disse, é um negociador. Acho que isso pode ser uma coisa boa para a Grã-Bretanha e para a Europa e é sobre isso que precisamos de nos concentrar hoje”, disse Boris Johnson. Para saber mais, [clique aqui](http://pt.euronews.com/2016/11/14/trump-pode-ser-bom-para-europa-diz-boris-johnson).