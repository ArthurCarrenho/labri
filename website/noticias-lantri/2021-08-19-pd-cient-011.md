---
slug: producao-cientifica-011
tags: [Lantri - Produção]
title: "Professor Coordenador do LANTRI, Marcelo Mariano, faz análise dos últimos 30 anos do Mercosul"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 011"
image_url: https://i.imgur.com/am5urjM.png
descricao: "O objetivo deste artigo é analisar o Mercosul na política externa brasileira no período de 1991 a 2021. Argumentaremos que há uma trajetória de gradual perda de centralidade do bloco para a política externa brasileira e de redução nos níveis de intercâmbio comercial..."
---

<div align="center">

![](https://i.imgur.com/am5urjM.png)

</div>


>O objetivo deste artigo é analisar o Mercosul na política externa brasileira no período de 1991 a 2021. Argumentaremos que há uma trajetória de gradual perda de centralidade do bloco para a política externa brasileira e de redução nos níveis de intercâmbio comercial...*

<!--truncate-->

Com Haroldo Ramanzini Júnior, professor do Instituto de Economia e Relações Internacionais da Universidade Federal de Uberlândia (UFU), e Tullo Vigevani, professor de Ciência Política e Relações Internacionais da Universidade Estadual Paulista (UNESP), Marcelo Passini Mariano publica na revista "Lua Nova", artigo intitulado "O Brasil e o Mercosul: Atores Domésticos e Oscilações da Política Externa nos últimos 30 anos". O texto aborda a política externa brasileira e a importância comercial do Mercosul para o país, de 1991 a 2021.


Veja abaixo o resumo do trabalho e acesso o link para o texto completo!


"O objetivo deste artigo é analisar o Mercosul na política externa brasileira no período de 1991 a 2021. Argumentaremos que há uma trajetória de gradual perda de centralidade do bloco para a política externa brasileira e de redução nos níveis de intercâmbio comercial. Assim, o declínio da prioridade do bloco para o Brasil, observado nos governos Temer e Bolsonaro, não pode ser considerado fato desenraizado de problemas que já se manifestavam nos governos anteriores, em verdade desde 1995, ainda que com diferentes características. Nestes trinta anos, as elites empresariais e políticas brasileiras não mantiveram o mesmo interesse pela integração. Do mesmo modo, as transformações do sistema internacional e a evolução que produziram na percepção das elites a respeito do papel que o Brasil deveria desempenhar no mundo não foram absorvidas como incentivos sistêmicos para o aprofundamento da integração regional. O bloco completa trinta anos demonstrando importante resiliência, apesar do contexto de menor engajamento dos países, dos efeitos acumulados das sucessivas crises políticas e comerciais e da diminuição da importância econômica em relação a outros parceiros extrabloco, fatores que colocam importantes interrogações em relação ao seu futuro"


Se interessou? Para ter acesso ao artigo, basta clicar aqui. 