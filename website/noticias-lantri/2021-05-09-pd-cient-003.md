---
slug: producao-cientifica-003
tags: [Lantri - Produção]
title: "A política como tragédia e ação moral na obra de Hans J. Morgenthau"
author: Equipe Lantri
tipo_publicacao: "Produção Cientica - 003"
image_url: https://i.imgur.com/cvxoFsM.png
descricao: "O pesquisador Pedro Diniz Rocha, do LANTRI, produziu para a revista Conjuntura Astral o artigo intitulado ''A política como tragédia e ação moral na obra de Hans J. Morgenthau''..."
---

<div align="center">

![](https://i.imgur.com/cvxoFsM.png)

</div>


>O pesquisador Pedro Diniz Rocha, do LANTRI, produziu para a revista Conjuntura Astral o artigo intitulado ''A política como tragédia e ação moral na obra de Hans J. Morgenthau''...*

<!--truncate-->

O pesquisador Pedro Diniz Rocha, do LANTRI, produziu para a revista Conjuntura Astral o artigo intitulado ''A política como tragédia e ação moral na obra de Hans J. Morgenthau''.


O objetivo deste artigo é o de desvendar o papel da moralidade na ação estatal no Realismo Clássico a partir de revisita a obra de Hans J. Morgenthau. Argumenta-se que a teoria política proposta pelo autor tem base deontológica mista (ou fraca) e se constrói em oposição àquelas deontológicas fortes e utilitaristas. Nesse contexto, se tem que o estadista em ofício pode ser levado pela natureza da política a adotar ações moralmente condenáveis, mas não se livra da culpa de suas escolhas e possui responsabilidade moral no exercício de sua agência. Na impossibilidade de realizar o bem maior em âmbito político e consciente de não poder fazê-lo, o tomador de decisão prudente deve escolher cursos de ação a partir da lógica do mal menor. Ao revisitar Morgenthau a luz da teoria política internacional destacando o debate sobre ação moral contida em seus escritos, o artigo busca contribuir tanto para literatura especializada em tradições de ética internacional, quanto para o esforço contemporâneo de releitura do Realismo Clássico em meio às teorias de Relações Internacionais. 


Para ter acesso completo à produção, basta [clicar aqui](https://seer.ufrgs.br/ConjunturaAustral/article/view/110573). 