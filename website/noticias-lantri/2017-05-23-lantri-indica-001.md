---
slug: lantri-indica-001
tags: [Lantri - Indica]
title: "Filmes e séries nas Relações Internacionais"
author: Equipe LANTRI
tipo_publicacao: "Lantri Indica - 001"
image_url: https://i.imgur.com/wjB7344.png
descricao: "Acompanhar uma série e ver algum filme de vez em quando é muito bom, mas já pensou em fazer disso parte do seu trabalho? Seja você estudante de RI e/ou pesquisador, serviços de streaming e derivados podem ser uma ótima ferramenta de pesquisa, especialmente em temas contemporâneos, onde falta uma documentação adequada em livros e periódicos acadêmicos..."
---

<div align="center">

![](https://i.imgur.com/wjB7344.png)

</div>


>*Acompanhar uma série e ver algum filme de vez em quando é muito bom, mas já pensou em fazer disso parte do seu trabalho? Seja você estudante de RI e/ou pesquisador, serviços de streaming e derivados podem ser uma ótima ferramenta de pesquisa, especialmente em temas contemporâneos, onde falta uma documentação adequada em livros e periódicos acadêmicos...*

<!--truncate-->

Acompanhar uma série e ver algum filme de vez em quando é muito bom, mas já pensou em fazer disso parte do seu trabalho? Seja você estudante de RI e/ou pesquisador, serviços de streaming e derivados podem ser uma ótima ferramenta de pesquisa, especialmente em temas contemporâneos, onde falta uma documentação adequada em livros e periódicos acadêmicos. Assim, materiais audiovisuais facilitam a compreensão de alguns casos e dão inspiração para outros.


Um exemplo aqui trazido é do tema referente ao ciberespaço, um assunto novo nas Relações Internacionais. Há diversos documentários, e até mesmo filmes, que tratam de questões contemporâneas envolvendo ciberespionagem, deep web, entre outros. Um deles trata do funcionamento do Wikileaks, o filme The Fifth Estate (“O Quinto Poder”), baseado no livro “Inside Wikileaks”, de Daniel Domscheit-Berg, co-fundador do site, ao lado de Assange. Há também o documentário We Steal Secrets – the story of Wikileaks (“Nós Roubamos Segredos – a história do Wikileaks”), que mostra desde o surgimento do site até o asilo de Assange. Ainda nesse tema, são dicas os documentários The Internet's Own Boy: The Story of Aaron Swartz; We Are Legion: the story of the hacktivists e Deep Web.

Daniel Brühl e Benedict Cumberbatch criam o Wikileaks em The Fifth Estate (O Quinto Poder)Daniel Brühl e Benedict Cumberbatch criam o Wikileaks em The Fifth Estate (O Quinto Poder)

Seriados com temas políticos de grande relevância também podem ser facilmente encontrados na Netflix, por exemplo. Além da produção original de enorme sucesso, House of Cards, sobre os bastidores da política estadunidense, outras séries são recomendadas aos estudantes e pesquisadores da área de RI. Homeland acompanha o trabalho de uma agente da CIA contra ameaças terroristas nos EUA (trata-se de uma versão americana de uma série israelense chamada “Prisoners of War” ou, no original, “Hatufim”); The Americans mostra um casal soviético vivendo nos EUA em plena Guerra Fria; Narcos, protagonizada pelo brasileiro Wagner Moura, acompanha as atividades do traficante Pablo Escobar. Recentemente, a Netflix disponibilizou a série original britânica “House of Cards”, da década de 1990, que inspirou a atual versão protagonizada por Kevin Spacey.

<div align="center">

![](https://i.imgur.com/8dBMEE7.png)

</div>

Kevin Spacey (Frank Underwood) e Robin Wright (Claire Underwood) em House of CardsKevin Spacey (Frank Underwood) e Robin Wright (Claire Underwood) em House of Cards
 

Outras dicas, mas que não se encontram na Netflix, são as séries The Newsroom, sobre a produção de um telejornal estadunidense, trazendo temas recentes, como o caso Snowden; Tyrant, que acompanha a luta pelo poder de um país fictício no Oriente Médio entre dois irmãos, um ditador, o outro com ideias mais democráticas, envolvendo inclusive a ameaça do Califado, uma clara referência ao Estado Islâmico; Madam Secretary, mostra o dia a dia da secretária de Estado dos EUA, em um estilo mais leve e familiar; e Mr. Robot que traz a temática hacker na tentativa de derrubada do sistema econômico mundial, incluindo novos assuntos como bitcoin. Para quem procura algo mais rápido, a indicação é a minissérie The Night Manager, que com 6 episódios conta a história de um ex-gerente de hotel que com a ajuda da inteligência britânica tenta derrubar um grande traficante de armas.

<div align="center">

![](https://i.imgur.com/UHGipI6.png)

</div>

Tom Hiddleston interpreta Jonathan Pine, em The Night ManagerTom Hiddleston interpreta Jonathan Pine, em The Night Manager

Assim, essas produções auxiliam a compreensão de casos a serem estudados e, mesmo muitas sendo histórias ficcionais, inspiram ideias para quem busca pesquisar tais temas, além de cumprir a função primordial de entreter.